# These are user environment specific settings. When attempting to develop on a
# new machine, some of these settings will need to change.

# Tool Chain Binaries

RISCV_CC = riscv64-unknown-elf-gcc
RISCV_LD = riscv64-unknown-elf-ld
RISCV_AR = riscv64-unknown-elf-ar
RISCV_OBJCOPY = riscv64-unknown-elf-objcopy
RISCV_OBJDUMP = riscv64-unknown-elf-objdump
RISCV_STRIP = riscv64-unknown-elf-strip

ARM_OBJCOPY = arm-linux-gnueabi-objcopy

PYTHON2 = python
PYTHON3 = python3

ifeq ($(UNAME),Darwin)
	READLINK=greadlink
else
	READLINK=readlink
endif

SIM_CC = vcs

# The compiler for the simulator
HOST_CC = gcc

# The script used to verify
VERIFY_SCRIPT = sdiff

VIVADO_BIN = C:\Xilinx\Vivado\2018.2\bin
SDK_BIN = C:\Xilinx\SDK\2018.2\bin

# Synthesis Configuration (These can be specified on the command line)
VERSION = new
# TODO: `make help` should identify valid boards
VALID_BOARD_NAMES = pynq <default>, zedboard
# BOARD_NAME ?= zedboard
BOARD_NAME ?= pynq

SV2V = sv2v