#ifndef __LIBC_EXTENSIONS_H
#define __LIBC_EXTENSIONS_H

/**
 * Gets the maximum of two values, which should be of the same type.
 **/
#define max(x, y) (((x) > (y)) ? (x) : (y))

/**
 * Gets the minimum of two values, which should be of the same type.
 **/
#define min(x, y) (((x) < (y)) ? (x) : (y))

/**
 * Gets the length of a statically allocated array.
 *
 * The result will be incorrect if array is not statically allocated (e.g. it is
 * a pointer).
 **/
#define array_len(array) (sizeof(array) / sizeof((array)[0]))

/**
 * Gets the length of a statically allocated string.
 *
 * The result will be incorrect if the string is not statically allocated (e.g.
 * it is a pointer).
 **/
#define string_len(string) (array_len(string) - 1)


#endif