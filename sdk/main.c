/*
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include "platform.h"
#include "rdump.h"
#include "remote.h"
#include "xil_printf.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define remotePrintf(message, ...) (printf(message "\n\r", ##__VA_ARGS__))

size_t trimNewlines(char* string, size_t length) {
    while (length > 1 && string[length - 1] == '\n') {
        string[length - 1] = '\0';
        length--;
    }
    return length;
}

void printDebugMessage(void) {
    char message[REMOTE_DEBUG_LIMIT];

    if (remote_readReg(REMOTE_DEBUG_FAST(REMOTE_DEBUG_FAST_ENABLED))
        == REMOTE_DEBUG_FAST_MAGIC) {
        remotePrintf("%#x debugFAST: %#x %#x",
                     remote_readReg(REMOTE_CONTROL(REMOTE_TIMER_LOW)),
                     remote_readReg(REMOTE_DEBUG_FAST(1)),
                     remote_readReg(REMOTE_DEBUG_FAST(2)));
    } else {
        // Must be the slow path
        for (size_t i = 0; i < REMOTE_DEBUG_LIMIT / sizeof(uint32_t); i++) {
            uint32_t data = remote_readReg(REMOTE_DEBUG(i * sizeof(uint32_t)));

            for (size_t byte = i * sizeof(uint32_t);
                 byte < (i + 1) * sizeof(uint32_t);
                 byte++) {
                message[byte] = data & 0xFF;
                data >>= 8;
            }
        }

        size_t size   = strnlen(message, REMOTE_DEBUG_LIMIT - 1);
        message[size] = 0;
        trimNewlines(message, size);

        remotePrintf("%#x debug: %s",
                     remote_readReg(REMOTE_CONTROL(REMOTE_TIMER_LOW)),
                     message);
    }


    // For some reason this doesn't actually work
    // for (size_t i = 0; i < REMOTE_DEBUG_LIMIT / sizeof(uint32_t); i++) {
    //     remote_writeReg(REMOTE_DEBUG(i * sizeof(uint32_t)), 0xDDDDDDDD);
    // }
    // assert(remote_readReg(REMOTE_DEBUG(0)) == 0xDDDDDDDD);
}

int main() {
    init_platform();

    remotePrintf("Starting the remote core. Word Size: %d Initial PC: %#x",
                 sizeof(unsigned),
                 remote_readReg(REMOTE_CONTROL(REMOTE_PC)));
    remotePrintf("Halted: %#x", remote_readReg(REMOTE_CONTROL(REMOTE_HALT)));
    remote_writeReg(REMOTE_CONTROL(REMOTE_HALT), 0);
    uint32_t lastPC = 0;
    while (1) {
        uint32_t haltStatus = remote_readReg(REMOTE_CONTROL(REMOTE_HALT));
        if (haltStatus & 0x2) {
            // There is a pending debug operation
            printDebugMessage();
            // Resume
            remote_writeReg(REMOTE_CONTROL(REMOTE_HALT), 0);
        } else if (haltStatus) {
            remotePrintf("Halting: %x", haltStatus);
            break;
        }
        uint32_t errorCode = remote_readReg(REMOTE_CONTROL(REMOTE_ERROR));
        if (errorCode != 0) {
            remotePrintf("ErrorCode: %#x Address: %#x Index: %#x",
                         errorCode,
                         remote_readReg(REMOTE_CONTROL(REMOTE_ERROR_ADDRESS)),
                         remote_readReg(REMOTE_CONTROL(REMOTE_ERROR_INDEX)));
            for (int i = 0; i < 16; i++) {
                remotePrintf(
                    "Array[%d] = %#x",
                    i,
                    remote_readReg(REMOTE_CONTROL(REMOTE_ERROR_ARRAY + i)));
            }
            registerDump();
        }
        uint32_t pc = remote_readReg(REMOTE_CONTROL(REMOTE_PC));
        // remotePrintf("PC: %#x", pc);
        remotePrintf("PC: %#x Timer: 0x%lx%08lx NextTick: 0x%lx%08lx",
                     pc,
                     remote_readReg(REMOTE_CONTROL(REMOTE_TIMER_HIGH)),
                     remote_readReg(REMOTE_CONTROL(REMOTE_TIMER_LOW)),
                     remote_readReg(REMOTE_CONTROL(REMOTE_NEXT_TIMER_HIGH)),
                     remote_readReg(REMOTE_CONTROL(REMOTE_NEXT_TIMER_LOW)));

        lastPC = pc;
    }

    registerDump();
    remotePrintf("***EXIT***");
    while (1) {
        // spin
    }

    cleanup_platform();
    return 0;
}
