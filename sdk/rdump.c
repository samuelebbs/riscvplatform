#include "rdump.h"
#include "libc_extensions.h"
#include "remote.h"
#include "riscv_register_names.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// The maximum length of an ISA and ABI alias name for a register
#define ISA_NAME_MAX_LEN 3
#define ABI_NAME_MAX_LEN 5

// The maximum number of hexadecimal and decimal digits for a 32-bit integer
#define INT32_MAX_DIGITS 10
#define INT32_MAX_HEX_DIGITS (2 * sizeof(uint32_t))

static const size_t ISA_NAME_COL_LEN =
    max(ISA_NAME_MAX_LEN, string_len("ISA Name"));
static const size_t ABI_NAME_COL_LEN =
    max(ABI_NAME_MAX_LEN + string_len("()"), string_len("ABI Name"));
static const size_t REG_HEX_COL_LEN =
    max(INT32_MAX_HEX_DIGITS + string_len("0x"), string_len("Hex Value"));
static const size_t REG_UINT_COL_LEN =
    max(INT32_MAX_DIGITS + string_len("()"), string_len("Uint Value"));
static const size_t REG_INT_COL_LEN =
    max(INT32_MAX_DIGITS + string_len("()") + 1, string_len("Int Value"));

static void printRegisterHeader(void) {
    remotePrintf("***DUMP***");
    int width = remotePrintf("%-*s %-*s   %-*s %-*s %-*s",
                             (int)ISA_NAME_COL_LEN,
                             "ISA Name",
                             (int)ABI_NAME_COL_LEN,
                             "ABI Name",
                             (int)REG_HEX_COL_LEN,
                             "Hex Value",
                             (int)REG_UINT_COL_LEN,
                             "Uint Value",
                             (int)REG_INT_COL_LEN,
                             "Int Value");
    width -= 2;  // remove extra counts from the remote protocol

    char separatorLine[width + 1];
    memset(separatorLine, '-', width);
    separatorLine[width] = '\0';
    remotePrintf("%s", separatorLine);
}

static void printRegister(int index) {
    const register_name_t* reg_name = &RISCV_REGISTER_NAMES[index];
    uint32_t value = remote_readReg(REMOTE_CONTROL(REMOTE_REGISTER_FILE)
                                    + index * sizeof(uint32_t));
    char     abi_name[ABI_NAME_COL_LEN + 1];
    snprintf(abi_name, sizeof(abi_name), "(%s)", reg_name->abi_name);

    char hex_value[REG_HEX_COL_LEN + 1];
    snprintf(hex_value, sizeof(hex_value), "0x%08lx", value);
    char uint_value[REG_UINT_COL_LEN + 1];
    snprintf(uint_value, sizeof(uint_value), "(%lu)", value);
    char int_value[REG_INT_COL_LEN + 1];
    snprintf(int_value, sizeof(int_value), "(%ld)", (int32_t)value);

    remotePrintf("%-*s %-*s = %-*s %-*s %-*s",
                 (int)ISA_NAME_COL_LEN,
                 reg_name->isa_name,
                 (int)ABI_NAME_COL_LEN,
                 abi_name,
                 (int)REG_HEX_COL_LEN,
                 hex_value,
                 (int)REG_UINT_COL_LEN,
                 uint_value,
                 (int)REG_INT_COL_LEN,
                 int_value);
}

void registerDump(void) {
    remotePrintf("PC: %#x Timer: 0x%lx%08lx NextTick: 0x%lx%08lx",
                 remote_readReg(REMOTE_CONTROL(REMOTE_PC)),
                 remote_readReg(REMOTE_CONTROL(REMOTE_TIMER_HIGH)),
                 remote_readReg(REMOTE_CONTROL(REMOTE_TIMER_LOW)),
                 remote_readReg(REMOTE_CONTROL(REMOTE_NEXT_TIMER_HIGH)),
                 remote_readReg(REMOTE_CONTROL(REMOTE_NEXT_TIMER_LOW)));

    printRegisterHeader();

    for (int i = 0; i < 32; i++) {
        printRegister(i);
    }
}
