#ifndef __REMOTE_H
#define __REMOTE_H

#include "full_chip.h"
#include "xil_io.h"
#include "xparameters.h"

#define remotePrintf(message, ...) (printf(message "\n\r", ##__VA_ARGS__))

// Address Mapping
#define REMOTE_CONTROL_PREFIX 0x0
#define REMOTE_IO_PREFIX 0x2
#define REMOTE_CSR_PREFIX 0xc

// Control Mapping {[11:2], 2'b0}
#define REMOTE_HALT 0x1
#define REMOTE_ERROR 0x2

#define REMOTE_PC 0x4
#define REMOTE_BUTTON 0x8
#define REMOTE_SWITCH 0x9
#define REMOTE_LED 0xa
#define REMOTE_RGB 0xb

#define REMOTE_TIMER_LOW 0x10
#define REMOTE_TIMER_HIGH 0x11
#define REMOTE_NEXT_TIMER_LOW 0x12
#define REMOTE_NEXT_TIMER_HIGH 0x13

#define REMOTE_REGISTER_FILE 0x40

#define REMOTE_ERROR_ADDRESS 0x60
#define REMOTE_ERROR_INDEX 0x61
#define REMOTE_ERROR_ARRAY 0x70

#define REMOTE_ERROR_WRITE_INDEX 0x0
#define REMOTE_ERROR_READ_INDEX 0x1
#define REMOTE_ERROR_ENABLE_INDEX 0x2

#define REMOTE_DEBUG_LIMIT 256

#define REMOTE_CONTROL(address) ((REMOTE_CONTROL_PREFIX << 12) | (address << 2))
#define REMOTE_DEBUG(address) ((REMOTE_IO_PREFIX << 12) | (address))

#define REMOTE_DEBUG_FAST_MAGIC 0xDEB6FA57

#define REMOTE_DEBUG_FAST_BASE 0x200
#define REMOTE_DEBUG_FAST_ENABLED 0
#define REMOTE_DEBUG_FAST(number)                                              \
    ((REMOTE_IO_PREFIX << 12) | REMOTE_DEBUG_FAST_BASE | (number * 4))

#define remote_writeReg(reg, value)                                            \
    (FULL_CHIP_mWriteReg(XPAR_FULL_CHIP_0_S00_AXI_BASEADDR, reg, value))
#define remote_readReg(reg)                                                    \
    (FULL_CHIP_mReadReg(XPAR_FULL_CHIP_0_S00_AXI_BASEADDR, reg))

#endif