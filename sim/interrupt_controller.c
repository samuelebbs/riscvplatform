#include "log.h"
#include <control_status_register.h>
#include <interrupt_controller.h>
#include <riscv_cause.h>
#include <stdlib.h>

void interruptController_check(cpu_state_t* cpu_state) {
    // Prioritize keyboard over timer interrupts per RISC-V spec
    if (csr_interruptEnabled(cpu_state, EXTERNAL_INTERRUPT)
        && cpu_state->cycle >= cpu_state->nextKeyboardTick
        && cpu_state->keyboardList.size > 0) {
        cpu_state->nextKeyboardTick = cpu_state->cycle
                                      + (rand() % (CYCLES_PER_TICK * 10))
                                      + KEYBOARD_MIN_TICK;
        char c = cpu_state->keyboardList.data[0];
        ARRAYLIST_REMOVEAT(&cpu_state->keyboardList, 0);
        csr_deliverInterrupt(cpu_state, EXTERNAL_INTERRUPT, c, cpu_state->pc);
    } else if (csr_interruptEnabled(cpu_state, TIMER_INTERRUPT)
               && cpu_state->cycle >= cpu_state->next_timer) {
        cpu_state->next_timer += CYCLES_PER_TICK;
        // This PC value is pointing to the next instruction (the instruction
        // that should be executed after the interrupt is handled)
        log_tracef("Delivering Timer Tick: %lu value: %lu epc: %#x",
                   cpu_state->cycle,
                   cpu_state->ticks - 1,
                   cpu_state->pc);
        csr_deliverInterrupt(
            cpu_state, TIMER_INTERRUPT, cpu_state->ticks++, cpu_state->pc);
    }
}