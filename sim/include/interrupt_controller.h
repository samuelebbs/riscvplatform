#ifndef __INTERRUPT_CONTROLLER_H
#define __INTERRUPT_CONTROLLER_H

#include <sim.h>

// #define CYCLES_PER_TICK 1000000

// This is lower than the actual core because the core experiences additional
// delays due to cache misses which we don't represent well.
#define CYCLES_PER_TICK 50000
// #define CYCLES_PER_TICK 500000

#define KEYBOARD_MIN_TICK 100000

void interruptController_check(cpu_state_t* cpu_state);

#endif