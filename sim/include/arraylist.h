#ifndef __ARRAY_LIST_H
#define __ARRAY_LIST_H

#include <stddef.h>
#include <string.h>

#define ARRAYLIST_DECLARE(element_type, arraylist_type)                        \
    typedef struct {                                                           \
        element_type* data;                                                    \
        size_t        limit;                                                   \
        size_t        size;                                                    \
    } arraylist_type;


#define ARRAYLIST_INIT(list, initial_limit)                                    \
    do {                                                                       \
        (list)->data  = calloc((initial_limit), sizeof((list)->data[0]));      \
        (list)->limit = (initial_limit);                                       \
        (list)->size  = 0;                                                     \
    } while (0)

#define ARRAYLIST_ADD(list, element)                                           \
    do {                                                                       \
        if ((list)->size == (list)->limit) {                                   \
            (list)->limit = (list)->limit * 2;                                 \
            (list)->data  = realloc((list)->data,                              \
                                   (list)->limit * sizeof((list)->data[0]));  \
        }                                                                      \
        (list)->data[(list)->size++] = element;                                \
    } while (0)

#define ARRAYLIST_REMOVEAT(list, index)                                        \
    do {                                                                       \
        (list)->size--;                                                        \
        if ((index) == (list)->size) { /* Ignore the last element, already     \
                                          decremented size */                  \
        } else {                       /* Push everything over */              \
            memcpy(&(list)->data[(index)],                                     \
                   &(list)->data[(index) + 1],                                 \
                   ((list)->size - (index)) * sizeof((list)->data[0]));        \
        }                                                                      \
    } while (0)

#endif