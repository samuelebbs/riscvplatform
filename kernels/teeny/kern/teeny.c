/** @file teeny.c
 *
 *  @brief Creates a tiny address space (just big enough
 *         to map the guest-kernel ELF image).
 *  @author de0u
 *
 *  PebPeb-only executable -- it doesn't work on hardware.
 */

#include <asm.h>
#include <assert.h>
#include <common_kern.h>
#include <compiler.h>
#include <console.h>
#include <csr.h>
#include <environment.h>
#include <lmm.h> /* lmm_remove_free() */
#include <malloc.h>
#include <malloc_internal.h>
#include <page.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    // start, end of kernel image (odd declarations)
    extern unsigned char __text_start[];
    extern unsigned char __end[];
    unsigned char *      addr;
    uint32_t *           pt0, *pd;
    int                  p;

    console_init();
    lprintf("Well, here I am!");

    pt0 = _smemalign(PAGE_SIZE, PAGE_SIZE);
    pd  = _smemalign(PAGE_SIZE, PAGE_SIZE);
    if (!pt0 || !pd) {
        panic("Cannot allocate memory");
    }
    memset(pt0, 0, PAGE_SIZE);
    memset(pd, 0, PAGE_SIZE);

    addr = (unsigned char *)(((unsigned int)__text_start)
                             & ((unsigned int)~0x0FFF));
    p    = (unsigned int)addr >> 12;

    printf("Addr %p, p %d (end %p)\n", addr, p, __end);
    printf("pt0: %p pd: %p\n", (void *)pt0, (void *)pd);

    while (addr <= __end) {
        lprintf("Mapping %p as page %d\n", addr, p);
        // Kernel|Executable|Writable|Readable|Valid
        pt0[p] = (p << 10) | 0xf;  // XWRV
        addr += PAGE_SIZE;
        ++p;
    }

    // Direct map VRAM
    pt0[1] = (1 << 10) | 0xf;  //
    // Direct map DEBUG
    pt0[2] = (2 << 10) | 0xf;  //

    pd[0] = (((uint32_t)pt0) >> 2) | 0x1;  // Valid Non-Leaf

    printf("Transitioning into hyperspace...\n");
    lprintf("Transitioning into hyperspace...");
    satp_t satp = {
        .ppn                  = ((uint32_t)pd) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    STATIC_ASSERT(sizeof(satp) == sizeof(uint32_t));
    set_satp(satp);
    lprintf("YAY!!\n");
    printf("YAY!!!\n");

    environment_exit(66);

    panic("Exit didn't?");
}
