#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include <asm.h>
#include <string.h>
#include <video_defines.h>


int indirection(int b, va_list args) {
    unsigned long long u;

    if (b != 0x1234abcd) {
        hardware_halt(0xBADBAD4);
    }

    u = va_arg(args, unsigned long long);
    if (u != 0x1122334455667788llu) {
        hardware_halt(0xBADBAD2);
    }

    u = va_arg(args, unsigned long long);
    if (u != 0x1111222233334444llu) {
        hardware_halt(0xBADBAD3);
    }

    return 200;
}

// Expected va_test(0x1234ABCD, 0x1122334455667788,0x1111222233334444)
int va_test(int a, ...) {
    va_list args;

    va_start(args, a);

    if (a != 0x1234ABCD) {
        hardware_halt(0xBADBAD1);
    }

    int result = indirection(a, args);
    if (result != 200) {
        return result;
    }

    va_end(args);

    return 100;
}


/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    // int x = 5;
    // int y = 10;
    unsigned long long u    = 0x100d;
    int                base = 16;

    if (u % base != 0xdllu) {
        hardware_halt(0xBAD1);
    }

    unsigned long long cycles = 0x80000000;
    if (cycles % base != 0llu) {
        hardware_halt(0xBAD2);
    }

    cycles = 0x80000013;
    if (cycles % base != 0x3llu) {
        hardware_halt(0xBAD3);
    }

    cycles = 0x1234567890abcd;
    if (cycles % base != 0xdllu) {
        hardware_halt(0xBAD4);
    }
    int base2 = 4096;
    if (cycles % base2 != 0xbcd) {
        hardware_halt(0xBAD5);
    }
    if (cycles / base != 0x1234567890abcllu) {
        hardware_halt(0xBAD6);
    }

    int result =
        va_test(0x1234ABCD, 0x1122334455667788llu, 0x1111222233334444llu);
    if (result != 100) {
        hardware_halt(0xBAD7);
    }

    printf("This is a test: %lld [%llx] %d [%x] %lld [%llx]",
           u,
           u,
           base,
           base,
           cycles,
           cycles);
    hardware_halt(333);

    panic("Exit failed?");
}
