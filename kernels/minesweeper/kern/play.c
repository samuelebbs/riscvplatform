/** @file game_play.c
 * @brief Implementation of the play state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#include "play.h"
#include "console.h"
#include "footer.h"
#include "timer.h"
#include <asm.h>
#include <board.h>
#include <contracts.h>
#include <minesweeper_levels.h>
#include <score.h>
#include <stdbool.h>
#include <stdio.h>

#define CURSOR_BLINK_PERIOD 50

typedef enum { PAUSED_GAME, IN_GAME, PRE_GAME, LOSE, WIN } PlayState_t;

static int currentLevel = -1;

// CAUTION: DRIVEN BY INTERRUPT!
static int ticksInGame = 0;

// CAUTION: USED BY INTERRUPT!
static PlayState_t   currentState = PRE_GAME;
static volatile bool isDrawing    = false;

static bool optimizeDraw = false;

#define PLAY_COLOR (FGND_LGRAY | BGND_BLACK)

/************************
 *      NEXT_STATE_LOGIC
 ************************/

static void play_nextState_internal(int key);

/** @brief Implements the core behaviors of the play state
 *
 * @param key the key that was most recently pressed
 * @return the next gameState to go to
 */
GameState_t play_nextState(int key) {
    REQUIRES(0 <= currentLevel);

    if (currentState == LOSE) {
        // If the user just lost, their next action is a reset
        key = RESTART_KEY;
    } else if (currentState == WIN) {
        // If the user just won, their next action is a skip
        key = SKIP_KEY;
        // Don't forget to report the score
        score_submit(ticksInGame);
    }

    GameState_t nextState = PLAY;
    int         nextLevel;  // Declaring up here to make the compiler happy
    switch (key) {
        case HELP_KEY:
            // Help the user out if they go into the help screen
            play_nextState_internal(PAUSE_KEY);
            nextState = HELP;
            break;
        case EXIT_KEY:
            play_reset(0);      // Reset all state to the initial conditions
            nextState = TITLE;  // They gave up, go back to the lauch screen
            break;
        case RESTART_KEY:
            play_reset(
                currentLevel);  // Just reset back to the start of this level
            nextState = PLAY;   // Keep on playing
            break;
        case SKIP_KEY:
            nextLevel = currentLevel + 1;
            if (nextLevel >= numlevels) {
                play_reset(0);
                nextState = END_GAME;
            } else {
                play_reset(currentLevel + 1);  // Cheaters always win....
                nextState = PLAY;  // We'll be nice and let them play
            }
            break;
        case LEFT_KEY:
        case 'W':
        case 'A':
        case 'S':
        case 'D':
        case UP_KEY:
        case DOWN_KEY:
        case RIGHT_KEY:
            // WARNING: This is pretty fragile.
            if (currentState == IN_GAME || currentState == PRE_GAME) {
                board_moveCursor(key);
            }
            // If the user tries to move the cursor while paused, ignore them
            // The currentState should never be LOSE while the key is a move
            ASSERT(currentState != LOSE);
            nextState = PLAY;
            break;
        case OPEN_KEY:
        case MARK_KEY:
        case PAUSE_KEY:
        case RESUME_KEY:
            play_nextState_internal(key);
            nextState = PLAY;
            break;
        default:
            // Somebody is just randomly smashing the keyboard.
            // Please remove the cat
            nextState = PLAY;
            break;
    }
    if (nextState != PLAY) {
        optimizeDraw = false;
    }

    return nextState;
}

/** @brief Determines if a key press is an action key
 *
 * @param key the key that was pressed
 * @return true if the key was an action key, false otherwise
 */
static bool play_isActionKey(int key) {
    switch (key) {
        case OPEN_KEY:
        case MARK_KEY:
            return true;
        default:
            return false;
    }
}

/** @brief Converts an actionResult_t into a playState_t
 *
 * @param actionResult the actionResult to be converted
 * @return the converted actionResult
 */
static PlayState_t play_processActionResult(ActionResult_t actionResult) {
    switch (actionResult) {
        case MINE:
            return LOSE;
        case GOOD_PLAY:
            return IN_GAME;
        case ALL_CLEAR:
            return WIN;
        default:
            panic("Unknown Action: %d", actionResult);
            return IN_GAME;  // How did you get here?
    }
}

/** @brief Manage internal FSM
 *
 * @param key the key that was most recently pressed
 */
static void play_nextState_internal(int key) {
    REQUIRES(key == OPEN_KEY || key == MARK_KEY || key == PAUSE_KEY
             || key == RESUME_KEY);

    // We should never be able to transition out of LOSE or WIN.
    // Leaving lose or win requires a RESET
    ASSERT(currentState != LOSE);
    ASSERT(currentState != WIN);

    PlayState_t nextState = currentState;

    switch (currentState) {
        case PAUSED_GAME:
            if (key == RESUME_KEY) {
                nextState = IN_GAME;
            }
            break;
        case PRE_GAME:
            if (key == PAUSE_KEY) {
                // Don't let the user pause in PRE_GAME.
                // They haven't actually started playing yet
                break;
            }
            // WARNING: FALL THROUGH ENABLED!
            // fallthrough
        case IN_GAME:
            if (key == PAUSE_KEY) {
                ASSERT(currentState == IN_GAME);
                nextState = PAUSED_GAME;
            } else if (play_isActionKey(key)) {
                ActionResult_t result = board_applyAction(key);
                nextState             = play_processActionResult(result);
            }
            break;
        default:
            // This should never be reachable
            panic("Unknown play state: %d", currentState);
            break;
    }

    // This is used in the interrupt, but since it is a single word store
    // we should be safe to not disable interrupts
    currentState = nextState;
}

/** @brief Reset the game to a specific level
 *
 * @param level the level to reset to
 */
void play_reset(int32_t level) {
    currentLevel = level;

    // This is used in the interrupt, but since it is a single word store
    // we should be safe to not disable interrupts
    currentState = PRE_GAME;

    // Even though this is driven by the interrupt, since we are in the PRE_GAME
    // state, we can conclude that we don't need to disable interrupts
    // since the tick counter only increases while in game
    ticksInGame = 0;

    board_init(currentLevel);

    optimizeDraw = false;
}

/************************
 *      OUTPUT_LOGIC
 ************************/


static void play_updateHeader() {
    int32_t lastRow, lastCol;
    console_getCursor(&lastRow, &lastCol);
    console_setCursor(0, 0);
    char buffer[TIME_BUFFER_SIZE];
    timer_toString(ticksInGame, buffer, TIME_BUFFER_SIZE);
    printf("LEVEL: %-10d TIME: %-10s MINES: %03d\n",
           (currentLevel + 1),
           buffer,
           board_getRunningMineCount());

    console_setCursor(lastRow, lastCol);
}


/** @brief Displays the play screen */
void play_output(void) {
    disable_interrupts();
    isDrawing = true;
    if (!optimizeDraw) {
        console_setTermColor(PLAY_COLOR);
        console_clear();
        optimizeDraw = true;  // Next time we are optimal
    } else {
        console_setCursor(0, 0);
    }

    play_updateHeader();
    printf("\n");  // Move the cursor down

    switch (currentState) {
        case PAUSED_GAME:
            printf("GAME PAUSED! Press '%c' to resume\n", RESUME_KEY);
            board_draw(BLUR);
            break;
        case IN_GAME:
        case PRE_GAME:
            printf("Press '%c' for help!\n", HELP_KEY);
            board_draw(NORMAL);
            break;
        case LOSE:
            printf("Game Over! Press any key to try again\n");
            board_draw(REVEAL);
            break;
        case WIN:
            printf("You Win! Press any key to advance to the next level\n");
            board_draw(NORMAL);
            break;
        default:
            panic("Unknown play state: %d", currentState);
            // How did you get here?
    }

    footer_draw(DEFAULT_FOOTER);

    isDrawing = false;
    enable_interrupts();
}

/************************
 *      TIMER TICK
 ************************/

/** @brief Timer Tick for Play State
 *
 * @param numTicks the number of ticks since the app launched
 */
void play_tick(uint32_t numTicks) {
    if (currentState == IN_GAME) {
        ++ticksInGame;
    }

    if (!isDrawing) {
        // Toggle the cursor
        if ((currentState == IN_GAME || currentState == PRE_GAME)
            && numTicks % CURSOR_BLINK_PERIOD == 0) {
            board_blinkCursor();
        }

        if (numTicks % 100 == 0) {
            play_updateHeader();
        }
    }
}