/** @file game_title.c
 * @brief Implementation of the title state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#include "console.h"
#include "footer.h"
#include "game.h"
#include "play.h"
#include "score.h"
#include "timer.h"
#include <contracts.h>
#include <stdbool.h>
#include <stdio.h>
#include <video_defines.h>

// Start writing text at the top third of the screen
#define START_OF_TEXT_ROW (CONSOLE_HEIGHT / 3)

// The terminal color for the title screen
#define TITLE_COLOR (FGND_BLUE)

#define DEVELOPER "Reid Long (relong)"


/** @brief Displays the title screen */
void title_output(void) {
    console_setTermColor(TITLE_COLOR);
    console_clear();

    console_setCursor(START_OF_TEXT_ROW, 0);

    printf("                  WELCOME TO MINESWEEPER!\n");
    printf("                  Developer: %s\n", DEVELOPER);
    printf("                  High Scores!\n");

    uint32_t scores[SCORE_LIMIT];
    score_getAll(scores);

    for (int i = 0; i < SCORE_LIMIT; i++) {
        char buffer[TIME_BUFFER_SIZE];
        timer_toString(scores[SCORE_LIMIT - i - 1], buffer, TIME_BUFFER_SIZE);
        printf("                         #%d: %s\n", (i + 1), buffer);
    }
    printf("\n");
    printf("                  Press '%c' for help\n", HELP_KEY);
    printf("                  Press any key to continue\n");

    footer_draw(DEFAULT_FOOTER);
}

/** @brief Determines the next state
 *
 * @param key the key that was pressed
 * @return the next state
 */
GameState_t title_nextState(int key) {
    switch (key) {
        case HELP_KEY:
            return HELP;
        default:
            return PLAY;
    }
}