/** @file game_scores.h
 * @brief Defines the game_scores interface
 *
 * @author Reid Long (relong)
 * @bug the API limits the number of instances of this to a single instance
 */

#ifndef __GAME_SCORES_H
#define __GAME_SCORES_H

#define SCORE_LIMIT 5

#include <stdbool.h>
#include <stdint.h>

void score_getAll(uint32_t scoreArray[SCORE_LIMIT]);

void score_submit(uint32_t scoreTime);

#endif