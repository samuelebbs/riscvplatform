/** @file game_play.h
 * @brief Defintions for the play state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#ifndef __GAME_PLAY_H
#define __GAME_PLAY_H

#include "game.h"
#include <stdint.h>

#define HELP_KEY 'h'
#define PAUSE_KEY 'p'
#define RESUME_KEY 'u'
#define EXIT_KEY 'e'
#define RESTART_KEY 'r'
#define SKIP_KEY 'v'
#define UP_KEY 'w'
#define LEFT_KEY 'a'
#define DOWN_KEY 's'
#define RIGHT_KEY 'd'
#define OPEN_KEY 'o'
#define MARK_KEY 'm'

void play_output(void);

GameState_t play_nextState(int key);

void play_tick(uint32_t numTicks);

void play_reset(int32_t level);

#endif
