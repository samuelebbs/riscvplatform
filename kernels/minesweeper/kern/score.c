/** @file game_scores.c
 * @brief Manages the scores for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#include "score.h"
#include <contracts.h>
#include <stdlib.h>

/** @brief Stores the last n scores */
static uint32_t scores[SCORE_LIMIT];

/** @brief points to the last score earned */
static int last;

/** @brief Reports the last n scores
 *
 * The first element int the array is the oldest score, the last element is the
 *  most recent score
 * @param scoreArray the destination where the scores should be written
 * @return true if scoreArray has the new scores, false otherwise
 */
void score_getAll(uint32_t scoreArray[SCORE_LIMIT]) {
    REQUIRES(scoreArray != NULL);

    int index = last;
    for (int i = 0; i < SCORE_LIMIT; i++) {
        scoreArray[i] = scores[index];
        index         = (index + 1) % SCORE_LIMIT;
    }
}

/** @brief Adds a score to the history list
 *
 * @param scoreTime the score to be reported
 * @return true if successful, false otherwise
 */
void score_submit(uint32_t scoreTime) {
    scores[last] = scoreTime;
    last         = (last + 1) % SCORE_LIMIT;
}
