#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 5

static volatile int interrupts = 0;

static void interrupt_dispatch(ureg_t* state);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    printf("Hello World\n");

    interrupt_install(&interrupt_dispatch, NULL);

    enable_machine_interrupts();

    int steps = 0;
    while (interrupts < INTERRUPT_LIMIT) {
        steps++;
    }

    disable_machine_interrupts();

    if (steps > 6000) {
        printf("Success: saw %d interrupts\n", interrupts);
    } else {
        printf("Failure: %d steps to see %d interrupts\n", steps, interrupts);
    }


    environment_exit(200);

    panic("Exit failed?");
}

static int ticks = 0;

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;
    if (state->cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            printf("Tick: %d [%ld (%lx)]\n", ticks, state->value, state->value);
        }
    } else {
        printf("Tock: %c\n", (char)state->value);
    }
}