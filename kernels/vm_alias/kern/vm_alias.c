#include <asm.h>
#include <assert.h>
#include <common_kern.h>
#include <compiler.h>
#include <console.h>
#include <contracts.h>
#include <csr.h>
#include <environment.h>
#include <interrupt.h>
#include <launch.h>
#include <lmm.h> /* lmm_remove_free() */
#include <malloc.h>
#include <malloc_internal.h>
#include <page.h>
#include <riscv_status.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

static bool memcheck(void* buffer, char check, size_t size) {
    for (size_t i = 0; i < size; i++) {
        if (((char*)buffer)[i] != check) {
            return false;
        }
    }
    return true;
}

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    interrupt_install(&machine_interrupt_monitor, NULL);

    console_init();

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    // Just keep using the current stack for now
    uint64_t stack;

    launch_mret(&stack);
    panic("Launch didn't?");
}

static void supervisor_main(void) {
    lprintf("Setting up initial VM");
    // Setup page table
    // Bottom 16MB direct mapped for kernel
    uint32_t* pageDirectory       = smemalign(PAGE_SIZE, PAGE_SIZE);
    uint32_t* directMapPageTables = smemalign(PAGE_SIZE, PAGE_SIZE * 4);

    // Setup the page table entries (direct mapped)
    for (int pageTableIndex = 0; pageTableIndex < 4096; pageTableIndex++) {
        uint32_t guestFrame                 = pageTableIndex << 10;
        directMapPageTables[pageTableIndex] = guestFrame | 0xf;  // kXWRV
    }

    // Setup the page directory
    for (int pageDirectoryIndex = 0; pageDirectoryIndex < 4;
         pageDirectoryIndex++) {
        // Non-Leaf Page table entry
        uint32_t pageTableNumber =
            (uint32_t)&directMapPageTables[pageDirectoryIndex
                                           * (PAGE_SIZE / sizeof(uint32_t))]
            >> 12;
        pageDirectory[pageDirectoryIndex] = (pageTableNumber << 10) | 0x1;  // V
    }

    uint32_t* buffer1 = smemalign(PAGE_SIZE, PAGE_SIZE);
    uint32_t* buffer2 = smemalign(PAGE_SIZE, PAGE_SIZE);
    lprintf("Initializing buffers 1: %p 2: %p", (void*)buffer1, (void*)buffer2);
    memset(buffer1, 0x11, PAGE_SIZE);
    memset(buffer2, 0x22, PAGE_SIZE);
    ASSERT(memcheck(buffer1, 0x11, PAGE_SIZE));
    ASSERT(memcheck(buffer2, 0x22, PAGE_SIZE));

    lprintf("Enabling virtual memory");
    satp_t satp = {
        .ppn                  = ((uint32_t)pageDirectory) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    set_satp(satp);

    ASSERT(memcheck(buffer1, 0x11, PAGE_SIZE));
    ASSERT(memcheck(buffer2, 0x22, PAGE_SIZE));

    uint32_t  buffer1Frame = (uintptr_t)buffer1 >> 12;
    uint32_t  buffer2Frame = (uintptr_t)buffer2 >> 12;
    uint32_t* buffer1Entry = &directMapPageTables[buffer1Frame];
    uint32_t* buffer2Entry = &directMapPageTables[buffer2Frame];

    // Set the virtual address of buffer2 to point to the frame of buffer1
    *buffer2Entry = (buffer1Frame << 10) | 0xf;
    sfence_vma(buffer2);
    ASSERT(memcheck(buffer2, 0x11, PAGE_SIZE));
    ASSERT(memcheck(buffer1, 0x11, PAGE_SIZE));

    // Set the virtual address of buffer1 to point to the frame of buffer2
    *buffer1Entry = (buffer2Frame << 10) | 0xf;
    sfence_vma(buffer1);
    ASSERT(memcheck(buffer1, 0x22, PAGE_SIZE));
    ASSERT(memcheck(buffer2, 0x11, PAGE_SIZE));

    // Write to buffer1 (frame buffer2)
    memset(buffer1, 0x33, PAGE_SIZE);
    ASSERT(memcheck(buffer1, 0x33, PAGE_SIZE));
    ASSERT(memcheck(buffer2, 0x11, PAGE_SIZE));

    // Set buffer2 to point to frame 2 again and check that the values are
    // updated
    *buffer2Entry = (buffer2Frame << 10) | 0xf;
    sfence_vma(buffer2);
    ASSERT(memcheck(buffer2, 0x33, PAGE_SIZE));
    ASSERT(memcheck(buffer1, 0x33, PAGE_SIZE));

    // Write to buffer2 and expect both buffers to be updated
    memset(buffer2, 0x44, PAGE_SIZE);
    ASSERT(memcheck(buffer1, 0x44, PAGE_SIZE));
    ASSERT(memcheck(buffer2, 0x44, PAGE_SIZE));

    lprintf("All tests passed");

    environment_exit(555);

    panic("Exit didn't?");
}