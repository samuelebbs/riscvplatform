#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include "timer.h"
#include <asm.h>
#include <contracts.h>
#include <csr.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 100

static volatile int interrupts = 0;
static volatile int done       = 0;
static int          ticks      = 0;

extern int wait(volatile int* until);

static void interrupt_dispatch(ureg_t* state);


#define BUFFER_SIZE 2048
static volatile int count = 0;
static volatile int buffer[BUFFER_SIZE];

int doWork(void) {
    for (int i = 0; i < BUFFER_SIZE; i++) {
        if (buffer[i] != count) {
            return -5;
        }
    }
    count++;
    for (int i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = count;
    }
    for (int i = 0; i < BUFFER_SIZE; i++) {
        if (buffer[i] != count) {
            return -6;
        }
    }
    return 0;
}

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    interrupt_install(&interrupt_dispatch, NULL);

    enable_machine_interrupts();

    int result = 0;
#ifdef __HARDWARE__
    while (result == 0) {
#else
    while (interrupts < INTERRUPT_LIMIT && result == 0) {
#endif

        result = doWork();

        disable_machine_interrupts();
        console_setTermColor(FGND_BGRN);
        // Update the timer
        char timer[CONSOLE_WIDTH];
        timer_toString(ticks, timer, CONSOLE_WIDTH);
        console_setCursor(0, 0);
#ifndef __HARDWARE__
        strcpy(timer, "<sim>");
#endif
        printf("Time Since Boot: %s\n", timer);

        // Update the performance counters
        uint64_t cycles  = csr_getCycles();
        uint64_t retired = csr_getInstructionsRetired();
#ifndef __HARDWARE__
        cycles  = 0xFA7F00D;
        retired = 0xD0BAD;
#endif
        printf("Cycles:  %#16llx Instructions Retired: %#16llx\n",
               cycles,
               retired);

        uint64_t iHit  = csr_getICacheHit();
        uint64_t iMiss = csr_getICacheMiss();
        uint64_t dHit  = csr_getDCacheHit();
        uint64_t dMiss = csr_getDCacheMiss();
        uint64_t dVRAM = csr_getDVRAMHit();
#ifndef __HARDWARE__
        iHit  = 100;
        iMiss = 50;
        dHit  = 100;
        dMiss = 75;
        dVRAM = 15;
#endif
        uint64_t dRate = (dHit + dVRAM) * 100 / (dHit + dVRAM + dMiss);
        uint64_t iRate = (iHit * 100) / (iHit + iMiss);
        printf("Data Cache Hit: %8lld%% Instruction Cache Hit: %14lld%%\n",
               dRate,
               iRate);
        enable_machine_interrupts();
    }

    disable_machine_interrupts();

    environment_exit(200);

    panic("Exit failed?");
}

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;
#ifndef __HARDWARE__
    // This is absurdly verbose on a real system
    if (interrupts % 5 == 0) {
        lprintf("Found an interrupt: %d %lx %lx",
                interrupts,
                state->cause,
                state->value);
    }
#endif

    ASSERT(!machine_interrupts_enabled());
    if (state->cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            printf("Tick: %d [%ld (%lx)]\n", ticks, state->value, state->value);
        }
    } else {
        static int key_press_count = 1;
        ASSERT(state->value != 0);
        lprintf("Tock[%d]: %c (%lx)\n",
                key_press_count++,
                (char)state->value,
                state->value);
    }

    if (interrupts > INTERRUPT_LIMIT) {
        done = 1;
    }
    ASSERT(!machine_interrupts_enabled());
}