/** @file vast.c
 *
 *  @brief Creates a large address space and explores it
 *
 *  Vast maps the bottom four megabytes of "physical memory"
 *  repeatedly into the entire 32-bit virtual address space.
 *  It then reads from every other page (there shouldn't be
 *  any faults), totaling up the values it reads, and then
 *  lprintf()s the nonsense value that results.
 *
 *  When complete, it hv_exit(99)
 *
 *  @author de0u
 *  @author relong
 *
 *  Hello is a PebPeb-only executable -- it doesn't work on hardware.
 *
 */

#include <asm.h>
#include <assert.h>
#include <common_kern.h>
#include <console.h>
#include <csr.h>
#include <environment.h>
#include <lmm.h> /* lmm_remove_free() */
#include <malloc.h>
#include <malloc_internal.h>
#include <page.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    uint32_t *pt, *pd;

    console_init();

    lprintf("Hello from a brand new kernel!");

    pt = _smemalign(PAGE_SIZE, PAGE_SIZE);
    pd = _smemalign(PAGE_SIZE, PAGE_SIZE);
    if (!pt || !pd) {
        panic("Cannot allocate memory");
    }

    for (uint32_t i = 0; i < (PAGE_SIZE / sizeof(uint32_t)); i++) {
        // Kernel|Executable|Writable|Readable|Valid
        pt[i] = (i << 10) | 0xf;              // XWRV
        pd[i] = (((uint32_t)pt) >> 2) | 0x1;  // Valid Non-Leaf page table
    }

    lprintf("I hope this works...");
    printf("Setting Page Directory\n");
    satp_t satp = {
        .ppn                  = ((uint32_t)pd) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    set_satp(satp);
    lprintf("Whew!!  Now let's take a look around.");
    printf("Page Directory Set successfully\n");
    lprintf("Reading now");

    int t = 0;
    for (uint64_t p = 0; p <= 0xFFFFFFFF; p += 8192) {
        t += *(int *)(uintptr_t)(uint32_t)p;
    }

    lprintf("YAY!!! (t = 0x%x, FWIW)\n", t);
    printf("Vast passing\n");

    environment_exit(99);

    while (1) {
        lprintf("In dead man loop");
        continue;
    }

    return -1;
}
