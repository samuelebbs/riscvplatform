#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 100

static volatile int interrupts = 0;
static volatile int done       = 0;

extern int wait(volatile int* until);

static void interrupt_dispatch(ureg_t* state);


#define BUFFER_SIZE 2048
static volatile int count = 0;
static volatile int buffer[BUFFER_SIZE];

int doWork(void) {
    for (int i = 0; i < BUFFER_SIZE; i++) {
        if (buffer[i] != count) {
            return -5;
        }
    }
    count++;
    for (int i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = count;
    }
    for (int i = 0; i < BUFFER_SIZE; i++) {
        if (buffer[i] != count) {
            return -6;
        }
    }
    return 0;
}

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    printf("Running interrupt_test\n");

    interrupt_install(&interrupt_dispatch, NULL);

    enable_machine_interrupts();
    int result = wait(&done);
    disable_machine_interrupts();

    if (25 < result && result < 300) {
        printf("Success");
        lprintf("Success: saw %d interrupts\n", interrupts);
        environment_exit(200);
    } else {
        printf("Failure: %d steps to see %d interrupts\n", result, interrupts);
        environment_exit(-20);
    }

    panic("Exit failed?");
}

static int ticks = 0;

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;
    if (interrupts % 5 == 0) {
        lprintf("Found an interrupt: %d %lx %lx",
                interrupts,
                state->cause,
                state->value);
    }

    if (state->cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            // printf("Tick: %d [%ld (%lx)]\n", ticks, state->value,
            // state->value);
            lprintf("Tick: %d %d", ticks, count);
        }
    } else {
        printf("Tock: %c\n", (char)state->value);
    }

    if (interrupts > INTERRUPT_LIMIT) {
        done = 1;
    }
}