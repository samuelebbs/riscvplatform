#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include <string.h>
#include <video_defines.h>


/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();

    extern char __text_start[], __text_end[];
    extern char __rodata_start[], __rodata_end[];
    extern char __data_start[], __data_end[];
    extern char __bss_start[], __bss_end[];
    extern char end[], __end[];

    printf("text: [%p, %p)\n", __text_start, __text_end);
    printf("rodata: [%p, %p)\n", __rodata_start, __rodata_end);
    printf("data: [%p, %p)\n", __data_start, __data_end);
    printf("bss: [%p, %p)\n", __bss_start, __bss_end);
    printf("end: %p == %p\n", end, __end);

    environment_exit(101);

    panic("Exit failed?");
}
