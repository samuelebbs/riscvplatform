#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <contracts.h>
#include <launch.h>
#include <riscv_status.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 100

static volatile int interrupts = 0;
static volatile int done       = 0;

extern int wait(volatile int* until);

static void interrupt_dispatch(ureg_t* state);

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    interrupt_install(&machine_interrupt_monitor, &interrupt_dispatch);

    console_init();

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    // Just keep using the current stack for now
    uint64_t stack;

    launch_mret(&stack);
    panic("Launch didn't?");
}

static void supervisor_main(void) {
    printf("Running tick_tock_register_test\n");

    enable_supervisor_interrupts();

    int result = wait(&done);

    disable_supervisor_interrupts();

    if (75000 < result && result < 900000) {
        printf("Success: saw %d interrupts\n", interrupts);
    } else {
        printf("Failure: %d steps to see %d interrupts\n", result, interrupts);
    }

    environment_exit(200);

    panic("Exit failed?");
}

static int ticks = 0;

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;

    ASSERT(!supervisor_interrupts_enabled());
    if (interrupts % 5 == 0) {
        lprintf("Found an interrupt: %d %lx %lx",
                interrupts,
                state->cause,
                state->value);
    }

    if (state->cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            printf("Tick: %d [%ld (%lx)]\n", ticks, state->value, state->value);
        }
    } else {
        printf("Tock: %c\n", (char)state->value);
    }

    if (interrupts > INTERRUPT_LIMIT) {
        done = 1;
    }
    ASSERT(!supervisor_interrupts_enabled());
}