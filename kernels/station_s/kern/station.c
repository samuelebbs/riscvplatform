#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <launch.h>
#include <riscv_status.h>
#include <string.h>
#include <video_defines.h>

/* https://classictrainsongs.com/down_by_station/down_by_the_station.htm */
char* lyrics[] = {"Down by the station early in the morning,\n",
                  "See the little puffer bellies all in a row.\n",
                  "See the station master turn the little handle,\n",
                  "Chug, chug, toot, toot, off we go.\n",
                  NULL};

void twiddle(void) {
    volatile int accumulator = 0;
    while (accumulator != -1) ++accumulator;
    panic("Uh oh, overflow, population, common group");
}

// If you make this much larger, what happens next will shock you!
#define DELAY_TICKS 10

volatile int tick = 0;
volatile int line = 0;

static void supervisor_interrupt_dispatch(ureg_t* state);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg = (1 << (TIMER_INTERRUPT & CAUSE_INDEX_MASK))
                       | (1 << (EXTERNAL_INTERRUPT & CAUSE_INDEX_MASK));
    set_mideleg(mideleg);

    interrupt_install(&machine_interrupt_monitor,
                      &supervisor_interrupt_dispatch);

    console_init();

    // Just keep using the current stack
    uint64_t stack;

    set_mepc((uint32_t)(uintptr_t)&twiddle);
    set_mpp(SUPERVISOR_MODE);

    enable_supervisor_interrupts();

    launch_mret(&stack);

    panic("Launch didn't?");
}

static void supervisor_interrupt_dispatch(ureg_t* state) {
    (void)state;
    if (++tick == DELAY_TICKS) {
        tick = 0;
        printf("%s", lyrics[line]);
        lprintf("%s", lyrics[line]);
        ++line;
        if (lyrics[line] == NULL)
            environment_exit(0);
    }
}
