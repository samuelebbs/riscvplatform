/** @file timing.c
 *
 *  @brief Verifies that hypervisor timer ticks are working
 *
 *  @author relong
 *
 *  PebPeb-only executable -- it doesn't work on hardware.
 *
 */

#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include "timer.h"
#include <asm.h>
#include <csr.h>
#include <launch.h>
#include <riscv_status.h>
#include <string.h>
#include <video_defines.h>

static const int    TICK_LIMIT = 5;
static volatile int tickCount  = 0;

#define DELAY_STEPS (2)

extern int wait(int steps);

#define BUFFER_SIZE 2048
static volatile int count = 0;
static volatile int buffer[BUFFER_SIZE];

int doWork(void) {
    for (int i = 0; i < BUFFER_SIZE; i++) {
        if (buffer[i] != count) {
            return -5;
        }
    }
    count++;
    for (int i = 0; i < BUFFER_SIZE; i++) {
        buffer[i] = count;
    }
    for (int i = 0; i < BUFFER_SIZE; i++) {
        if (buffer[i] != count) {
            return -6;
        }
    }
    return 0;
}

static void supervisor_interrupt_dispatch(ureg_t* state) {
    (void)state;
    tickCount++;
    lprintf("Tick called: %d", tickCount);
}

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    // Install handlers

    interrupt_install(&machine_interrupt_monitor,
                      &supervisor_interrupt_dispatch);

    console_init();

    // Just keep using the current stack
    uint64_t stack;

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    launch_mret(&stack);

    panic("Launch didn't?");
}

static void supervisor_main(void) {
    /////////////////////////////////////////
    printf("Trying to enable interrupts\n");
    enable_supervisor_interrupts();

    printf("Interrupts enabled\n");

    int count = 0;
    while (tickCount < TICK_LIMIT) {
        count++;
        int result = wait(DELAY_STEPS);
        lprintf("In loop body: %d, %d, %d\n", count, tickCount, result);
        if (result <= 0) {
            environment_exit(10);
        }
    }

    if (count <= 0) {
        printf("Bad count?: %d\n", count);
        environment_exit(-11);
    }

    ////////////////////////////////////////
    printf("Disable interrupts\n");
    disable_supervisor_interrupts();

    int originalTick = tickCount;
    lprintf("Running next test: %d @ %d\n", tickCount, count);

    for (int index = 0; index < count; index++) {
        int result = wait(DELAY_STEPS);
        lprintf("Waiting in loop: %d, %d, %d\n", index, tickCount, result);
    }

    if (tickCount != originalTick) {
        printf("Tick incrementing without interrupts?\n");
        environment_exit(-12);
    }

    ///////////////////////////////////////
    enable_supervisor_interrupts();

    for (int index = 0; index < count; index++) {
        int result = wait(DELAY_STEPS);
        lprintf("Waiting in loop2: %d, %d, %d\n", index, tickCount, result);
    }

    disable_supervisor_interrupts();

    lprintf("Final tick count: %d\n", tickCount);

    if (tickCount <= originalTick) {
        printf("Tick didn't increment second pass?\n");
        environment_exit(-13);
    }

    printf("All tests passed\n");
    environment_exit(88);

    panic("Exit failed?");
}