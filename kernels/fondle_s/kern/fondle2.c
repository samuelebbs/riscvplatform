/** @file fondle2.c
 *
 *  The test proceeds as follows.
 *
 *  1. Insert a static mapping which direct maps the bottom 16MB for user
 *     and kernel space.
 *
 *  2. A special 4MB region of the address space is mapped for kernel space
 *     only.
 *
 *  3. In kernel mode the numbers 0 through 1023 are written to the pages
 *     at the top of memory.
 *
 *  4. The top page table is reversed and hv_adjustpg() is called on all
 *     of the addresses in the top 4MB.
 *
 *  5. Values are checked to make sure they reversed along with the frames.
 *
 *  6. iret goes to "user" mode and expects a page fault when it tries
 *     to touch the top memory.
 *
 *  @author mischnal
 *  @author relong
 *
 */

#include <asm.h>
#include <assert.h>
#include <common_kern.h>
#include <compiler.h>
#include <console.h>
#include <csr.h>
#include <environment.h>
#include <interrupt.h>
#include <launch.h>
#include <malloc_internal.h>
#include <page.h>
#include <riscv_status.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KERNEL_PAGE_DIRECTORY_INDEX 410
#define STACK_SIZE 512

static const uint32_t MB             = 1024 * 1024;
static const uint32_t KERNEL_ADDRESS = KERNEL_PAGE_DIRECTORY_INDEX << 22;
static const uint32_t MAGIC_OFFSET   = 0x9f0;


static int tests_passed  = 0;
static int tests_started = 0;

static uint64_t user_stack[STACK_SIZE];
static uint64_t kernel_stack[STACK_SIZE];
static uint64_t machine_stack[STACK_SIZE];

#define STACK_TOP(stack_low)                                                   \
    ((uint32_t)(((uintptr_t) & (stack_low)[STACK_SIZE - 1])))

static void page_fault(void);
static void tick(void);
static void write_test(void);
static void rev_array(void* arrv, size_t elm_size, size_t array_size);
static void read_test(void);
static void supervisor_interrupt_dispatch(ureg_t* state);

#define dual_printf(fmt, ...)                                                  \
    do {                                                                       \
        lprintf_unsafe(fmt, __VA_ARGS__);                                      \
        printf(fmt "\n", __VA_ARGS__);                                         \
    } while (0)

#define dual_print(msg) dual_printf("%s", msg)

static void supervisor_main(void);


/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    uint32_t medeleg =
        DELEGATION_MASK(USER_ECALL) | DELEGATION_MASK(LOAD_PAGE_FAULT);
    set_medeleg(medeleg);

    interrupt_install(&machine_interrupt_monitor,
                      &supervisor_interrupt_dispatch);

    console_init();

    // We need to setup the machine stack since we might enter from user-mode
    set_mscratch(STACK_TOP(machine_stack));

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    // Just keep using the current stack for now
    uint64_t stack;

    launch_mret(&stack);
    panic("Launch didn't?");
}

static void supervisor_main(void) {
    dual_print("Starting fondle");

    // Setup page table
    // Bottom 16MB direct mapped for both kernel and user access
    uint32_t* pageDirectory       = _smemalign(PAGE_SIZE, PAGE_SIZE);
    uint32_t* directMapPageTables = _smemalign(PAGE_SIZE, PAGE_SIZE * 4);

    for (int pageTableIndex = 0; pageTableIndex < PAGE_SIZE; pageTableIndex++) {
        uint32_t guestFrame = pageTableIndex << 10;
        // User|Executable|Writable|Readable|Valid
        directMapPageTables[pageTableIndex] = guestFrame | 0x1f;  // UXWRV
    }
    for (int pageDirectoryIndex = 0; pageDirectoryIndex < 4;
         pageDirectoryIndex++) {
        // Non-Leaf Page table entry
        uint32_t pageTableNumber =
            (uint32_t)&directMapPageTables[pageDirectoryIndex
                                           * (PAGE_SIZE / sizeof(uint32_t))]
            >> 12;
        pageDirectory[pageDirectoryIndex] = (pageTableNumber << 10) | 0x1;  // V
    }
    // 4MB special region mapped for kernel only starting with the 4096th frame
    uint32_t* kernelPageTable = _smemalign(PAGE_SIZE, PAGE_SIZE);
    for (size_t pageTableIndex = 0;
         pageTableIndex < (PAGE_SIZE / sizeof(uint32_t));
         pageTableIndex++) {
        uint32_t guestFrame = (pageTableIndex + 4096) << 10;
        // Executable|Writable|Readable|Valid
        kernelPageTable[pageTableIndex] = guestFrame | 0xf;  // XWRV
    }
    uint32_t kernelPageTableNumber = (uint32_t)kernelPageTable >> 12;
    pageDirectory[KERNEL_PAGE_DIRECTORY_INDEX] =
        (kernelPageTableNumber << 10) | 0x1;  // V

    dual_print("Setting up page directory");
    // Need to set the SUM bit to 1 so that the kernel can access pages with
    // user permissions
    enable_supervisor_user_access();


    satp_t satp = {
        .ppn                  = ((uint32_t)pageDirectory) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    STATIC_ASSERT(sizeof(satp) == sizeof(uint32_t));
    set_satp(satp);

    write_test();

    rev_array(kernelPageTable, sizeof(uint32_t), PAGE_SIZE / sizeof(uint32_t));

    for (uint32_t guest_virtual = KERNEL_ADDRESS;
         guest_virtual < (KERNEL_ADDRESS + 4 * MB);
         guest_virtual += PAGE_SIZE) {
        sfence_vma((void*)(uintptr_t)guest_virtual);
    }

    read_test();

    dual_print("Entering user mode");

    // Go to user mode
    set_sepc((uint32_t)read_test);
    // Force 16-byte alignment
    uint64_t* user_stack_top = (uint64_t*)STACK_TOP(user_stack);
    set_sscratch(STACK_TOP(kernel_stack));
    set_spp(USER_MODE);

    // Switches argument to sp and then calls SRET
    launch_sret(user_stack_top);

    panic("launch didn't?");
}

static void supervisor_interrupt_dispatch(ureg_t* state) {
    switch (state->cause) {
        case USER_ECALL:
            if (state->a0 == 0xd) {
                // This is probably an lprintf request, pass it through
                ecall(0xd);
                state->epc += 4;  // Move to next instruction
            } else {
                dual_printf("Unexpected user_ecall: %#lx", state->a0);
                environment_exit(-30);
            }
            break;
        case TIMER_INTERRUPT:
        case EXTERNAL_INTERRUPT:
            tick();
            break;
        case LOAD_PAGE_FAULT:
            page_fault();
            break;
        default:
            dual_printf("Unexpected interrupt: %#lx %#lx %#lx",
                        state->cause,
                        state->value,
                        state->epc);
            environment_exit(-20);
            break;
    }
}

static void write_test(void) {
    dual_print("fondle: starting write test");
    tests_started++;

    int i = 0;
    // Set values 0 - 1023
    for (uint32_t guest_virtual = KERNEL_ADDRESS;
         guest_virtual < (KERNEL_ADDRESS + 4 * MB);
         guest_virtual += PAGE_SIZE) {
        *(int*)(guest_virtual + MAGIC_OFFSET) = i++;
    }
    i = 0;
    for (uint32_t guest_virtual = KERNEL_ADDRESS;
         guest_virtual < (KERNEL_ADDRESS + 4 * MB);
         guest_virtual += PAGE_SIZE) {
        int i_prime = *(int*)(guest_virtual + MAGIC_OFFSET);
        if (i != i_prime) {
            dual_printf("Value written to page %lx did not appear (%d != %d)",
                        guest_virtual,
                        i,
                        i_prime);
            environment_exit(-11);
        }
        i++;
    }
    dual_print("fondle: Write test complete");
    tests_passed++;
}

static void read_test(void) {
    dual_print("fondle: starting read test");
    tests_started++;
    int i = (PAGE_SIZE / sizeof(uint32_t)) - 1;

    for (uint32_t guest_virtual = KERNEL_ADDRESS;
         guest_virtual < (KERNEL_ADDRESS + 4 * MB);
         guest_virtual += PAGE_SIZE) {
        int i_prime = *(int*)(guest_virtual + MAGIC_OFFSET);
        if (i != i_prime) {
            dual_printf("Value written to page %lx did not appear (%d != %d)",
                        guest_virtual,
                        i,
                        i_prime);
            environment_exit(-11);
        }
        i--;
    }
    dual_print("fondle: Read test completed");
    tests_passed++;
}

static void page_fault(void) {
    lprintf("In page fault handler");

    if (tests_passed == 2 && tests_started == 3) {
        dual_print("Fondle passed");
        environment_exit(1);
    } else {
        dual_printf("Fondle failed (early page fault) %d %d",
                    tests_passed,
                    tests_started);
        environment_exit(-13);
    }
}

static void tick(void) {
    // Interrupts are always enabled in user-mode
    lprintf("Tick called?");
}

static void rev_array(void* arrv, size_t elm_size, size_t array_size) {
    size_t beg, end;
    char*  arr = (char*)arrv;

    beg = 0;
    end = (elm_size * (array_size - 1));

    while (beg < end) {
        size_t i;
        for (i = 0; i < elm_size; i++) {
            char temp    = arr[beg + i];
            arr[beg + i] = arr[end + i];
            arr[end + i] = temp;
        }
        beg += elm_size;
        end -= elm_size;
    }
}
