/** @file dumper.c
 *
 *  @brief Minimal guest kernel -- uses just lprintf()
 *
 *  @author de0u
 *  @author relong
 *
 *  Hello is a PebPeb-only executable -- it doesn't work on hardware.
 *
 */

#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <asm.h>
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

/* memory includes. */
#include <lmm.h> /* lmm_remove_free() */


char bigarray[2 * 1024 * 1024];

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    printf("Well, here I am!\n");
    printf("Hello there\n");
    // console_putBytes("Hello there\n", 12);

    extern lmm_t malloc_lmm;
    lmm_dump(&malloc_lmm);

    printf("Successfully running as a microkernel\n");
    // console_putBytes("Successfully running as a microkernel\n", 38);

    environment_exit(0x55);

    printf("Shouldn't have gotten here\n");

    while (1) {
        continue;
    }
}
