/***************************************************************************//**

  @file         main.c

  @author       Stephen Brennan

  @date         Created Wednesday, 10 June 2015

  @brief        Main program for tetris.

  @copyright    Copyright (c) 2015, Stephen Brennan.  Released under the Revised
                BSD License.  See LICENSE.txt for details.

*******************************************************************************/


#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "keyboard.h"
#include "timer.h"
#include <asm.h>
#include <csr.h>
#include <drivers.h>
#include <interrupt_cause.h>
#include <string.h>
#include <video_defines.h>
#include "footer.h"
#include <contracts.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "tetris.h"

int cell_color(char c) {
    switch (c) {
        case TC_CELLI: return FGND_CYAN;
        case TC_CELLJ: return FGND_BLUE;
        case TC_CELLL: return FGND_BRWN;
        case TC_CELLO: return FGND_YLLW; //0xE0;
        case TC_CELLS: return FGND_GREEN;
        case TC_CELLT: return FGND_MAG;
        case TC_CELLZ: return FGND_RED;
        default: return FGND_BLACK;
    }
}

/*
  Print the tetris board onto the ncurses window.
 */
void display_board(tetris_game *obj)
{
  int i, j;
  for (i = 0; i < obj->rows; i++) {
    console_setCursor(1 + i, 1);
    for (j = 0; j < obj->cols; j++) {
      console_drawChar(i, 2*j+0, 'X', cell_color(tg_get(obj, i, j)));
      console_drawChar(i, 2*j+1, 'X', cell_color(tg_get(obj, i, j)));
    }
  }
}

/*
  Display score information in a dedicated window.
 */
void display_score(tetris_game *tg)
{
  console_setCursor(14, 2*11+1);
  printf("Score\n%d\n", tg->points);
  printf("Level\n%d\n", tg->level);
  printf("Lines\n%d\n", tg->lines_remaining);
}

uint32_t g_ticks;
void game_tick(uint32_t ticks) {
    g_ticks = ticks;
    // TODO: Implement
}

/*
  Main tetris game!
 */
int kernel_main(void) {
  drivers_install(game_tick);
  enable_interrupts();

  tetris_game *tg = tg_create(20, 10);
  tetris_move move = TM_NONE;
  bool running = true;

  // Game loop
  while (running) {
    running = tg_tick(tg, move);
    console_clear();
    display_board(tg);
    display_score(tg);
    
    int key = readchar();
    switch (key) {      
    case 'A': /* fallthrough */
    case 'a': /* fallthrough */
    case 'J': /* fallthrough */
    case 'j':
      move = TM_LEFT;
      break;
    case 'D': /* fallthrough */
    case 'd': /* fallthrough */
    case 'L': /* fallthrough */
    case 'l':
      move = TM_RIGHT;
      break;
    case 'W': /* fallthrough */
    case 'w': /* fallthrough */
    case 'I': /* fallthrough */
    case 'i':
      move = TM_CLOCK;
      break;
    case 'S': /* fallthrough */
    case 's': /* fallthrough */
    case 'K': /* fallthrough */
    case 'k':
      move = TM_DROP;
      break;
    case 'q':
      running = false;
      move = TM_NONE;
      break;
    case 'p':
      console_clear();
      printf("PAUSED");
      while(readchar() == -1);
      move = TM_NONE;
      break;
    case ' ':
      move = TM_HOLD;
      break;
    default:
      move = TM_NONE;
    }
  }

  // Output ending message.
  printf("Game over!\n");
  printf("You finished with %d points on level %d.\n", tg->points, tg->level);

  // Deinitialize Tetris
  tg_delete(tg);
  return 0;
}