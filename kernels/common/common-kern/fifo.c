//# PRINT AFTER common/inc/fifo.h
#include "fifo.h"
#include "debug.h"
#include "error_code_helper.h"
#include "log.h"
#include <contracts.h>
#include <malloc.h>

static const int32_t CREATED_MAGIC = 0x77771100;
static const int32_t DESTROY_MAGIC = -998811;

#ifdef DEBUG
static bool fifo_isValid(FIFO_t* fifo) {
    REQUIRES(fifo != NULL);
    if (fifo->created != CREATED_MAGIC) {
        log_fatalf("FIFO not created? %d (%x)",
                   (int)fifo->created,
                   (int)fifo->created);
        return false;
    }

    if (fifo->head >= fifo->size) {
        log_fatalf("FIFO head too big: %d >= %d", fifo->head, fifo->size);
        return false;
    }

    if (fifo->tail >= fifo->size) {
        log_fatalf("FIFO tail too big: %d >= %d", fifo->head, fifo->size);
        return false;
    }

    if (fifo->buffer == NULL) {
        log_fatal("FIFO buffer not allocated?");
        return false;
    }

    if (fifo->size == 0) {
        log_fatal("FIFO size 0?");
        return false;
    }

    return true;
}

#endif

ErrorCode_t fifo_init(FIFO_t* fifo, size_t size) {
    REQUIRES(fifo != NULL);
    WARN_UNLESS(fifo->created != CREATED_MAGIC, fifo->created);

    fifo->buffer = smalloc(size * sizeof(FIFO_e));
    RETURN_ON_NULL_WITH_MESSAGE(
        fifo->buffer, FIFO_LOW_MEMORY, "Unable to allocate fifo buffer");
    fifo->head = 0;
    fifo->tail = 0;
    fifo->size = size;

    fifo->created = CREATED_MAGIC;

    ENSURES(fifo_isValid(fifo));
    return SUCCESS;
}

void fifo_destroy(FIFO_t* fifo) {
    REQUIRES(fifo != NULL);
    REQUIRES(fifo_isValid(fifo));
    fifo->created = DESTROY_MAGIC;
    sfree(fifo->buffer, fifo->size * sizeof(FIFO_e));
}

static size_t fifo_safeIncrement(size_t current, size_t limit) {
    size_t next = current + 1;
    if (next >= limit) {
        ASSERT(next == limit);
        next = 0;
    }

    ENSURES(next < limit);
    return next;
}

ErrorCode_t fifo_enqueue(FIFO_t* fifo, FIFO_e data) {
    REQUIRES(fifo != NULL);
    REQUIRES(fifo_isValid(fifo));

    size_t tail = fifo->tail;
    size_t head = fifo->head;

    size_t nextTail = fifo_safeIncrement(tail, fifo->size);

    if (nextTail == head) {
        // Full
        log_warnf("Unable to add element to fifo: H: %d T: %d NT: %d S: %d",
                  head,
                  tail,
                  nextTail,
                  fifo->size);
        return FIFO_FULL;
    } else {
        fifo->buffer[tail] = data;
        fifo->tail         = nextTail;
        return SUCCESS;
    }
}
ErrorCode_t fifo_dequeue(FIFO_t* fifo, FIFO_e* data) {
    REQUIRES(fifo != NULL);
    REQUIRES(fifo_isValid(fifo));
    REQUIRES(data != NULL);

    size_t tail = fifo->tail;
    size_t head = fifo->head;

    if (head == tail) {
        log_warnf("Unable to dequeue from empty fifo: H: %d T: %d S %d",
                  head,
                  tail,
                  fifo->size);
        return FIFO_EMPTY;
    } else {
        size_t nextHead = fifo_safeIncrement(head, fifo->size);
        *data           = fifo->buffer[head];
        fifo->head      = nextHead;
        return SUCCESS;
    }
}
ErrorCode_t fifo_removeLast_unsafe(FIFO_t* fifo, FIFO_e* data) {
    REQUIRES(fifo != NULL);
    REQUIRES(fifo_isValid(fifo));
    REQUIRES(data != NULL);
    // This is not interrupt safe with respect to enqueue and dequeue since it
    // is modifying tail like enqueue.

    size_t tail = fifo->tail;
    size_t head = fifo->head;

    if (head == tail) {
        log_warnf("Unable to remove from fifo: H: %d T: %d S: %d",
                  head,
                  tail,
                  fifo->size);
        return FIFO_EMPTY;
    } else {
        size_t nextTail;
        if (tail == 0) {
            ASSERT(fifo->size > 0);
            nextTail = fifo->size - 1;
        } else {
            nextTail = tail - 1;
        }
        ASSERT(nextTail < fifo->size);
        *data      = fifo->buffer[nextTail];
        fifo->tail = nextTail;
        return SUCCESS;
    }
}