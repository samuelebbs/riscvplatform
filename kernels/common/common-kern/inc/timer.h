//# PRINT AFTER io.c
#ifndef __TIMER_H
#define __TIMER_H

#include <stddef.h>
#include <stdint.h>

#define FREQUENCY_MHZ 100
#define MHZ_PER_SECOND 1000000
#define TIMER_TICK_CYCLES 250000
#define TICK_PER_SECOND (FREQUENCY_MHZ * MHZ_PER_SECOND / TIMER_TICK_CYCLES)
#define SECOND_PER_MINUTE 60
#define MINUTE_PER_HOUR 60

typedef void (*Tickback_t)(uint32_t tick);

void timer_init(Tickback_t tickback);

void timer_handler(uint32_t tick);

void timer_toString(uint32_t ticks, char* buffer, size_t length);

uint32_t timer_getTicks(void);

#endif
