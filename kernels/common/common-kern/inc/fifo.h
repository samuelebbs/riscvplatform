//# PRINT AFTER map.c
#ifndef __FIFO_H
#define __FIFO_H

#include "error_code.h"
#include <attributes.h>
#include <stddef.h>
#include <stdint.h>

typedef char FIFO_e;

typedef struct {
    int32_t created;
    FIFO_e* buffer;
    size_t  head;
    size_t  tail;
    size_t  size;
} FIFO_t;

MUST_CHECK_RETURN(ErrorCode_t) fifo_init(FIFO_t* fifo, size_t size);
void fifo_destroy(FIFO_t* fifo);

MUST_CHECK_RETURN(ErrorCode_t) fifo_enqueue(FIFO_t* fifo, FIFO_e data);
MUST_CHECK_RETURN(ErrorCode_t) fifo_dequeue(FIFO_t* fifo, FIFO_e* data);
MUST_CHECK_RETURN(ErrorCode_t)
fifo_removeLast_unsafe(FIFO_t* fifo, FIFO_e* data);


#endif
