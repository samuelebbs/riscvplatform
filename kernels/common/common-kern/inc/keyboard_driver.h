#ifndef __KEYBOARD_DRIVER_H
#define __KEYBOARD_DRIVER_H

#include <stdint.h>

int keyboardDriver_init(void);

void keyboardDriver_handler(uint32_t ascii);

int keyboardDriver_readchar(void);

#endif