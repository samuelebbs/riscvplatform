
#ifndef __DRIVERS_H
#define __DRIVERS_H

#include "timer.h"
#include <stdint.h>

int drivers_install(Tickback_t tickback);

#endif