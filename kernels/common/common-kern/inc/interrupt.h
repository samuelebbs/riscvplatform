//# PRINT AFTER riscv/dispatch_asm.S
#ifndef __INTERRUPT_H
#define __INTERRUPT_H

#ifndef __ASSEMBLER__
#include <stdint.h>
#include <ureg.h>

typedef void (*CallBack_t)(ureg_t* state);

void interrupt_install(CallBack_t machineInterruptCallback,
                       CallBack_t supervisorInterruptCallback);

void machine_interrupt_handler(ureg_t* ureg, uint32_t modeSwitch);
void supervisor_interrupt_handler(ureg_t* ureg, uint32_t modeSwitch);

extern void* machine_interrupt_wrapper;
extern void* supervisor_interrupt_wrapper;

// This provides default monitoring behavior for a supervisor program
void machine_interrupt_monitor(ureg_t* state);

#endif

#endif