#include <environment.h>

int getbytes(const char *filename, int offset, int size, char *buf) {
    (void)filename;
    (void)offset;
    (void)size;
    (void)buf;
    // Better not call this
    environment_exit(-22);
    return -1;
}