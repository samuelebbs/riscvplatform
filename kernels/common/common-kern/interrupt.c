//# PRINT AFTER common/inc/interrupt.h
#include "interrupt.h"
#include <asm.h>
#include <compiler.h>
#include <contracts.h>
#include <environment.h>
#include <stdlib.h>

static CallBack_t machineCallback;
static CallBack_t supervisorCallback;

void interrupt_install(CallBack_t machineInterruptCallback,
                       CallBack_t supervisorInterruptCallback) {
    REQUIRES(machineInterruptCallback != NULL);
    machineCallback    = machineInterruptCallback;
    supervisorCallback = supervisorInterruptCallback;

    set_mtvec(&machine_interrupt_wrapper);
    set_stvec(&supervisor_interrupt_wrapper);
    set_mscratch(0);
    set_sscratch(0);
}

//
//
//
// WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING
//
//
// Carefully read both interrupt.c and interrupt_asm.S before making changes.
// There are complex invariants being passed between the two files to ensure
// that the state is properly restored after an interrupt.
//
void machine_interrupt_handler(ureg_t* ureg, uint32_t modeSwitch) {
    // The stack should be aligned to at least an 8-byte boundary to ensure GCC
    // doesn't generate bad code.
    ASSERT((ureg->sp & 0x1) == 0);  // 2 byte-aligned
    ASSERT((ureg->sp & 0x3) == 0);  // 4-byte aligned
    ASSERT((ureg->sp & 0x7) == 0);  // 8-byte aligned
    // Sanity checks
    ASSERT(ureg->zero == 0);
    ASSERT(ureg->ra != 0);  // Probably a bad idea
    ASSERT(!machine_interrupts_enabled());

    STATIC_ASSERT(sizeof(ureg_t) == UREG_SIZE);
    // Force the struct to be 16-byte aligned
    STATIC_ASSERT((sizeof(ureg_t) & 0xf) == 0);

    uint32_t scratch = get_mscratch();
    if (modeSwitch) {
        // We are coming from a user interrupt, so the sp value in the ureg
        // struct is incorrect.
        ureg->sp = scratch;
        // Since now we are in kernel mode we should clear the mscratch register
        set_mscratch(0);
    } else {
        // If we didn't mode switch, we must be in kernel mode
        ASSERT(scratch == 0);
    }

    // TODO: Consider removing this as an optimization since it is unnecessary
    // debug code
    mstatus_t status = get_mstatus();
    STATIC_ASSERT(sizeof(status) == sizeof(uint32_t));

    ASSERT(ureg->value == get_mtval());
    ASSERT(ureg->epc == get_mepc());
    ASSERT(ureg->cause == get_mcause());
    ASSERT(CSR_TO_UINT(ureg->status) == CSR_TO_UINT(status));
    (machineCallback)(ureg);

    ASSERT(!machine_interrupts_enabled());

    // Restore the saved epc value
    set_mepc(ureg->epc);

    // Currently, we are saving/restoring it because we want to be sure that the
    // MPP value is correct, but this also restores the MIE flag correctly as
    // well based on what happened prior to this interrupt which seems like a
    // good thing.
    set_mstatus(ureg->status);
}

void supervisor_interrupt_handler(ureg_t* ureg, uint32_t modeSwitch) {
    // The stack should be aligned to at least an 8-byte boundary to ensure GCC
    // doesn't generate bad code.
    ASSERT((ureg->sp & 0x1) == 0);  // 2 byte-aligned
    ASSERT((ureg->sp & 0x3) == 0);  // 4-byte aligned
    ASSERT((ureg->sp & 0x7) == 0);  // 8-byte aligned
    // Sanity checks
    ASSERT(ureg->zero == 0);
    ASSERT(ureg->ra != 0);  // Probably a bad idea
    ASSERT(!supervisor_interrupts_enabled());

    STATIC_ASSERT(sizeof(ureg_t) == UREG_SIZE);
    // Force the struct to be 16-byte aligned
    STATIC_ASSERT((sizeof(ureg_t) & 0xf) == 0);

    uint32_t scratch = get_sscratch();
    if (modeSwitch) {
        // We are coming from a user interrupt, so the sp value in the ureg
        // struct is incorrect.
        ureg->sp = scratch;
        // Since now we are in kernel mode we should clear the mscratch register
        set_sscratch(0);
    } else {
        // If we didn't mode switch, we must be in kernel mode
        ASSERT(scratch == 0);
    }

    // TODO: Consider removing this as an optimization since it is unnecessary
    // debug code
    mstatus_t status = get_sstatus();
    STATIC_ASSERT(sizeof(status) == sizeof(uint32_t));

    ASSERT(ureg->value == get_stval());
    ASSERT(ureg->epc == get_sepc());
    ASSERT(ureg->cause == get_scause());
    ASSERT(CSR_TO_UINT(ureg->status) == CSR_TO_UINT(status));
    (supervisorCallback)(ureg);

    ASSERT(!supervisor_interrupts_enabled());

    // Restore the saved epc value
    set_sepc(ureg->epc);

    // Currently, we are saving/restoring it because we want to be sure that the
    // MPP value is correct, but this also restores the MIE flag correctly as
    // well based on what happened prior to this interrupt which seems like a
    // good thing.
    set_sstatus(ureg->status);
}

void machine_interrupt_monitor(ureg_t* state) {
    switch (state->cause) {
        case SUPERVISOR_ECALL:
            if (state->a0 == 0xd) {
                ecall(0xd);  // Debug pass through
                state->epc += 4;
            } else if (state->a0 == 0xa) {
                // This is probably an exit
                hardware_halt(state->t6);  // x31
            } else {
                panic("Unknown ECALL: %x", state->a0);
            }
            break;
        default:
            panic("Unknown machine fault: %x", state->cause);
    }
}