/** @file 410user/progs/ck1.c
 *  @author ?
 *  @author elly1
 *  @brief Runs the checkpoint 1 script.
 *  @public yes
 *  @for p3
 *  @covers gettid
 *  @status done
 */

#include <environment.h>
#include <syscall.h> /* gettid() */

/* Main */
int main() {
    int tid;

    tid = gettid();

    lprintf("my tid is: %d", tid);

    while (1) continue;
}
