#include <stdint.h>
#include <stdio.h>
#include <syscall.h> /* for PAGE_SIZE */
#include <thread.h>

static void *NORETURN waiter(void *p) {
    int status;

    thr_join((int)p, (void **)&status);
    printf("Thread %d exited '%c'\n", (int)p, (char)status);
    if ((char)status == '!') {
        set_status(100);
    } else {
        set_status(-15);
    }

    thr_exit((void *)0);

    while (1) continue; /* placate compiler portably */
}

int main(void) {
    thr_init(16 * PAGE_SIZE);

    (void)thr_create(waiter, (void *)(uintptr_t)thr_getid());

    sleep(10); /* optional, of course!! */

    thr_exit((void *)'!');

    while (1) continue; /* placate compiler portably */
}
