/** @file 410user/progs/fork_bomb.c
 *  @author zra
 *  @brief Tests fork() in low-memory conditions.
 *  @public yes
 *  @for p3
 *  @covers fork oom
 *  @status done
 */

/* Includes */
#include "410_tests.h"
#include <report.h>
#include <stdlib.h>
#include <syscall.h> /* for fork */

DEF_TEST_NAME("fork_bomb:");

/* Main */
int main() {
    report_start(START_4EVER);

    while (1) {
        fork();
    }
}
