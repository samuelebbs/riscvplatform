410U_TEST_OBJS := report.o testasm.o
410U_TEST_OBJS := $(410U_TEST_OBJS:%=$(410UDIR)/libtest/%)
ALL_410UOBJS += $(410U_TEST_OBJS)
410UCLEANS += $(410UDIR)/libtest.a

$(410UDIR)/libtest.a: $(410U_TEST_OBJS)
