#include <string.h>

void bzero(void *__to, unsigned int __n) {
    memset(__to, 0, __n);
}