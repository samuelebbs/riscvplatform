#include "environment.h"
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>

void environment_vprintf(const char* fmt, va_list vl) {
    char str[DEBUG_STRING_LIMIT];
    vsnprintf(str, sizeof(str) - 1, fmt, vl);
    str[DEBUG_STRING_LIMIT - 1] = 0;
    debug_print(strlen(str), str);
}

void environment_printf(const char* fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    environment_vprintf(fmt, ap);
    va_end(ap);
}
