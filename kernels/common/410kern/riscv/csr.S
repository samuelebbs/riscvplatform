#include <riscv_csr.h>

#define CAT(x,y) x ## y

#define INSTALL_CSR_ACCESSOR(function, csr) \
.global function ;                            \
function:                                   \
  CSRR a1, CAT(csr, h) ;                         \
  CSRR a0, csr ;                        \
  CSRR t0, CAT(csr, h) ;                         \
  BNE a1, t0, function ;                      \
  RET

#define INSTALL_CSR32_ACCESSOR(function, csr)\
.global function;\
function:\
  CSRR a0, csr;\
  mv a1, zero;\
  RET

INSTALL_CSR_ACCESSOR(csr_getCycles, CYCLE_COUNT);
INSTALL_CSR_ACCESSOR(csr_getInstructionsRetired, INSTRUCTIONS_RETIRED);
INSTALL_CSR_ACCESSOR(csr_getInstructionsFetched, INSTRUCTIONS_FETCHED);

INSTALL_CSR_ACCESSOR(csr_getInstructionTLBHit, INSTRUCTION_TLB_HIT);
INSTALL_CSR_ACCESSOR(csr_getInstructionTLBMiss, INSTRUCTION_TLB_MISS);
INSTALL_CSR_ACCESSOR(csr_getICacheHit, INSTRUCTION_CACHE_HIT);
INSTALL_CSR_ACCESSOR(csr_getICacheMiss, INSTRUCTION_CACHE_MISS);

INSTALL_CSR_ACCESSOR(csr_getDataTLBHit, DATA_TLB_HIT);
INSTALL_CSR_ACCESSOR(csr_getDataTLBMiss, DATA_TLB_MISS);
INSTALL_CSR_ACCESSOR(csr_getDCacheHit, DATA_CACHE_HIT);
INSTALL_CSR_ACCESSOR(csr_getDCacheMiss, DATA_CACHE_MISS);
INSTALL_CSR_ACCESSOR(csr_getDVRAMHit, DATA_VRAM_HIT);
INSTALL_CSR_ACCESSOR(csr_getDRemoteHit, DATA_REMOTE_HIT);

INSTALL_CSR_ACCESSOR(csr_getStoreConditionalSuccess, STORE_CONDITIONAL_SUCCESS);
INSTALL_CSR_ACCESSOR(csr_getStoreConditionalReject, STORE_CONDITIONAL_REJECT);

INSTALL_CSR_ACCESSOR(csr_getBranchRetired, BRANCH_RETIRED);
INSTALL_CSR_ACCESSOR(csr_getForwardBranchPredictedCorrect,
    FORWARD_BRANCH_PREDICTED_CORRECT);
INSTALL_CSR_ACCESSOR(csr_getForwardBranchPredictedIncorrect,
    BACKWARD_BRANCH_PREDICTED_CORRECT);
INSTALL_CSR_ACCESSOR(csr_getBackwardBranchPredictedCorrect,
    FORWARD_BRANCH_PREDICTED_INCORRECT);
INSTALL_CSR_ACCESSOR(csr_getBackwardBranchPredictedIncorrect,
    BACKWARD_BRANCH_PREDICTED_INCORRECT);

INSTALL_CSR_ACCESSOR(csr_getJumpRetired, JUMP_RETIRED);
INSTALL_CSR_ACCESSOR(csr_getJumpPredictedCorrect, JUMP_PREDICTED_CORRECT);
INSTALL_CSR_ACCESSOR(csr_getJumpPredictedIncorrect, JUMP_PREDICTED_INCORRECT);
INSTALL_CSR_ACCESSOR(csr_getReturnPredictedCorrect, RETURN_PREDICTED_CORRECT);
INSTALL_CSR_ACCESSOR(csr_getReturnPredictedIncorrect,
  RETURN_PREDICTED_INCORRECT);

INSTALL_CSR_ACCESSOR(csr_getBTBHit, BTB_HIT);

INSTALL_CSR32_ACCESSOR(csr_getKeyboardInterruptUser,
  KEYBOARD_INTERRUPT_USER_32);
INSTALL_CSR32_ACCESSOR(csr_getKeyboardInterruptSupervisor,
  KEYBOARD_INTERRUPT_SUPERVISOR_32);
INSTALL_CSR_ACCESSOR(csr_getExceptionUser, EXCEPTION_USER);
INSTALL_CSR_ACCESSOR(csr_getExceptionSupervisor, EXCEPTION_SUPERVISOR);
INSTALL_CSR_ACCESSOR(csr_getTimerInterruptUser, TIMER_INTERRUPT_USER);
INSTALL_CSR_ACCESSOR(csr_getTimerInterruptSupervisor,
    TIMER_INTERRUPT_SUPERVISOR);






































