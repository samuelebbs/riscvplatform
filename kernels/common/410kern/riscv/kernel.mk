410K_X86_OBJS := \
					asm.o \


410K_X86_OBJS := $(410K_X86_OBJS:%=$(410KDIR)/riscv/%)

ALL_410KOBJS += $(410K_X86_OBJS)
410KCLEANS += $(410KDIR)/libriscv.a

$(410KDIR)/libriscv.a: $(410K_X86_OBJS)
