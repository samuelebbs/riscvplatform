#ifndef __CSR_H
#define __CSR_H

#include <stdint.h>

uint64_t csr_getCycles(void);
uint64_t csr_getInstructionsRetired(void);
uint64_t csr_getInstructionsFetched(void);

uint64_t csr_getInstructionTLBHit(void);
uint64_t csr_getInstructionTLBMiss(void);
uint64_t csr_getICacheHit(void);
uint64_t csr_getICacheMiss(void);

uint64_t csr_getDataTLBHit(void);
uint64_t csr_getDataTLBMiss(void);
uint64_t csr_getDCacheHit(void);
uint64_t csr_getDCacheMiss(void);
uint64_t csr_getDVRAMHit(void);
uint64_t csr_getDRemoteHit(void);

uint64_t csr_getStoreConditionalSuccess(void);
uint64_t csr_getStoreConditionalReject(void);

uint64_t csr_getBranchRetired(void);
uint64_t csr_getForwardBranchPredictedCorrect(void);
uint64_t csr_getForwardBranchPredictedIncorrect(void);
uint64_t csr_getBackwardBranchPredictedCorrect(void);
uint64_t csr_getBackwardBranchPredictedIncorrect(void);

uint64_t csr_getJumpRetired(void);
uint64_t csr_getJumpPredictedCorrect(void);
uint64_t csr_getJumpPredictedIncorrect(void);
uint64_t csr_getReturnPredictedCorrect(void);
uint64_t csr_getReturnPredictedIncorrect(void);

uint64_t csr_getBTBHit(void);

uint64_t csr_getKeyboardInterruptUser(void);
uint64_t csr_getKeyboardInterruptSupervisor(void);
uint64_t csr_getExceptionUser(void);
uint64_t csr_getExceptionSupervisor(void);
uint64_t csr_getTimerInterruptUser(void);
uint64_t csr_getTimerInterruptSupervisor(void);

#endif