#include <asm.h>
#include <contracts.h>
#include <riscv_csr.h>
#include <riscv_status.h>

void set_mpp(uint32_t mode) {
    REQUIRES(mode <= 3 && mode != 0x2);
    mstatus_t mstatus = get_mstatus();
    mstatus.mpp       = mode & 0x3;
    set_mstatus(mstatus);
}

void set_spp(uint32_t mode) {
    REQUIRES(mode <= 1);  // Only can be supervisor or user
    mstatus_t sstatus = get_sstatus();
    sstatus.spp       = mode & 0x1;  // Upper bit will always be 0
    set_sstatus(sstatus);
}

bool machine_interrupts_enabled(void) {
    mstatus_t mstatus = get_mstatus();

    return !!mstatus.mie;
}

bool supervisor_interrupts_enabled(void) {
    mstatus_t sstatus = get_sstatus();
    return !!sstatus.sie;
}
