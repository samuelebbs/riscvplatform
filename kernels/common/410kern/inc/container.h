#ifndef __CONTAINER_H
#define __CONTAINER_H

#include <stddef.h>
// This is defined in the <stddef.h>, but is reproduced here for reference
// #define offsetof(TYPE, MEMBER) ((size_t) & ((TYPE *)0)->MEMBER)

// This is nasty code, GCC doesn't like it much
// #pragma GCC diagnostic ignored "-Wcast-qual"
// #pragma GCC diagnostic ignored "-Wpedantic"

#define container_of(ptr, type, member)                                        \
    ((type *)((char *)(ptr)-offsetof(type, member)))

// #define container_of(ptr, type, member)
//     ({
//         const typeof(((type *)0)->member) *__mptr = (ptr);
//         (type *)((char *)(__mptr) - offsetof(type, member));
//     })

#endif