#ifndef __ENVIRONMENT_H
#define __ENVIRONMENT_H

#include <stdarg.h>

#define DEBUG_BASE_ADDRESS 0x2000
#define DEBUG_STRING_LIMIT 256

#define DEBUG_FAST_BASE 0x2200
#define REMOTE_DEBUG_FAST_MAGIC 0xDEB6FA57
#define DEBUG_FAST_ENABLED (DEBUG_FAST_BASE)
#define DEBUG_FAST_ARG1 (DEBUG_FAST_BASE + 4)
#define DEBUG_FAST_ARG2 (DEBUG_FAST_BASE + 8)

#define ECALL_ARG_DEBUG 0xd

typedef enum { MACHINE_ENV, SUPERVISOR_ENV } EnvironmentMode_t;

void environment_init(EnvironmentMode_t mode);

void environment_exit(int exitStatus);

void environment_vprintf(const char *fmt, va_list ap);
void environment_printf(const char *fmt, ...)
    __attribute__((__format__(__printf__, 1, 2)));
void environment_printf_unsafe(const char *fmt, ...)
    __attribute__((__format__(__printf__, 1, 2)));

#define lprintf(...) environment_printf(__VA_ARGS__)
#define lprintf_unsafe(...) environment_printf_unsafe(__VA_ARGS__)

void environment_fast(int arg1, int arg2);

#endif
