#include "environment_lock.h"
#include <asm.h>

void environmentLock_init(EnvironmentLock_t* lock, bool supervisorMode) {
    lock->supervisorMode    = supervisorMode;
    lock->interruptsEnabled = false;
}

void environmentLock_lock(EnvironmentLock_t* lock) {
    if (lock->supervisorMode) {
        bool interruptsEnabled = supervisor_interrupts_enabled();
        disable_supervisor_interrupts();
        lock->interruptsEnabled = interruptsEnabled;
    } else {
        bool interruptsEnabled = machine_interrupts_enabled();
        disable_machine_interrupts();
        lock->interruptsEnabled = interruptsEnabled;
    }
}

void environmentLock_unlock(EnvironmentLock_t* lock) {
    if (!lock->interruptsEnabled) {
        return;
    }

    if (lock->supervisorMode) {
        enable_supervisor_interrupts();
    } else {
        enable_machine_interrupts();
    }
}