#include "environment.h"
#include "environment_lock.h"
#include <asm.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio/stdio.h>
#include <stdlib.h>
#include <string.h>

static EnvironmentLock_t lock;

void environment_init(EnvironmentMode_t mode) {
    environmentLock_init(&lock, mode == SUPERVISOR_ENV);
}

void environment_exit(int exitStatus) {
    environmentLock_lock(&lock);
    environment_printf("Exiting with status: %d", exitStatus);
    hardware_halt(exitStatus);
    panic("Halt didn't?");
}

void environment_vprintf(const char* fmt, va_list vl) {
    char str[DEBUG_STRING_LIMIT];
    vsnprintf(str, sizeof(str) - 1, fmt, vl);
    str[DEBUG_STRING_LIMIT - 1] = 0;

    environmentLock_lock(&lock);
    memcpy((char*)DEBUG_BASE_ADDRESS, str, strlen(str) + 1);
    // This will trap into machine mode if executing in supervisor mode
    ecall(ECALL_ARG_DEBUG);
    environmentLock_unlock(&lock);
}

// FIXME: This is a gross hack necessary to allow fondle to lprintf in user mode
// using only 410kern code
static void environment_vprintf_unsafe(const char* fmt, va_list vl) {
    char str[DEBUG_STRING_LIMIT];
    vsnprintf(str, sizeof(str) - 1, fmt, vl);
    str[DEBUG_STRING_LIMIT - 1] = 0;
    memcpy((char*)DEBUG_BASE_ADDRESS, str, strlen(str) + 1);
    ecall(ECALL_ARG_DEBUG);
}

void environment_printf_unsafe(const char* fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    environment_vprintf_unsafe(fmt, ap);
    va_end(ap);
}

void environment_printf(const char* fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    environment_vprintf(fmt, ap);
    va_end(ap);
}

void environment_fast(int arg1, int arg2) {
    environmentLock_lock(&lock);
    *(int*)(DEBUG_FAST_ENABLED) = REMOTE_DEBUG_FAST_MAGIC;
    *(int*)(DEBUG_FAST_ARG1)    = arg1;
    *(int*)(DEBUG_FAST_ARG2)    = arg2;

    ecall(ECALL_ARG_DEBUG);
    *(int*)(DEBUG_FAST_ENABLED) = 0;
    environmentLock_unlock(&lock);
}
