SELF_DIR = $(dir $(TEST))

410KDIR = $(SELF_DIR)410kern
410SDIR = $(SELF_DIR)spec
STUKDIR = $(SELF_DIR)kern
410UDIR = $(SELF_DIR)410user
STUUDIR = $(SELF_DIR)user

UPROGDIR = progs
UFILEDIR = files

BUILDDIR = $(SELF_DIR)temp

PROGS = $(410_PROGS) $(STUDENT_PROGS)

FILES = $(410FILES) $(STUDENTFILES)

KERNEL_INCLUDE_PATHS = $(STUKDIR) $(STUKDIR)/inc $(STUKDIR)/common/inc $(410INCLUDE_PATHS)

USER_SOURCES = $(PROGS) $(FILES)

COMMON_FILES = $(COMMON_SOURCES) $(COMMON_HEADERS)

KERNEL_SOURCES = $(410SOURCES) $(addprefix $(STUKDIR)/,$(STUDENT_SOURCES)) $(addprefix $(STUKDIR)/common/,$(COMMON_SOURCES)) $(if $(strip $(USER_SOURCES)),$(BUILDDIR)/user_apps.o)

410KERNEL_LIBS = \
				 libelf.a \
				 liblmm.a \
				 libmalloc.a \
				 libmisc.a \
				 libRNG.a \
				 libstdio.a \
				 libstdlib.a \
				 libstring.a \
				 libriscv.a \

410RISCV_SOURCES = asm.S csr.S riscv_status.c
410STDIO_SOURCES = puts.c putchar.c printf.c doprnt.c sprintf.c
410STDLIB_SOURCES = panic.c
410STRING_SOURCES = memcpy.c memset.c strlen.c memcmp.c strcmp.c strncmp.c strcpy.c
410MALLOC_SOURCES = malloc_lmm.c calloc.c malloc.c free.c memalign.c realloc.c sfree.c smalloc.c smemalign.c
410LMM_SOURCES = lmm_init.c lmm_add_region.c lmm_add_free.c lmm_free.c lmm_dump.c lmm_alloc.c lmm_alloc_aligned.c lmm_alloc_gen.c
410MISC_SOURCES = environment.c environment_lock.c launch.S
# This breaks all kinds of things! Don't include in common_kern
# RNG_SOURCES = mt19937int.c
# MALLOC_SOURCES =
# LMM_SOURCES =
# RNG_SOURCES =

410SOURCES = $(410KDIR)/entry.c $(addprefix $(410KDIR)/riscv/,$(410RISCV_SOURCES)) $(addprefix $(410KDIR)/stdio/,$(410STDIO_SOURCES)) $(addprefix $(410KDIR)/stdlib/,$(410STDLIB_SOURCES)) $(addprefix $(410KDIR)/string/,$(410STRING_SOURCES)) $(addprefix $(410KDIR)/malloc/,$(410MALLOC_SOURCES)) $(addprefix $(410KDIR)/lmm/,$(410LMM_SOURCES)) $(addprefix $(410KDIR)/misc/,$(410MISC_SOURCES)) $(addprefix $(410KDIR)/,$(410KERN_SOURCES))

410INCLUDE_PATHS = $(410KDIR) $(410KDIR)/inc $(410SDIR) $(patsubst lib%.a,$(410KDIR)/%,$(410KERNEL_LIBS))


####################### Make support for building user programs

AUTOSTACK_OBJS = autostack.o

THREAD_OBJS = malloc.o panic.o\
atomic.o list.o map.o ureg_dump.o reference_count.o\
thread.o thread_map.o thread_asm.o\
mutex.o cond.o sem.o rwlock.o token_lock.o\

SYSCALL_OBJS = \
set_status.o \
vanish.o \
print.o \
misbehave.o \
fork.o \
exec.o \
wait.o \
yield.o \
deschedule.o \
make_runnable.o \
gettid.o \
sleep.o \
swexn.o \
getchar.o \
readline.o \
set_term_color.o \
get_cursor_pos.o \
set_cursor_pos.o \
halt.o \
readfile.o \
task_vanish.o \
new_pages.o \
remove_pages.o \
get_ticks.o \
debug_print.o \



$(BUILDDIR)/exec2obj:
	mkdir -p $(BUILDDIR)
	$(HOST_CC) -I $(SELF_DIR) $(COMMON_CFLAGS) -o $@ $(410UDIR)/exec2obj.c

$(BUILDDIR)/__DIR_LISTING__: $(PROGS:%=$(BUILDDIR)/%.strip) $(FILES:%=$(BUILDDIR)/%)
	mkdir -p $(BUILDDIR)
	(printf "%s\0" $(sort $(notdir $(PROGS) $(FILES))); printf "\0") > $(BUILDDIR)/__DIR_LISTING__

$(PROGS:%=$(BUILDDIR)/%) :
$(BUILDDIR)/%.strip : $(BUILDDIR)/%
	$(RISCV_STRIP) -o $(BUILDDIR)/$(notdir $@) $(BUILDDIR)/$(notdir $<)
	$(RISCV_OBJDUMP) $(RISCV_OBJDUMP_FLAGS) $^ > $^.$(DISAS_EXTENSION)

$(BUILDDIR)/user_apps.o: $(BUILDDIR)/exec2obj $(BUILDDIR)/__DIR_LISTING__ $(PROGS:%=$(BUILDDIR)/%)
	@printf "User Sources >$(USER_SOURCES)<\n"
	(cd $(BUILDDIR) && ./exec2obj __DIR_LISTING__ $(PROGS) $(FILES))
	$(RISCV_CC) $(RISCV_CFLAGS) -I $(410KDIR)/inc -o $@ -c $(BUILDDIR)/user_apps.c

$(410_PROGS:%=$(BUILDDIR)/%) : \
$(BUILDDIR)/% : $(410UDIR)/$(UPROGDIR)/%
	mkdir -p $(BUILDDIR)
	cp $<.bin $@

$(STUDENT_PROGS:%=$(BUILDDIR)/%) : \
$(BUILDDIR)/% : $(STUUDIR)/$(UPROGDIR)/%
	mkdir -p $(BUILDDIR)
	cp $<.bin $(BUILDDIR)/$(notdir $@)

STUUPROGS = $(patsubst %,$(STUUDIR)/$(UPROGDIR)/%,$(STUDENT_PROGS))
410UPROGS = $(patsubst %,$(410UDIR)/$(UPROGDIR)/%,$(410_PROGS))

CRT0 = $(410UDIR)/crt0.o

410USER_LIBS_EARLY = libthrgrp.a
410USER_LIBS_LATE = libmisc.a libstdio.a libstdlib.a libstring.a libmalloc.a libtest.a libriscv.a
STUDENT_LIBS_EARLY = libautostack.a libthread.a
STUDENT_LIBS_LATE = libsyscall.a

# 410USER_LIBS_EARLY =
# 410USER_LIBS_LATE = libRNG.a libx86.a libsimics.a   \
#
# STUDENT_LIBS_EARLY =

410ULIBS_EARLY = $(patsubst %,$(410UDIR)/%,$(410USER_LIBS_EARLY))
410ULIBS_LATE = $(patsubst %,$(410UDIR)/%,$(410USER_LIBS_LATE))
STUULIBS_EARLY = $(patsubst %,$(STUUDIR)/%,$(STUDENT_LIBS_EARLY))
STUULIBS_LATE = $(patsubst %,$(STUUDIR)/%,$(STUDENT_LIBS_LATE))

# Suffix for dependency files
DEP_SUFFIX = dep

ULIBS=$(410ULIBS_EARLY) $(STUULIBS_EARLY) $(410ULIBS_LATE) $(STUULIBS_LATE)

ULDFLAGS = -static -T$(RISCV_USER_LINKER_SCRIPT) --fatal-warnings --entry=_main -melf32lriscv

UINCLUDES = -I$(410SDIR) -I$(410UDIR) -I$(410UDIR)/inc \
						-I$(STUUDIR)/inc \
			$(patsubst %.a,-I$(410UDIR)/%,$(410USER_LIBS_EARLY) $(410USER_LIBS_LATE)) \
			$(patsubst %.a,-I$(STUUDIR)/%,$(STUDENT_LIBS_EARLY) $(STUDENT_LIBS_LATE))

COMMA := ,
ULIBGROUP = -Wl,--start-group,$(subst $() $(),$(COMMA),$(ULIBS)),--end-group

LGCC = $(shell $(RISCV_CC) $(RISCV_ARCH) -print-libgcc-file-name)

$(STUUPROGS): %: %.o $(CRT0) $(ULIBS)
	$(RISCV_LD) $(ULDFLAGS) -o $@.bin $< $(CRT0) --start-group $(ULIBS) $(LGCC)  --end-group

$(410UPROGS): %: %.o $(CRT0) $(ULIBS)
	$(RISCV_LD) $(ULDFLAGS)  -o $@.bin $< $(CRT0) --start-group $(ULIBS) $(LGCC) --end-group

################### GENERIC RULES ######################
%.o: %.S
	$(RISCV_CC) $(RISCV_CFLAGS) $(HARDWARE_CFLAGS) -DASSEMBLER $(UINCLUDES) -c -MD -MP -MF $(@:.o=.$(DEP_SUFFIX)) -MT $@ -o $@ $<
	$(RISCV_OBJCOPY) -R .comment -R .note $@ $@

%.o: %.s
	@echo "You should use the .S file extension rather than .s"
	@echo ".s does not support precompiler directives (like #include)"
	@false

%.o: %.c
	@expand -t 8 $< | awk 'length > 81 \
	  { err++; \
	    errs[err] = "Warning: line " NR " in $< is longer than 80 characters"; \
	  } END { \
	    if (err > 2 && err < 1000) for (i = 1; i <= err; i++) print errs[i];\
	    exit(err > 10 && err < 1000) }'
	$(RISCV_CC) $(RISCV_CFLAGS) $(HARDWARE_CFLAGS) $(UINCLUDES) -lgcc -c -MD -MP -MF $(@:.o=.$(DEP_SUFFIX)) -MT $@ -o $@ $<
	$(RISCV_OBJCOPY) -R .comment -R .note $@ $@

%.a:
	rm -f $@
	$(RISCV_AR) rc $@ $^

# # 410user/files/* and user/files/* are dependencies, but
# # they are not targets to be built.  In particular, if
# # both 410user/files/x and 410user/files/x.S exist, don't
# # try to build one from the other.
.PHONY: $(410FILES:%=$(410UDIR)/$(UFILEDIR)/%)
.PHONY: $(STUDENTFILES:%=$(STUUDIR)/$(UFILEDIR)/%)

$(410FILES:%=$(BUILDDIR)/%) : \
$(BUILDDIR)/% : $(410UDIR)/$(UFILEDIR)/%
	mkdir -p $(BUILDDIR)
	cp $< $@

$(STUDENTFILES:%=$(BUILDDIR)/%) : \
$(BUILDDIR)/% : $(STUUDIR)/$(UFILEDIR)/%
	mkdir -p $(BUILDDIR)
	cp $< $@

include $(patsubst %.a,$(410UDIR)/%/user.mk,$(410USER_LIBS_EARLY) $(410USER_LIBS_LATE))

################# STUDENT LIBRARY RULES #################

STUU_THREAD_OBJS := $(THREAD_OBJS:%=$(STUUDIR)/libthread/%)
ALL_STUUOBJS += $(STUU_THREAD_OBJS)
STUUCLEANS += $(STUUDIR)/libthread.a
$(STUUDIR)/libthread.a: $(STUU_THREAD_OBJS)

STUU_SYSCALL_OBJS := $(SYSCALL_OBJS:%=$(STUUDIR)/libsyscall/%)
ALL_STUUOBJS += $(STUU_SYSCALL_OBJS)
STUUCLEANS += $(STUUDIR)/libsyscall.a
$(STUUDIR)/libsyscall.a: $(STUU_SYSCALL_OBJS)

STUU_AUTOSTACK_OBJS := $(AUTOSTACK_OBJS:%=$(STUUDIR)/libautostack/%)
ALL_STUUOBJS += $(STUU_AUTOSTACK_OBJS)
STUUCLEANS += $(STUUDIR)/libautostack.a
$(STUUDIR)/libautostack.a: $(STUU_AUTOSTACK_OBJS)