//# PRINT AFTER riscv_cause.h
#ifndef __RISCV_VIRTUAL_MEMORY_H
#define __RISCV_VIRTUAL_MEMORY_H

#include <attributes.h>

typedef struct {
    unsigned valid : 1;
    unsigned readable : 1;
    unsigned writable : 1;
    unsigned executable : 1;
    unsigned userAccessable : 1;
    unsigned global : 1;
    unsigned accessed : 1;
    unsigned dirty : 1;
    unsigned rsw : 2;
    unsigned ppn : 22;
} PACKED PageTableEntry_t;

typedef union {
    void* address;
    struct PACKED {
        unsigned pageOffset : 12;
        unsigned vpn0 : 10;
        unsigned vpn1 : 10;
    } translation;
} VirtualAddress_t;

#endif