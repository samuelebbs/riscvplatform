//# PRINT AFTER syscall_int.h
#ifndef __UREG_H
#define __UREG_H

#define UREG_SIZE (36 * 4)

#define SP_INDEX 2
#define CAUSE_INDEX 32
#define VALUE_INDEX 33
#define EPC_INDEX 34
#define STATUS_INDEX 35

// clang-format off
#define SAVE(n) SW x ## n, (n*4)(sp)
#define RESTORE(n) LW x ## n, (n*4)(sp)

// Note: These macros do not push and restore the cause and value fields

#define PUSH_UREG(size)\
    /* Save all the state*/\
    SAVE(0);\
    SAVE(1);\
    /* Skip saving the stack pointer */\
    SAVE(3);\
    SAVE(4);\
    SAVE(5);\
    SAVE(6);\
    SAVE(7);\
    SAVE(8);\
    SAVE(9);\
    SAVE(10);\
    SAVE(11);\
    SAVE(12);\
    SAVE(13);\
    SAVE(14);\
    SAVE(15);\
    SAVE(16);\
    SAVE(17);\
    SAVE(18);\
    SAVE(19);\
    SAVE(20);\
    SAVE(21);\
    SAVE(22);\
    SAVE(23);\
    SAVE(24);\
    SAVE(25);\
    SAVE(26);\
    SAVE(27);\
    SAVE(28);\
    SAVE(29);\
    SAVE(30);\
    SAVE(31);\
    /* Saved the patched stack pointer */ \
    ADDI t0, sp, size;\
    SW t0, (2*4)(sp)

#define POP_UREG()\
    /* Restore state */\
    RESTORE(0);\
    RESTORE(1);\
    /* Skip restoring the stack pointer */\
    RESTORE(3);\
    RESTORE(4);\
    RESTORE(5);\
    RESTORE(6);\
    RESTORE(7);\
    RESTORE(8);\
    RESTORE(9);\
    RESTORE(10);\
    RESTORE(11);\
    RESTORE(12);\
    RESTORE(13);\
    RESTORE(14);\
    RESTORE(15);\
    RESTORE(16);\
    RESTORE(17);\
    RESTORE(18);\
    RESTORE(19);\
    RESTORE(20);\
    RESTORE(21);\
    RESTORE(22);\
    RESTORE(23);\
    RESTORE(24);\
    RESTORE(25);\
    RESTORE(26);\
    RESTORE(27);\
    RESTORE(28);\
    RESTORE(29);\
    RESTORE(30);\
    RESTORE(31);\
    /* Now it is safe to restore sp */\
    RESTORE(2)


// clang-format on


#ifndef __ASSEMBLER__
#ifndef ASSEMBLER

#include <riscv_cause.h>
#include <stdint.h>
#include <riscv_csr.h>

typedef struct {
    uint32_t zero;
    uint32_t ra;
    uint32_t sp;
    uint32_t gp;
    uint32_t tp;
    uint32_t t0;
    uint32_t t1;
    uint32_t t2;
    uint32_t s0;
    uint32_t s1;
    uint32_t a0;
    uint32_t a1;
    uint32_t a2;
    uint32_t a3;
    uint32_t a4;
    uint32_t a5;
    uint32_t a6;
    uint32_t a7;
    uint32_t s2;
    uint32_t s3;
    uint32_t s4;
    uint32_t s5;
    uint32_t s6;
    uint32_t s7;
    uint32_t s8;
    uint32_t s9;
    uint32_t s10;
    uint32_t s11;
    uint32_t t3;
    uint32_t t4;
    uint32_t t5;
    uint32_t t6;

    uint32_t  cause;
    uint32_t  value;
    uint32_t  epc;
    mstatus_t status;
} ureg_t;

#endif
#endif

#endif