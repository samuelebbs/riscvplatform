//# PRINT AFTER riscv_register_names.h
#ifndef __RISCV_CSR_H
#define __RISCV_CSR_H

#define SIE_MASK (1 << 1)
#define MIE_MASK (1 << 3)
#define SUM_MASK (1 << 18)
#define MPP_MASK (0x3 << 11)

// This must be kept in sync with rtl/include/riscv/riscv_csr.vh
#define CYCLE_COUNT cycle
#define INSTRUCTIONS_RETIRED instret
#define INSTRUCTIONS_FETCHED hpmcounter3

#define INSTRUCTION_TLB_HIT hpmcounter29
#define INSTRUCTION_TLB_MISS hpmcounter30
#define INSTRUCTION_CACHE_HIT hpmcounter10
#define INSTRUCTION_CACHE_MISS hpmcounter11

#define DATA_TLB_HIT hpmcounter5
#define DATA_TLB_MISS hpmcounter24
#define DATA_CACHE_HIT hpmcounter12
#define DATA_CACHE_MISS hpmcounter13
#define DATA_VRAM_HIT hpmcounter14
#define DATA_REMOTE_HIT hpmcounter31

#define STORE_CONDITIONAL_SUCCESS hpmcounter15
#define STORE_CONDITIONAL_REJECT hpmcounter16

#define BRANCH_RETIRED hpmcounter4
#define FORWARD_BRANCH_PREDICTED_CORRECT hpmcounter6
#define BACKWARD_BRANCH_PREDICTED_CORRECT hpmcounter7
#define FORWARD_BRANCH_PREDICTED_INCORRECT hpmcounter8
#define BACKWARD_BRANCH_PREDICTED_INCORRECT hpmcounter9

#define JUMP_RETIRED hpmcounter23
#define JUMP_PREDICTED_CORRECT hpmcounter25
#define JUMP_PREDICTED_INCORRECT hpmcounter27
#define RETURN_PREDICTED_CORRECT hpmcounter26
#define RETURN_PREDICTED_INCORRECT hpmcounter28

#define BTB_HIT hpmcounter18

#define KEYBOARD_INTERRUPT_USER_32 hpmcounter17
#define KEYBOARD_INTERRUPT_SUPERVISOR_32 hpmcounter17h
#define EXCEPTION_USER hpmcounter19
#define EXCEPTION_SUPERVISOR hpmcounter20
#define TIMER_INTERRUPT_USER hpmcounter21
#define TIMER_INTERRUPT_SUPERVISOR hpmcounter22

#ifndef __ASSEMBLER__

#include <attributes.h>

typedef enum {
    USER_MODE       = 0,
    SUPERVISOR_MODE = 1,
    MACHINE_MODE    = 3,
} ProcessorMode_t;

typedef struct {
    unsigned uie : 1;  // User Interrupts Enabled
    unsigned sie : 1;  // Supervisor Interrupts Enabled
    unsigned zero1 : 1;
    // MIE: 1 => Enabled
    unsigned mie : 1;   // Machine Interrupts Enabled
    unsigned upie : 1;  // User Previous Interrupts Enabled
    unsigned spie : 1;  // Supervisor Previous Interrupts Enabled
    unsigned zero2 : 1;
    // MPIE: Holds MIE prior to interrupt
    unsigned mpie : 1;  // Machine Previous Interrupts Enabled
    unsigned spp : 1;   // Supervisor Previous Privilege
    unsigned zero3 : 2;
    // 3 if previously running in machine mode, 0 if previously running in
    // user mode
    unsigned mpp : 2;  // Machine Previous Privilege
    unsigned fs : 2;   // Floating Point Status
    unsigned xs : 2;   // Extension Status
    // Makes machine mode operations translate as if the privilege mode is
    // MPP
    unsigned mprv : 1;  // Modify Privilege
    // Allows the supervisor mode to access pages marked user accessible
    unsigned sum : 1;  // Supervisor User Memory access
    // Makes executable-only pages readable
    unsigned mxr : 1;  // Machine executable readable
    // Makes SFENCE.VMA and modifying satp cause illegal instruction
    // exception
    unsigned tvm : 1;  // Trap Virtual Memory

    // Makes WFI cause an illegal instruction exception
    unsigned tw : 1;  // Timeout Wait
    // Makes SRET cause an illegal instruction exception
    unsigned tsr : 1;  // Trap SRET
    unsigned zero4 : 8;
    // Indicates if FS/XS are non-zero
    unsigned sd : 1;  // Some Dirty
} PACKED mstatus_t;

typedef struct {
    unsigned ppn : 22;
    unsigned asid : 9;
    unsigned virtualMemoryEnabled : 1;
} PACKED satp_t;

#define CSR_TO_UINT(csr) (*(uint32_t*)&(csr))

#endif

#endif