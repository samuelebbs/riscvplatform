#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <contracts.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 100

static volatile int interrupts = 0;
static volatile int done       = 0;

extern int wait(volatile int* until);

static void interrupt_dispatch(ureg_t* state);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    printf("Running tick_tock_register_test\n");

    interrupt_install(&interrupt_dispatch, NULL);

    enable_machine_interrupts();

    int result = wait(&done);

    disable_machine_interrupts();

    if (75000 < result && result < 900000) {
        printf("Success: saw %d interrupts\n", interrupts);
    } else {
        printf("Failure: %d steps to see %d interrupts\n", result, interrupts);
    }

    environment_exit(200);

    panic("Exit failed?");
}

static int ticks = 0;

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;

    ASSERT(!machine_interrupts_enabled());
    if (interrupts % 5 == 0) {
        lprintf("Found an interrupt: %d %lx %lx",
                interrupts,
                state->cause,
                state->value);
    }

    if (state->cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            printf("Tick: %d [%ld (%lx)]\n", ticks, state->value, state->value);
        }
    } else {
        printf("Tock: %c\n", (char)state->value);
    }

    if (interrupts > INTERRUPT_LIMIT) {
        done = 1;
    }
    ASSERT(!machine_interrupts_enabled());
}