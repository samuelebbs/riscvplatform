//# PRINT AFTER memory/tlb_eviction_selector.sv
`ifndef __DRAM_FILL_VH
`define __DRAM_FILL_VH

`include "riscv_types.vh"
`include "cache.vh"

interface DRAMFillInterface();
    PhysicalAddress_t address;
    logic requestFill;

    logic requestComplete;

    // Passive
    CacheResponse_t cacheResponse;

    modport Requester(
        output address,
        output requestFill,

        input requestComplete,

        // Passive
        output cacheResponse
    );

    modport Filler(
        input address,
        input requestFill,

        output requestComplete,

        // Passive
        input cacheResponse
    );

endinterface

`endif