//# PRINT AFTER memory/memory.sv
`ifndef __MEMORY_ARBITER_VH
`define __MEMORY_ARBITER_VH

typedef enum {INST_DATA, TABLE_WALK, DRAM_FILL} MemorySelect_t;

typedef enum {MEMORY_UNLOCKED, TABLE_WALK_LOCK, DRAM_LOCK, MEMORY_WAITING} MemoryLock_t;

interface MemoryArbiterInterface();
    // FIXME: This is actually a pretty terrible interface since the user is
    // required to hold specific signals while holding the lock. It would be
    // better to have the interface have an explicit requestLock/dropLock with
    // the assumption that the lock is held after being granted until the lock
    // is explicitly dropped.
    logic requestLock;
    logic isIdle;
    MemoryLock_t handoff;

    MemoryLock_t lock;

    modport Requester(
        output requestLock,
        output isIdle,
        output handoff,

        input lock
    );

    modport Arbiter(
        input requestLock,
        input isIdle,
        input handoff,

        output lock
    );

endinterface

`endif