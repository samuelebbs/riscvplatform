//# PRINT AFTER video/vga_driver.sv
`ifndef __KEYBOARD_VH
`define __KEYBOARD_VH

interface KeyboardInterface(input logic clock, clear);
    logic [7:0] readch, scancode;
    logic ascii_ready;

    modport Controller(
        input clock, clear,
        input readch, scancode, ascii_ready
    );

    modport Device(
        input clock, clear,
        output readch, scancode, ascii_ready
    );
endinterface

`endif