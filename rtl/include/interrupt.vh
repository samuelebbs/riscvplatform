//# PRINT AFTER core/branch_predictor.sv
`ifndef __INTERRUPT_VH
`define __INTERRUPT_VH

`include "riscv_types.vh"

interface InterruptDevice();
    Word_t tval;
    logic interruptAccepted, interruptPending;

    modport InterruptController(
        input tval,
        input interruptPending,
        output interruptAccepted
    );

    modport Device(
        output tval,
        output interruptPending,
        input interruptAccepted
    );

endinterface

`endif