//# PRINT AFTER core/write_back.sv
`ifndef __REMOTE_VH
`define __REMOTE_VH

`include "riscv_types.vh"
`include "register_file.vh"
`include "riscv_csr.vh"

// Address Mapping
`define REMOTE_CONTROL_PREFIX 4'h0
`define REMOTE_IO_PREFIX 4'h2
`define REMOTE_CSR_PREFIX 4'b11??

// Control Mapping {[11:2], 2'b0}
`define REMOTE_HALT 10'h1
`define REMOTE_ERROR 10'h2

`define REMOTE_PC 10'h4

`define REMOTE_TIMER_LOW 10'h10
`define REMOTE_TIMER_HIGH 10'h11
`define REMOTE_NEXT_TIMER_LOW 10'h12
`define REMOTE_NEXT_TIMER_HIGH 10'h13

`define REMOTE_REGISTER_FILE 10'b10?????

`define REMOTE_ERROR_ADDRESS 10'h60
`define REMOTE_ERROR_INDEX 10'h61
// 10'h70
`define REMOTE_ERROR_ARRAY 10'b111????

`define REMOTE_ERROR_WRITE_INDEX 5'd0
`define REMOTE_ERROR_READ_INDEX 5'd1
`define REMOTE_ERROR_ENABLE_INDEX 5'd2

interface RemoteCoreInterface();

    // Read Interface
    ControlStatusRegisterName_t csrName;
    isa_reg_t registerIndex;

    Word_t readCSR;
    Word_t readRegister;

    Word_t pc;

    // Control Interface
    logic coreHalt, coreDebug;
    logic halted; // All pipeline states should stall
    logic dump;

    modport Core(
        input csrName,
        input registerIndex,
        output readCSR,
        output readRegister,
        output pc,

        output coreHalt, coreDebug,
        input halted,
        input dump
    );

    modport Remote(
        output csrName,
        output registerIndex,
        input readCSR,
        input readRegister,
        input pc,

        input coreHalt, coreDebug,
        output halted,
        output dump
    );

endinterface

interface RemoteDRAMInterface();

    logic readError;
    logic writeError;
    logic writeEnableMissing;

    logic [5:0] index;
    Word_t [15:0] writeArray;
    PhysicalAddress_t addressBase;

    modport DRAM(
        output readError,
        output writeError,
        output writeEnableMissing,

        output index, writeArray, addressBase
    );

    modport Remote(
        input readError,
        input writeError,
        input writeEnableMissing,

        input index, writeArray, addressBase
    );

endinterface

interface RemoteTimerInterface();

    logic halted;

    logic [63:0] currentTime, nextTickTime;

    modport Timer(
        input halted,
        output currentTime,
        output nextTickTime
    );

    modport Remote(
        output halted,
        input currentTime,
        input nextTickTime
    );

endinterface

`endif