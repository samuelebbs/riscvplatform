//# PRINT AFTER remote/remote_axi.sv
`ifndef __VIDEO_VH
`define __VIDEO_VH

`include "svo_defines.vh"

`define CONSOLE_WIDTH 80
`define CONSOLE_HEIGHT 25

`define FOREGROUND(color) (color)
`define BACKGROUND(color) ((color) << 4)

`define BLACK 8'h0
`define BLUE  8'h1
`define GREEN 8'h2
`define CYAN  8'h3
`define RED   8'h4
`define MAGENTA   8'h5
`define BROWN  8'h6
`define LIGHT_GRAY 8'h7
`define DARK_GRAY 8'h8
`define BRIGHT_BLUE 8'h9
`define BRIGHT_GREEN  8'hA
`define ORANGE 8'hB
`define PINK  8'hC
`define PURPLE  8'hD
`define YELLOW  8'hE
`define WHITE 8'hF

interface VideoInterfaceInternal();

    logic clock, clear;

    // pixel output stream
    // control[0] ... start of frame
    logic valid;
    logic ready;
    logic [23:0] data;
    logic [0:0] control;

    modport Controller(
        input clock, clear,

        output valid,
        input ready,
        output data,
        output control
    );

    modport Driver(
        output clock, clear,

        input valid,
        output ready,
        input data,
        input control
    );

endinterface

`endif