//# PRINT AFTER core/instruction_decode.sv
`ifndef __EXECUTE_VH
`define __EXECUTE_VH

`include "riscv_types.vh"
`include "register_file.vh"

typedef struct packed {
    Word_t aluResult;
    RegisterResponse_t rd_EX;
    Word_t jumpTarget;
} ExecuteState_t;

`endif