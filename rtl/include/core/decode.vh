//# PRINT AFTER core/instruction_wait.sv
`ifndef __DECODE_VH
`define __DECODE_VH

`include "riscv_csr.vh"
`include "register_file.vh"
`include "riscv_types.vh"

typedef enum {
    INVALID_INSTRUCTION = 'd0,
    ILLEGAL_INSTRUCTION_FAULT,
    NORMAL, /* PC + 4 */
    JUMP, /* TARGET | ABORT */
    BRANCH, /* TARGET | ABORT */
    CSR, /* PC + 4 | Stall Detection */
    FENCE, /* PC + 4 | FENCE [SFENCE.VMA] */
    PRIVILEGE_RAISE, /* tvec | FENCE */
    PRIVILEGE_LOWER_SRET, /* epc | FENCE */
    PRIVILEGE_LOWER_MRET /* epc | FENCE */
} InstructionType_t;

typedef enum {
    NO_MEMORY_OPERATION = 'd0,

    LOAD_BYTE,
    LOAD_HALF_WORD,
    LOAD_WORD,
    LOAD_BYTE_UNSIGNED,
    LOAD_HALF_WORD_UNSIGNED,

    STORE_BYTE,
    STORE_HALF_WORD,
    STORE_WORD,

    LOAD_RESERVED,
    STORE_CONDITIONAL,

    FLUSH_TRANSLATION
} MemoryOperation_t;

typedef enum {
    NO_CSR_OPERATION = 'd0,
    CSR_WRITE,
    CSR_X0
} CSROperation_t;

typedef enum {
    ALU_ADD,
    ALU_SUBTRACT,

    ALU_OR,
    ALU_XOR,
    ALU_AND,
    ALU_CLEAR_MASK,

    ALU_SHIFT_LEFT,
    ALU_SHIFT_RIGHT_LOGICAL,
    ALU_SHIFT_RIGHT_ARITHMETIC,

    ALU_EQUAL,
    ALU_NOT_EQUAL,
    ALU_LESS_THAN_SIGNED,
    ALU_LESS_THAN_UNSIGNED,
    ALU_GREATER_THAN_OR_EQUAL_SIGNED,
    ALU_GREATER_THAN_OR_EQUAL_UNSIGNED
} ArithmeticOperation_t;

typedef enum {
    ALU_SELECT_ZERO,
    ALU_SELECT_REGISTER,
    ALU_SELECT_PC,
    ALU_SELECT_IMMEDIATE,
    ALU_SELECT_CSR
} ArithmeticOperandSelect_t;

typedef enum {
    JUMP_SELECT_ZERO,
    JUMP_SELECT_PC,
    JUMP_SELECT_RS1
} JumpBaseSelect_t;

typedef enum {
    NO_WRITE_BACK = 'd0,
    WRITE_BACK_SELECT_ALU, /* resolves in EX */
    WRITE_BACK_SELECT_PC_PLUS_4, /* resolves in EX */
    WRITE_BACK_SELECT_MEMORY, /* resolves in MW */
    WRITE_BACK_SELECT_CSR /* resolves in EX */
} WriteBackSelect_t;

typedef enum {
    NO_IMMEDIATE,
    I_TYPE,
    U_TYPE,
    SB_TYPE,
    UJ_TYPE,
    S_TYPE,
    CSR_TYPE
} ImmediateType_t;

typedef struct packed {
    ImmediateType_t immediateType;
    Word_t value;
} Immediate_t;

typedef struct packed {
    InstructionType_t instructionType;

    MemoryOperation_t memoryOperation;

    ArithmeticOperation_t aluOperation;
    ArithmeticOperandSelect_t aluOperand1, aluOperand2;

    JumpBaseSelect_t jumpBaseSelect;

    WriteBackSelect_t writeBackSelect;

    ControlStatusRegisterName_t csrName;
    CSROperation_t csrOperation;
    Word_t csrValue;

    RegisterResponse_t rs1, rs2;
    Register_t rd;

    Immediate_t immediate;
} InstructionDecodeState_t;

`define DECODE_FUNCTIONS\
    function automatic logic decodeGenerateFence(InstructionDecodeState_t state);\
        unique case(state.instructionType)\
            INVALID_INSTRUCTION: return 1'b0;\
            ILLEGAL_INSTRUCTION_FAULT: return 1'b1; /* Faults generate fences */\
            NORMAL: return 1'b0;\
            JUMP: return 1'b0;\
            BRANCH: return 1'b0;\
            CSR: begin\
                unique case(state.csrName)\
                    SATP: return 1'b1; /* This may make serious changes to the virtual memory system */\
                    SSTATUS: return 1'b1; /* These may change the SUM bit */\
                    MSTATUS: return 1'b1;\
                    default: return 1'b0; /* By default CSR instructions don't fence */\
                endcase\
            end\
            FENCE: return 1'b1;\
            PRIVILEGE_LOWER_SRET: return 1'b1;\
            PRIVILEGE_LOWER_MRET: return 1'b1;\
            PRIVILEGE_RAISE: return 1'b1;\
        endcase\
    endfunction


`endif