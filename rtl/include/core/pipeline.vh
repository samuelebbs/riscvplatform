//# PRINT AFTER memory/dram_axi.sv
`ifndef __PIPELINE_VH
`define __PIPELINE_VH

`include "memory.vh"

`include "fetch.vh"
`include "decode.vh"
`include "execute.vh"
`include "branch.vh"
`include "memory_stage.vh"

typedef struct packed {
    InstructionFetchState_t fetch;
} InstructionFetchExport_t;

typedef struct packed {
    InstructionFetchState_t fetch;
    InstructionWaitState_t instructionWait;
} InstructionWaitExport_t;

typedef struct packed {
    InstructionFetchState_t fetch;
    InstructionWaitState_t instructionWait;
    InstructionDecodeState_t decode;
} InstructionDecodeExport_t;

typedef struct packed {
    InstructionFetchState_t fetch;
    InstructionWaitState_t instructionWait;
    InstructionDecodeState_t decode;
    ExecuteState_t execute;
} ExecuteExport_t;

typedef struct packed {
    InstructionFetchState_t fetch;
    InstructionWaitState_t instructionWait;
    InstructionDecodeState_t decode;
    ExecuteState_t execute;
    BranchResolveState_t branch;
} BranchResolveExport_t;

typedef struct packed {
    InstructionFetchState_t fetch;
    InstructionWaitState_t instructionWait;
    InstructionDecodeState_t decode;
    ExecuteState_t execute;
    BranchResolveState_t branch;
    MemoryRequestState_t memoryRequest;
} MemoryRequestExport_t;

typedef struct packed {
    InstructionFetchState_t fetch;
    InstructionWaitState_t instructionWait;
    InstructionDecodeState_t decode;
    ExecuteState_t execute;
    BranchResolveState_t branch;
    MemoryRequestState_t memoryRequest;
    MemoryWaitState_t memoryWait;
} MemoryWaitExport_t;

`define STATE_FUNCTIONS\
    function automatic logic isBranch(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.decode.instructionType == BRANCH;\
    endfunction\
\
    function automatic Direction_t getDirection(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.branch.simpleDirection;\
    endfunction\
\
    function automatic logic isJump(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.decode.instructionType == JUMP;\
    endfunction\
\
    function automatic PredictionResult_t getPrediction(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.branch.predictionResult;\
    endfunction\
\
    function automatic logic isBTBHit(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.fetch.prediction.isHit;\
    endfunction\
\
    function automatic logic isJumpReturn(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.fetch.prediction.counter == FUNCTION_RETURN;\
    endfunction\
\
    function automatic TranslationType_t getInstructionTranslationType(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.instructionWait.instruction.translationResponse.translationType;\
    endfunction\
\
    function automatic ResponseType_t getInstructionResponseType(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.instructionWait.instruction.responseType;\
    endfunction\
\
    function automatic TranslationType_t getDataTranslationType(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.memoryWait.response.translationResponse.translationType;\
    endfunction\
\
    function automatic ResponseType_t getDataResponseType(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.memoryWait.response.responseType;\
    endfunction\
\
    function automatic MemoryOperation_t getMemoryOperation(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.decode.memoryOperation;\
    endfunction\
\
    function automatic StoreConditionalResult_t getStoreConditionalResult(MemoryWaitExport_t memoryWaitExport);\
        return memoryWaitExport.memoryRequest.storeConditionalResult;\
    endfunction\
\
    function automatic logic hasPendingException(MemoryWaitExport_t memoryWaitExport);\
        return (getDataResponseType(memoryWaitExport) == PAGE_FAULT) ||\
            (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT) ||\
            (memoryWaitExport.decode.instructionType == ILLEGAL_INSTRUCTION_FAULT);\
    endfunction\
\
    function automatic logic hasPendingException_IW(InstructionWaitState_t instructionWait);\
        return instructionWait.instruction.responseType == PAGE_FAULT;\
    endfunction

`endif