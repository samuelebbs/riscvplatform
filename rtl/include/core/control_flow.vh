//# PRINT AFTER timer/timer_controller.sv
`ifndef __CONTROL_FLOW_VH
`define __CONTROL_FLOW_VH

`include "riscv_types.vh"
`include "riscv_csr.vh"
`include "decode.vh"

interface ControlFlowAsyncInterrupt();

    Word_t tval;
    ExceptionCause_t cause;
    logic interruptPending;

    logic interruptAccepted;

    modport ControlFlow(
        input tval, cause,
        input interruptPending,
        output interruptAccepted
    );

    modport InterruptController(
        output tval, cause,
        output interruptPending,
        input interruptAccepted
    );

endinterface

interface PipelineControl();

    // Control Points
    logic flush, stall;

    // Status Points
    logic ready; // False indicates a stall request
    logic fencing; // If there is an instruction fence of a pending exception
    logic idle;

    InstructionDecodeState_t decode;

    modport ControlFlow(
        output flush, stall,

        input ready, fencing, idle,
        input decode
    );

    modport Stage(
        input flush, stall,

        output ready, fencing, idle,
        output decode
    );

endinterface

`endif