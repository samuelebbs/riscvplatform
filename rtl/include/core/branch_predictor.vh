//# PRINT AFTER core/control_status_registers.sv
`ifndef __BRANCH_PREDICTOR_VH
`define __BRANCH_PREDICTOR_VH

`include "riscv_types.vh"

typedef enum logic[2:0] {
    WEAKLY_NOT_TAKEN,
    STRONGLY_NOT_TAKEN,
    WEAKLY_TAKEN,
    STRONGLY_TAKEN,
    FUNCTION_RETURN
} CounterState_t;

typedef struct packed {
    Word_t tagPC;
    Word_t nextPC;
    CounterState_t counter;
    logic isHit;
} Prediction_t;

`endif