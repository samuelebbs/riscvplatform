//# PRINT AFTER core/branch_resolve.sv
`ifndef __MEMORY_STAGE_VH
`define __MEMORY_STAGE_VH

typedef enum {
  NO_CONDITIONAL = 'd0,
  STORE_SUCCESS,
  STORE_REJECTED
} StoreConditionalResult_t;

typedef struct packed {
    StoreConditionalResult_t storeConditionalResult;
} MemoryRequestState_t;

typedef struct packed {
    MemoryResponse_t response;
    RegisterResponse_t rd_MEM; // This supersedes rd_EX
} MemoryWaitState_t;

`endif