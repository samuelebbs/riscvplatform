//# PRINT AFTER video/video_mock.sv
`ifdef SIMULATION_18447

`default_nettype none

`include "keyboard.vh"
`include "config.vh"

module KeyboardMock(
    KeyboardInterface.Device keyboard
);

    assign keyboard.scancode = 8'b0;
    localparam SEED = 32'hC001D00D;

    logic [63:0] cycleCount;

    Counter #(.WIDTH($bits(cycleCount))) cycleCounter(
        .q(cycleCount),
        .clock(keyboard.clock),
        .clear(keyboard.clear),
        .enable(1'b1)
    );

    class KeyboardThread;

        logic [7:0] keyboardQueue[$];
        logic [63:0] nextKeyTick;

        function void init();
            int fd;
            $display("Initializing keyboard");

            fd = $fopen("keyboard.inp", "rb");
            if(fd == 0) begin
                $fatal("Error opening keyboard.inp");
            end

            forever begin
                string placeholder;
                int bytes_read;
                logic [7:0] ascii;

                bytes_read = $fread(ascii, fd);
                if(bytes_read == 0 && $ferror(fd, placeholder)) begin
                    $fatal("Unable to read keyboard file");
                end else if(bytes_read == 0) begin
                    break;
                end

                assert (bytes_read == 1) else $fatal("Bytes Read: %d", bytes_read);

                keyboardQueue.push_back(ascii);
            end

            $display("Keyboard Size: %d", keyboardQueue.size());
            foreach(keyboardQueue[i])$display("\tkeyboard[%0d] = %0s",i,keyboardQueue[i]);

            nextKeyTick = `TIMER_TICK_CYCLES / 2;
            $srandom(SEED);

        endfunction

        task waitForKeyTick();
            while(cycleCount < nextKeyTick) begin
                @(posedge keyboard.clock);
            end
        endtask

        task run();
            while(keyboardQueue.size() > 0) begin
                waitForKeyTick();
                keyboard.readch <= keyboardQueue.pop_front();
                keyboard.ascii_ready <= 1'b1;
                @(posedge keyboard.clock);
                keyboard.readch <= 8'b0;
                keyboard.ascii_ready <= 1'b0;

                nextKeyTick = cycleCount + $urandom_range(0,`TIMER_TICK_CYCLES * 10) + `KEYBOARD_MIN_TICK;
            end
        endtask

    endclass

    initial begin
        KeyboardThread kt = new;
        kt.init();
        keyboard.readch = 8'd0;
        keyboard.ascii_ready = 1'b0;
        #1; // Let the rest of the world settle down
        while(keyboard.clear) begin
            @(posedge keyboard.clock);
        end

        kt.run();
    end

endmodule

`endif