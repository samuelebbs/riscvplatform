//# PRINT AFTER video/svo_tmds.v
`ifndef SIMULATION_18447

`include "board_config.vh"

`ifdef VGA_OUTPUT

`default_nettype none

`include "video.vh"
`include "svo_defines.vh"

module VGADriver #(`SVO_DEFAULT_PARAMS)(
    VideoInterfaceInternal.Driver internalDriver,

    output logic [3:0] vga_r, vga_g, vga_b,
    output logic vga_vs, vga_hs,

    input logic clk_pixel, clk_5x_pixel, clk_pixel_resetn
);

    `SVO_DECLS

    assign internalDriver.clock = clk_pixel;
    assign internalDriver.clear = !clk_pixel_resetn;

    logic video_enc_tvalid;
    logic video_enc_tready;
    logic [SVO_BITS_PER_PIXEL-1:0] video_enc_tdata;
    logic [3:0] video_enc_tuser;

    svo_enc #( `SVO_PASS_PARAMS ) svo_enc (
        .clk(clk_pixel),
        .resetn(clk_pixel_resetn),

        .in_axis_tvalid(internalDriver.valid),
        .in_axis_tready(internalDriver.ready),
        .in_axis_tdata(internalDriver.data),
        .in_axis_tuser(internalDriver.control[0]),

        .out_axis_tvalid(video_enc_tvalid),
        .out_axis_tready(video_enc_tready),
        .out_axis_tdata(video_enc_tdata),
        .out_axis_tuser(video_enc_tuser)
    );

    assign video_enc_tready = 1;

    // TODO: Technically it would be awesome if we adjusted the
    // SVO_BITS_PER_PIXEL down to 12 if we were using VGA, but instead we'll
    // just select 4-bits off of the 8-bit color code and hope this looks
    // reasonable.
    assign vga_r = video_enc_tdata[6:2]; // [7:0]
    assign vga_g = video_enc_tdata[13:10]; // [15:8]
    assign vga_b = video_enc_tdata[21:18]; // [23:16]

    assign vga_hs = video_enc_tuser[1];
    assign vga_vs = video_enc_tuser[2];

endmodule

`endif
`endif