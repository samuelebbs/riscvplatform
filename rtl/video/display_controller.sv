//# PRINT AFTER include/video/video.vh
`default_nettype none

`include "svo_defines.vh"
`include "memory.vh"
`include "video.vh"
`include "compiler.vh"


module DisplayController
    #(`SVO_DEFAULT_PARAMS) (
    MemoryMappedIOInterface.Requester videoMemory,
    VideoInterfaceInternal.Controller videoInternal
);

    `SVO_DECLS;

    localparam CONSOLE_HOR_PIXELS = 640;
    localparam CONSOLE_VER_PIXELS = 400;
    localparam CONSOLE_X1 = ((SVO_HOR_PIXELS - CONSOLE_HOR_PIXELS) / 2);
    localparam CONSOLE_Y1 = ((SVO_VER_PIXELS - CONSOLE_VER_PIXELS) / 2);
    localparam CONSOLE_X2 = CONSOLE_X1 + CONSOLE_HOR_PIXELS;
    localparam CONSOLE_Y2 = CONSOLE_Y1 + CONSOLE_VER_PIXELS;

    logic display_border_tvalid;
    logic display_border_tready;
    logic [SVO_BITS_PER_PIXEL-1:0] display_border_tdata;
    logic [2:0] display_border_tuser;

    svo_rect #(`SVO_PASS_PARAMS) display_border(
        .clk(videoInternal.clock),
        .resetn(~videoInternal.clear),

        .x1(14'd0), .y1(14'd0),
        .x2(svo_coordinate(SVO_HOR_PIXELS-1)), .y2(svo_coordinate(SVO_VER_PIXELS-1)),
        .border(svo_rgb(8'hFF, 8'h00, 8'h00)),
        .fill(svo_rgb(8'h00, 8'h00, 8'h00)),

        .out_axis_tvalid(display_border_tvalid),
        .out_axis_tready(display_border_tready),
        .out_axis_tdata(display_border_tdata),
        .out_axis_tuser(display_border_tuser)
    );

    logic console_border_tvalid;
    logic console_border_tready;
    logic [SVO_BITS_PER_PIXEL-1:0] console_border_tdata;
    logic [2:0] console_border_tuser;

    svo_rect #(`SVO_PASS_PARAMS) console_border(
        .clk(videoInternal.clock),
        .resetn(~videoInternal.clear),

        .x1(svo_coordinate(CONSOLE_X1-1)), .y1(svo_coordinate(CONSOLE_Y1-1)),
        .x2(svo_coordinate(CONSOLE_X2)), .y2(svo_coordinate(CONSOLE_Y2)),
        .border(svo_rgb(8'h00, 8'hFF, 8'h00)),
        .fill(svo_rgb(8'hFF, 8'hFF, 8'hFF)),

        .out_axis_tvalid(console_border_tvalid),
        .out_axis_tready(console_border_tready),
        .out_axis_tdata(console_border_tdata),
        .out_axis_tuser(console_border_tuser)
    );

    logic video_border_tvalid;
    logic video_border_tready;
    logic [SVO_BITS_PER_PIXEL-1:0] video_border_tdata;
    logic [0:0] video_border_tuser;


    svo_overlay #(`SVO_PASS_PARAMS) border_overlay(
        .clk(videoInternal.clock),
        .resetn(~videoInternal.clear),
        .enable(1'b1),

        .in_axis_tvalid(display_border_tvalid),
        .in_axis_tready(display_border_tready),
        .in_axis_tdata(display_border_tdata),
        .in_axis_tuser(display_border_tuser[0]),

        .over_axis_tvalid(console_border_tvalid),
        .over_axis_tready(console_border_tready),
        .over_axis_tdata(console_border_tdata),
        .over_axis_tuser({|console_border_tuser[2:1], console_border_tuser[0]}),

        .out_axis_tvalid(video_border_tvalid),
        .out_axis_tready(video_border_tready),
        .out_axis_tdata(video_border_tdata),
        .out_axis_tuser(video_border_tuser)
    );

    logic text_tvalid;
    logic text_tready;
    logic [SVO_BITS_PER_PIXEL-1:0] text_tdata;
    logic [1:0] text_tuser;


    text_console #(`SVO_PASS_PARAMS,
        .CONSOLE_HOR_PIXELS(CONSOLE_HOR_PIXELS),
        .CONSOLE_VER_PIXELS(CONSOLE_VER_PIXELS),
        .CONSOLE_ABS_X1(CONSOLE_X1),
        .CONSOLE_ABS_Y1(CONSOLE_Y1),
        .CONSOLE_ABS_X2(CONSOLE_X2),
        .CONSOLE_ABS_Y2(CONSOLE_Y2)) text_console(
        .videoMemory(videoMemory),
        .clock(videoInternal.clock),
        .reset_n(~videoInternal.clear),

        .out_axis_tvalid(text_tvalid),
        .out_axis_tready(text_tready),
        .out_axis_tdata(text_tdata),
        .out_axis_tuser(text_tuser)
    );

    svo_overlay #(`SVO_PASS_PARAMS) final_overlay(
        .clk(videoInternal.clock),
        .resetn(~videoInternal.clear),
        .enable(1'b1),

        .in_axis_tvalid(video_border_tvalid),
        .in_axis_tready(video_border_tready),
        .in_axis_tdata(video_border_tdata),
        .in_axis_tuser(video_border_tuser[0]),

        .over_axis_tvalid(text_tvalid),
        .over_axis_tready(text_tready),
        .over_axis_tdata(text_tdata),
        .over_axis_tuser(text_tuser),

        .out_axis_tvalid(videoInternal.valid),
        .out_axis_tready(videoInternal.ready),
        .out_axis_tdata(videoInternal.data),
        .out_axis_tuser(videoInternal.control)
    );

endmodule