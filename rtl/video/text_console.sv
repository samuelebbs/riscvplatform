//# PRINT AFTER video/display_controller.sv
`default_nettype none

`include "svo_defines.vh"
`include "memory.vh"
`include "video.vh"
`include "compiler.vh"

module text_console #(
    `SVO_DEFAULT_PARAMS,
    parameter CONSOLE_HOR_PIXELS = 640,
    parameter CONSOLE_VER_PIXELS = 400,
    parameter CONSOLE_ABS_X1 = 0,
    parameter CONSOLE_ABS_Y1 = 0,
    parameter CONSOLE_ABS_X2 = CONSOLE_ABS_X1 + CONSOLE_HOR_PIXELS,
    parameter CONSOLE_ABS_Y2 = CONSOLE_ABS_Y1 + CONSOLE_VER_PIXELS
    ) (
    MemoryMappedIOInterface.Requester videoMemory,
    input logic clock, reset_n,

    // output stream
    // tuser[0] ... start of frame
    // tuser[1] ... pixel valid (in console)
    output logic out_axis_tvalid,
    input logic out_axis_tready,
    output logic [SVO_BITS_PER_PIXEL-1:0] out_axis_tdata,
    output logic [1:0] out_axis_tuser
);
    `SVO_DECLS
    `MEMORY_FUNCTIONS


    assign videoMemory.clock = clock;
    assign videoMemory.clear = ~reset_n;

    ////////////////////////////////////// Display State
    logic [`SVO_XYBITS-1:0] x, y;
    logic in_x, in_y;

    assign in_x = CONSOLE_ABS_X1 <= x && x < CONSOLE_ABS_X2;
    assign in_y = CONSOLE_ABS_Y1 <= y && y < CONSOLE_ABS_Y2;

    assign out_axis_tuser = {(in_x && in_y), (!x && !y)};

    always_ff @(posedge clock) begin
        if(~reset_n) begin
            x <= 0;
            y <= 0;
            out_axis_tvalid <= 1'b0;
        end else begin
            out_axis_tvalid <= 1'b1;
            if (out_axis_tvalid && out_axis_tready) begin
                if (x == SVO_HOR_PIXELS-1) begin
                    x <= 0;
                    y <= (y == SVO_VER_PIXELS-1) ? 0 : y + 1;
                end else begin
                    x <= x + 1;
                end
            end
        end
    end


    ////////////////////////////// VRAM Address Generation

    // This logic only works if the console is not adjacent to any of the edges
    // of the display. It will need to be re-written once we have a console that
    // actually fits to screen

    // TODO: It actually probably makes more sense to have this based on a
    // virtual index instead of a pair of virtual coordinates and then have
    // derived fields for the virtual coordinates
    logic [`SVO_XYBITS-1:0] virtual_x;
    logic [`SVO_XYBITS-1:0] virtual_y;

    always_comb begin
        virtual_x = ~0;
        virtual_y = ~0;
        // Two cycle latency since we have one cycle latency to read the VRAM
        // and another cycle latency to read the charmap
        if(CONSOLE_ABS_X1 - 2 <= x && x < CONSOLE_ABS_X2)
            virtual_x = x - CONSOLE_ABS_X1 + 2;

        if(CONSOLE_ABS_Y1 <= y && y < CONSOLE_ABS_Y2)
            virtual_y = y - CONSOLE_ABS_Y1;
    end

   // Increment counters for characters
   localparam CHAR_WIDTH  = 8;
   localparam CHAR_HEIGHT = 16;

   localparam CONSOLE_SIZE = `CONSOLE_WIDTH * `CONSOLE_HEIGHT;

    logic [$clog2(`CONSOLE_WIDTH)-1:0] character_cell_x;
    logic [$clog2(`CONSOLE_HEIGHT)-1:0] character_cell_y;
    logic [10:0] character_cell_index;
    PhysicalAddress_t character_address;

    `STATIC_ASSERT($bits(character_cell_index) >= $clog2(CONSOLE_SIZE), cell_valid);

    assign character_address = {character_cell_index[10:1], 2'b0} + `VRAM_START;

    always_comb begin
        character_cell_x = virtual_x / CHAR_WIDTH;
        character_cell_y = virtual_y / CHAR_HEIGHT;
        character_cell_index = character_cell_x +
            (`CONSOLE_WIDTH * character_cell_y);
        videoMemory.request = simpleMemoryRead(character_address);
    end

    //////////////////////////// ASCII Generation

    logic [1:0][7:0] character, color;

    logic [10:0] character_cell_index_AG;
    logic [`SVO_XYBITS-1:0] virtual_x_AG;
    logic [`SVO_XYBITS-1:0] virtual_y_AG;

    always_ff @(posedge clock) begin
        character_cell_index_AG <= character_cell_index;
        virtual_x_AG <= virtual_x;
        virtual_y_AG <= virtual_y;
    end

    assign {color[1], character[1], color[0], character[0]} =
        videoMemory.response.readData;

    logic [3:0] bitmap_row;
    logic [2:0] bitmap_col;
    logic [6:0] ascii_code;
    logic [7:0] color_code;
    logic bitmap_pixel;

    always_ff @(posedge clock) begin
        bitmap_col <= virtual_x_AG[2:0];
        bitmap_row <= virtual_y_AG[3:0];
        ascii_code <= character[character_cell_index_AG[0]][6:0];
        color_code <= color[character_cell_index_AG[0]];
    end

    charmap charmap(
        .clk(clock),
        .ascii_code(ascii_code),
        .row(bitmap_row),
        .col(bitmap_col),
        .pixel(bitmap_pixel)
    );

    ////////////////////////// Output Generation
    logic [SVO_BITS_PER_PIXEL-1:0] foreground, background;

    function logic [SVO_BITS_PER_PIXEL-1:0] convertColor(logic [3:0] color_code);
        unique case (color_code)
            `BLACK: return svo_rgb(8'h0, 8'h0, 8'h0);
            `BLUE: return svo_rgb(8'h0, 8'h0, 8'hFF);
            `GREEN: return svo_rgb(8'h0, 8'hFF, 8'h0);
            `CYAN: return svo_rgb(8'h00, 8'hFF, 8'hFF);
            `RED: return svo_rgb(8'hFF, 8'h00, 8'h00);
            `MAGENTA: return svo_rgb(8'hFF, 8'h0, 8'hFF);
            `BROWN: return svo_rgb(8'h31, 8'h0C, 8'h0C);
            `LIGHT_GRAY: return svo_rgb(8'h80, 8'h80, 8'h80);
            `DARK_GRAY: return svo_rgb(8'h33, 8'h33, 8'h33);
            `BRIGHT_BLUE: return svo_rgb(8'h7E, 8'hE0, 8'hFF);
            `BRIGHT_GREEN: return svo_rgb(8'ha1, 8'hee, 8'h33);
            `ORANGE: return svo_rgb(8'hFF, 8'h80, 8'h00);
            `PINK: return svo_rgb(8'hFF, 8'hC0, 8'hCB);
            `PURPLE: return svo_rgb(8'h80, 8'h00, 8'hFF);
            `YELLOW: return svo_rgb(8'hFF, 8'hFF, 8'h0);
            `WHITE: return svo_rgb(8'hFF, 8'hFF, 8'hFF);
        endcase
    endfunction

    always_comb begin
        foreground = convertColor(color_code[3:0]);
        background = convertColor(color_code[7:4]);
    end

    assign out_axis_tdata = bitmap_pixel ? foreground : background;

endmodule