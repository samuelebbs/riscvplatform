//# PRINT AFTER include/core/pipeline.vh
`default_nettype none

`include "memory.vh"
`include "remote.vh"
`include "interrupt.vh"
`include "register_file.vh"
`include "control_flow.vh"
`include "control_status_registers.vh"
`include "pipeline.vh"
`include "branch.vh"
`include "fetch.vh"

module Core(
    MemoryInterface.Requester instruction, data,

    RemoteCoreInterface.Core remote,

    InterruptDevice.InterruptController keyboard, timer,
    MemoryControl.Core memoryControl,

    input logic clock, clear
);

    // 8 stage pipeline
    // Instruction Fetch (instruction address request)
    // Instruction Wait (instruction data response)
    // Instruction Decode (decode)
    // EXecute (ALU operations)
    // Branch Resolve (determine branch targets)
    // Memory Request (load/store address request)
    // Memory Wait (load/store data response)
    // Write Back (update performance counters and register file)

    InstructionFetchExport_t fetch_IW;
    InstructionWaitExport_t instructionWait_ID;
    InstructionDecodeExport_t decode_EX;
    ExecuteExport_t execute_BR;
    BranchResolveExport_t branch_MR;
    MemoryRequestExport_t memoryRequest_MW;
    MemoryWaitExport_t memoryWait_WB;

    PipelineControl fetchControl();
    PipelineControl instructionWaitControl();
    PipelineControl decodeControl();
    PipelineControl executeControl();
    PipelineControl branchControl();
    PipelineControl memoryRequestControl();
    PipelineControl memoryWaitControl();

    FetchPrediction fetchPrediction();
    BranchResolve branchPredictionResolution();

    RegisterFileRequest rs1_ID();
    RegisterFileRequest rs2_ID();
    RegisterFileRequest remoteRegisterRequest();

    RegisterReport rd_EX();
    RegisterReport rd_BR();
    RegisterReport rd_MR();
    RegisterReport rd_MW();
    RegisterReport rd_WB();

    RegisterFile registerFile(
        .rs1_ID(rs1_ID.RegisterFile),
        .rs2_ID(rs2_ID.RegisterFile),
        .remote(remoteRegisterRequest.RegisterFile),
        .rd_EX(rd_EX.RegisterFile),
        .rd_BR(rd_BR.RegisterFile),
        .rd_MR(rd_MR.RegisterFile),
        .rd_MW(rd_MW.RegisterFile),
        .rd_WB(rd_WB.RegisterFile),
        `ifdef SIMULATION_18447
        .dump(remote.dump),
        `endif
        .clock,
        .clear
    );

    CSRInterruptDelivery controlFlow_InterruptDelivery();
    CSRInterruptReturn controlFlow_InterruptReturn();

    CSRReadInterface csr_ID();
    CSRReadInterface csr_remote();

    CSRWriteInterface csr_WB();

    CSRCoreObserver csrCoreObserver();

    ControlStatusRegisters csrs(
        .interruptDelivery(controlFlow_InterruptDelivery.ControlStatusRegister),
        .interruptReturn(controlFlow_InterruptReturn.ControlStatusRegister),

        .memoryControl,
        .observer(csrCoreObserver.ControlStatusRegister),

        .request_ID(csr_ID.ControlStatusRegister),
        .remote(csr_remote.ControlStatusRegister),
        .request_WB(csr_WB.ControlStatusRegister),

        .instructionWait_ID,
        .memoryWait_WB,

        `ifdef SIMULATION_18447
        .dump(remote.dump),
        `endif
        .clock,
        .clear,
        .halted(remote.halted)
    );

    ControlFlowAsyncInterrupt controlFlowAsyncInterrupt();

    InterruptController interruptController(
        .keyboard,
        .timer,
        .controlFlow(controlFlowAsyncInterrupt.InterruptController),
        .csr(csrCoreObserver.Observer),
        .clock,
        .clear
    );

    ControlFlow controlFlow(
        .csrInterruptDelivery(controlFlow_InterruptDelivery.ControlFlow),
        .csrInterruptReturn(controlFlow_InterruptReturn.ControlFlow),
        .csrObserver(csrCoreObserver.Observer),

        .asyncInterrupt(controlFlowAsyncInterrupt.ControlFlow),

        .fetch(fetchControl.ControlFlow),
        .instructionWait(instructionWaitControl.ControlFlow),
        .decode(decodeControl.ControlFlow),
        .execute(executeControl.ControlFlow),
        .branch(branchControl.ControlFlow),
        .memoryRequest(memoryRequestControl.ControlFlow),
        .memoryWait(memoryWaitControl.ControlFlow),

        .fetchPrediction(fetchPrediction.ControlFlow),
        .branchResolve(branchPredictionResolution.ControlFlow),

        .writeBack(memoryWait_WB),

        .remoteHalted(remote.halted),
        .coreHalt(remote.coreHalt),
        .coreDebug(remote.coreDebug),

        .clock,
        .clear
    );

    // Pipeline

    InstructionFetchStage fetch(
        .controlFlow(fetchControl.Stage),
        .prediction(fetchPrediction.Fetch),
        .memoryRequest(instruction.request),
        .memoryReady(instruction.ready),
        .fetch_IW,
        .clock,
        .clear,
        .halted(remote.halted)
    );

    InstructionWaitStage instructionWait(
        .controlFlow(instructionWaitControl.Stage),
        .fetch_IW,
        .memoryResponse(instruction.response),
        .instructionWait_ID,
        .clock,
        .clear
    );

    InstructionDecodeStage decode(
        .controlFlow(decodeControl.Stage),
        .instructionWait_ID,
        .csr(csr_ID.Requester),
        .rs1(rs1_ID.Requester),
        .rs2(rs2_ID.Requester),
        .csrObserver(csrCoreObserver.Observer),
        .decode_EX,
        .clock,
        .clear
    );

    ExecuteStage execute(
        .controlFlow(executeControl.Stage),
        .decode_EX,
        .rd_EX(rd_EX.Reporter),
        .execute_BR,
        .clock,
        .clear
    );

    BranchResolveStage branch(
        .controlFlow(branchControl.Stage),
        .execute_BR,
        .rd_BR(rd_BR.Reporter),
        .predictionResolution(branchPredictionResolution.Branch),
        .branch_MR,
        .clock,
        .clear
    );

    MemoryRequestStage memoryRequest(
        .controlFlow(memoryRequestControl.Stage),
        .branch_MR,
        .memoryRequest(data.request),
        .memoryReady(data.ready),
        .rd_MR(rd_MR.Reporter),
        .memoryRequest_MW,
        .clock,
        .clear
    );

    MemoryWaitStage memoryWait(
        .controlFlow(memoryWaitControl.Stage),
        .memoryRequest_MW,
        .memoryResponse(data.response),
        .rd_MW(rd_MW.Reporter),
        .memoryWait_WB,
        .clock,
        .clear
    );

    WriteBackStage writeBack(
        .memoryWait_WB,
        .rd_WB(rd_WB.Reporter),
        .csr(csr_WB.Requester),
        .lastPCRetired(remote.pc),
        .clock,
        .clear
    );

    // Remote interface connections
    remoteRegisterResultAlwaysValid: assert property (
        @(posedge clock) disable iff(clear)
        remoteRegisterRequest.response.resultValid
    );

    always_comb begin
        remoteRegisterRequest.request = '{
            number: remote.registerIndex,
            isValid: 1'b1
        };
        remote.readRegister = remoteRegisterRequest.response.result;
    end

    always_comb begin
        csr_remote.name = remote.csrName;
        remote.readCSR = csr_remote.value;
    end

endmodule