//# PRINT AFTER include/interrupt.vh
`default_nettype none

`include "interrupt.vh"
`include "control_flow.vh"
`include "control_status_registers.vh"
`include "riscv_csr.vh"

module InterruptController(
    InterruptDevice.InterruptController keyboard, timer,

    ControlFlowAsyncInterrupt.InterruptController controlFlow,
    CSRCoreObserver.Observer csr,

    input logic clock, clear
);

    `CSR_FUNCTIONS;

    acceptOnlyWhenPending: assert property (@(posedge clock) disable iff(clear)
        controlFlow.interruptAccepted |-> controlFlow.interruptPending
    );

    function automatic logic interruptEnabled(Word_t cause);
        unique case (csr.currentLevel)
            USER_MODE: return 1'b1; // User mode interrupts always enabled
            SUPERVISOR_MODE: begin
                // If we are in supervisor mode and the interrupt is delegated to
                // supervisor mode, we use the supervisor interrupts enabled flag to
                // determine if the interrupt is enabled, otherwise the interrupt
                // must be enabled and will be taken in machine mode.
                if(isDelegated(csr.mideleg, cause)) begin
                    return csr.status.sie;
                end else begin
                    return 1'b1;
                end
            end
            MACHINE_MODE: return csr.status.mie;
        endcase
    endfunction

    always_comb begin
        timer.interruptAccepted = 1'b0;
        keyboard.interruptAccepted = 1'b0;
        controlFlow.interruptPending = 1'b0;
        controlFlow.tval = `WORD_POISON;
        controlFlow.cause = RESERVED_INVALID_CAUSE;

        if(interruptEnabled(EXTERNAL_INTERRUPT) &&
            keyboard.interruptPending
        ) begin
            controlFlow.interruptPending = 1'b1;
            controlFlow.tval = keyboard.tval;
            controlFlow.cause = EXTERNAL_INTERRUPT;
            if(controlFlow.interruptAccepted) begin
                keyboard.interruptAccepted = 1'b1;
            end
        end else if(interruptEnabled(TIMER_INTERRUPT) &&
            timer.interruptPending
        ) begin
            controlFlow.interruptPending = 1'b1;
            controlFlow.tval = timer.tval;
            controlFlow.cause = TIMER_INTERRUPT;
            if(controlFlow.interruptAccepted) begin
                timer.interruptAccepted = 1'b1;
            end

        end
    end

endmodule