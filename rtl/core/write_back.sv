//# PRINT AFTER core/memory_wait.sv
`default_nettype none

`include "pipeline.vh"
`include "register_file.vh"
`include "control_status_registers.vh"
`include "riscv_types.vh"

module WriteBackStage(
    input MemoryWaitExport_t memoryWait_WB,

    RegisterReport.Reporter rd_WB,
    CSRWriteInterface.Requester csr,

    output Word_t lastPCRetired,

    input logic clock, clear
);

    fetchPCTagMatch: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid |-> memoryWait_WB.fetch.pc == memoryWait_WB.fetch.prediction.tagPC
    );

    aluSanity: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.writeBackSelect == WRITE_BACK_SELECT_ALU |->
        memoryWait_WB.execute.aluResult == rd_WB.response.result
    );

    writeBackResultValid: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.writeBackSelect != NO_WRITE_BACK |->
        rd_WB.response.resultValid || memoryWait_WB.memoryWait.response.responseType == PAGE_FAULT
    );

    rdValidSelectValid: assert property (@(posedge clock) disable iff(clear)
        rd_WB.response.resultValid |-> memoryWait_WB.decode.writeBackSelect != NO_WRITE_BACK
    );

    pcPlus4WriteBackSanity: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.writeBackSelect == WRITE_BACK_SELECT_PC_PLUS_4 |->
        rd_WB.response.result == (memoryWait_WB.fetch.pc + 32'd4)
    );

    memorySanity: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        memoryWait_WB.memoryRequest.storeConditionalResult == STORE_REJECTED ||
        (memoryWait_WB.memoryWait.response.responseType != INVALID_MEMORY_RESPONSE &&
            memoryWait_WB.memoryWait.response.responseType != TRANSLATION_INVALIDATE)
    );

    memoryResponseSanity: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.memoryWait.response.responseType != INVALID_MEMORY_RESPONSE |->
        memoryWait_WB.decode.memoryOperation != NO_MEMORY_OPERATION
    );

    memoryFaultSanity: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.memoryWait.response.responseType == PAGE_FAULT |->
        !rd_WB.response.resultValid
    );

    always_comb begin
        rd_WB.response = memoryWait_WB.memoryWait.rd_MEM;
        // a memory page fault is the only time there will be a potentially
        // valid rd register that won't have a valid result.
        if(memoryWait_WB.memoryWait.response.responseType == PAGE_FAULT) begin
            rd_WB.response = '{default:0};
        end
    end

    assign csr.name = memoryWait_WB.decode.csrName;
    assign csr.value = memoryWait_WB.execute.aluResult;
    assign csr.writeEnable = (memoryWait_WB.decode.csrOperation == CSR_WRITE);

    `ifdef SIMULATION_18447
    always @(posedge clock) begin
        if(memoryWait_WB.memoryWait.rd_MEM.result == `WORD_POISON && memoryWait_WB.memoryWait.rd_MEM.resultValid) begin
            $display($time, "Found poison: %p", memoryWait_WB);
        end
    end
    `endif

    Register #(.WIDTH($bits(lastPCRetired))) lastPC(
        .q(lastPCRetired),
        .d(memoryWait_WB.fetch.pc),
        .enable(memoryWait_WB.fetch.valid),
        .clock,
        .clear
    );


endmodule