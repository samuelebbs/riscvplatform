//# PRINT AFTER include/core/decode.vh
`default_nettype none

`include "riscv_types.vh"
`include "control_status_registers.vh"
`include "register_file.vh"
`include "decode.vh"
`include "riscv_isa.vh"
`include "riscv_csr.vh"
`include "config.vh"

module RISCVDecoder(
    input Word_t instruction,

    CSRReadInterface.Requester csr,
    RegisterFileRequest.Requester rs1, rs2,
    CSRCoreObserver.Observer csrObserver,

    output InstructionDecodeState_t decodeState,

    input logic clock, clear
);

    `CSR_FUNCTIONS

    Immediate_t immediate;

    always_comb begin
        decodeState.csrValue = csr.value;
        decodeState.rs1 = rs1.response;
        decodeState.rs2 = rs2.response;
        decodeState.immediate = immediate;
    end

    Register_t rs1Request, rs2Request;

    assign csr.name = decodeState.csrName;
    assign rs1.request = rs1Request;
    assign rs2.request = rs2Request;

    ImmediateType_t immediateType;

    ImmediateDecoder immediateDecoder(
        .instruction(instruction),
        .immediateType,
        .immediate
    );

    // The various fields of an instruction
    opcode_t            opcode;
    funct7_t            funct7;
    rtype_funct3_t      rtype_funct3;
    rtype_m_funct3_t rtype_m_funct3;
    itype_int_funct3_t  itype_int_funct3;
    itype_load_funct3_t itype_load_funct3;
    itype_funct12_t     itype_funct12;
    stype_funct3_t stype_funct3;
    amo_funct5_t amo_funct5;
    sbtype_funct3_t sbtype_funct3;
    itype_system_funct3_t itype_system_funct3;

    // Decode the opcode and various function codes for the instruction
    assign opcode           = opcode_t'(instruction[6:0]);
    assign funct7           = funct7_t'(instruction[31:25]);
    assign rtype_funct3     = rtype_funct3_t'(instruction[14:12]);
    assign rtype_m_funct3 = rtype_m_funct3_t'(instruction[14:12]);
    assign itype_int_funct3 = itype_int_funct3_t'(instruction[14:12]);
    assign itype_load_funct3 = itype_load_funct3_t'(instruction[14:12]);
    assign itype_funct12    = itype_funct12_t'(instruction[31:20]);
    assign stype_funct3 = stype_funct3_t'(instruction[14:12]);
    assign amo_funct5 = amo_funct5_t'(instruction[31:27]);
    assign sbtype_funct3 = sbtype_funct3_t'(instruction[14:12]);
    assign itype_system_funct3 = itype_system_funct3_t'(instruction[14:12]);

    isa_reg_t rs1Index, rs2Index, rdIndex;

    assign rs1Index = isa_reg_t'(instruction[19:15]);
    assign rs2Index = isa_reg_t'(instruction[24:20]);
    assign rdIndex = isa_reg_t'(instruction[11:7]);

    assign decodeState.rd = '{
        number: rdIndex,
        isValid: (decodeState.writeBackSelect != NO_WRITE_BACK)
    };

    assign decodeState.csrName = ControlStatusRegisterName_t'(instruction[31:20]);

    always_comb begin
        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
        decodeState.memoryOperation = NO_MEMORY_OPERATION;
        decodeState.aluOperation = ALU_ADD;
        decodeState.aluOperand1 = ALU_SELECT_ZERO;
        decodeState.aluOperand2 = ALU_SELECT_ZERO;
        decodeState.jumpBaseSelect = JUMP_SELECT_ZERO;
        decodeState.writeBackSelect = NO_WRITE_BACK;
        decodeState.csrOperation = NO_CSR_OPERATION;

        rs1Request = '{
            number: rs1Index,
            isValid: 1'b0
        };
        rs2Request = '{
            number: rs2Index,
            isValid: 1'b0
        };
        immediateType = NO_IMMEDIATE;

        unique case(opcode)
            OP_OP: begin
                decodeState.instructionType = NORMAL;
                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                decodeState.aluOperand2 = ALU_SELECT_REGISTER;
                rs1Request.isValid = 1'b1;
                rs2Request.isValid = 1'b1;
                decodeState.writeBackSelect = WRITE_BACK_SELECT_ALU;
                unique case(funct7)
                    FUNCT7_INT: begin
                        unique case(rtype_funct3)
                            FUNCT3_ADD_SUB: decodeState.aluOperation = ALU_ADD;
                            FUNCT3_SLL: decodeState.aluOperation = ALU_SHIFT_LEFT;
                            FUNCT3_SLT: decodeState.aluOperation = ALU_LESS_THAN_SIGNED;
                            FUNCT3_SLTU: decodeState.aluOperation = ALU_LESS_THAN_UNSIGNED;
                            FUNCT3_XOR: decodeState.aluOperation = ALU_XOR;
                            FUNCT3_SRL_SRA: decodeState.aluOperation = ALU_SHIFT_RIGHT_LOGICAL;
                            FUNCT3_OR: decodeState.aluOperation = ALU_OR;
                            FUNCT3_AND: decodeState.aluOperation = ALU_AND;
                            default: begin
                                decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                                decodeState.writeBackSelect = NO_WRITE_BACK;
                            end
                        endcase
                    end
                    FUNCT7_ALT_INT: begin
                        unique case(rtype_funct3)
                            FUNCT3_ADD_SUB: decodeState.aluOperation = ALU_SUBTRACT;
                            FUNCT3_SRL_SRA: decodeState.aluOperation = ALU_SHIFT_RIGHT_ARITHMETIC;
                            default: begin
                                decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                                decodeState.writeBackSelect = NO_WRITE_BACK;
                            end
                        endcase
                    end
                    default: begin
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                        decodeState.writeBackSelect = NO_WRITE_BACK;
                    end
                endcase
            end
            OP_IMM: begin
                decodeState.instructionType = NORMAL;
                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                decodeState.aluOperand2 = ALU_SELECT_IMMEDIATE;
                rs1Request.isValid = 1'b1;
                decodeState.writeBackSelect = WRITE_BACK_SELECT_ALU;
                immediateType = I_TYPE;
                unique case(itype_int_funct3)
                    FUNCT3_ADDI: decodeState.aluOperation = ALU_ADD;
                    FUNCT3_SLTI: decodeState.aluOperation = ALU_LESS_THAN_SIGNED;
                    FUNCT3_SLTIU: decodeState.aluOperation = ALU_LESS_THAN_UNSIGNED;
                    FUNCT3_XORI: decodeState.aluOperation = ALU_XOR;
                    FUNCT3_ORI: decodeState.aluOperation = ALU_OR;
                    FUNCT3_ANDI: decodeState.aluOperation = ALU_AND;
                    FUNCT3_SLLI: decodeState.aluOperation = ALU_SHIFT_LEFT;
                    FUNCT3_SRLI_SRAI: begin
                        unique case (funct7)
                            FUNCT7_INT: decodeState.aluOperation = ALU_SHIFT_RIGHT_LOGICAL;
                            FUNCT7_ALT_INT: decodeState.aluOperation = ALU_SHIFT_RIGHT_ARITHMETIC;
                            default: begin
                                decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                                decodeState.writeBackSelect = NO_WRITE_BACK;
                            end
                        endcase
                    end
                    default: begin
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                        decodeState.writeBackSelect = NO_WRITE_BACK;
                    end
                endcase
            end
            OP_LOAD: begin
                decodeState.instructionType = NORMAL;
                decodeState.aluOperation = ALU_ADD;
                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                decodeState.aluOperand2 = ALU_SELECT_IMMEDIATE;
                rs1Request.isValid = 1'b1;
                decodeState.writeBackSelect = WRITE_BACK_SELECT_MEMORY;
                immediateType = I_TYPE;
                unique case(itype_load_funct3)
                    FUNCT3_LB: decodeState.memoryOperation = LOAD_BYTE;
                    FUNCT3_LH: decodeState.memoryOperation = LOAD_HALF_WORD;
                    FUNCT3_LW: decodeState.memoryOperation = LOAD_WORD;
                    FUNCT3_LBU: decodeState.memoryOperation = LOAD_BYTE_UNSIGNED;
                    FUNCT3_LHU: decodeState.memoryOperation = LOAD_HALF_WORD_UNSIGNED;
                    default: begin
                        decodeState.writeBackSelect = NO_WRITE_BACK;
                        decodeState.memoryOperation = NO_MEMORY_OPERATION;
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                    end
                endcase
            end
            OP_STORE: begin
                decodeState.instructionType = NORMAL;
                decodeState.aluOperation = ALU_ADD;
                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                decodeState.aluOperand2 = ALU_SELECT_IMMEDIATE;
                rs1Request.isValid = 1'b1;
                rs2Request.isValid = 1'b1;
                immediateType = S_TYPE;
                unique case(stype_funct3)
                    FUNCT3_SB: decodeState.memoryOperation = STORE_BYTE;
                    FUNCT3_SH: decodeState.memoryOperation = STORE_HALF_WORD;
                    FUNCT3_SW: decodeState.memoryOperation = STORE_WORD;
                    default: begin
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                        decodeState.memoryOperation = NO_MEMORY_OPERATION;
                    end
                endcase
            end
            OP_AMO: begin
                decodeState.instructionType = NORMAL;
                decodeState.aluOperation = ALU_ADD;
                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                decodeState.aluOperand2 = ALU_SELECT_ZERO;
                rs1Request.isValid = 1'b1;
                decodeState.writeBackSelect = WRITE_BACK_SELECT_MEMORY;
                unique case(amo_funct5)
                    FUNCT5_LR: begin
                        decodeState.memoryOperation = LOAD_RESERVED;
                    end
                    FUNCT5_SC: begin
                        rs2Request.isValid = 1'b1;
                        decodeState.memoryOperation = STORE_CONDITIONAL;
                    end
                    default: begin
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                        decodeState.memoryOperation = NO_MEMORY_OPERATION;
                        decodeState.writeBackSelect = NO_WRITE_BACK;
                    end
                endcase
            end
            OP_LUI: begin
                decodeState.instructionType = NORMAL;
                immediateType = U_TYPE;

                decodeState.aluOperation = ALU_ADD;
                decodeState.aluOperand1 = ALU_SELECT_ZERO;
                decodeState.aluOperand2 = ALU_SELECT_IMMEDIATE;

                decodeState.writeBackSelect = WRITE_BACK_SELECT_ALU;
            end

            OP_AUIPC: begin
                decodeState.instructionType = NORMAL;
                immediateType = U_TYPE;

                decodeState.aluOperation = ALU_ADD;
                decodeState.aluOperand1 = ALU_SELECT_PC;
                decodeState.aluOperand2 = ALU_SELECT_IMMEDIATE;

                decodeState.writeBackSelect = WRITE_BACK_SELECT_ALU;
            end

            OP_JAL: begin
                decodeState.instructionType = JUMP;
                immediateType = UJ_TYPE;

                decodeState.jumpBaseSelect = JUMP_SELECT_PC;
                decodeState.writeBackSelect = WRITE_BACK_SELECT_PC_PLUS_4;
            end

            OP_JALR: begin
                decodeState.instructionType = JUMP;
                immediateType = I_TYPE;

                decodeState.jumpBaseSelect = JUMP_SELECT_RS1;
                decodeState.writeBackSelect = WRITE_BACK_SELECT_PC_PLUS_4;

                rs1Request.isValid = 1'b1;
            end
            OP_BRANCH: begin
                decodeState.instructionType = BRANCH;
                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                decodeState.aluOperand2 = ALU_SELECT_REGISTER;
                immediateType = SB_TYPE;
                rs1Request.isValid = 1'b1;
                rs2Request.isValid = 1'b1;
                decodeState.jumpBaseSelect = JUMP_SELECT_PC;
                unique case(sbtype_funct3)
                    FUNCT3_BEQ: decodeState.aluOperation = ALU_EQUAL;
                    FUNCT3_BNE: decodeState.aluOperation = ALU_NOT_EQUAL;
                    FUNCT3_BLT: decodeState.aluOperation = ALU_LESS_THAN_SIGNED;
                    FUNCT3_BGE: decodeState.aluOperation = ALU_GREATER_THAN_OR_EQUAL_SIGNED;
                    FUNCT3_BLTU: decodeState.aluOperation = ALU_LESS_THAN_UNSIGNED;
                    FUNCT3_BGEU: decodeState.aluOperation = ALU_GREATER_THAN_OR_EQUAL_UNSIGNED;
                    default: begin
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                    end
                endcase
            end
            OP_SYSTEM: begin
                unique case(itype_system_funct3)
                    FUNCT3_PRIV: begin
                        unique casez(itype_funct12)
                            FUNCT12_ECALL: begin
                                rs1Request.number = isa_reg_t'(`ECALL_ARG_REGISTER);
                                rs1Request.isValid = 1'b1;
                                decodeState.instructionType = PRIVILEGE_RAISE;
                            end
                            FUNCT12_MRET: begin
                                if(
                                    csrObserver.currentLevel == MACHINE_MODE
                                ) begin
                                    decodeState.instructionType =
                                        PRIVILEGE_LOWER_MRET;
                                end else begin
                                    decodeState.instructionType =
                                        ILLEGAL_INSTRUCTION_FAULT;
                                end
                            end
                            FUNCT12_SRET: begin
                                if(csrObserver.currentLevel == MACHINE_MODE ||
                                    csrObserver.currentLevel == SUPERVISOR_MODE
                                ) begin
                                    decodeState.instructionType =
                                        PRIVILEGE_LOWER_SRET;
                                end else begin
                                    decodeState.instructionType =
                                        ILLEGAL_INSTRUCTION_FAULT;
                                end
                            end
                            `FUNCT12_SFENCE: begin
                                rs1Request.isValid = 1'b1;
                                rs2Request.isValid = 1'b1;
                                decodeState.memoryOperation = FLUSH_TRANSLATION;

                                decodeState.aluOperation = ALU_ADD;
                                decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                                decodeState.aluOperand2 = ALU_SELECT_ZERO;
                                unique case(csrObserver.currentLevel)
                                    MACHINE_MODE: decodeState.instructionType = FENCE;
                                    SUPERVISOR_MODE: decodeState.instructionType = FENCE;
                                    USER_MODE: begin
                                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                                        decodeState.memoryOperation = NO_MEMORY_OPERATION;
                                    end
                                endcase
                            end
                            default: begin
                                decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                                decodeState.memoryOperation = NO_MEMORY_OPERATION;
                            end
                        endcase
                    end
                    FUNCT3_CSRRW: begin
                        decodeState.instructionType = CSR;
                        decodeState.csrOperation = CSR_WRITE;

                        rs1Request.isValid = 1'b1;
                        decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                        decodeState.aluOperand2 = ALU_SELECT_ZERO;
                        decodeState.aluOperation = ALU_ADD;
                        decodeState.writeBackSelect = WRITE_BACK_SELECT_CSR;
                    end
                    FUNCT3_CSRRS: begin
                        decodeState.instructionType = CSR;
                        decodeState.csrOperation = (rs1Request.number == X0) ? CSR_X0 : CSR_WRITE;

                        rs1Request.isValid = 1'b1;
                        decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                        decodeState.aluOperand2 = ALU_SELECT_CSR;
                        decodeState.aluOperation = ALU_OR;
                        decodeState.writeBackSelect = WRITE_BACK_SELECT_CSR;
                    end
                    FUNCT3_CSRRC: begin
                        decodeState.instructionType = CSR;
                        decodeState.csrOperation = (rs1Request.number == X0) ? CSR_X0 : CSR_WRITE;

                        rs1Request.isValid = 1'b1;
                        decodeState.aluOperand1 = ALU_SELECT_REGISTER;
                        decodeState.aluOperand2 = ALU_SELECT_CSR;
                        decodeState.aluOperation = ALU_CLEAR_MASK;
                        decodeState.writeBackSelect = WRITE_BACK_SELECT_CSR;
                    end
                    FUNCT3_CSRRWI: begin
                        decodeState.instructionType = CSR;
                        decodeState.csrOperation = CSR_WRITE;

                        immediateType = CSR_TYPE;
                        decodeState.aluOperand1 = ALU_SELECT_IMMEDIATE;
                        decodeState.aluOperand2 = ALU_SELECT_ZERO;
                        decodeState.aluOperation = ALU_ADD;
                        decodeState.writeBackSelect = WRITE_BACK_SELECT_CSR;
                    end
                    FUNCT3_CSRRSI: begin
                        decodeState.instructionType = CSR;
                        decodeState.csrOperation = (rs1Request.number == X0) ? CSR_X0 : CSR_WRITE;

                        immediateType = CSR_TYPE;
                        decodeState.aluOperand1 = ALU_SELECT_IMMEDIATE;
                        decodeState.aluOperand2 = ALU_SELECT_CSR;
                        decodeState.aluOperation = ALU_OR;
                        decodeState.writeBackSelect = WRITE_BACK_SELECT_CSR;
                    end
                    FUNCT3_CSRRCI: begin
                        decodeState.instructionType = CSR;
                        decodeState.csrOperation = (rs1Request.number == X0) ? CSR_X0 : CSR_WRITE;

                        immediateType = CSR_TYPE;
                        decodeState.aluOperand1 = ALU_SELECT_IMMEDIATE;
                        decodeState.aluOperand2 = ALU_SELECT_CSR;
                        decodeState.aluOperation = ALU_CLEAR_MASK;
                        decodeState.writeBackSelect = WRITE_BACK_SELECT_CSR;
                    end
                    default: begin
                        decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
                        decodeState.writeBackSelect = NO_WRITE_BACK;
                        decodeState.csrOperation = NO_CSR_OPERATION;
                        decodeState.memoryOperation = NO_MEMORY_OPERATION;
                    end
                endcase
            end
            default: begin
                decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
            end
        endcase

        if(decodeState.instructionType == CSR &&
            !hasSuffientPermissions(decodeState.csrName, csrObserver.currentLevel)
        ) begin
            decodeState.instructionType = ILLEGAL_INSTRUCTION_FAULT;
            decodeState.writeBackSelect = NO_WRITE_BACK;
            decodeState.csrOperation = NO_CSR_OPERATION;
        end

        if(decodeState.instructionType == ILLEGAL_INSTRUCTION_FAULT) begin
            decodeState.memoryOperation = NO_MEMORY_OPERATION;
            decodeState.writeBackSelect = NO_WRITE_BACK;
            decodeState.csrOperation = NO_CSR_OPERATION;
        end
    end

    rdValidWriteBackRegisterFile: assert property (@(posedge clock) disable iff(clear)
        decodeState.rd.isValid |-> decodeState.writeBackSelect != NO_WRITE_BACK
    );

    writeBackSelectedRDValid: assert property (@(posedge clock) disable iff(clear)
        decodeState.writeBackSelect != NO_WRITE_BACK |-> decodeState.rd.isValid
    );
endmodule

module ImmediateDecoder(
    input Word_t instruction,
    input ImmediateType_t immediateType,

    output Immediate_t immediate
);

    Word_t value;

    assign immediate = '{
        immediateType: immediateType,
        value: value
    };

    always_comb begin
        value = `WORD_POISON;
        unique case(immediateType)
            NO_IMMEDIATE: value = 32'd0;
            I_TYPE: value = {{21{instruction[31]}}, instruction[30:20]};
            U_TYPE: value = {instruction[31:12], 12'd0};
            SB_TYPE: value = {{20{instruction[31]}}, instruction[7], instruction[30:25], instruction[11:8], 1'b0};
            UJ_TYPE: value = {{12{instruction[31]}}, instruction[19:12], instruction[20], instruction[30:21], 1'b0};
            S_TYPE: value = {{21{instruction[31]}}, instruction[30:25], instruction[11:7]};
            CSR_TYPE: value = {27'd0, instruction[19:15]};
        endcase
    end

endmodule