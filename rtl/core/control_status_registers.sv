//# PRINT AFTER include/core/control_status_registers.vh
`default_nettype none

`include "control_status_registers.vh"
`include "compiler.vh"
`include "memory.vh"
`include "pipeline.vh"
`include "config.vh"
`include "riscv_types.vh"
`include "decode.vh"
`include "riscv_csr.vh"

`define __INSTALL_COUNTER(name, increment) \
    generate \
        logic [63:0] name;\
        Counter #(.WIDTH($bits(name))) name``_counter(\
            .clock, \
            .clear, \
            .enable((increment)), \
            .q(name)\
        ); \
    endgenerate

`define __INSTALL_COUNTER32(name, increment) \
    generate \
        logic [63:0] name;\
        assign name[63:32] = 32'b0;\
        name``noOverflow: assert property (@(posedge clock) disable iff(clear)\
            name[31:0] != 32'hffffffff\
        );\
        Counter #(.WIDTH(32)) name``_counter(\
            .clock,\
            .clear,\
            .enable((increment)),\
            .q(name[31:0])\
        );\
    endgenerate

`define __INSTALL_IF_VALID_COUNTER(name, increment) \
    `__INSTALL_COUNTER(name, ((increment) && memoryWait_WB.fetch.valid))

`define __INSTALL_LOG(name)\
    always @(posedge clock) begin \
        if(dump && !clear) begin \
            $display("Counter: %-35s Value: %d", "name", name);\
        end\
    end

`ifdef SIMULATION_18447
`define INSTALL_COUNTER(name, increment) \
    `__INSTALL_COUNTER(name, increment);\
    `__INSTALL_LOG(name)

`define INSTALL_IF_VALID_COUNTER(name, increment) \
    `__INSTALL_IF_VALID_COUNTER(name, increment);\
    `__INSTALL_LOG(name)

`define INSTALL_COUNTER32(name, increment)\
    `__INSTALL_COUNTER32(name, increment);\
    `__INSTALL_LOG(name)

`else
    `define INSTALL_COUNTER(name, increment) `__INSTALL_COUNTER(name, increment)
    `define INSTALL_IF_VALID_COUNTER(name, increment) \
        `__INSTALL_IF_VALID_COUNTER(name, increment)
    `define INSTALL_COUNTER32(name, increment) \
        `__INSTALL_COUNTER32(name, increment)
`endif

`define INSTALL_SIMPLE_CSR(name, variable)\
    generate\
        Word_t variable;\
        SimpleControlStatusRegister #(.NAME(name)) name``_register(\
            .request(request_WB),\
            .register(variable),\
            .clock,\
            .clear\
        );\
    endgenerate

`define CSR_PASS_PARAM\
    .status,\
    .stvec,\
    .sscratch,\
    .sepc,\
    .scause,\
    .stval,\
    .satp,\
    .medeleg,\
    .mideleg,\
    .mtvec,\
    .mscratch,\
    .mepc,\
    .mcause,\
    .mtval,\
    .cycleCount,\
    .instructionsRetired,\
    .instructionsFetched,\
    .branchesRetired,\
    .forwardBranchesPredictedCorrectly,\
    .forwardBranchesPredictedIncorrectly,\
    .backwardBranchesPredictedCorrectly,\
    .backwardBranchesPredictedIncorrectly,\
    .jumpsRetired,\
    .jumpsPredictedCorrectly,\
    .jumpsPredictedIncorrectly,\
    .returnPredictedCorrectly,\
    .returnPredictedIncorrectly,\
    .instructionTLBHits,\
    .instructionTLBMisses,\
    .instructionCacheHits,\
    .instructionCacheMisses,\
    .dataTLBHits,\
    .dataTLBMisses,\
    .dataCacheHits,\
    .dataCacheMisses,\
    .dataVRAMHits,\
    .dataRemoteHits,\
    .storeConditionalSuccesses,\
    .storeConditionalRejections,\
    .keyboardInterruptedUser,\
    .keyboardInterruptedSupervisor,\
    .timerInterruptedUser,\
    .timerInterruptedSupervisor,\
    .exceptionsInUserMode,\
    .exceptionsInSupervisorMode,\
    .btbHit

module ControlStatusRegisters (
    CSRInterruptDelivery.ControlStatusRegister interruptDelivery,
    CSRInterruptReturn.ControlStatusRegister interruptReturn,

    MemoryControl.Core memoryControl,

    CSRReadInterface.ControlStatusRegister request_ID, remote,
    CSRWriteInterface.ControlStatusRegister request_WB,

    CSRCoreObserver.ControlStatusRegister observer,

    // Used to update performance counters
    input InstructionWaitExport_t instructionWait_ID,
    input MemoryWaitExport_t memoryWait_WB,


    `ifdef SIMULATION_18447
    input logic dump,
    `endif

    input logic clock, clear, halted
);
    `STATE_FUNCTIONS;
    `CSR_FUNCTIONS;

    // These assumptions are pervasive throughout the core
    instructionTypeValid: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.instructionType != INVALID_INSTRUCTION |->
        memoryWait_WB.fetch.valid
    );

    validInstructionType: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid |->
        memoryWait_WB.decode.instructionType != INVALID_INSTRUCTION
    );

    AddressTranslationProtection_t satp;
    `STATIC_ASSERT($bits(satp) == `XLEN, satp_width);

    SimpleControlStatusRegister #(.NAME(SATP)) satpRegister(
        .request(request_WB),
        .register(satp),
        .clock,
        .clear
    );

    `INSTALL_SIMPLE_CSR(MTVEC, mtvec);
    `INSTALL_SIMPLE_CSR(MSCRATCH, mscratch);
    `INSTALL_SIMPLE_CSR(MIDELEG, mideleg);
    `INSTALL_SIMPLE_CSR(MEDELEG, medeleg);

    Status_t status;
    `STATIC_ASSERT($bits(Status_t) == `XLEN, status_width);
    Word_t mepc, mtval, sepc, stval;
    ExceptionCause_t mcause, scause;
    `STATIC_ASSERT($bits(mcause) == `XLEN, cause_width);
    PrivilegeLevel_t currentLevel;

    `INSTALL_SIMPLE_CSR(STVEC, stvec);
    `INSTALL_SIMPLE_CSR(SSCRATCH, sscratch);


    assign observer.status = status;
    assign observer.currentLevel = currentLevel;
    assign observer.mideleg = mideleg;

    always_comb begin
        interruptDelivery.trapVector = `WORD_POISON;
        if(interruptDelivery.deliverInterrupt) begin
            if(deliverToSupervisor(interruptDelivery.cause)) begin
                interruptDelivery.trapVector = stvec;
            end else begin
                interruptDelivery.trapVector = mtvec;
            end
        end
    end

    always_comb begin
        unique if(interruptReturn.instructionType == PRIVILEGE_LOWER_MRET) begin
            interruptReturn.epc = mepc;
        end else if(interruptReturn.instructionType == PRIVILEGE_LOWER_SRET) begin
            interruptReturn.epc = sepc;
        end else begin
            interruptReturn.epc = `WORD_POISON;
        end
    end

    deliverInterruptUnique: assert property (@(posedge clock) disable iff(clear)
        interruptDelivery.deliverInterrupt |->
        interruptReturn.instructionType != PRIVILEGE_LOWER_MRET &&
        !request_WB.writeEnable &&
        interruptReturn.instructionType != PRIVILEGE_LOWER_SRET
    );

    interruptReturnUnique: assert property (@(posedge clock) disable iff(clear)
        interruptReturn.instructionType == PRIVILEGE_LOWER_SRET ||
        interruptReturn.instructionType == PRIVILEGE_LOWER_MRET |->
        !interruptDelivery.deliverInterrupt && !request_WB.writeEnable
    );

    writeUnique: assert property (@(posedge clock) disable iff(clear)
        request_WB.writeEnable |->
        interruptReturn.instructionType != PRIVILEGE_LOWER_MRET &&
        interruptReturn.instructionType != PRIVILEGE_LOWER_SRET &&
        !interruptDelivery.deliverInterrupt
    );

    statusZeroConsistent: assert property (@(posedge clock) disable iff(clear)
        status.zero4 == 8'b0 && status.zero3 == 2'd0 &&
        status.zero2 == 1'd0 && status.zero1 == 1'd0
    );

    function automatic logic deliverToSupervisor(Word_t cause);
        if(currentLevel == MACHINE_MODE) return 1'b0;
        if(isInterruptCause(cause)) return isDelegated(mideleg, cause);
        else return isDelegated(medeleg, cause);
    endfunction

    always_ff @(posedge clock) begin
        if(clear) begin
            status <= '{default:0};
            mepc <= 32'b0;
            mtval <= 32'b0;
            mcause <= ExceptionCause_t'(32'd0);
            sepc <= 32'b0;
            stval <= 32'b0;
            scause <= ExceptionCause_t'(32'd0);
            currentLevel <= MACHINE_MODE;
        end else begin
            // Status
            if (interruptDelivery.deliverInterrupt) begin
                // Interrupt Delivery
                if(deliverToSupervisor(interruptDelivery.cause)) begin
                    status.sie <= INTERRUPTS_DISABLED;
                    status.spie <= status.sie;
                    status.spp <= currentLevel[0];

                    currentLevel <= SUPERVISOR_MODE;

                    sepc <= interruptDelivery.epc;
                    scause <= interruptDelivery.cause;
                    stval <= interruptDelivery.value;
                end else begin
                    status.mie <= INTERRUPTS_DISABLED;
                    status.mpie <= status.mie;
                    status.mpp <= currentLevel;

                    currentLevel <= MACHINE_MODE;

                    mepc <= interruptDelivery.epc;
                    mcause <= interruptDelivery.cause;
                    mtval <= interruptDelivery.value;
                end
            end else if (interruptReturn.instructionType == PRIVILEGE_LOWER_MRET) begin
                // Interrupt Return
                status.mie <= status.mpie;
                status.mpp <= USER_MODE;
                status.mpie <= INTERRUPTS_ENABLED;

                currentLevel <= status.mpp;
            end else if(interruptReturn.instructionType == PRIVILEGE_LOWER_SRET) begin
                status.sie <= status.spie;
                status.spp <= USER_MODE;
                status.spie <= INTERRUPTS_ENABLED;

                currentLevel <= PrivilegeLevel_t'(status.spp);
            end else if(request_WB.writeEnable) begin
                case(request_WB.name)
                    MSTATUS: status <= request_WB.value & `MSTATUS_WRITE_MASK;
                    SSTATUS: status <= (request_WB.value & `SSTATUS_WRITE_MASK)
                                     | (status & ~`SSTATUS_WRITE_MASK);
                    MEPC: mepc <= request_WB.value;
                    SEPC: sepc <= request_WB.value;
                    MTVAL: mtval <= request_WB.value;
                    STVAL: stval <= request_WB.value;
                    MCAUSE: mcause <= request_WB.value;
                    SCAUSE: scause <= request_WB.value;
                endcase
            end
        end
    end

    // Memory Control
    always_comb begin
        // FIXME: It would be great it there was an extra register before we use satp and even status here to make timing easier. The challenge with just adding one now is that we need to be extra careful that the control flow handoff does the right thing and stalls the pipeline long enough
        memoryControl.supervisorPagesEnabled = (currentLevel == SUPERVISOR_MODE || currentLevel == MACHINE_MODE);
        memoryControl.userPagesEnabled = (currentLevel == USER_MODE || (status.sum && memoryControl.supervisorPagesEnabled));
        memoryControl.virtualMemoryEnabled = satp.virtualMemoryEnabled;
        memoryControl.rootPageTable = {satp.rootPageTablePageNumber, 12'b0};
    end

    // Performance Counters

    `INSTALL_COUNTER(cycleCount, !halted);
    `INSTALL_COUNTER(instructionsRetired, memoryWait_WB.fetch.valid);
    `INSTALL_COUNTER(instructionsFetched, instructionWait_ID.fetch.valid);

    // Branches

    branchHasDirection: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid && isBranch(memoryWait_WB) |->
            (getDirection(memoryWait_WB) == FORWARD) ||
            (getDirection(memoryWait_WB) == BACKWARD)
    );

    branchHasPredictionResult: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid && isBranch(memoryWait_WB) |->
            (getPrediction(memoryWait_WB) == CORRECT_PREDICTION) ||
            (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        branchesRetired, isBranch(memoryWait_WB)
    );

    `INSTALL_IF_VALID_COUNTER(
        forwardBranchesPredictedCorrectly,
        isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == FORWARD) &&
        (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        forwardBranchesPredictedIncorrectly,
        isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == FORWARD) &&
        (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        backwardBranchesPredictedCorrectly,
        isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == BACKWARD) &&
        (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        backwardBranchesPredictedIncorrectly,
        isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == BACKWARD) &&
        (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)
    );

    // Jumps

    jumpHasDirection: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid && isJump(memoryWait_WB) |->
            (getDirection(memoryWait_WB) == FORWARD) ||
            (getDirection(memoryWait_WB) == BACKWARD)
    );

    jumpHasPredictionResult: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid && isJump(memoryWait_WB) |->
            (getPrediction(memoryWait_WB) == CORRECT_PREDICTION) ||
            (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        jumpsRetired, isJump(memoryWait_WB)
    );

    `INSTALL_IF_VALID_COUNTER(
        jumpsPredictedCorrectly,
        isJump(memoryWait_WB) &&
        (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        jumpsPredictedIncorrectly,
        isJump(memoryWait_WB) &&
        (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        returnPredictedCorrectly,
        isJump(memoryWait_WB) && isJumpReturn(memoryWait_WB) &&
        (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)
    );

    `INSTALL_IF_VALID_COUNTER(
        returnPredictedIncorrectly,
        isJump(memoryWait_WB) && isJumpReturn(memoryWait_WB) &&
        (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)
    );

    `INSTALL_COUNTER(
        btbHit,
        (isJump(memoryWait_WB) || isBranch(memoryWait_WB)) &&
        isBTBHit(memoryWait_WB)
    );

    // Instruction Memory Access

    instructionTranslationTypeValid: assert property(@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid |->
            (getInstructionTranslationType(memoryWait_WB) != INVALID_TRANSLATION) &&
            (getInstructionTranslationType(memoryWait_WB) != TLB_INVALIDATE)
    );

    instructionMemoryResponseTypeValid: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid |->
            (getInstructionResponseType(memoryWait_WB) != INVALID_MEMORY_RESPONSE) &&
            (getInstructionResponseType(memoryWait_WB) != VRAM_IO) &&
            (getInstructionResponseType(memoryWait_WB) != REMOTE_IO) &&
            (getInstructionResponseType(memoryWait_WB) != TRANSLATION_INVALIDATE)
    );

    `INSTALL_IF_VALID_COUNTER(
        instructionTLBHits,
        getInstructionTranslationType(memoryWait_WB) == TLB_HIT
    );

    `INSTALL_IF_VALID_COUNTER(
        instructionTLBMisses,
        getInstructionTranslationType(memoryWait_WB) == TLB_MISS
    );

    `INSTALL_IF_VALID_COUNTER(
        instructionCacheHits,
        getInstructionResponseType(memoryWait_WB) == CACHE_HIT
    );

    `INSTALL_IF_VALID_COUNTER(
        instructionCacheMisses,
        getInstructionResponseType(memoryWait_WB) == CACHE_MISS
    );

    // Data Memory Access

    memoryOperationValid: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.memoryOperation != NO_MEMORY_OPERATION |-> memoryWait_WB.fetch.valid
    );

    dataTranslationTypeValid: assert property(@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.memoryOperation != NO_MEMORY_OPERATION |->
            (getDataTranslationType(memoryWait_WB) != INVALID_TRANSLATION) ||
            memoryWait_WB.memoryRequest.storeConditionalResult == STORE_REJECTED
    );

    dataTranslationTypeMemoryOperation: assert property (@(posedge clock) disable iff(clear)
        getDataTranslationType(memoryWait_WB) != INVALID_TRANSLATION |->
        memoryWait_WB.decode.memoryOperation != NO_MEMORY_OPERATION
    );

    dataMemoryResponseTypeValid: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.decode.memoryOperation != NO_MEMORY_OPERATION |->
            (getDataResponseType(memoryWait_WB) != INVALID_MEMORY_RESPONSE) ||
            memoryWait_WB.memoryRequest.storeConditionalResult == STORE_REJECTED
    );

    dataResponseTypeMemoryOperation: assert property (@(posedge clock) disable iff(clear)
        (getDataResponseType(memoryWait_WB) != INVALID_MEMORY_RESPONSE) |->
        memoryWait_WB.decode.memoryOperation != NO_MEMORY_OPERATION
    );

    `INSTALL_IF_VALID_COUNTER(
        dataTLBHits,
        getDataTranslationType(memoryWait_WB) == TLB_HIT
    );

    `INSTALL_IF_VALID_COUNTER(
        dataTLBMisses,
        getDataTranslationType(memoryWait_WB) == TLB_MISS
    );

    `INSTALL_IF_VALID_COUNTER(
        dataCacheHits,
        getDataResponseType(memoryWait_WB) == CACHE_HIT
    );

    `INSTALL_IF_VALID_COUNTER(
        dataCacheMisses,
        getDataResponseType(memoryWait_WB) == CACHE_MISS
    );

    `INSTALL_IF_VALID_COUNTER(
        dataVRAMHits,
        getDataResponseType(memoryWait_WB) == VRAM_IO
    );

    `INSTALL_IF_VALID_COUNTER(
        dataRemoteHits,
        getDataResponseType(memoryWait_WB) == REMOTE_IO
    );

    // Store Conditional

    storeOperationHasResult: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid && (getMemoryOperation(memoryWait_WB) == STORE_CONDITIONAL) |->
        (getStoreConditionalResult(memoryWait_WB) != NO_CONDITIONAL)
    );

    storeConditionalResultWithOperation: assert property (@(posedge clock) disable iff(clear)
        memoryWait_WB.fetch.valid && (getStoreConditionalResult(memoryWait_WB) != NO_CONDITIONAL) |->
        (getMemoryOperation(memoryWait_WB) == STORE_CONDITIONAL)
    );

    `INSTALL_IF_VALID_COUNTER(
        storeConditionalSuccesses,
        (getStoreConditionalResult(memoryWait_WB) == STORE_SUCCESS)
    );

    `INSTALL_IF_VALID_COUNTER(
        storeConditionalRejections,
        (getStoreConditionalResult(memoryWait_WB) == STORE_REJECTED)
    );

    // Interrupts
    `INSTALL_COUNTER32(
        keyboardInterruptedUser,
        interruptDelivery.deliverInterrupt &&
        (interruptDelivery.cause == EXTERNAL_INTERRUPT) &&
        currentLevel == USER_MODE
    );

    `INSTALL_COUNTER32(
        keyboardInterruptedSupervisor,
        interruptDelivery.deliverInterrupt &&
        (interruptDelivery.cause == EXTERNAL_INTERRUPT) &&
        (currentLevel == SUPERVISOR_MODE || currentLevel == MACHINE_MODE)
    );

    `INSTALL_COUNTER(
        timerInterruptedUser,
        interruptDelivery.deliverInterrupt &&
        (interruptDelivery.cause == TIMER_INTERRUPT) &&
        currentLevel == USER_MODE
    );

    `INSTALL_COUNTER(
        timerInterruptedSupervisor,
        interruptDelivery.deliverInterrupt &&
        (interruptDelivery.cause == TIMER_INTERRUPT) &&
        (currentLevel == SUPERVISOR_MODE || currentLevel == MACHINE_MODE)
    );

    `INSTALL_COUNTER32(
        exceptionsInUserMode,
        interruptDelivery.deliverInterrupt &&
        (interruptDelivery.cause != TIMER_INTERRUPT && interruptDelivery.cause != EXTERNAL_INTERRUPT) &&
        (currentLevel == USER_MODE)
    );

    `INSTALL_COUNTER32(
        exceptionsInSupervisorMode,
        interruptDelivery.deliverInterrupt &&
        (interruptDelivery.cause != TIMER_INTERRUPT && interruptDelivery.cause != EXTERNAL_INTERRUPT) &&
        (currentLevel == SUPERVISOR_MODE || currentLevel == MACHINE_MODE)
    );

    CSRRead csrRead_ID(
        .request(request_ID),
        `CSR_PASS_PARAM
    );

    CSRRead csrRead_remote(
        .request(remote),
        `CSR_PASS_PARAM
    );

endmodule

module CSRRead(
    CSRReadInterface.ControlStatusRegister request,

    input Status_t status,

    input Word_t stvec,
    input Word_t sscratch,
    input Word_t sepc,
    input ExceptionCause_t scause,
    input Word_t stval,
    input AddressTranslationProtection_t satp,

    input Word_t medeleg,
    input Word_t mideleg,
    input Word_t mtvec,
    input Word_t mscratch,
    input Word_t mepc,
    input ExceptionCause_t mcause,
    input Word_t mtval,

    input logic [63:0] cycleCount,
    input logic [63:0] instructionsRetired,
    input logic [63:0] instructionsFetched,
    input logic [63:0] branchesRetired,
    input logic [63:0] forwardBranchesPredictedCorrectly,
    input logic [63:0] forwardBranchesPredictedIncorrectly,
    input logic [63:0] backwardBranchesPredictedCorrectly,
    input logic [63:0] backwardBranchesPredictedIncorrectly,
    input logic [63:0] jumpsRetired,
    input logic [63:0] jumpsPredictedCorrectly,
    input logic [63:0] jumpsPredictedIncorrectly,
    input logic [63:0] returnPredictedCorrectly,
    input logic [63:0] returnPredictedIncorrectly,
    input logic [63:0] instructionTLBHits,
    input logic [63:0] instructionTLBMisses,
    input logic [63:0] instructionCacheHits,
    input logic [63:0] instructionCacheMisses,
    input logic [63:0] dataTLBHits,
    input logic [63:0] dataTLBMisses,
    input logic [63:0] dataCacheHits,
    input logic [63:0] dataCacheMisses,
    input logic [63:0] dataVRAMHits,
    input logic [63:0] dataRemoteHits,
    input logic [63:0] storeConditionalSuccesses,
    input logic [63:0] storeConditionalRejections,
    input logic [63:0] keyboardInterruptedUser,
    input logic [63:0] keyboardInterruptedSupervisor,
    input logic [63:0] timerInterruptedUser,
    input logic [63:0] timerInterruptedSupervisor,
    input logic [63:0] exceptionsInUserMode,
    input logic [63:0] exceptionsInSupervisorMode,
    input logic [63:0] btbHit
);

    function automatic Word_t lowWord(logic [63:0] counter);
        return counter[31:0];
    endfunction

    function automatic Word_t highWord(logic [63:0] counter);
        return counter[63:32];
    endfunction

    // Dummy since we want to make sure the types are included properly
    PerformanceCounter_t performanceCounter;
    assign performanceCounter = PerformanceCounter_t'(request.name);

    always_comb begin
        request.value = 32'b0;
        unique case(request.name)
            NONE: request.value = 32'b0;

            SSTATUS: request.value = status & `SSTATUS_WRITE_MASK;
            MSTATUS: request.value = status & `MSTATUS_WRITE_MASK;


            STVEC: request.value = stvec;
            SSCRATCH: request.value = sscratch;
            SEPC: request.value = sepc;
            SCAUSE: request.value = scause;
            STVAL: request.value = stval;
            SATP: request.value = satp;

            MEDELEG: request.value = medeleg;
            MIDELEG: request.value = mideleg;
            MTVEC: request.value = mtvec;
            MSCRATCH: request.value = mscratch;
            MEPC: request.value = mepc;
            MCAUSE: request.value = mcause;
            MTVAL: request.value = mtval;

            CYCLE_COUNT: request.value = lowWord(cycleCount);
            CYCLE_COUNTH: request.value = highWord(cycleCount);
            INSTRUCTIONS_RETIRED: request.value =  lowWord(instructionsRetired);
            INSTRUCTIONS_RETIREDH: request.value = highWord(instructionsRetired);
            INSTRUCTIONS_FETCHED: request.value = lowWord(instructionsFetched);
            INSTRUCTIONS_FETCHEDH: request.value = highWord(instructionsFetched);

            BRANCH_RETIRED: request.value = lowWord(branchesRetired);
            BRANCH_RETIREDH: request.value = highWord(branchesRetired);
            FORWARD_BRANCH_PREDICTED_CORRECT: request.value = lowWord(forwardBranchesPredictedCorrectly);
            FORWARD_BRANCH_PREDICTED_CORRECTH: request.value = highWord(forwardBranchesPredictedCorrectly);
            BACKWARD_BRANCH_PREDICTED_CORRECT: request.value = lowWord(backwardBranchesPredictedCorrectly);
            BACKWARD_BRANCH_PREDICTED_CORRECTH: request.value = highWord(backwardBranchesPredictedCorrectly);
            FORWARD_BRANCH_PREDICTED_INCORRECT: request.value = lowWord(forwardBranchesPredictedIncorrectly);
            FORWARD_BRANCH_PREDICTED_INCORRECTH: request.value = highWord(forwardBranchesPredictedIncorrectly);
            BACKWARD_BRANCH_PREDICTED_INCORRECT: request.value = lowWord(backwardBranchesPredictedIncorrectly);
            BACKWARD_BRANCH_PREDICTED_INCORRECTH: request.value = highWord(backwardBranchesPredictedIncorrectly);

            JUMP_RETIRED: request.value = lowWord(jumpsRetired);
            JUMP_RETIREDH: request.value = highWord(jumpsRetired);
            JUMP_PREDICTED_CORRECT: request.value = lowWord(jumpsPredictedCorrectly);
            JUMP_PREDICTED_CORRECTH: request.value = highWord(jumpsPredictedCorrectly);
            JUMP_PREDICTED_INCORRECT: request.value = lowWord(jumpsPredictedIncorrectly);
            JUMP_PREDICTED_INCORRECTH: request.value = highWord(jumpsPredictedIncorrectly);
            RETURN_PREDICTED_CORRECT: request.value = lowWord(returnPredictedCorrectly);
            RETURN_PREDICTED_CORRECTH: request.value = highWord(returnPredictedCorrectly);
            RETURN_PREDICTED_INCORRECT: request.value = lowWord(returnPredictedIncorrectly);
            RETURN_PREDICTED_INCORRECTH: request.value = highWord(returnPredictedIncorrectly);

            BTB_HIT: request.value = lowWord(btbHit);
            BTB_HITH: request.value = highWord(btbHit);

            INSTRUCTION_TLB_HIT: request.value = lowWord(instructionTLBHits);
            INSTRUCTION_TLB_HITH: request.value = highWord(instructionTLBHits);
            INSTRUCTION_TLB_MISS: request.value = lowWord(instructionTLBMisses);
            INSTRUCTION_TLB_MISSH: request.value = highWord(instructionTLBMisses);
            INSTRUCTION_CACHE_HIT: request.value = lowWord(instructionCacheHits);
            INSTRUCTION_CACHE_HITH: request.value = highWord(instructionCacheHits);
            INSTRUCTION_CACHE_MISS: request.value = lowWord(instructionCacheMisses);
            INSTRUCTION_CACHE_MISSH: request.value = highWord(instructionCacheMisses);

            DATA_TLB_HIT: request.value = lowWord(dataTLBHits);
            DATA_TLB_HITH: request.value = highWord(dataTLBHits);
            DATA_TLB_MISS: request.value = lowWord(dataTLBMisses);
            DATA_TLB_MISSH: request.value = highWord(dataTLBMisses);
            DATA_CACHE_HIT: request.value = lowWord(dataCacheHits);
            DATA_CACHE_HITH: request.value = highWord(dataCacheHits);
            DATA_CACHE_MISS: request.value = lowWord(dataCacheMisses);
            DATA_CACHE_MISSH: request.value = highWord(dataCacheMisses);
            DATA_VRAM_HIT: request.value = lowWord(dataVRAMHits);
            DATA_VRAM_HITH: request.value = highWord(dataVRAMHits);
            DATA_REMOTE_HIT: request.value = lowWord(dataRemoteHits);
            DATA_REMOTE_HITH: request.value = highWord(dataRemoteHits);

            STORE_CONDITIONAL_SUCCESS: request.value = lowWord(storeConditionalSuccesses);
            STORE_CONDITIONAL_SUCCESSH: request.value = highWord(storeConditionalSuccesses);
            STORE_CONDITIONAL_REJECT: request.value = lowWord(storeConditionalRejections);
            STORE_CONDITIONAL_REJECTH: request.value = highWord(storeConditionalRejections);

            // Keyboard interrupt CSR is fused (low bits/high bits)
            KEYBOARD_INTERRUPT_USER: request.value = lowWord(keyboardInterruptedUser);
            KEYBOARD_INTERRUPT_SUPERVISOR: request.value = lowWord(keyboardInterruptedSupervisor);

            EXCEPTION_USER: request.value = lowWord(exceptionsInUserMode);
            EXCEPTION_USERH: request.value = highWord(exceptionsInUserMode);
            EXCEPTION_SUPERVISOR: request.value = lowWord(exceptionsInSupervisorMode);
            EXCEPTION_SUPERVISORH: request.value = highWord(exceptionsInSupervisorMode);

            TIMER_INTERRUPT_USER: request.value = lowWord(timerInterruptedUser);
            TIMER_INTERRUPT_USERH: request.value = highWord(timerInterruptedUser);
            TIMER_INTERRUPT_SUPERVISOR: request.value = lowWord(timerInterruptedSupervisor);
            TIMER_INTERRUPT_SUPERVISORH: request.value = highWord(timerInterruptedSupervisor);
            default: begin
                request.value = 32'b0;
            end
        endcase
    end

endmodule

module SimpleControlStatusRegister
    #(parameter ControlStatusRegisterName_t NAME = NONE)(
    CSRWriteInterface.ControlStatusRegister request,
    output Word_t register,
    input logic clock, clear
);

    logic csrWriteEnable;

    assign csrWriteEnable = request.writeEnable && (request.name == NAME);

    Register #(.WIDTH($bits(register))) csr(
        .d(request.value),
        .q(register),
        .enable(csrWriteEnable),
        .clock,
        .clear
    );

endmodule