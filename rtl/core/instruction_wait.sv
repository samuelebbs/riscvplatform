//# PRINT AFTER core/instruction_fetch.sv
`default_nettype none

`include "control_flow.vh"
`include "pipeline.vh"
`include "memory.vh"
`include "riscv_types.vh"
`include "fetch.vh"

module InstructionWaitStage(
    PipelineControl.Stage controlFlow,

    input InstructionFetchExport_t fetch_IW,

    input MemoryResponse_t memoryResponse,

    output InstructionWaitExport_t instructionWait_ID,

    input logic clock, clear
);

    validResponseType: assert property (@(posedge clock) disable iff(clear)
        fetch_IW.fetch.valid && memoryResponse.responseType != INVALID_MEMORY_RESPONSE |->
        (memoryResponse.responseType == CACHE_HIT) ||
        (memoryResponse.responseType == CACHE_MISS) ||
        (memoryResponse.responseType == PAGE_FAULT)
    );

    function automatic logic isValidMemoryResponse(MemoryResponse_t response, VirtualAddress_t expectedAddress);
        return (response.responseType != INVALID_MEMORY_RESPONSE) &&
                (response.request.virtualAddress == expectedAddress);
    endfunction

    // If we generate back pressure on fetch to stall, it will assume we are
    // holding onto the memoryResponse it already generated, so we should hold
    // it

    MemoryResponse_t memoryResponse_IF;

    logic validMemoryResponse_IF;

    assign validMemoryResponse_IF = isValidMemoryResponse(memoryResponse, fetch_IW.fetch.pc);

    // I think we can get away with loading this unconditionally when the
    // response is valid because if the response is valid that means it is
    // synchronized with the fetch pipeline state which is what we want if we
    // are putting back pressure on fetch
    Register #(.WIDTH($bits(memoryResponse_IF))) memoryResponseRegister(
        .q(memoryResponse_IF),
        .d(memoryResponse),
        .clock,
        .clear,
        .enable(validMemoryResponse_IF)
    );

    MemoryResponse_t memoryResponse_selected;

    // If the memory response we have saved matches the fetch pipeline state,
    // then we route that one as the selected state, otherwise, we assume that
    // we aren't in this special case, so we direct to whatever is the live
    // response.
    always_comb begin
        memoryResponse_selected = '{default:0};
        if(fetch_IW.fetch.valid) begin
            if(isValidMemoryResponse(memoryResponse_IF, fetch_IW.fetch.pc)) begin
                memoryResponse_selected = memoryResponse_IF;
            end else begin
                memoryResponse_selected = memoryResponse;
            end
        end
    end

    InstructionWaitState_t waitState_IW_END;

    assign waitState_IW_END.instruction = memoryResponse_selected;

    // We are ready to proceed whenever we have a valid memory response
    always_comb begin
        controlFlow.ready = 1'b0;
        controlFlow.fencing = 1'b0;
        if(fetch_IW.fetch.valid) begin
            // If the fetch is valid, we need to wait until there is a matching memory response
            controlFlow.ready = isValidMemoryResponse(memoryResponse_selected, fetch_IW.fetch.pc);
            // We need to mask this fencing state with the response being valid
            // in case we get a page fault on a danglingly memory operation
            controlFlow.fencing = (memoryResponse_selected.responseType == PAGE_FAULT) && controlFlow.ready;
        end else begin
            // If the fetch isn't valid, there is nothing for us to wait for so we are ready
            controlFlow.ready = 1'b1;
        end
    end

    assign controlFlow.idle = !fetch_IW.fetch.valid;
    assign controlFlow.decode = '{default:0};

    InstructionWaitExport_t waitExport_IW_END;

    assign waitExport_IW_END.fetch = fetch_IW.fetch;
    assign waitExport_IW_END.instructionWait = waitState_IW_END;

    Register #(.WIDTH($bits(waitExport_IW_END))) instructionWaitPipelineRegister(
        .d(waitExport_IW_END),
        .q(instructionWait_ID),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );

endmodule