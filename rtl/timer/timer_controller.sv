//# PRINT AFTER keyboard/keyboard_controller.sv
`default_nettype none

`include "interrupt.vh"
`include "remote.vh"
`include "config.vh"

module TimerController(
    InterruptDevice.Device interruptController,
    RemoteTimerInterface.Timer remote,
    input logic clock, clear
);

    // TODO: The RISC-V specification says that these fields should be exposed
    // as memory mapped IO

    logic [63:0] nextTimerInterrupt;
    logic [63:0] currentTime;

    Counter #(.WIDTH($bits(currentTime))) timerCounter(
        .clock,
        .clear,
        .enable(~remote.halted),
        .q(currentTime)
    );

    Register #(.WIDTH($bits(nextTimerInterrupt)), .CLEAR_VALUE(`TIMER_TICK_CYCLES)) nextTickRegister(
        .q(nextTimerInterrupt),
        .d(nextTimerInterrupt + `TIMER_TICK_CYCLES),
        .clock,
        .clear,
        .enable(interruptController.interruptAccepted)
    );

    // TODO: This should probably start at one, but may break software tests
    Word_t tickCount;
    Counter #(.WIDTH($bits(tickCount))) tickCounter(
        .clock,
        .clear,
        .enable(interruptController.interruptAccepted),
        .q(tickCount)
    );

    assign interruptController.interruptPending = (currentTime >= nextTimerInterrupt);
    assign interruptController.tval = tickCount;
    assign remote.currentTime = currentTime;
    assign remote.nextTickTime = nextTimerInterrupt;

endmodule