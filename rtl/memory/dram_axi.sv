//# PRINT AFTER memory/dram_wrapper.sv
`ifndef SIMULATION_18447

`default_nettype none

`include "dram.vh"
`include "compiler.vh"
`include "riscv_types.vh"

module DRAMAXIDriver #(
    // Width of M_AXI address bus.
    // The master generates the read and write addresses of width specified as C_M_AXI_ADDR_WIDTH.
    parameter integer C_M_AXI_ADDR_WIDTH    = 32,
    // Width of M_AXI data bus.
    // The master issues write data and accept read data where the width of the data bus is C_M_AXI_DATA_WIDTH
    parameter integer C_M_AXI_DATA_WIDTH    = 32,
    // Transaction number is the number of write
    // and read transactions the master will perform as a part of this example memory test.
    parameter integer C_M_TRANSACTIONS_NUM  = 16,
    parameter integer PHYSICAL_ADDRESS_OFFSET = 'h8000000
    )(
    DRAMAXIInterface.AXI dram,
    // AXI clock signal
    input logic  M_AXI_ACLK,
    // AXI active low reset signal
    input logic  M_AXI_ARESETN,

    // Master Interface Write Address Channel ports. Write address (issued by master)
    output logic [C_M_AXI_ADDR_WIDTH-1 : 0] M_AXI_AWADDR,
    // Write channel Protection type.
    // This signal indicates the privilege and security level of the transaction,
    // and whether the transaction is a data access or an instruction access.
    output logic [2 : 0] M_AXI_AWPROT,
    // Write address valid.
    // This signal indicates that the master signaling valid write address and control information.
    output logic  M_AXI_AWVALID,
    // Write address ready.
    // This signal indicates that the slave is ready to accept an address and associated control signals.
    input logic  M_AXI_AWREADY,
    // Master Interface Write Data Channel ports. Write data (issued by master)
    output logic [C_M_AXI_DATA_WIDTH-1 : 0] M_AXI_WDATA,
    // Write strobes.
    // This signal indicates which byte lanes hold valid data.
    // There is one write strobe bit for each eight bits of the write data bus.
    output logic [C_M_AXI_DATA_WIDTH/8-1 : 0] M_AXI_WSTRB,
    // Write valid. This signal indicates that valid write data and strobes are available.
    output logic  M_AXI_WVALID,
    // Write ready. This signal indicates that the slave can accept the write data.
    input logic  M_AXI_WREADY,
    // Master Interface Write Response Channel ports.
    // This signal indicates the status of the write transaction.
    input logic [1 : 0] M_AXI_BRESP,
    // Write response valid.
    // This signal indicates that the channel is signaling a valid write response
    input logic  M_AXI_BVALID,
    // Response ready. This signal indicates that the master can accept a write response.
    output logic  M_AXI_BREADY,
    // Master Interface Read Address Channel ports. Read address (issued by master)
    output logic [C_M_AXI_ADDR_WIDTH-1 : 0] M_AXI_ARADDR,
    // Protection type.
    // This signal indicates the privilege and security level of the transaction,
    // and whether the transaction is a data access or an instruction access.
    output logic [2 : 0] M_AXI_ARPROT,
    // Read address valid.
    // This signal indicates that the channel is signaling valid read address and control information.
    output logic  M_AXI_ARVALID,
    // Read address ready.
    // This signal indicates that the slave is ready to accept an address and associated control signals.
    input logic  M_AXI_ARREADY,
    // Master Interface Read Data Channel ports. Read data (issued by slave)
    input logic [C_M_AXI_DATA_WIDTH-1 : 0] M_AXI_RDATA,
    // Read response. This signal indicates the status of the read transfer.
    input logic [1 : 0] M_AXI_RRESP,
    // Read valid. This signal indicates that the channel is signaling the required read data.
    input logic  M_AXI_RVALID,
    // Read ready. This signal indicates that the master can accept the read data and response information.
    output logic  M_AXI_RREADY
);
    logic readEdge, writeEdge;

    EdgeTrigger readTrigger(.signal(dram.readEnable), .isEdge(readEdge), .clock(M_AXI_ACLK), .clear(~M_AXI_ARESETN));
    EdgeTrigger writeTrigger(.signal(dram.writeEnable), .isEdge(writeEdge), .clock(M_AXI_ACLK), .clear(~M_AXI_ARESETN));

    logic [C_M_AXI_ADDR_WIDTH-1:0] savedAddressBase;
    logic [C_M_TRANSACTIONS_NUM-1:0][C_M_AXI_DATA_WIDTH-1:0] savedWriteData;

    // Write signals

    //flag that marks the completion of write transactions.
    logic     writes_done;
    //write response acceptance
    logic     axi_bready;
    //Asserts when there is a write response error
    logic    write_resp_error;
    logic   write_error_cumulative;
    //Flag is asserted when the write index reaches the last write transaction number
    logic     last_write;
    //index counter to track the number of write transaction issued
    logic [$clog2(C_M_TRANSACTIONS_NUM):0]    write_index; // This needs to be big enough that we don't overflow
    //A pulse to initiate a write transaction
    logic     start_single_write;
    //Asserts when a single beat write transaction is issued and remains asserted till the completion of write transaction.
    logic     write_issued;

    // Read Signals

    logic reads_done;
    //Asserts when there is a read response error
    logic    read_resp_error;
    logic read_error_cumulative;

    // Flag is asserted when the read index reaches the last read transaction number
    logic     last_read;
    // index counter to track the number of read transaction issued
    logic [$clog2(C_M_TRANSACTIONS_NUM):0]    read_index; // This needs to be big enough that we don't overflow

    // A pulse to initiate a read transaction
    logic     start_single_read;
    // Asserts when a single beat read transaction is issued and remains asserted till the completion of read transaction.
    logic     read_issued;


    // AXI4LITE signals
    //write address valid
    logic     axi_awvalid;
    //write data valid
    logic     axi_wvalid;
    //read data acceptance
    logic     axi_rready;
    // read address valid
    logic     axi_arvalid;

    //write address
    logic [C_M_AXI_ADDR_WIDTH-1 : 0]  axi_awaddr;
    //write data
    logic [C_M_AXI_DATA_WIDTH-1 : 0]  axi_wdata;
    //read addresss
    logic [C_M_AXI_ADDR_WIDTH-1 : 0]  axi_araddr;

    logic[C_M_TRANSACTIONS_NUM-1:0][C_M_AXI_DATA_WIDTH-1:0] readDataArray;

    assign dram.readData = readDataArray;

    logic doSetup;

    enum {IDLE, WRITE, READ} state;

    assign doSetup = (readEdge || writeEdge) && (state == IDLE);

    // FIXME: We don't actually support all 34 bits of physical address space,
    // for that matter we don't support even close to 34 bits...
    Register #(.WIDTH(C_M_AXI_ADDR_WIDTH)) address_reg(
        .q(savedAddressBase),
        .d(dram.addressBase[31:0]),
        .clock(M_AXI_ACLK), .enable(doSetup), .clear(~M_AXI_ARESETN));

    Register #(.WIDTH($bits(dram.writeData))) data_reg(
        .q(savedWriteData),
        .d(dram.writeData),
        .clock(M_AXI_ACLK), .enable(doSetup), .clear(~M_AXI_ARESETN));

    Register #(.WIDTH(1)) writeError_reg(
        .q(write_error_cumulative), .d(1'b1), .clock(M_AXI_ACLK), .enable(write_resp_error), .clear(doSetup || ~M_AXI_ARESETN));

    Register #(.WIDTH(1)) readError_reg(
        .q(read_error_cumulative), .d(1'b1), .clock(M_AXI_ACLK), .enable(read_resp_error), .clear(doSetup || ~M_AXI_ARESETN));

    // I/O Connections assignments

    //Write Response (B)
    assign M_AXI_BREADY = axi_bready;
    assign M_AXI_AWVALID    = axi_awvalid;
    //Write Data(W)
    assign M_AXI_WVALID = axi_wvalid;
    //Set all byte strobes in this example
    assign M_AXI_WSTRB  = 4'b1111;
    //Adding the offset address to the base addr of the slave
    assign M_AXI_AWADDR = PHYSICAL_ADDRESS_OFFSET + savedAddressBase + axi_awaddr;
    //AXI 4 write data
    assign M_AXI_WDATA  = axi_wdata;
    assign M_AXI_AWPROT = 3'b000;

    // Read Interface
    //Read and Read Response (R)
    assign M_AXI_RREADY = axi_rready;
    //Read Address (AR)
    assign M_AXI_ARADDR = PHYSICAL_ADDRESS_OFFSET + savedAddressBase + axi_araddr;
    assign M_AXI_ARVALID    = axi_arvalid;
    assign M_AXI_ARPROT = 3'b001;

    always_ff @(posedge M_AXI_ACLK) begin
        if(~M_AXI_ARESETN) begin
            state <= IDLE;
            dram.requestError <= 1'b0;
            dram.requestCompleted <= 1'b0;
            dram.requestAccepted <= 1'b0;

            start_single_write <= 1'b0;
            write_issued <= 1'b0;

            start_single_read <= 1'b0;
            read_issued <= 1'b0;


        end else begin
            case (state)
                IDLE: begin
                    if(readEdge) begin
                        state <= READ;
                        dram.requestError <= 1'b0;
                        dram.requestCompleted <= 1'b0;
                        dram.requestAccepted <= 1'b1;
                    end else if(writeEdge) begin
                        state <= WRITE;
                        dram.requestError <= 1'b0;
                        dram.requestCompleted <= 1'b0;
                        dram.requestAccepted <= 1'b1;
                    end
                end
                WRITE: begin
                    // This state is responsible to issue start_single_write pulse to
                    // initiate a write transaction. Write transactions will be
                    // issued until last_write signal is asserted.
                    // write controller
                    dram.requestAccepted <= 1'b0;
                    if(writes_done) begin
                        state <= IDLE;
                        dram.requestCompleted <= 1'b1;
                        dram.requestError <= write_error_cumulative;
                    end else begin
                        if (~axi_awvalid && ~axi_wvalid && ~M_AXI_BVALID && ~last_write && ~start_single_write && ~write_issued) begin
                            start_single_write <= 1'b1;
                            write_issued  <= 1'b1;
                        end else if (axi_bready) begin
                            write_issued  <= 1'b0;
                        end else begin
                            start_single_write <= 1'b0; // Negate to generate a pulse
                        end
                    end
                end
                READ: begin
                    dram.requestAccepted <= 1'b0;
                    // This state is responsible to issue start_single_read pulse to
                    // initiate a read transaction. Read transactions will be
                    // issued until last_read signal is asserted.
                    // read controller
                    if (reads_done) begin
                        state <= IDLE;
                        dram.requestCompleted <= 1'b1;
                        dram.requestError <= read_error_cumulative;
                    end else begin
                        if (~axi_arvalid && ~M_AXI_RVALID && ~last_read && ~start_single_read && ~read_issued) begin
                            start_single_read <= 1'b1;
                            read_issued  <= 1'b1;
                        end else if (axi_rready) begin
                            read_issued  <= 1'b0;
                        end else begin
                            start_single_read <= 1'b0; //Negate to generate a pulse
                        end
                    end
                end
                default:
                    state <= IDLE;
            endcase
        end
    end

    //Write Addresses
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0  || doSetup) begin
            axi_awaddr <= 0;
        end else if (M_AXI_AWREADY && axi_awvalid) begin
            // Signals a new write address/ write data is
            // available by user logic
            axi_awaddr <= axi_awaddr + 32'h00000004;
      end
    end

    // Write data generation
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup ) begin
            // This needs to be the original data in since we don't read it again until after the write is back
            axi_wdata <= dram.writeData[0];
        end else if (M_AXI_WREADY && axi_wvalid) begin
            // Signals a new write address/ write data is
            // available by user logic
            axi_wdata <= savedWriteData[write_index];
      end
    end

    // Write Control

    //The write response channel provides feedback that the write has committed
    //to memory. BREADY will occur after both the data and the write address
    //has arrived and been accepted by the slave, and can guarantee that no
    //other accesses launched afterwards will be able to be reordered before it.

    //The BRESP bit [1] is used indicate any errors from the interconnect or
    //slave for the entire write burst. This example will capture the error.

    //While not necessary per spec, it is advisable to reset READY signals in
    //case of differing reset latencies between master/slave.

    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup) begin
            axi_bready <= 1'b0;
        end else if (M_AXI_BVALID && ~axi_bready) begin
            // accept/acknowledge bresp with axi_bready by the master
            // when M_AXI_BVALID is asserted by slave
            axi_bready <= 1'b1;
        end else if (axi_bready) begin
            // deassert after one clock cycle
            axi_bready <= 1'b0;
        end else begin
            // retain the previous value
            axi_bready <= axi_bready;
        end
    end

    //Flag write errors
    assign write_resp_error = (axi_bready & M_AXI_BVALID & M_AXI_BRESP[1]);

    //Check for last write completion.

    //This logic is to qualify the last write count with the final write
    //response. This demonstrates how to confirm that a write has been
    //committed.

    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup)
            writes_done <= 1'b0;
        else if (last_write && M_AXI_BVALID && axi_bready)
            writes_done <= 1'b1; //The writes_done should be associated with a bready response
        else
            writes_done <= writes_done;
    end

    //Terminal write count

    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup)
            last_write <= 1'b0;
        else if ((write_index == C_M_TRANSACTIONS_NUM) && M_AXI_AWREADY) begin
            //The last write should be associated with a write address ready response
            last_write <= 1'b1;
        end else
            last_write <= last_write;
    end

    // start_single_write triggers a new write
    // transaction. write_index is a counter to
    // keep track with number of write transaction
    // issued/initiated
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup) begin
            write_index <= 0;
        end else if (start_single_write) begin
            // Signals a new write address/ write data is
            // available by user logic
            write_index <= write_index + 1;
        end
    end

    // The purpose of the write address channel is to request the address and
    // command information for the entire transaction.  It is a single beat
    // of information.

    // Note for this example the axi_awvalid/axi_wvalid are asserted at the same
    // time, and then each is deasserted independent from each other.
    // This is a lower-performance, but simpler control scheme.

    // AXI VALID signals must be held active until accepted by the partner.

    // A data transfer is accepted by the slave when a master has
    // VALID data and the slave acknowledges it is also READY. While the master
    // is allowed to generated multiple, back-to-back requests by not
    // deasserting VALID, this design will add rest cycle for
    // simplicity.

    // Since only one outstanding transaction is issued by the user design,
    // there will not be a collision between a new request and an accepted
    // request on the same clock cycle.

    always_ff @(posedge M_AXI_ACLK) begin
        //Only VALID signals must be deasserted during reset per AXI spec
        //Consider inverting then registering active-low reset for higher fmax
        if (M_AXI_ARESETN == 0 || doSetup) begin
            axi_awvalid <= 1'b0;
        end else begin
            //Signal a new address/data command is available by user logic
            if (start_single_write) begin
                axi_awvalid <= 1'b1;
            end else if (M_AXI_AWREADY && axi_awvalid) begin
                //Address accepted by interconnect/slave (issue of M_AXI_AWREADY by slave)
                axi_awvalid <= 1'b0;
          end
      end
    end

    //The write data channel is for transferring the actual data.
    //The data generation is specific to the example design, and
    //so only the WVALID/WREADY handshake is shown here
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0  || doSetup) begin
            axi_wvalid <= 1'b0;
        end else if (start_single_write) begin
            //Signal a new address/data command is available by user logic
            axi_wvalid <= 1'b1;
        end else if (M_AXI_WREADY && axi_wvalid) begin
            //Data accepted by interconnect/slave (issue of M_AXI_WREADY by slave)
            axi_wvalid <= 1'b0;
       end
    end


    // Read Control


    // Check for last read completion.
    // This logic is to qualify the last read count with the final read response/data.
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup)
            reads_done <= 1'b0;
        else if (last_read && M_AXI_RVALID && axi_rready) begin
            //The reads_done should be associated with a read ready response
            reads_done <= 1'b1;
        end else
            reads_done <= reads_done;
    end

    //The Read Data channel returns the results of the read request
    //The master will accept the read data by asserting axi_rready
    //when there is a valid read data available.
    //While not necessary per spec, it is advisable to reset READY signals in
    //case of differing reset latencies between master/slave.

    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup) begin
            axi_rready <= 1'b0;
        end else if (M_AXI_RVALID && ~axi_rready) begin
            // accept/acknowledge rdata/rresp with axi_rready by the master
            // when M_AXI_RVALID is asserted by slave
            axi_rready <= 1'b1;
        end else if (axi_rready) begin
            // deassert after one clock cycle
            axi_rready <= 1'b0;
        end
        // retain the previous value
    end

    //Flag write errors
    assign read_resp_error = (axi_rready & M_AXI_RVALID & M_AXI_RRESP[1]);

    //Terminal Read Count

    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup)
            last_read <= 1'b0;
        else if ((read_index == C_M_TRANSACTIONS_NUM) && (M_AXI_ARREADY) ) begin
            //The last read should be associated with a read address ready response
            last_read <= 1'b1;
        end else
            last_read <= last_read;
    end

    //start_single_read triggers a new read transaction. read_index is a counter to
    //keep track with number of read transaction issued/initiated

    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup) begin
            read_index <= 0;
        end else if (start_single_read) begin
            // Signals a new read address is
            // available by user logic
            read_index <= read_index + 1;
        end
    end

    // A new axi_arvalid is asserted when there is a valid read address
    // available by the master. start_single_read triggers a new read
    // transaction
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0 || doSetup) begin
            axi_arvalid <= 1'b0;
        end else if (start_single_read) begin
            // Signal a new read address command is available by user logic
            axi_arvalid <= 1'b1;
        end else if (M_AXI_ARREADY && axi_arvalid) begin
            // RAddress accepted by interconnect/slave (issue of M_AXI_ARREADY by slave)
            axi_arvalid <= 1'b0;
        end
        // retain the previous value
    end

    //Read Addresses
    always_ff @(posedge M_AXI_ACLK) begin
        if (M_AXI_ARESETN == 0  || doSetup) begin
            axi_araddr <= 0;
        end else if (M_AXI_ARREADY && axi_arvalid) begin
            // Signals a new write address/ write data is
            // available by user logic
            axi_araddr <= axi_araddr + 32'h00000004;
        end
    end

    localparam CLEAR_LITTERAL = {C_M_AXI_DATA_WIDTH{1'b0}};
    // Read Data
    always_ff @(posedge M_AXI_ACLK) begin
        if(M_AXI_ARESETN == 0 || doSetup) begin
            readDataArray <= {C_M_TRANSACTIONS_NUM{CLEAR_LITTERAL}};
        end else if(M_AXI_RVALID && axi_rready) begin
            readDataArray[read_index-1] <= M_AXI_RDATA;
        end
    end

endmodule

`endif