//# PRINT AFTER include/memory/translation.vh
`default_nettype none

`include "cache.vh"
`include "translation.vh"
`include "config.vh"

module TranslationLookasideBuffer(
    TranslationInterface.TLB instruction, data,

    TranslationFill.TLB translationFill,

    input logic clock, clear
);

    TranslationEntry_t [`TLB_ENTRIES-1:0] translationEntries;

    TranslationBlock instructionBlock(
        .request(instruction),
        .translationEntries(translationEntries),
        .clock,
        .clear
    );

    TranslationBlock dataBlock(
        .request(data),
        .translationEntries(translationEntries),
        .clock,
        .clear
    );

    instructionNeverInvalidate: assert property (
        @(posedge clock) disable iff(clear)
        instruction.flushMode == KEEP
    );

    assign translationFill.dataResponse = data.translation;
    assign translationFill.instructionResponse = instruction.translation;

    TranslationFlushMode_t dataFlushMode_last;
    Register #(.WIDTH($bits(dataFlushMode_last))) dataFlushModeRegister(
        .q(dataFlushMode_last),
        .d(data.flushMode),
        .enable(1'b1),
        .clock,
        .clear
    );

    data_index_valid: assert property (@(posedge clock) disable iff(clear) data.translation.index < `TLB_ENTRIES);
    fill_index_valid: assert property (@(posedge clock) disable iff(clear) translationFill.newTranslation.index < `TLB_ENTRIES);

    always_ff @(posedge clock) begin
        if(clear)
            translationEntries <= '{default:0};
        else begin
            unique if(data.flushMode == INVALIDATE_ALL)
                translationEntries <= '{default:0};
            else if(translationFill.newTranslation.entry.isValid)
                translationEntries[translationFill.newTranslation.index] <= translationFill.newTranslation.entry;
            else if(data.translation.entry.isValid && (dataFlushMode_last == INVALIDATE_PAGE)) begin
                // This check is delayed by one clock cycle since the TLB read
                // is sequential, so there will be a dead-cycle where the entry
                // exists, but should be cleared. This is safe because a TLB
                // flush is a fence operation anyway.
                translationEntries[data.translation.index] <= '{default:0};
            end else begin
                // Hold the value
            end
        end
    end

endmodule

module TranslationBlock(
    TranslationInterface.TLB request,
    input TranslationEntry_t [`TLB_ENTRIES-1:0] translationEntries,
    input logic clock, clear
);

    // First we go through all of the entries in the TLB, if an entry is valid
    // then we check if the tag matches the tag we are trying to match again, if
    // it does, then we know that
    TranslationResponse_t[`TLB_ENTRIES/2-1:0] subResults, subResults_sync;

    generate
        genvar i;
        for(i = 0; i < `TLB_ENTRIES; i = i + 2) begin : translation_check
            always_comb begin
                unique if(translationEntries[i].isValid && (translationEntries[i].virtualTag == request.virtualTag))
                    subResults[i/2] = '{
                        entry:  translationEntries[i],
                        index: i
                    };
                else if(translationEntries[i+1].isValid && (translationEntries[i+1].virtualTag == request.virtualTag))
                    subResults[i/2] = '{
                        entry: translationEntries[i+1],
                        index: i + 1
                    };
                else
                    subResults[i/2] = '{default:0};
            end
        end
    endgenerate

    Register #(.WIDTH($bits(subResults))) subResultsRegister(
        .q(subResults_sync),
        .d(subResults),
        .clock,
        .clear(clear || !request.virtualTagValid),
        .enable(1'b1)
    );


    ArrayOrReduction #(.WIDTH($bits(TranslationResponse_t)), .SIZE(`TLB_ENTRIES/2)) reduction(
        .inputs(subResults_sync),
        .result(request.translation)
    );

endmodule
