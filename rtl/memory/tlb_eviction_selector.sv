//# PRINT AFTER memory/translation_lookaside_buffer.sv
`default_nettype none

`include "translation.vh"
`include "compiler.vh"

module TLBEvictionSelector(
    input TranslationResponse_t instructionResponse, dataResponse,

    input logic tlbSelectIndex,
    output TLBIndex_t evictionIndex,

    input logic clock, clear
);

    `STATIC_ASSERT(2**$bits(TLBIndex_t) == `TLB_ENTRIES, tlb_index_valid);

    NotMostRecentlyUsedPolicy #(.WIDTH($bits(TLBIndex_t))) policy(
        .select(tlbSelectIndex),
        .index(evictionIndex),
        .recentValid(
            {instructionResponse.entry.isValid, dataResponse.entry.isValid}
        ),
        .recent({instructionResponse.index, dataResponse.index}),
        .clock,
        .clear
    );

endmodule