//# PRINT AFTER testbench.sv
`ifdef SIMULATION_18447

`default_nettype none

`include "dram.vh"
`include "compiler.vh"
`include "riscv_types.vh"

typedef logic [15:0][3:0][7:0] MemoryBlock_t;

module DRAMMock(
    DRAMAXIInterface.AXI dram,
    input logic clock, clear
    );

    uniqueEnable: assert property (@(posedge clock) disable iff(clear)
        !(dram.readEnable && dram.writeEnable)
    );
    // We only support 256 MB of physical memory
    boundedAddress: assert property (@(posedge clock) disable iff(clear)
        savedAddressBase[31:28] == 4'b0
    );

    localparam BLOCKS = `PHYSICAL_MEMORY_SIZE/($bits(MemoryBlock_t)/8);

    `STATIC_ASSERT(`BYTE_WIDTH == 8, byte_width);
    MemoryBlock_t memory[0:BLOCKS-1];

    function automatic Word_t getWord(PhysicalAddress_t address);
        logic [$clog2(BLOCKS):0] blockIndex;
        logic [4:0] wordIndex;
        blockIndex = address[$clog2(BLOCKS)+6:6];
        wordIndex = address[5:2];
        return memory[blockIndex][wordIndex];
    endfunction

    enum {
        IDLE,
        READ_ACCEPT,
        READ_COMPLETE,
        WRITE_ACCEPT,
        WRITE_COMPLETE
    } state, nextState;

    logic readEdge, writeEdge;

    EdgeTrigger readTrigger(
        .signal(dram.readEnable),
        .isEdge(readEdge),
        .clock,
        .clear
    );
    EdgeTrigger writeTrigger(
        .signal(dram.writeEnable),
        .isEdge(writeEdge),
        .clock,
        .clear
    );

    logic doSetup;

    PhysicalAddress_t savedAddressBase;
    Word_t [15:0] savedWriteData;

    Register #(.WIDTH($bits(savedAddressBase))) address_reg(
        .q(savedAddressBase),
        .d(dram.addressBase),
        .clock, .enable(doSetup), .clear);

    Register #(.WIDTH($bits(savedWriteData))) data_reg(
        .q(savedWriteData),
        .d(dram.writeData),
        .clock, .enable(doSetup), .clear);

    assign dram.requestError = 1'b0; // No errors

    logic loadMemory;
    logic readMemory;

    Word_t [15:0] readData;

    assign dram.readData = readData;

    // The parameter to $fseek that indicates to seek from the end of file
    localparam SEEK_END = 2;

    `STATIC_ASSERT($bits(memory[0]) == 512, memory_block_width);

    function automatic void initializeMemory(
        ref MemoryBlock_t mymemory[0:BLOCKS-1]
    );
        int segment_fd, file_size;
        int word_index, byte_index, block_index;
        string segment_path;

        $display("Initializing Memory");

        segment_path = "mem.all.bin";
        segment_fd = $fopen(segment_path, "rb");
        if(segment_fd == 0) begin
            file_error(segment_fd, segment_path, "Unable to open file");
        end



        // Determine the size of the memory segment data file
        assert($fseek(segment_fd, 0, SEEK_END) == 0) else $fatal;
        file_size = $ftell(segment_fd);
        assert($rewind(segment_fd) == 0) else $fatal;

        // Load the memory segment if the file size does not exceed its size
        if (file_size * `BYTE_WIDTH > $bits(mymemory)) begin
            $fatal("Error: %s: File is too large for memory segment.",
                    segment_path);
        end

        word_index = 0;
        byte_index = 0;
        block_index = 0;

        $display("Zeroing initial memory");

        while(block_index < `TEXT_START / ($bits(mymemory[0]) / 8)) begin
            mymemory[block_index] = 512'b0;
            block_index += 1;
        end

        assert(block_index == `TEXT_START / ($bits(mymemory[0]) / 8));

        $display("Loading from file");

        /* Since $fread always reads data in big-endian order, load the segment
         * one byte at a time to ensure little-endian ordering. */
        while (1) begin
            string placeholder;
            logic [7:0] data_byte;
            int bytes_read;

            // Read the next byte from the file
            bytes_read = $fread(data_byte, segment_fd);
            if (bytes_read == 0 && $ferror(segment_fd, placeholder) != 0) begin
                file_error(segment_fd, segment_path, "Unable to read file");
            end else if (bytes_read == 0) begin
                break;
            end

            assert(bytes_read == 1) else $fatal("Bytes Read: %d", bytes_read);

            // Write the byte to the segment, and advance the indices as needed
            mymemory[block_index][word_index][byte_index] = data_byte;
            // $display("Check: %h", mymemory[block_index]);
            byte_index += 1;
            if (byte_index == 4) begin
                byte_index = 0;
                word_index += 1;
            end
            if(word_index == 16) begin
                word_index = 0;
                block_index += 1;
            end
        end

        $display("Clearing remaining memory");

        while(byte_index != 0 || word_index != 0) begin
            mymemory[block_index][word_index][byte_index] = 8'b0;
            byte_index += 1;
            if(byte_index == 4) begin
                byte_index = 0;
                word_index += 1;
            end
            if(word_index == 16) begin
                word_index = 0;
                block_index += 1;
            end
        end

        while(block_index < BLOCKS) begin
            mymemory[block_index] = 512'b0;
            block_index += 1;
        end

        $fclose(segment_fd);

        $display("Memory initialized");
    endfunction

    // Reports an error after a file I/O operation fails, and exits
    function automatic void file_error(int fd, string file_path, string msg);
        string error_cause;
        assert($ferror(fd, error_cause) != 0) else $fatal;

        $fatal("Error: %s: %s: %s.\n", file_path, msg, error_cause);
    endfunction: file_error

    logic [$clog2(BLOCKS):0] index;

    assign index = savedAddressBase[$clog2(BLOCKS)+6:6];

    logic initialized;
    always_ff @(posedge clock) begin
        if(clear) begin
            if(initialized == 0 || initialized === 'x) begin
                initializeMemory(memory);
                initialized = 1'b1;
            end
        end else if(loadMemory) begin
            memory[index] <= savedWriteData;
            readData <= {16{32'b0}};
        end else if(readMemory)
            readData <= memory[index];
        else
            readData <= {16{32'b0}};
    end


    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;

    always_comb begin
        nextState = state;
        dram.requestAccepted = 1'b0;
        dram.requestCompleted = 1'b0;
        doSetup = 1'b0;
        loadMemory = 1'b0;
        readMemory = 1'b0;
        unique case(state)
            IDLE:
                if(readEdge) begin
                    nextState = READ_ACCEPT;
                    doSetup = 1'b1;
                end else if(writeEdge) begin
                    nextState = WRITE_ACCEPT;
                    doSetup = 1'b1;
                end
            READ_ACCEPT: begin
                dram.requestAccepted = 1'b1;
                nextState = READ_COMPLETE;
                readMemory = 1'b1;
            end
            READ_COMPLETE: begin
                dram.requestCompleted = 1'b1;
                nextState = IDLE;
            end
            WRITE_ACCEPT: begin
                dram.requestAccepted = 1'b1;
                nextState = WRITE_COMPLETE;
            end
            WRITE_COMPLETE: begin
                loadMemory = 1'b1;
                dram.requestCompleted = 1'b1;
                nextState = IDLE;
            end
        endcase
    end

endmodule

`endif