//# PRINT AFTER include/memory/memory_arbiter.vh
`default_nettype none

`include "memory_arbiter.vh"

module MemoryArbiter(
    MemoryArbiterInterface.Arbiter instruction, data, tableWalk, dramFill,

    output MemorySelect_t select,
    input logic clock, clear
);

    // We are going to use MemoryLock as the state
    MemoryLock_t lock, nextLock;

    // Instruction and Data operations never request locks
    instructionNoLock: assert property (
        @(posedge clock) disable iff(clear)
        !instruction.requestLock
    );

    dataNoLock: assert property (
        @(posedge clock) disable iff(clear)
        !data.requestLock
    );

    instructionNoHandoff: assert property(
        @(posedge clock) disable iff(clear)
        instruction.handoff == MEMORY_UNLOCKED
    );

    dataNoHandoff: assert property (
        @(posedge clock) disable iff(clear)
        data.handoff == MEMORY_UNLOCKED
    );

    waitingOnLock: assert property (
        @(posedge clock) disable iff(clear)
        lock == MEMORY_WAITING |-> dramFill.requestLock || tableWalk.requestLock
    );

    // It would be nice to assert this, but it isn't quite true
    // dramNeverBusyWithoutLock: assert property (
    //     @(posedge clock) disable iff(clear)
    //     !dramFill.isIdle |-> lock == DRAM_LOCK
    // );

    // tableWalkNeverBusyWithoutLock: assert property (
    //     @(posedge clock) disable iff(clear)
    //     !tableWalk.isIdle |-> lock == TABLE_WALK_LOCK
    // );

    // dramLockOnlyDRAMBusy: assert property (
    //     @(posedge clock) disable iff(clear)
    //     lock == DRAM_LOCK |-> instruction.isIdle && data.isIdle
    // );

    // walkLockOnlyWalkBusy: assert property (
    //     @(posedge clock) disable iff(clear)
    //     lock == TABLE_WALK_LOCK |-> instruction.isIdle && data.isIdle
    // );

    always_comb begin
        instruction.lock = lock;
        data.lock = lock;
        tableWalk.lock = lock;
        dramFill.lock = lock;
    end

    always_ff @(posedge clock)
        if(clear)
            lock <= MEMORY_UNLOCKED;
        else
            lock <= nextLock;

    always_comb begin
        nextLock = lock;
        select = INST_DATA;
        unique case(lock)
            MEMORY_UNLOCKED: begin
                select = INST_DATA;
                if(tableWalk.requestLock || dramFill.requestLock) begin
                    nextLock = MEMORY_WAITING;
                end
            end
            MEMORY_WAITING: begin
                select = INST_DATA;
                // Once everything else is idle, we can assign a lock
                if(!instruction.isIdle || !data.isIdle) begin
                    // These operations need to finish first before the lock can
                    // be acquired
                    nextLock = MEMORY_WAITING;
                end else if(dramFill.requestLock) begin
                    // At this point we know that instruction and data are idle
                    // and we know that table walk cannot be busy unless it has
                    // the lock
                    nextLock = DRAM_LOCK;
                end else if(tableWalk.requestLock) begin
                    // At this point we know that instruction and data are idle,
                    // and dram fill isn't requesting a lock and dram fill can't
                    // be busy unless it has the lock
                    nextLock = TABLE_WALK_LOCK;
                end else begin
                    // This shouldn't happen
                    nextLock = MEMORY_UNLOCKED;
                end
            end
            TABLE_WALK_LOCK: begin
                if(tableWalk.handoff == DRAM_LOCK) begin
                    // At this point table walk is holding the lock, and is
                    // trying to pass it to dram
                    select = DRAM_FILL;
                    nextLock = DRAM_LOCK;
                end else if(tableWalk.requestLock) begin
                    // Table Walk is holding the lock still and isn't trying to
                    // hand it off to dram
                    nextLock = TABLE_WALK_LOCK;
                    select = TABLE_WALK;
                end else begin
                    // We no longer want the lock
                    nextLock = MEMORY_UNLOCKED;
                    select = INST_DATA;
                end
            end
            DRAM_LOCK: begin

                if(dramFill.handoff == TABLE_WALK_LOCK) begin
                    // At this point dram is holding the lock and is trying to
                    // pass it back to table walk
                    select = TABLE_WALK;
                    nextLock = TABLE_WALK_LOCK;
                end else if(dramFill.requestLock) begin
                    // dram is holding the lock still and isn't trying to hand
                    // it off to table walk
                    nextLock = DRAM_LOCK;
                    select = DRAM_FILL;
                end else begin
                    nextLock = MEMORY_UNLOCKED;
                    select = INST_DATA;
                end
            end
        endcase
    end

endmodule