//# PRINT AFTER memory/data_memory.sv
`default_nettype none

`include "memory_arbiter.vh"
`include "memory.vh"
`include "cache.vh"
`include "translation.vh"
`include "memory_controller.vh"
`include "dram_fill.vh"
`include "table_walk.vh"
`include "riscv_types.vh"

module MemoryController(
    MemoryInterface.Memory request,

    MemoryArbiterInterface.Requester arbiter,
    MemoryControl.Memory control,
    TableWalkInterface.Requester tableWalk,
    DRAMFillInterface.Requester dram,

    input CacheResponse_t cacheResponse,

    // TLB Signals
    input TranslationEntry_t translation_tlb,

    // Control Signals
    output MemoryMode_t memoryMode,
    output VirtualAddress_t virtualAddress,
    output ResponseType_t responseType,

    output PhysicalAddress_t physicalAddress,
    output MemoryRequest_t request_saved,

    // Status Points
    input ResponseType_t responseType_selected,
    input Word_t readData_selected,
    input logic memoryHit,

    input logic clock, clear
);

    `CACHE_FUNCTIONS
    `MEMORY_FUNCTIONS
    `TRANSLATION_FUNCTIONS

    readEnableOnly: assert property (@(posedge clock) disable iff(clear)
        request.request.readEnable |->
        !(|request.request.writeEnable) &&
        (request.request.flushMode == KEEP)
    );

    writeEnableOnly: assert property (@(posedge clock) disable iff(clear)
        |request.request.writeEnable |->
        !request.request.readEnable &&
        (request.request.flushMode == KEEP)
    );

    invalidateOnly: assert property (@(posedge clock) disable iff(clear)
        (request.request.flushMode != KEEP) |->
        !request.request.readEnable &&
        !(|request.request.writeEnable)
    );

    // Since we have a TLB invalidate, we know the next operation shouldn't be
    // another memory operation since we need to fence to protect the
    // instruction stream
    invalidateMustFence: assert property (@(posedge clock) disable iff(clear)
        (request.request.flushMode != KEEP) && request.ready |=>
        request.request.flushMode == KEEP &&
        !request.request.readEnable &&
        !(|request.request.writeEnable)
    );

    addressAligned: assert property (@(posedge clock) disable iff(clear)
        request.request.readEnable ||
        (|request.request.writeEnable) ||
        (request.request.flushMode == INVALIDATE_PAGE) |->
        request.request.virtualAddress[1:0] == 2'b0
    );

    invalidateVirtualMemoryEnabled: assert property (
        @(posedge clock) disable iff(clear)
        request.request.flushMode != KEEP |-> control.virtualMemoryEnabled
    );

    // Unused arbiter signals
    assign arbiter.requestLock = 1'b0;
    assign arbiter.handoff = MEMORY_UNLOCKED;

    assign dram.cacheResponse = cacheResponse;

    enum {
        S_IDLE,
        S_READ,
        S_TLB_MISS,
        S_CACHE_MISS,
        S_RESTART_READ,
        S_WRITE,
        S_INVALIDATE,
        S_PAGE_FAULT
    } state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= S_IDLE;
        else
            state <= nextState;

    Register #(.WIDTH($bits(request_saved))) requestRegister(
        .d(request.request),
        .q(request_saved),
        .enable(request.ready),
        .clear,
        .clock
    );

    TranslationType_t translationType_next, translationType_current;
    Register #(.WIDTH($bits(translationType_current))) translationTypeRegister(
        .d(translationType_next),
        .q(translationType_current),
        .enable(1'b1),
        .clock,
        .clear
    );

    ResponseType_t responseType_next;
    Register #(.WIDTH($bits(responseType))) responseTypeRegister(
        .d(responseType_next),
        .q(responseType),
        .enable(1'b1),
        .clock,
        .clear
    );

    TranslationEntry_t translation_last, translation_selected;
    always_comb begin
        if(translationType_current == VIRTUAL_MEMORY_DISABLED) begin
            translation_selected = directMappedTranslation(
                request_saved.virtualAddress
            );
        end else begin
            translation_selected = translation_tlb;
        end
    end

    Register #(.WIDTH($bits(translation_last))) translationLastRegister(
        .d(translation_selected),
        .q(translation_last),
        .enable(1'b1),
        .clock,
        .clear
    );

    assign tableWalk.virtualAddress = request_saved.virtualAddress;

    assign physicalAddress = translateVirtualAddress(
        request_saved.virtualAddress,
        translation_selected
    );

    logic startDRAMRequest;
    Register #(.WIDTH($bits(dram.address))) dramAddressRegister(
        .d(physicalAddress),
        .q(dram.address),
        .enable(startDRAMRequest),
        .clock,
        .clear
    );

    noDoubleTLBMiss: assert property (
        @(posedge clock) disable iff(clear)
        state == S_READ && !translation_selected.isValid |->
        translationType_current != TLB_MISS
    );

    always_comb begin
        nextState = state;

        request.ready = 1'b0;
        request.response = '{default:0};

        arbiter.isIdle = 1'b1;

        memoryMode = NO_MODE;
        virtualAddress = `VIRTUAL_ADDRESS_POISON;

        translationType_next = translationType_current;
        responseType_next = responseType;

        tableWalk.requestTranslation = 1'b0;

        startDRAMRequest = 1'b0;
        dram.requestFill = 1'b0;

        unique case(state)
            S_IDLE: begin
                if(clear || arbiter.lock != MEMORY_UNLOCKED) begin
                    request.ready = 1'b0;
                    arbiter.isIdle = 1'b1;
                    translationType_next = INVALID_TRANSLATION;
                    responseType_next = INVALID_MEMORY_RESPONSE;
                    // nextState = S_IDLE; T1
                end else begin
                    request.ready = 1'b1;
                    virtualAddress = request.request.virtualAddress;

                    unique if(request.request.readEnable ||
                        (|request.request.writeEnable)
                    ) begin
                        arbiter.isIdle = 1'b0;

                        memoryMode = READ_MODE;

                        if(control.virtualMemoryEnabled) begin
                            translationType_next = TLB_HIT;
                        end else begin
                            translationType_next = VIRTUAL_MEMORY_DISABLED;
                        end

                        responseType_next = CACHE_HIT;

                        nextState = S_READ; // T2
                    end else if(request.request.flushMode != KEEP) begin
                        arbiter.isIdle = 1'b0;

                        memoryMode = INVALIDATE_MODE;

                        translationType_next = TLB_INVALIDATE;
                        responseType_next = TRANSLATION_INVALIDATE;

                        nextState = S_INVALIDATE; // T3
                    end else begin
                        arbiter.isIdle = 1'b1;
                        translationType_next = INVALID_TRANSLATION;
                        responseType_next = INVALID_MEMORY_RESPONSE;
                        // nextState = S_IDLE; // T4
                    end
                end
            end
            S_READ: begin
                // If virtual memory is disabled, translation_selected will be a
                // direct map entry
                if(!translation_selected.isValid) begin
                    // TLB Miss, need to request a table walk fill.
                    //
                    // FIXME:  We don't want to drop the lock here since we are
                    // going to be requesting a table walk in the next cycle.
                    // Careful analysis of the race conditions may indicate that
                    // it is in fact safe to drop the lock now, but it is
                    // definitely safe to hold the lock and delay a cycle.
                    arbiter.isIdle = 1'b0;

                    translationType_next = TLB_MISS;
                    nextState = S_TLB_MISS; // T5
                end else if (
                    /* TODO: Extend the request semantics to handle instruction
                      can only access executable pages */

                    /* Read request and not readable */
                    (request_saved.readEnable &&
                        !translation_selected.readable) ||
                    /* Write request and not writable */
                    (|request_saved.writeEnable &&
                        !translation_selected.writable) ||
                    /* Read from user page without user privileges */
                    (translation_selected.userAccessable &&
                        !control.userPagesEnabled) ||
                    /* Read from supervisor page without supervisor privileges
                    */
                    (!translation_selected.userAccessable &&
                        !control.supervisorPagesEnabled)
                ) begin
                    arbiter.isIdle = 1'b1;
                    // No need to speculate here since we are hitting a page
                    // fault anyway
                    nextState = S_PAGE_FAULT; // T6
                end else if(
                    /** TODO: It would be nice to expand this to support better
                        access protections and fault when instruction stream
                        tries to read invalid data */
                   !memoryHit
                ) begin
                    // FIXME:  We don't want to drop the lock here since we are
                    // going to be requesting a dram fill in the next cycle.
                    // Careful analysis of the race conditions may indicate that
                    // it is in fact safe to drop the lock now, but it is
                    // definitely safe to hold the lock and delay a cycle.
                    arbiter.isIdle = 1'b0;

                    startDRAMRequest = 1'b1;
                    responseType_next = CACHE_MISS;

                    nextState = S_CACHE_MISS; // T7
                end else if(|request_saved.writeEnable) begin
                    // We had a write request with a cache hit on read
                    arbiter.isIdle = 1'b0;

                    memoryMode = WRITE_MODE;
                    virtualAddress = request_saved.virtualAddress;

                    // As an optimization the response is exported even though
                    // the operation hasn't finished yet.
                    request.response = memoryResponse(
                        request_saved, /* internal */
                        translationType_current, /* internal */
                        translation_selected, /* internal */
                        responseType_selected, /* external */
                        `WORD_POISON
                    );

                    nextState = S_WRITE; // T8
                end else begin
                    // We had a simple read request on a cache hit. The output
                    // should be valid now
                    request.response = memoryResponse(
                        request_saved, /* internal */
                        translationType_current, /* internal */
                        translation_selected, /* internal */
                        responseType_selected, /* external */
                        readData_selected /* external */
                    );

                    if(arbiter.lock != MEMORY_UNLOCKED) begin
                        arbiter.isIdle = 1'b1;
                        nextState = S_IDLE; // T9
                    end else begin
                        // This is a copy of the idle state speculative logic
                        // for read/write requests
                        request.ready = 1'b1;
                        virtualAddress = request.request.virtualAddress;

                        unique if(request.request.readEnable ||
                            (|request.request.writeEnable)
                        ) begin
                            arbiter.isIdle = 1'b0;

                            memoryMode = READ_MODE;

                            if(control.virtualMemoryEnabled) begin
                                translationType_next = TLB_HIT;
                            end else begin
                                translationType_next = VIRTUAL_MEMORY_DISABLED;
                            end
                            responseType_next = CACHE_HIT;

                            nextState = S_READ; // T10
                        end else if(request.request.flushMode != KEEP) begin
                            arbiter.isIdle = 1'b0;

                            memoryMode = INVALIDATE_MODE;

                            translationType_next = TLB_INVALIDATE;
                            responseType_next = TRANSLATION_INVALIDATE;

                            nextState = S_INVALIDATE; // T11
                        end else begin
                            arbiter.isIdle = 1'b1;
                            nextState = S_IDLE; // T12
                        end
                    end
                end
            end
            S_TLB_MISS: begin
                tableWalk.requestTranslation = 1'b1;

                unique case(tableWalk.result)
                    INVALID_TABLE_WALK: begin
                        arbiter.isIdle = 1'b1;
                        nextState = S_TLB_MISS; // T13
                    end
                    TRANSLATION_FILLED: begin
                        // We are planning on doing an operation, so we don't
                        // want to lose the lock
                        arbiter.isIdle = 1'b0;
                        nextState = S_RESTART_READ; // T14
                    end
                    TRANSLATION_FAULT: begin
                        arbiter.isIdle = 1'b1;
                        translationType_next = TABLE_WALK_FAULT;
                        nextState = S_PAGE_FAULT; // T15
                    end
                endcase
            end
            S_CACHE_MISS: begin
                if(dram.requestComplete) begin
                    // We are planning on doing an operation, so we don't want
                    // to lose the lock.
                    arbiter.isIdle = 1'b0;
                    nextState = S_RESTART_READ; // T16
                end else begin
                    arbiter.isIdle = 1'b1;

                    dram.requestFill = 1'b1;

                    nextState = S_CACHE_MISS; // T17
                end
            end
            S_RESTART_READ: begin
                // The goal of this state is to remove tlb miss and cache miss
                // re-run operations off of the critical path. This is okay
                // since these are "uncommon" operations and we don't need to
                // aggressively speculate them.
                arbiter.isIdle = 1'b0;

                memoryMode = READ_MODE;
                virtualAddress = request_saved.virtualAddress;

                nextState = S_READ; // T18
            end
            S_WRITE: begin
                if(arbiter.lock != MEMORY_UNLOCKED) begin
                    arbiter.isIdle = 1'b1;
                    nextState = S_IDLE; // T19
                end else begin
                    // This is a copy of the idle state speculative logic
                    // for read/write requests
                    request.ready = 1'b1;
                    virtualAddress = request.request.virtualAddress;

                    unique if(request.request.readEnable || (|request.request.writeEnable)) begin
                        arbiter.isIdle = 1'b0;

                        memoryMode = READ_MODE;

                        translationType_next = control.virtualMemoryEnabled ? TLB_HIT : VIRTUAL_MEMORY_DISABLED;
                        responseType_next = CACHE_HIT;

                        nextState = S_READ; // T20
                    end else if(request.request.flushMode != KEEP) begin
                        arbiter.isIdle = 1'b0;

                        memoryMode = INVALIDATE_MODE;

                        translationType_next = TLB_INVALIDATE;
                        responseType_next = TRANSLATION_INVALIDATE;

                        nextState = S_INVALIDATE; // T21
                    end else begin
                        arbiter.isIdle = 1'b1;
                        nextState = S_IDLE; // T22
                    end
                end
            end
            S_INVALIDATE: begin
                // Always succeeds in one cycle; however if the translation is
                // page specific that entry won't be cleared until the next
                // cycle so we can't speculate here (luckily a translation clear
                // is a fence so it will always be okay anyway)
                request.response = memoryResponse(
                    request_saved,
                    TLB_INVALIDATE,
                    translation_selected, /* junk data */
                    TRANSLATION_INVALIDATE,
                    `WORD_POISON
                );
                arbiter.isIdle = 1'b1;
                nextState = S_IDLE; // T23
            end
            S_PAGE_FAULT: begin
                // This could be optimized into an earlier state, but moving the
                // output into it's own state ensures that this is off of the
                // critical path.
                request.response = memoryFaultResponse(
                    request_saved,
                    translationType_current,
                    translation_last
                );
                arbiter.isIdle = 1'b1;
                nextState = S_IDLE; // T24
            end
        endcase

    end

endmodule