//# PRINT AFTER library/xilinx_bram_byte_write.sv
`default_nettype none

//  Xilinx True Dual Port RAM Byte Write Read First Dual Clock RAM
//  This code implements a parameterizable true dual port memory (both ports can read and write).
//  The behavior of this RAM is when data is written, the prior memory contents at the write
//  address are presented on the output port.

module xilinx_bram_double_clock #(
  parameter NB_COL    = 4   , // Specify number of columns (number of bytes)
  parameter COL_WIDTH = 8   , // Specify column width (byte width, typically 8 or 9)
  parameter RAM_DEPTH = 1024, // Specify RAM depth (number of entries)
  parameter INIT_FILE = ""                        // Specify name/location of RAM initialization file if using one (leave blank if not)
) (
  input  [ $clog2(RAM_DEPTH)-1:0] addra, // Port A address bus, width determined from RAM_DEPTH
  input  [ $clog2(RAM_DEPTH)-1:0] addrb, // Port B address bus, width determined from RAM_DEPTH
  input  [(NB_COL*COL_WIDTH)-1:0] dina , // Port A RAM input data
  input  [(NB_COL*COL_WIDTH)-1:0] dinb , // Port B RAM input data
  input                           clka , // Port A clock
  input                           clkb , // Port B clock
  input  [            NB_COL-1:0] wea  , // Port A write enable
  input  [            NB_COL-1:0] web  , // Port B write enable
  output [(NB_COL*COL_WIDTH)-1:0] douta, // Port A RAM output data
  output [(NB_COL*COL_WIDTH)-1:0] doutb  // Port B RAM output data
);

  reg [(NB_COL*COL_WIDTH)-1:0] BRAM [RAM_DEPTH-1:0];
  reg [(NB_COL*COL_WIDTH)-1:0] ram_data_a = {(NB_COL*COL_WIDTH){1'b0}};
  reg [(NB_COL*COL_WIDTH)-1:0] ram_data_b = {(NB_COL*COL_WIDTH){1'b0}};

  // The following code either initializes the memory values to a specified file or to all zeros to match hardware
  generate
    if (INIT_FILE != "") begin: use_init_file
      initial
        $readmemh(INIT_FILE, BRAM, 0, RAM_DEPTH-1);
    end else begin: init_bram_to_zero
      integer ram_index;
      initial
        for (ram_index = 0; ram_index < RAM_DEPTH; ram_index = ram_index + 1)
          BRAM[ram_index] = {(NB_COL*COL_WIDTH){1'b0}};
    end
  endgenerate

  always @(posedge clka) begin
      ram_data_a <= BRAM[addra];
  end

  always @(posedge clkb) begin
    ram_data_b <= BRAM[addrb];
  end

`ifdef SIMULATION_18447
generate
    genvar i;
    for (i = 0; i < NB_COL; i = i+1) begin: byte_write
      always @(posedge clka)
        if (wea[i])
          BRAM[addra][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= dina[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
        always @(posedge clkb)
        if (web[i])
          BRAM[addrb][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= dinb[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
      end
    endgenerate
`else
  generate
    genvar i;
    for (i = 0; i < NB_COL; i = i+1) begin: byte_write
      always @(posedge clka)
        if (wea[i])
          BRAM[addra][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= dina[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
        always @(posedge clkb)
        if (web[i])
          BRAM[addrb][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= dinb[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
      end
    endgenerate
`endif

  // The following is a 1 clock cycle read latency at the cost of a longer clock-to-out timing
   assign douta = ram_data_a;
   assign doutb = ram_data_b;

endmodule
