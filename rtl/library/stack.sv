//# PRINT AFTER library/xilinx_sram.sv
`default_nettype none

module Stack #(parameter SIZE=64, WIDTH=32) (
    input logic [WIDTH-1:0] push_data,
    // If full, the bottom (oldest) of the stack is truncated
    input logic push_valid,
    input logic pop_valid,

    output logic [WIDTH-1:0] peak_data,

    input logic clock, clear
);

    // stack_top points to the last element pushed
    // stack_next points to the location above the last element pushed
    logic [$clog2(SIZE)-1:0] stack_top, stack_next;

    // If we are popping, then the next location is still the stack_top,
    // otherwise the element goes on top of the last element (index + 1)
    assign stack_next = pop_valid ? stack_top : stack_top + 1'd1;

    xilinx_bram_single #(.RAM_WIDTH(WIDTH), .RAM_DEPTH(SIZE)) bram(
        .address_read(stack_top),
        .dataOut(peak_data),

        .address_write(stack_next),
        .dataIn(push_data),
        .writeEnable(push_valid),

        .clock
    );

    always_ff @(posedge clock)
        if(clear)
            stack_top <= {WIDTH{1'b0}};
        else if(push_valid && pop_valid)
            stack_top <= stack_top; // Add one and subtract one is net zero
        else if(push_valid)
            stack_top <= stack_top + 1'd1;
        else if(pop_valid)
            stack_top <= stack_top - 1'd1;

endmodule