//# PRINT AFTER library/xilinx_bram.sv
`default_nettype none

//  Xilinx Single Port Read First RAM
//  This code implements a parameterizable single-port read-first memory where when data
//  is written to the memory, the output reflects the prior contents of the memory location.
//  If the output data is not needed during writes or the last read value is desired to be
//  retained, it is suggested to set WRITE_MODE to NO_CHANGE as it is more power efficient.
//  If a reset or enable is not necessary, it may be tied off or removed from the code.
//  Modify the parameters for the desired RAM characteristics.

module xilinx_bram_single #(
  parameter RAM_WIDTH = 18,                       // Specify RAM data width
  parameter RAM_DEPTH = 1024,                     // Specify RAM depth (number of entries)
  parameter INIT_FILE = ""                        // Specify name/location of RAM initialization file if using one (leave blank if not)
) (
  input [$clog2(RAM_DEPTH)-1:0] address_read,  // Address bus, width determined from RAM_DEPTH
  output [RAM_WIDTH-1:0] dataOut,          // RAM output data

  input [$clog2(RAM_DEPTH)-1:0] address_write,
  input [RAM_WIDTH-1:0] dataIn,           // RAM input data
  input writeEnable,                            // Write enable

  input clock                           // Clock
);

  (* ram_style = "block" *) reg [RAM_WIDTH-1:0] BRAM [RAM_DEPTH-1:0];
  reg [RAM_WIDTH-1:0] ram_data = {RAM_WIDTH{1'b0}};

  // The following code either initializes the memory values to a specified file or to all zeros to match hardware
  generate
    if (INIT_FILE != "") begin: use_init_file
      initial
        $readmemh(INIT_FILE, BRAM, 0, RAM_DEPTH-1);
    end else begin: init_bram_to_zero
      integer ram_index;
      initial
        for (ram_index = 0; ram_index < RAM_DEPTH; ram_index = ram_index + 1)
          BRAM[ram_index] = {RAM_WIDTH{1'b0}};
    end
  endgenerate

  always @(posedge clock) begin
    if (writeEnable) begin
      BRAM[address_write] <= dataIn;
    end
    ram_data <= BRAM[address_read];
  end


  // The following is a 1 clock cycle read latency at the cost of a longer clock-to-out timing
  assign dataOut = ram_data;

endmodule
