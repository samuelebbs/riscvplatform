//# PRINT AFTER library/counter.sv
`default_nettype none


//  Xilinx True Dual Port RAM No Change Single Clock
//  This code implements a parameterizable true dual port memory (both ports can read and write).
//  This is a no change RAM which retains the last read value on the output during writes
//  which is the most power efficient mode.
//  If a reset or enable is not necessary, it may be tied off or removed from the code.

// RAM_WIDTH: Specify RAM data width
// RAM_DEPTH: Specify RAM depth (number of entries)
// INIT_FILE: Specify name/location of RAM initialization file if using one (leave blank if not)

module xilinx_bram #(parameter RAM_WIDTH=32, RAM_DEPTH=1024, INIT_FILE=""
) (
  input  wire [$clog2(RAM_DEPTH)-1:0] addressA    , // Port A address bus, width determined from RAM_DEPTH
  input  wire [$clog2(RAM_DEPTH)-1:0] addressB    , // Port B address bus, width determined from RAM_DEPTH
  input  wire [        RAM_WIDTH-1:0] dataInA     , // Port A RAM input data
  input  wire [        RAM_WIDTH-1:0] dataInB     , // Port B RAM input data
  input  wire                         clock       , // Clock
  input  wire                         writeEnableA, // Port A write enable
  input  wire                         writeEnableB, // Port B write enable
  output wire [        RAM_WIDTH-1:0] dataOutA    , // Port A RAM output data
  output wire [        RAM_WIDTH-1:0] dataOutB      // Port B RAM output data
);

  (* ram_style = "block" *) reg [RAM_WIDTH-1:0] bram [RAM_DEPTH-1:0];
  reg [RAM_WIDTH-1:0] bram_dataA = {RAM_WIDTH{1'b0}};
  reg [RAM_WIDTH-1:0] bram_dataB = {RAM_WIDTH{1'b0}};

  // The following code either initializes the memory values to a specified file or to all zeros to match hardware
  generate
    if (INIT_FILE != "") begin: use_init_file
      initial
        $readmemh(INIT_FILE, bram, 0, RAM_DEPTH-1);
    end else begin: init_bram_to_zero
      integer ram_index;
      initial
        for (ram_index = 0; ram_index < RAM_DEPTH; ram_index = ram_index + 1)
          bram[ram_index] = {RAM_WIDTH{1'b0}};
    end
  endgenerate

  always @(posedge clock)
    if (writeEnableA)
      bram[addressA] <= dataInA;
    else
      bram_dataA <= bram[addressA];

  always @(posedge clock)
    if (writeEnableB)
      bram[addressB] <= dataInB;
    else
      bram_dataB <= bram[addressB];

    // The following is a 1 clock cycle read latency at the cost of a longer clock-to-out timing
    assign dataOutA = bram_dataA;
    assign dataOutB = bram_dataB;


  endmodule


