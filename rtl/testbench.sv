//# PRINT AFTER keyboard/ps2_keyboard.v
`ifdef SIMULATION_18447

`default_nettype none

`include "keyboard.vh"
`include "video.vh"
`include "dram.vh"
`include "memory.vh"
`include "basic_io.vh"
`include "board_config.vh"


module top;

    logic clock, clear;

    initial begin
        clear = 1'b1;
        repeat(5) @(posedge clock);
        clear = 1'b0;
    end

    initial begin
        clock = 1;
        forever #50 clock = ~clock;
    end


    parameter SVO_MODE             =   "1280x720";
    parameter SVO_FRAMERATE        =   60;
    parameter SVO_BITS_PER_PIXEL   = 24;

    KeyboardInterface keyboard(.clock, .clear);
    VideoInterfaceInternal video();
    DRAMAXIInterface dram();
    MemoryMappedIOInterface axiRemoteRead();
    MemoryMappedIOInterface axiRemoteWrite();
    BasicIO basicIO();

    Chip #(`SVO_PASS_PARAMS) chip(
        .keyboard(keyboard.Controller),
        .video(video.Controller),
        .dram(dram.Chip),
        .axiRemoteRead(axiRemoteRead.Device),
        .axiRemoteWrite(axiRemoteWrite.Device),
        .basicIO(basicIO.Remote),
        .clock,
        .clear
    );

    KeyboardMock keyboardMock(.keyboard(keyboard.Device));

    VideoMock videoMock(
        .video(video.Driver),
        .clock,
        .clear
    );

    DRAMMock dramMock(
        .dram(dram.AXI),
        .clock,
        .clear
    );

    RemoteMock remoteMock(
        .remoteRead(axiRemoteRead.Requester),
        .remoteWrite(axiRemoteWrite.Requester),
        .clock,
        .clear
    );

`ifdef PYNQ_Z2
    assign basicIO.switch = 2'b0;
    assign basicIO.button = 4'b0;
`elsif ZEDBOARD
    assign basicIO.switch = 8'b0;
    assign basicIO.button_C = 1'b0;
    assign basicIO.button_D = 1'b0;
    assign basicIO.button_L = 1'b0;
    assign basicIO.button_R = 1'b0;
    assign basicIO.button_U = 1'b0;
`endif

endmodule


`endif