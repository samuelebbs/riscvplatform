//# PRINT AFTER video/vram.sv
`default_nettype none

`include "memory.vh"
`include "compiler.vh"
`include "riscv_types.vh"

module RemoteMemory(
    MemoryMappedIOInterface.Device axi, memory,
    input logic clock, clear
);
    `MEMORY_FUNCTIONS

    `STATIC_ASSERT(`REMOTE_SIZE == `PAGE_SIZE, remote_size);
    `STATIC_ASSERT(`PAGE_SIZE == (2**10 * `XLEN_BYTES), page_size);

    memoryWriteInRange: assert property(@(posedge clock) disable iff(clear)
        |memory.request.writeEnable |->
        isInRemote(memory.request.physicalAddress)
    );

    axiWriteInRange: assert property (@(posedge clock) disable iff(clear)
        |axi.request.writeEnable |-> isInRemote(axi.request.physicalAddress)
    );

    localparam NB_COL = 4;
    localparam COL_WIDTH = 8;

    `STATIC_ASSERT($bits(Word_t) == NB_COL * COL_WIDTH, word_size);

    xilinx_bram_byte_write #(
        .NB_COL(NB_COL),
        .COL_WIDTH(COL_WIDTH),
        .RAM_DEPTH(1024)
    ) remoteBRAM(
        .addra(memory.request.physicalAddress[11:2]),
        .dina(memory.request.writeData),
        .wea(memory.request.writeEnable),
        .douta(memory.response.readData),

        .addrb(axi.request.physicalAddress[11:2]),
        .dinb(axi.request.writeData),
        .web(axi.request.writeEnable),
        .doutb(axi.response.readData),

        .clka(clock)
    );

    SimpleMemoryRequest_t memoryRequest_saved, axiRequest_saved;

    Register #(.WIDTH($bits(memoryRequest_saved))) memoryRequestRegister (
        .q  (memoryRequest_saved),
        .d   (memory.request    ),
        .enable (1'b1            ),
        .clock,
        .clear
    );
    Register #(.WIDTH($bits(axiRequest_saved))) axiRequestRegister (
        .q  (axiRequest_saved),
        .d   (axi.request     ),
        .enable (1'b1           ),
        .clock,
        .clear
    );

    // Visible on the next clock edge
    always_comb begin
        memory.response.request = memoryRequest_saved;
        axi.response.request = axiRequest_saved;
        memory.response.responseValid = isInRemote(
            memoryRequest_saved.physicalAddress
        );
        axi.response.responseValid = isInRemote(
            axiRequest_saved.physicalAddress
        );
    end

endmodule