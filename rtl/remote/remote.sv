//# PRINT AFTER include/remote/basic_io.vh
`default_nettype none

`include "remote.vh"
`include "memory.vh"
`include "basic_io.vh"
`include "board_config.vh"

module Remote(
    RemoteCoreInterface.Remote core,
    RemoteDRAMInterface.Remote dram,
    RemoteTimerInterface.Remote timer,
    BasicIO.Remote basicIO,

    // Assumes these are on the clock/clear domain
    MemoryMappedIOInterface.Device axiRead, axiWrite, memory,

    output logic dump,
    input logic clock, clear
);

    `MEMORY_FUNCTIONS

    readIsRead: assert property(@(posedge clock) disable iff(clear)
        !(|axiRead.request.writeEnable)
    );

    writeIsWrite: assert property(@(posedge clock) disable iff(clear)
        !axiWrite.request.readEnable
    );

    axiReadAligned: assert property(@(posedge clock) disable iff(clear)
        axiRead.request.readEnable |->
        axiRead.request.physicalAddress[1:0] == 2'b0
    );

    axiWriteAligned: assert property(@(posedge clock) disable iff(clear)
        |axiWrite.request.writeEnable |->
        axiWrite.request.physicalAddress[1:0] == 2'b0
    );

    memoryAligned: assert property(@(posedge clock) disable iff(clear)
        memory.request.readEnable || (|memory.request.writeEnable) |->
        memory.request.physicalAddress[1:0] == 2'b0
    );

    MemoryMappedIOInterface axiMemory();

    RemoteMemory remoteMemory(
        .axi(axiMemory.Device),
        .memory(memory),
        .clock,
        .clear
    );

    logic halted_n, debug_n;
    logic [31:0] errorCode;
    Word_t nonMemoryResult;

    assign core.halted = ~halted_n;
    assign timer.halted = ~halted_n;
    assign core.dump = dump;

    // Read Logic
    always_comb begin
        core.csrName = ControlStatusRegisterName_t'(
            axiRead.request.physicalAddress[13:2]
        );
        core.registerIndex = axiRead.request.physicalAddress[6:2];
        nonMemoryResult = 32'b0;
        casez(axiRead.request.physicalAddress[15:12])
            `REMOTE_CONTROL_PREFIX: begin
                casez(axiRead.request.physicalAddress[11:2])
                    `REMOTE_HALT:
                        nonMemoryResult = {30'b0, ~debug_n, ~halted_n};
                    `REMOTE_ERROR: nonMemoryResult = errorCode;

                    `REMOTE_PC: nonMemoryResult = core.pc;

                    `REMOTE_TIMER_LOW:
                        nonMemoryResult = timer.currentTime[31:0];
                    `REMOTE_TIMER_HIGH:
                        nonMemoryResult = timer.currentTime[63:32];
                    `REMOTE_NEXT_TIMER_LOW:
                        nonMemoryResult = timer.nextTickTime[31:0];
                    `REMOTE_NEXT_TIMER_HIGH:
                        nonMemoryResult = timer.nextTickTime[63:32];
                    `REMOTE_REGISTER_FILE: nonMemoryResult = core.readRegister;

                    `REMOTE_ERROR_ADDRESS: nonMemoryResult = dram.addressBase;
                    `REMOTE_ERROR_INDEX: nonMemoryResult = {26'b0, dram.index};
                    `REMOTE_ERROR_ARRAY: begin
                        nonMemoryResult = dram.writeArray[
                            axiRead.request.physicalAddress[5:2]
                        ];
                    end
                endcase
            end
            `REMOTE_IO_PREFIX: begin
                // This will be handled by the remote memory unit
                nonMemoryResult = `WORD_POISON;
            end
            `REMOTE_CSR_PREFIX: begin
                // CSR Case
                nonMemoryResult = core.readCSR;
            end
        endcase
    end

// TODO: It is sad that basicIO is a part of the remote core. It would be better
// to re-factor this into a top level driver
`ifdef PYNQ_Z2
    assign basicIO.led = {clear, ~clear, ~halted_n, ~debug_n};
    assign basicIO.rgb_led[0] = errorCode[2:0];
    // Red when stopped, green when running
    assign basicIO.rgb_led[1] = {~halted_n, halted_n, 1'b0};
`elsif ZEDBOARD
    assign basicIO.led = {
        clear, ~clear, ~halted_n, ~debug_n,
        1'b0, errorCode[2:0]
    };
`endif

    logic oneShotDump;

    // Write Logic
    always_ff @(posedge clock) begin
        if(clear) begin
            halted_n <= 1'b0;
            debug_n <= 1'b1;
            errorCode <= 32'b0;
            oneShotDump <= 1'b0;
        end else begin
            if(dram.readError)
                errorCode[`REMOTE_ERROR_READ_INDEX] <= 1'b1;
            if(dram.writeError)
                errorCode[`REMOTE_ERROR_WRITE_INDEX] <= 1'b1;
            if(dram.writeEnableMissing)
                errorCode[`REMOTE_ERROR_ENABLE_INDEX] <= 1'b1;

            // There are potentially some races here where the core is trying to
            // halt, but the remote interface is trying to force a continue. The
            // remote interface should never un-halt a core unless the debug
            // flag is set or on initial boot. These invariants are not enforced
            // in hardware, but violating them likely results in a bad state.
            if (axiWrite.request.writeEnable[0] &&
                axiWrite.request.physicalAddress[15:0] ==
                    {`REMOTE_CONTROL_PREFIX, `REMOTE_HALT, 2'b0}
            ) begin
                {debug_n, halted_n} <= ~axiWrite.request.writeData[1:0];
            end else begin
                if(core.coreHalt || core.coreDebug)
                    halted_n <= 1'b0;
                if(core.coreDebug) begin
                    debug_n <= 1'b0;
                    halted_n <= 1'b0;
                end
            end

            if(core.coreHalt) begin
                // Whenever the core thinks it is done, we want to dump results
                // in simulation
                oneShotDump <= 1'b1;
            end
        end
    end

    assign dump = core.coreHalt && !oneShotDump;

    always_comb begin
        axiMemory.request = '{default:0};
        if(axiRead.request.readEnable) begin
            axiMemory.request = simpleMemoryRead(
                `REMOTE_START + {axiRead.request.physicalAddress[11:2], 2'b0}
            );
        end else if(axiWrite.request.writeEnable) begin
            axiMemory.request = simpleMemoryWrite(
                `REMOTE_START + {axiWrite.request.physicalAddress[11:2], 2'b0},
                4'hf,
                axiWrite.request.writeData
            );
        end
    end
    assign axiMemory.clock = clock;
    assign axiMemory.clear = clear;

    Word_t nonMemoryResult_sync;
    Register #(.WIDTH($bits(nonMemoryResult))) nonMemoryResultRegister(
        .d(nonMemoryResult),
        .q(nonMemoryResult_sync),
        .clock,
        .clear,
        .enable(1'b1)
    );

    SimpleMemoryRequest_t axiRead_saved;
    Register #(.WIDTH($bits(axiRead_saved))) axiReadRegister(
        .d(axiRead.request),
        .q(axiRead_saved),
        .clock,
        .clear,
        .enable(1'b1)
    );

    SimpleMemoryRequest_t axiWrite_saved;
    Register #(.WIDTH($bits(axiWrite_saved))) axiWriteRegister(
        .d(axiWrite.request),
        .q(axiWrite_saved),
        .clock,
        .clear,
        .enable(1'b1)
    );

    assign axiWrite.response = '{
        request: axiWrite_saved,
        readData: `WORD_POISON,
        responseValid: |axiWrite_saved.writeEnable
    };

    always_comb begin
        if(axiRead_saved.physicalAddress[15:12] == `REMOTE_IO_PREFIX) begin
            axiRead.response = axiMemory.response;
        end else begin
            axiRead.response.request = axiRead_saved;
            axiRead.response.responseValid = axiRead_saved.readEnable;
            axiRead.response.readData = nonMemoryResult_sync;
        end
    end

endmodule