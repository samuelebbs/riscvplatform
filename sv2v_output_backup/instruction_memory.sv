//# PRINT AFTER include/memory/memory_controller.vh
`default_nettype none

`include "table_walk.vh"
`include "memory.vh"
`include "translation.vh"
`include "dram_fill.vh"
`include "cache.vh"
`include "memory_arbiter.vh"
`include "config.vh"
`include "memory_controller.vh"

module InstructionMemory(
    MemoryInterface.Memory request,

    MemoryArbiterInterface.Requester arbiter,
    MemoryControl.Memory control,
    TableWalkInterface.Requester tableWalk,
    DRAMFillInterface.Requester dram,

    TranslationInterface.Requester tlb,
    CacheInterface.Requester cache,

    input logic clock, clear
);

    `CACHE_FUNCTIONS

    instructionReadOnly: assert property (@(posedge clock) disable iff(clear)
        request.request.writeEnable == 4'b0 && request.request.flushMode == KEEP
    );

    instructionPhysicalAccessValid: assert property (@(posedge clock) disable iff(clear)
        request.response.responseType == CACHE_HIT || request.response.responseType == CACHE_MISS |->
        !(`RESERVED_LOW <= request.response.translationResponse.physicalAddress &&
            request.response.translationResponse.physicalAddress < `RESERVED_HIGH)
    );

    instructionResponseTypeValid: assert property (@(posedge clock) disable iff(clear)
        request.response.responseType != INVALID_MEMORY_RESPONSE |->
        request.response.responseType == CACHE_HIT ||
        request.response.responseType == CACHE_MISS ||
        request.response.responseType == PAGE_FAULT
    );

    instructionAligned: assert property (@(posedge clock) disable iff(clear)
        request.request.readEnable |-> request.request.virtualAddress[1:0] == 2'b0
    );

    MemoryMode_t memoryMode;
    VirtualAddress_t virtualAddress;
    ResponseType_t responseType, responseType_selected;
    Word_t readData_selected;
    PhysicalAddress_t physicalAddress;
    logic memoryHit;

    MemoryController instructionController(
        .request,
        .arbiter,
        .control,
        .tableWalk,
        .dram,
        .cacheResponse(cache.response),

        .translation_tlb(tlb.translation.entry),

        .memoryMode,
        .virtualAddress,
        .responseType,
        .physicalAddress,
        .responseType_selected,
        .readData_selected,
        .request_saved(), /* Not needed for instruction operations */
        .memoryHit,

        .clock,
        .clear
    );

    instructionNeverWrite: assert property (@(posedge clock) disable iff (clear)
        memoryMode != WRITE_MODE
    );

    instructionNeverInvalidate: assert property (@(posedge clock) disable iff(clear)
        memoryMode != INVALIDATE_MODE
    );

    instructionInCache: assert property (@(posedge clock) disable iff(clear)
        responseType != INVALID_MEMORY_RESPONSE |-> isCacheAddress(physicalAddress)
    );

    validImpliesHit: assert property (@(posedge clock) disable iff(clear)
        memoryHit |-> isCacheTagHit(cache.response, cachePhysicalTag(physicalAddress))
    );


    // Never invalidate from the instruction stream
    assign tlb.flushMode = KEEP;
    assign tlb.virtualTag = cacheVirtualTag(virtualAddress);

    assign cache.physicalAddress = physicalAddress;
    assign memoryHit = isCacheHit(cache.response);

    always_comb begin
        cache.request = '{default:0};
        tlb.virtualTagValid = 1'b0;

        unique case(memoryMode)
            READ_MODE: begin
                cache.request = cacheReadRequest(virtualAddress);
                tlb.virtualTagValid = 1'b1;
            end
            NO_MODE: begin
                // Do nothing
            end
        endcase
    end

    // Nothing special about instruction responseTypes
    assign responseType_selected = responseType;
    assign readData_selected = cache.response.readData;

endmodule