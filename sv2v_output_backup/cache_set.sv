//# PRINT AFTER memory/cache.sv
`default_nettype none

`include "cache.vh"
`include "compiler.vh"

module CacheSet #(
    parameter CacheSetNumber_t SET_NUMBER=0
    )(
    CacheSetInterface.CacheSet instruction, data,

    input  logic           clock, clear
);

    `STATIC_ASSERT($bits(CacheTagEntry_t) <= 36); // We use more than one bram if this is true
    `STATIC_ASSERT($bits(CacheIndex_t) + $bits(CacheBlockOffset_t) == 10); // Assumption about address
    `STATIC_ASSERT($bits(CacheBlockOffset_t) == $clog2(`CACHE_BLOCK_SIZE));

    logic [9:0] instructionBlockIndex, dataBlockIndex;

    assign instructionBlockIndex = {instruction.request.index, instruction.request.blockOffset};
    assign dataBlockIndex        = {data.request.index, data.request.blockOffset};

    CacheTagEntry_t instructionTagIn, instructionTagOut, dataTagIn, dataTagOut;

    function automatic logic isWriteEnabled(CacheRequest_t request);
        if(!request.isValid)
            return 1'b0;
        unique case(request.requestType)
            CACHE_READ      : return 1'b0;
            CACHE_WRITE     : return 1'b1;
            CACHE_DRAM_FILL : return 1'b1;
        endcase
    endfunction

    function automatic CacheTagEntry_t packTagEntry(CacheRequest_t request);
        return '{
            isValid : 1'b1, // If we are writing, it must be valid FIXME: (I think this is true)
            isDirty : request.requestType == CACHE_WRITE,
            tag     : request.tag
        };
    endfunction

    assign instructionTagIn = packTagEntry(instruction.request);
    assign dataTagIn        = packTagEntry(data.request);

    logic[3:0] instructionWriteEnable, dataWriteEnable;
    assign instructionWriteEnable = {4{instruction.request.isValid}} & instruction.request.writeEnable;
    assign dataWriteEnable        = {4{data.request.isValid}} & data.request.writeEnable;

    CacheTagBRAM tagBRAM(
        .index_instruction(instruction.request.index),
        .tagIn_instruction(instructionTagIn),
        .writeEnable_instruction(|instructionWriteEnable),
        .tagOut_instruction(instructionTagOut),

        .index_data(data.request.index),
        .tagIn_data(dataTagIn),
        .writeEnable_data(|dataWriteEnable),
        .tagOut_data(dataTagOut),

        .clock,
        .clear
    );


    `STATIC_ASSERT($bits(CacheIndex_t) + $bits(CacheBlockOffset_t) == 10);

    Word_t instructionWordOut, dataWordOut;

    xilinx_bram_byte_write #(.NB_COL(4), .COL_WIDTH(8), .RAM_DEPTH(1024)) block_bram(
        .addra(instructionBlockIndex),
        .dina(instruction.request.writeData),
        .wea(instructionWriteEnable),
        .douta(instructionWordOut),

        .addrb(dataBlockIndex),
        .dinb(data.request.writeData),
        .web(dataWriteEnable),
        .doutb(dataWordOut),

        .clka(clock)
        );

    CacheRequest_t savedInstructionRequest, savedDataRequest;

    Register #(.WIDTH($bits(CacheRequest_t))) instruction_reg(
        .q(savedInstructionRequest),
        .d(instruction.request),
        .enable(1'b1), // No stalling in cache
        .clock,
        .clear
    );

    Register #(.WIDTH($bits(CacheRequest_t))) data_reg (
        .q  (savedDataRequest),
        .d   (data.request),
        .enable (1'b1            ),
        .clock                  ,
        .clear
    );


    // Visible on the next clock cycle

    function automatic CacheResponse_t packResponse(CacheRequest_t savedRequest, Word_t dataOut, CacheTagEntry_t tagOut);
        return '{
            request  : savedRequest,
            readData  : dataOut,
            tagEntry : tagOut,
            readSet : SET_NUMBER
        };
    endfunction

    assign instruction.response = packResponse(savedInstructionRequest, instructionWordOut, instructionTagOut);
    assign data.response = packResponse(savedDataRequest, dataWordOut, dataTagOut);

endmodule

typedef struct packed {
    logic dirty;
    CachePhysicalTag_t tag;
} CacheTagBRAMEntry_t;

module CacheTagBRAM (
    input CacheIndex_t index_instruction, index_data,
    input CacheTagEntry_t tagIn_instruction, tagIn_data,
    input logic writeEnable_instruction, writeEnable_data,

    output CacheTagEntry_t tagOut_instruction, tagOut_data,

    input logic clock, clear
);

    localparam TAG_COUNT = 2 ** $bits(CacheIndex_t);

    logic [TAG_COUNT-1:0] tagValid;

    noDoubleWrite: assert property (@(posedge clock) disable iff(clear)
        writeEnable_instruction && writeEnable_data |-> index_instruction != index_data
    );

    always_ff @(posedge clock)
        if(clear)
            tagValid <= '{default:0};
        else begin
            if(writeEnable_instruction) begin
                tagValid[index_instruction] <= tagIn_instruction.isValid;
            end

            if(writeEnable_data) begin
                tagValid[index_data] <= tagIn_data.isValid;
            end
        end

    always_ff @(posedge clock) begin
        tagOut_instruction.isValid <= tagValid[index_instruction];
        tagOut_data.isValid <= tagValid[index_data];
    end

    CacheTagBRAMEntry_t entryIn_instruction, entryIn_data, entryOut_instruction, entryOut_data;

    assign entryIn_instruction = '{
        dirty: tagIn_instruction.isDirty,
        tag: tagIn_instruction.tag
    };

    assign entryIn_data = '{
        dirty: tagIn_data.isDirty,
        tag: tagIn_data.tag
    };


    xilinx_bram #(.RAM_WIDTH($bits(entryIn_instruction)), .RAM_DEPTH(TAG_COUNT)) tag_bram (
        .addressA    (index_instruction),
        .dataInA     (entryIn_instruction),
        .writeEnableA(writeEnable_instruction),
        .dataOutA    (entryOut_instruction),

        .addressB    (index_data),
        .dataInB     (entryIn_data),
        .writeEnableB(writeEnable_data),
        .dataOutB    (entryOut_data),

        .clock
    );

    always_comb begin
        tagOut_instruction.isDirty = entryOut_instruction.dirty;
        tagOut_instruction.tag = entryOut_instruction.tag;
        tagOut_data.isDirty = entryOut_data.dirty;
        tagOut_data.tag = entryOut_data.tag;
    end

endmodule