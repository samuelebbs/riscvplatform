//# PRINT AFTER include/memory/memory.vh
`default_nettype none

`include "memory.vh"
`include "dram.vh"
`include "riscv_types.vh"
`include "compiler.vh"
`include "memory_arbiter.vh"
`include "cache.vh"
`include "translation.vh"
`include "table_walk.vh"
`include "dram_fill.vh"

module Memory_SV(
    MemoryInterface.Memory instruction, data,

    DRAMInterface.Memory dram,
    MemoryMappedIOInterface.Requester vram, remote,

    MemoryControl.Memory control,

    input logic clock, clear
);

    // If this isn't true, then there are likely many assumptions that break in
    // the memory subsystem
    `STATIC_ASSERT(`XLEN == 32);

    dataSingleEnable: assert property (@(posedge  clock) disable iff(clear)
        !(data.request.readEnable && (|data.request.writeEnable))
    );

    instructionNeverWrite: assert property (@(posedge clock) disable iff(clear)
        !(|instruction.request.writeEnable)
    );

    instructionNeverInvalidate: assert property (@(posedge clock) disable iff(clear)
        instruction.request.flushMode == KEEP
    );

    // We use this assumption to optimize the non-virtual memory case
    dataInvalidateOnlyVirtualMemoryEnabled: assert property (@(posedge clock) disable iff(clear)
        data.request.flushMode != KEEP |-> control.virtualMemoryEnabled
    );


        // Cache tag lengths are manually encoded assuming 32 bit virtual address
    `STATIC_ASSERT(`XLEN == 32);
    `STATIC_ASSERT($bits(VirtualAddress_t) == 32);
    `STATIC_ASSERT($bits(PhysicalAddress_t) == 34);
    `STATIC_ASSERT(
        $bits(CacheIndex_t) +
        $bits(CacheBlockOffset_t) +
        $bits(CacheVirtualTag_t) +
        32'd2 == $bits(VirtualAddress_t)
    );
    `STATIC_ASSERT(
        $bits(CacheIndex_t) +
        $bits(CacheBlockOffset_t) +
        $bits(CachePhysicalTag_t) +
        32'd2 == $bits(PhysicalAddress_t)
    );

    TableWalkInterface instructionTableWalk();
    TranslationInterface instructionTLB();
    DRAMFillInterface instructionDRAMFill();
    CacheInterface instructionCache();
    MemoryArbiterInterface instructionArbiter();

    InstructionMemory instructionMemory(
        .request(instruction),
        .tableWalk(instructionTableWalk.Requester),
        .tlb(instructionTLB.Requester),
        .dram(instructionDRAMFill.Requester),
        .cache(instructionCache.Requester),
        .arbiter(instructionArbiter.Requester),
        .control(control),
        .clock,
        .clear
    );

    TableWalkInterface dataTableWalk();
    TranslationInterface dataTLB();
    DRAMFillInterface dataDRAMFill();
    CacheInterface dataCache();
    MemoryArbiterInterface dataArbiter();

    DataMemory dataMemory(
        .request(data),
        .tableWalk(dataTableWalk.Requester),
        .tlb(dataTLB.Requester),
        .dram(dataDRAMFill.Requester),
        .cache(dataCache.Requester),
        .arbiter(dataArbiter.Requester),
        .vram(vram),
        .remote(remote),
        .control(control),
        .clock,
        .clear
    );

    MemoryArbiterInterface tableWalkArbiter();
    CacheInterface tableWalkCache();
    DRAMFillInterface tableWalkDRAMFill();
    TranslationFill tableWalkTranslation();

    TableWalk tableWalk(
        .instruction(instructionTableWalk.TableWalk),
        .data(dataTableWalk.TableWalk),
        .arbiter(tableWalkArbiter.Requester),
        .cache(tableWalkCache.Requester),
        .dram(tableWalkDRAMFill.Requester),
        .tlb(tableWalkTranslation.TableWalk),
        .control(control),
        .clock,
        .clear
    );

    // TranslationLookasideBuffer tlb(
    //     .instruction(instructionTLB.TLB),
    //     .data(dataTLB.TLB),
    //     .translationFill(tableWalkTranslation.TLB),
    //     .clock,
    //     .clear
    // );

    logic [51:0] tableWalkTranslation_newTranslation;
    logic [51:0] tableWalkTranslation_instructionResponse;
    logic [51:0] tableWalkTranslation_dataResponse;

    logic [19:0] dataTLB_virtualTag;
    logic dataTLB_virtualTagValid;
    logic [31:0] dataTLB_flushMode;
    logic [51:0] dataTLB_translation;

    logic [19:0] instructionTLB_virtualTag;
    logic instructionTLB_virtualTagValid;
    logic [31:0] instructionTLB_flushMode;
    logic [51:0] instructionTLB_translation;

    always_comb begin
        instructionTLB_virtualTag = instructionTLB.virtualTag;
        instructionTLB_virtualTagValid = instructionTLB.virtualTagValid;
        instructionTLB_flushMode = instructionTLB.flushMode;
        instructionTLB.translation = instructionTLB_translation;

        dataTLB_virtualTag = dataTLB.virtualTag;
        dataTLB_virtualTagValid = dataTLB.virtualTagValid;
        dataTLB_flushMode = dataTLB.flushMode;
        dataTLB.translation = dataTLB_translation;

        tableWalkTranslation_newTranslation = tableWalkTranslation.newTranslation;
        tableWalkTranslation.instructionResponse = tableWalkTranslation_instructionResponse;
        tableWalkTranslation.dataResponse = tableWalkTranslation_dataResponse;
    end




    TranslationLookasideBuffer tlb(
        .instruction_virtualTag(instructionTLB_virtualTag),
        .instruction_virtualTagValid(instructionTLB_virtualTagValid),
        .instruction_flushMode(instructionTLB_flushMode),
        .instruction_translation(instructionTLB_translation),
        .data_virtualTag(dataTLB_virtualTag),
        .data_virtualTagValid(dataTLB_virtualTagValid),
        .data_flushMode(dataTLB_flushMode),
        .data_translation(dataTLB_translation),
        .translationFill_newTranslation(tableWalkTranslation_newTranslation),
        .translationFill_instructionResponse(tableWalkTranslation_instructionResponse),
        .translationFill_dataResponse(tableWalkTranslation_dataResponse),
        .clock(clock),
        .clear(clear)
    );


    MemoryArbiterInterface dramFillArbiter();
    CacheInterface dramFillCache();

    DRAMFill dramFill(
        .instruction(instructionDRAMFill.Filler),
        .data(dataDRAMFill.Filler),
        .tableWalk(tableWalkDRAMFill.Filler),
        .arbiter(dramFillArbiter.Requester),
        .cache(dramFillCache.Filler),
        .dram(dram),
        .clock,
        .clear
    );

    MemorySelect_t memorySelect;

    MemoryArbiter arbiter(
        .instruction(instructionArbiter.Arbiter),
        .data(dataArbiter.Arbiter),
        .tableWalk(tableWalkArbiter.Arbiter),
        .dramFill(dramFillArbiter.Arbiter),
        .select(memorySelect),
        .clock,
        .clear
    );

    CacheInterface cache_selected();

    instOnlySelected: assert property (
        @(posedge clock) disable iff(clear)
        memorySelect != INST_DATA |-> !instructionCache.request.isValid
    );

    dataOnlySelected: assert property (
        @(posedge clock) disable iff(clear)
        memorySelect != INST_DATA |-> !dataCache.request.isValid
    );

    tableWalkOnlySelected: assert property (
        @(posedge clock) disable iff(clear)
        memorySelect != TABLE_WALK |-> !tableWalkCache.request.isValid
    );

    dramFillOnlySelected: assert property (
        @(posedge clock) disable iff(clear)
        memorySelect != DRAM_FILL |-> !dramFillCache.request.isValid
    );

    MemorySelect_t memorySelect_last;
    Register #(.WIDTH($bits(memorySelect_last))) memorySelectRegister(
        .d(memorySelect),
        .q(memorySelect_last),
        .clock,
        .clear,
        .enable(1'b1)
    );

    always_comb begin
        cache_selected.request = '{default:0};
        unique case(memorySelect)
            INST_DATA: begin
                cache_selected.request = instructionCache.request;
            end
            TABLE_WALK: begin
                cache_selected.request = tableWalkCache.request;
            end
            DRAM_FILL: begin
                cache_selected.request = dramFillCache.request;
            end
        endcase
    end

    always_comb begin
        instructionCache.response = '{default:0};
        instructionCache.rawResponses = cache_selected.rawResponses;
        tableWalkCache.response = '{default:0};
        tableWalkCache.rawResponses = cache_selected.rawResponses;
        dramFillCache.response = '{default:0};
        dramFillCache.rawResponses = cache_selected.rawResponses;
        cache_selected.physicalAddress = `PHYSICAL_ADDRESS_POISON;

        unique case(memorySelect_last)
            INST_DATA: begin
                instructionCache.response = cache_selected.response;
                cache_selected.physicalAddress = instructionCache.physicalAddress;
            end
            TABLE_WALK: begin
                tableWalkCache.response = cache_selected.response;
                cache_selected.physicalAddress = tableWalkCache.physicalAddress;
            end
            DRAM_FILL: begin
                dramFillCache.response = cache_selected.response;
                cache_selected.physicalAddress = dramFillCache.physicalAddress;
            end
        endcase
    end

    // The extra logic is on the instruction fetch line since that line has less
    // logic than the data stage.
    Cache cache(
        .instruction(cache_selected.Cache),
        .data(dataCache.Cache),
        .clock,
        .clear
    );

endmodule // Memory

