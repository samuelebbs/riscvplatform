//# PRINT AFTER include/memory/dram_fill.vh
`default_nettype none

`include "dram_fill.vh"
`include "dram.vh"
`include "cache.vh"
`include "memory_arbiter.vh"
`include "config.vh"
`include "compiler.vh"

module DRAMFill(
    DRAMFillInterface.Filler instruction, data, tableWalk,

    MemoryArbiterInterface.Requester arbiter,
    CacheInterface.Filler cache,
    DRAMInterface.Memory dram,

    input logic clock, clear
);

    `CACHE_FUNCTIONS

    enum {
        FILL_IDLE,
        FILL_LOCK,
        FILL_CHECK,
        FILL_WRITE_BACK,
        FILL_WRITE_BACK_WAIT,
        FILL_CLEAN,
        FILL_SUCCESS
    } state, nextState;

    enum {NONE, INST, DATA, WALK} mode, nextMode;

    always_ff @(posedge clock)
        if(clear) begin
            state <= FILL_IDLE;
            mode <= NONE;
        end else begin
            state <= nextState;
            mode <= nextMode;
        end

    lockWithRequest: assert property (
        @(posedge clock) disable iff(clear)
        state == FILL_LOCK |-> instruction.requestFill || data.requestFill || tableWalk.requestFill
    );

    PhysicalAddress_t address_selected;
    always_comb begin
        address_selected = `PHYSICAL_ADDRESS_POISON;
        unique case(mode)
            NONE: address_selected = `PHYSICAL_ADDRESS_POISON;
            INST: address_selected = instruction.address;
            DATA: address_selected = data.address;
            WALK: address_selected = tableWalk.address;
        endcase
    end

    function automatic CacheRequest_t readDirtyRequest(
        PhysicalAddress_t originalAddress,
        CacheBlockOffset_t offset
    );
        CacheIndex_t index;
        index = cachePhysicalIndex(originalAddress);
        return readRequestManual(index, offset);
    endfunction

    logic offsetCounter_clear, offsetCounter_increment;
    CacheBlockOffset_t blockOffset;
    Counter #(.WIDTH(4)) offsetCounter(
        .clock,
        .clear(clear | offsetCounter_clear) ,
        .enable(offsetCounter_increment),
        .q(blockOffset)
    );

    PhysicalAddress_t address_last;
    Register #(.WIDTH($bits(address_last))) addressLastRegister(
        .q(address_last),
        .d(dram.physicalAddress),
        .enable(1'b1),
        .clock,
        .clear
    );

    writeBackWaitCounterClear: assert property (
        @(posedge clock) disable iff(clear)
        state == FILL_WRITE_BACK_WAIT |-> blockOffset == 4'b0
    );

    transactionCompletePriorToSuccess: assert property (
        @(posedge clock) disable iff(clear)
        state == FILL_SUCCESS |-> $past(dram.transactionComplete, 1)
    );

    CacheSetNumber_t fillSet;
    logic fillSetSelect;

    CacheEvictionSelector evictionSelector(
        .instructionResponse(instruction.cacheResponse),
        .dataResponse(data.cacheResponse),
        .select(fillSetSelect),
        .setNumber(fillSet),
        .clock,
        .clear
    );

    CacheResponse_t evictedResponse;

    assign evictedResponse = cache.rawResponses[fillSet];

    writeBackCacheHit: assert property (
        @(posedge clock) disable iff(clear)
        state == FILL_WRITE_BACK |->
        evictedResponse.request.isValid && evictedResponse.tagEntry.isValid
    );

    writeBackDRAMValid: assert property (
        @(posedge clock) disable iff(clear)
        state == FILL_WRITE_BACK |=> dram.responseValid
    );

    always_comb begin : controlLogic
        nextState = state;
        nextMode = mode;

        arbiter.isIdle = 1'b0;
        arbiter.requestLock = 1'b0;
        arbiter.handoff = MEMORY_UNLOCKED;

        cache.request = '{default:0};
        cache.physicalAddress = `PHYSICAL_ADDRESS_POISON;
        fillSetSelect = 1'b0;

        instruction.requestComplete = 1'b0;
        data.requestComplete = 1'b0;
        tableWalk.requestComplete = 1'b0;

        offsetCounter_clear = 1'b0;
        offsetCounter_increment = 1'b0;

        dram.physicalAddress = `PHYSICAL_ADDRESS_POISON;
        dram.readEnable = 1'b0;
        dram.writeEnable = 1'b0;
        dram.writeData = `WORD_POISON;

        unique case(state)
            FILL_IDLE: begin
                arbiter.isIdle = 1'b1;
                nextMode = NONE;
                if (instruction.requestFill ||
                    data.requestFill ||
                    tableWalk.requestFill
                ) begin
                    // We cannot latch the request address yet since it may not
                    // be stable until the next cycle after request is asserted.
                    nextState = FILL_LOCK; // T1
                end else begin
                    nextState = FILL_IDLE; // T2
                end
            end
            FILL_LOCK: begin
                arbiter.requestLock = 1'b1;
                offsetCounter_clear = 1'b1;
                if(arbiter.lock != DRAM_LOCK) begin
                    // Wait here until the lock is acquired
                    arbiter.isIdle = 1'b1;
                    nextState = FILL_LOCK; // T3
                end else begin
                    arbiter.isIdle = 1'b0;
                    nextState = FILL_CHECK; // T4
                    // We will make this request early so that it is cached for the next stage.
                    fillSetSelect = 1'b1;
                    // Now that we have the lock, re-run the cache request to
                    // check if it is dirty or clean. We will prioritize data
                    // requests since those would be the most downstream
                    // possible if they exist, and then next tlb requests and
                    // then finally instruction requests. Observe that there
                    // can't be both a TLB and a DRAM request from instruction
                    // fetch, so our prioritization is always correct.
                    // DATA - DRAM (Oldest)
                    // DATA - TLB
                    // INST - DRAM
                    // INST - TLB (Youngest)
                    if(data.requestFill) begin
                        nextMode = DATA;
                        cache.request = readRequest(data.address);
                    end else if(tableWalk.requestFill) begin
                        nextMode = WALK;
                        cache.request = readRequest(tableWalk.address);
                    end else if(instruction.requestFill) begin
                        nextMode = INST;
                        cache.request = readRequest(instruction.address);
                    end else begin
                        `ifdef SIMULATION_18447
                        $error("Missing request?");
                        `endif
                        nextState = FILL_IDLE; // T5
                    end
                end
            end
            FILL_CHECK: begin
                arbiter.requestLock = 1'b1;
                cache.physicalAddress = address_selected;
                if(isCacheHit(cache.response)) begin
                    // False positive; another fill operation completed before
                    // this one could start
                    `ifdef SIMULATION_18447
                    $display("false positive on fill request: %x [%x] %p", address_selected, cachePhysicalTag(address_selected), cache.response);
                    `endif
                    nextState = FILL_SUCCESS; // T6
                end else begin
                    // If we don't have a cache hit, then the response is
                    // invalid, so we need to check the actual dirty bits that
                    // are forwarded with the interface to determine if we have
                    // a dirty or clean response
                    if(isCacheDirty(cache.rawResponses[fillSet])) begin
                        cache.request = readDirtyRequest(address_selected, blockOffset);
                        offsetCounter_increment = 1'b1;
                        nextState = FILL_WRITE_BACK; // T7
                    end else begin
                        offsetCounter_clear = 1'b1;
                        nextState = FILL_CLEAN; // T8
                    end
                end
            end
            FILL_WRITE_BACK: begin
                // At this point, the cache.response should be the data that
                // needs to be sent to dram. We also need to queue up the next
                // cache.request so that we can keep looping here, unless we are
                // on the last element.  There is an underlying assumption here
                // that when doing a write-back, dram is always ready so each
                // operation takes only a single cycle. This isn't true on the
                // read-clean branch since we might we will need to wait for the
                // results, but on write-back the dram wrapper will buffer for
                // us.
                arbiter.requestLock = 1'b1;
                // Send the current response to dram
                dram.physicalAddress = cachePhysicalAddress(evictedResponse);
                dram.writeData = evictedResponse.readData;
                dram.writeEnable = 1'b1;
                dram.readEnable = 1'b0;

                cache.physicalAddress = address_selected;

                // Setup the next request unless done
                if(blockOffset == 4'b0) begin
                    // This means we wrapped around so this is the last dram request
                    nextState = FILL_WRITE_BACK_WAIT; // T9
                end else begin
                    cache.request = readDirtyRequest(address_selected, blockOffset);
                    offsetCounter_increment = 1'b1;
                    // nextState = FILL_WRITE_BACK; T10
                end
            end
            FILL_WRITE_BACK_WAIT: begin
                arbiter.requestLock = 1'b1;
                cache.physicalAddress = address_selected;
                if(dram.transactionComplete) begin
                    nextState = FILL_CLEAN; // T11
                end else begin
                    nextState = FILL_WRITE_BACK_WAIT; // T12
                end
            end
            FILL_CLEAN: begin
                // Here we need to queue the current dram read request and then
                // wait until we have a valid result before issuing the cache
                // requests
                arbiter.requestLock = 1'b1;
                // Request dram base address for the current offset
                dram.writeEnable = 1'b0;
                dram.readEnable = 1'b1;

                cache.physicalAddress = address_last;

                if(dram.responseValid) begin
                    cache.request = cleanWriteRequest(address_last, dram.readData, fillSet);
                    if(blockOffset != 4'hf) begin
                        dram.physicalAddress = {address_selected[33:6], blockOffset + 4'd1, 2'b0};
                        offsetCounter_increment = 1'b1;
                        // T13
                    end else begin
                        dram.readEnable = 1'b0;
                        nextState = FILL_SUCCESS; // T14
                    end
                end else begin
                    dram.physicalAddress = {address_selected[33:6], blockOffset, 2'b0};
                    // T15
                end

            end
            FILL_SUCCESS: begin
                arbiter.isIdle = 1'b1;
                unique case (mode)
                    DATA: begin
                        data.requestComplete = 1'b1;
                    end
                    INST: begin
                        instruction.requestComplete = 1'b1;
                    end
                    WALK: begin
                        arbiter.handoff = TABLE_WALK_LOCK; // T16
                        tableWalk.requestComplete = 1'b1;
                    end
                endcase
                nextState = FILL_IDLE; // T17
            end
        endcase
    end // controlLogic

endmodule // DRAMFill

module CacheEvictionSelector(
    input CacheResponse_t instructionResponse, dataResponse,

    input logic select,

    // Sequentially set based on select
    output CacheSetNumber_t setNumber,

    input logic clock, clear
);

    `CACHE_FUNCTIONS

    setNumberValid: assert property (@(posedge clock) disable iff(clear)
        setNumber < `CACHE_SETS
    );

    `ifdef SINGLE_SET

    `STATIC_ASSERT(`CACHE_SETS == 1);
    assign setNumber = 1'd0;

    `else

    `STATIC_ASSERT(`CACHE_SETS > 1);
    logic instructionHit_last, dataHit_last;

    // TODO: Consider adding support for checking valid/dirty bits
    NotMostRecentlyUsedPolicy #(.WIDTH($bits(CacheSetNumber_t))) policy(
        .select,
        .index(setNumber),
        .recentValid({instructionHit_last, dataHit_last}),
        .recent({instructionResponse.readSet, dataResponse.readSet}),
        .clock,
        .clear
    );

    assign instructionHit_last = isCacheHit(instructionResponse);
    assign dataHit_last = isCacheHit(dataResponse);

    `endif

endmodule // CacheEvictionSelector