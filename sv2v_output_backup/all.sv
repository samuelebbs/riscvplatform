// `default_nettype none
`include "riscv_types.vh"
`include "memory.vh"
`include "dram.vh"
`include "translation.vh"
`include "config.vh"

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
module Chip (
	keyboard_clock,
	keyboard_clear,
	keyboard_readch,
	keyboard_scancode,
	keyboard_ascii_ready,
	video_clock,
	video_clear,
	video_valid,
	video_ready,
	video_data,
	video_control,
	dram_writeData,
	dram_readData,
	dram_addressBase,
	dram_readEnable,
	dram_writeEnable,
	dram_requestAccepted,
	dram_requestCompleted,
	dram_requestError,
	axiRemoteRead_request,
	axiRemoteRead_response,
	axiRemoteRead_clock,
	axiRemoteRead_clear,
	axiRemoteWrite_request,
	axiRemoteWrite_response,
	axiRemoteWrite_clock,
	axiRemoteWrite_clear,
	basicIO_switch,
	basicIO_button,
	basicIO_led,
	basicIO_rgb_led,
	clock,
	clear
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	input wire keyboard_clock;
	input wire keyboard_clear;
	input wire [7:0] keyboard_readch;
	input wire [7:0] keyboard_scancode;
	input wire keyboard_ascii_ready;
	input wire video_clock;
	input wire video_clear;
	output wire video_valid;
	input wire video_ready;
	output wire [23:0] video_data;
	output wire [0:0] video_control;
	output wire [511:0] dram_writeData;
	input wire [511:0] dram_readData;
	output wire [33:0] dram_addressBase;
	output wire dram_readEnable;
	output wire dram_writeEnable;
	input wire dram_requestAccepted;
	input wire dram_requestCompleted;
	input wire dram_requestError;
	input wire [70:0] axiRemoteRead_request;
	output wire [103:0] axiRemoteRead_response;
	input wire axiRemoteRead_clock;
	input wire axiRemoteRead_clear;
	input wire [70:0] axiRemoteWrite_request;
	output wire [103:0] axiRemoteWrite_response;
	input wire axiRemoteWrite_clock;
	input wire axiRemoteWrite_clear;
	input wire [1:0] basicIO_switch;
	input wire [3:0] basicIO_button;
	output wire [3:0] basicIO_led;
	output wire [5:0] basicIO_rgb_led;
	input wire clock;
	input wire clear;
	generate
		if ((! (32'h1000 < (32'h1000 + 'd4096)))) begin
			DoesNotExist foo();
		end
	endgenerate
	generate
		if ((! ((32'h1000 + 'd4096) <= 32'h2000))) begin
			DoesNotExist foo();
		end
	endgenerate
	generate
		if ((! (32'h2000 < (32'h2000 + 'd4096)))) begin
			DoesNotExist foo();
		end
	endgenerate
	wire dump;
	// expanded instance: instructionMemory
	wire [100:0] instructionMemory_request;
	wire [278:0] instructionMemory_response;
	wire instructionMemory_ready;
	// removed modport Memory
	// removed modport Requester
	// expanded instance: dataMemory
	wire [100:0] dataMemory_request;
	wire [278:0] dataMemory_response;
	wire dataMemory_ready;
	// removed modport Memory
	// removed modport Requester
	// expanded instance: dramMemory
	wire [33:0] dramMemory_physicalAddress;
	wire dramMemory_readEnable;
	wire dramMemory_writeEnable;
	wire [(32 - 1):0] dramMemory_writeData;
	wire dramMemory_responseValid;
	wire [(32 - 1):0] dramMemory_readData;
	wire dramMemory_transactionComplete;
	// removed modport Memory
	// removed modport DRAM
	// expanded instance: memoryVRAM
	wire memoryVRAM_clock;
	wire memoryVRAM_clear;
	wire [70:0] memoryVRAM_request;
	wire [103:0] memoryVRAM_response;
	// removed modport Requester
	// removed modport Device
	// expanded instance: memoryRemote
	wire memoryRemote_clock;
	wire memoryRemote_clear;
	wire [70:0] memoryRemote_request;
	wire [103:0] memoryRemote_response;
	// removed modport Requester
	// removed modport Device
	// expanded instance: memoryControl
	wire memoryControl_supervisorPagesEnabled;
	wire memoryControl_userPagesEnabled;
	wire memoryControl_virtualMemoryEnabled;
	wire [33:0] memoryControl_rootPageTable;
	// removed modport Core
	// removed modport Memory
	Memory memory(
		.instruction_ready(instructionMemory_ready),
		.instruction_request(instructionMemory_request),
		.instruction_response(instructionMemory_response),
		.data_ready(dataMemory_ready),
		.data_request(dataMemory_request),
		.data_response(dataMemory_response),
		.dram_physicalAddress(dramMemory_physicalAddress),
		.dram_readEnable(dramMemory_readEnable),
		.dram_writeEnable(dramMemory_writeEnable),
		.dram_writeData(dramMemory_writeData),
		.dram_responseValid(dramMemory_responseValid),
		.dram_readData(dramMemory_readData),
		.dram_transactionComplete(dramMemory_transactionComplete),
		.vram_request(memoryVRAM_request),
		.vram_response(memoryVRAM_response),
		.vram_clock(memoryVRAM_clock),
		.vram_clear(memoryVRAM_clear),
		.remote_request(memoryRemote_request),
		.remote_response(memoryRemote_response),
		.remote_clock(memoryRemote_clock),
		.remote_clear(memoryRemote_clear),
		.control_supervisorPagesEnabled(memoryControl_supervisorPagesEnabled),
		.control_userPagesEnabled(memoryControl_userPagesEnabled),
		.control_virtualMemoryEnabled(memoryControl_virtualMemoryEnabled),
		.control_rootPageTable(memoryControl_rootPageTable),
		.clock(clock),
		.clear(clear)
	);
	// expanded instance: dramRemote
	wire dramRemote_readError;
	wire dramRemote_writeError;
	wire dramRemote_writeEnableMissing;
	wire [5:0] dramRemote_index;
	wire [511:0] dramRemote_writeArray;
	wire [33:0] dramRemote_addressBase;
	// removed modport DRAM
	// removed modport Remote
	DRAMWrapper dramWrapper(
		.memory_physicalAddress(dramMemory_physicalAddress),
		.memory_readEnable(dramMemory_readEnable),
		.memory_writeEnable(dramMemory_writeEnable),
		.memory_writeData(dramMemory_writeData),
		.memory_responseValid(dramMemory_responseValid),
		.memory_readData(dramMemory_readData),
		.memory_transactionComplete(dramMemory_transactionComplete),
		.axi_writeData(dram_writeData),
		.axi_readData(dram_readData),
		.axi_addressBase(dram_addressBase),
		.axi_readEnable(dram_readEnable),
		.axi_writeEnable(dram_writeEnable),
		.axi_requestAccepted(dram_requestAccepted),
		.axi_requestCompleted(dram_requestCompleted),
		.axi_requestError(dram_requestError),
		.remote_readError(dramRemote_readError),
		.remote_writeError(dramRemote_writeError),
		.remote_writeEnableMissing(dramRemote_writeEnableMissing),
		.remote_index(dramRemote_index),
		.remote_writeArray(dramRemote_writeArray),
		.remote_addressBase(dramRemote_addressBase),
		.clock(clock),
		.clear(clear)
	);
	// expanded instance: keyboardInterrupt
	wire [(32 - 1):0] keyboardInterrupt_tval;
	wire keyboardInterrupt_interruptAccepted;
	wire keyboardInterrupt_interruptPending;
	// removed modport InterruptController
	// removed modport Device
	KeyboardController keyboardController(
		.keyboard_clock(keyboard_clock),
		.keyboard_clear(keyboard_clear),
		.keyboard_readch(keyboard_readch),
		.keyboard_scancode(keyboard_scancode),
		.keyboard_ascii_ready(keyboard_ascii_ready),
		.interruptController_tval(keyboardInterrupt_tval),
		.interruptController_interruptPending(keyboardInterrupt_interruptPending),
		.interruptController_interruptAccepted(keyboardInterrupt_interruptAccepted)
	);
	// expanded instance: displayVRAM
	wire displayVRAM_clock;
	wire displayVRAM_clear;
	wire [70:0] displayVRAM_request;
	wire [103:0] displayVRAM_response;
	// removed modport Requester
	// removed modport Device
	DisplayController #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) displayController(
		.videoMemory_request(displayVRAM_request),
		.videoMemory_response(displayVRAM_response),
		.videoMemory_clock(displayVRAM_clock),
		.videoMemory_clear(displayVRAM_clear),
		.videoInternal_clock(video_clock),
		.videoInternal_clear(video_clear),
		.videoInternal_valid(video_valid),
		.videoInternal_ready(video_ready),
		.videoInternal_data(video_data),
		.videoInternal_control(video_control)
	);
	VRAM vram(
		.memory_request(memoryVRAM_request),
		.memory_response(memoryVRAM_response),
		.memory_clock(memoryVRAM_clock),
		.memory_clear(memoryVRAM_clear),
		.display_request(displayVRAM_request),
		.display_response(displayVRAM_response),
		.display_clock(displayVRAM_clock),
		.display_clear(displayVRAM_clear)
	);
	// expanded instance: timerInterrupt
	wire [(32 - 1):0] timerInterrupt_tval;
	wire timerInterrupt_interruptAccepted;
	wire timerInterrupt_interruptPending;
	// removed modport InterruptController
	// removed modport Device
	// expanded instance: timerRemote
	wire timerRemote_halted;
	wire [63:0] timerRemote_currentTime;
	wire [63:0] timerRemote_nextTickTime;
	// removed modport Timer
	// removed modport Remote
	TimerController timer(
		.interruptController_tval(timerInterrupt_tval),
		.interruptController_interruptPending(timerInterrupt_interruptPending),
		.interruptController_interruptAccepted(timerInterrupt_interruptAccepted),
		.remote_halted(timerRemote_halted),
		.remote_currentTime(timerRemote_currentTime),
		.remote_nextTickTime(timerRemote_nextTickTime),
		.clock(clock),
		.clear(clear)
	);
	// expanded instance: coreRemote
	wire [(12 - 1):0] coreRemote_csrName;
	wire [($clog2(32) - 1):0] coreRemote_registerIndex;
	wire [(32 - 1):0] coreRemote_readCSR;
	wire [(32 - 1):0] coreRemote_readRegister;
	wire [(32 - 1):0] coreRemote_pc;
	wire coreRemote_coreHalt;
	wire coreRemote_coreDebug;
	wire coreRemote_halted;
	wire coreRemote_dump;
	// removed modport Core
	// removed modport Remote
	Core core(
		.instruction_ready(instructionMemory_ready),
		.instruction_request(instructionMemory_request),
		.instruction_response(instructionMemory_response),
		.data_ready(dataMemory_ready),
		.data_request(dataMemory_request),
		.data_response(dataMemory_response),
		.memoryControl_supervisorPagesEnabled(memoryControl_supervisorPagesEnabled),
		.memoryControl_userPagesEnabled(memoryControl_userPagesEnabled),
		.memoryControl_virtualMemoryEnabled(memoryControl_virtualMemoryEnabled),
		.memoryControl_rootPageTable(memoryControl_rootPageTable),
		.remote_csrName(coreRemote_csrName),
		.remote_registerIndex(coreRemote_registerIndex),
		.remote_readCSR(coreRemote_readCSR),
		.remote_readRegister(coreRemote_readRegister),
		.remote_pc(coreRemote_pc),
		.remote_coreHalt(coreRemote_coreHalt),
		.remote_coreDebug(coreRemote_coreDebug),
		.remote_halted(coreRemote_halted),
		.remote_dump(coreRemote_dump),
		.keyboard_tval(keyboardInterrupt_tval),
		.keyboard_interruptPending(keyboardInterrupt_interruptPending),
		.keyboard_interruptAccepted(keyboardInterrupt_interruptAccepted),
		.timer_tval(timerInterrupt_tval),
		.timer_interruptPending(timerInterrupt_interruptPending),
		.timer_interruptAccepted(timerInterrupt_interruptAccepted),
		.clock(clock),
		.clear(clear)
	);
	Remote remote(
		.core_csrName(coreRemote_csrName),
		.core_registerIndex(coreRemote_registerIndex),
		.core_readCSR(coreRemote_readCSR),
		.core_readRegister(coreRemote_readRegister),
		.core_pc(coreRemote_pc),
		.core_coreHalt(coreRemote_coreHalt),
		.core_coreDebug(coreRemote_coreDebug),
		.core_halted(coreRemote_halted),
		.core_dump(coreRemote_dump),
		.dram_readError(dramRemote_readError),
		.dram_writeError(dramRemote_writeError),
		.dram_writeEnableMissing(dramRemote_writeEnableMissing),
		.dram_index(dramRemote_index),
		.dram_writeArray(dramRemote_writeArray),
		.dram_addressBase(dramRemote_addressBase),
		.timer_halted(timerRemote_halted),
		.timer_currentTime(timerRemote_currentTime),
		.timer_nextTickTime(timerRemote_nextTickTime),
		.basicIO_switch(basicIO_switch),
		.basicIO_button(basicIO_button),
		.basicIO_led(basicIO_led),
		.basicIO_rgb_led(basicIO_rgb_led),
		.axiRead_request(axiRemoteRead_request),
		.axiRead_response(axiRemoteRead_response),
		.axiRead_clock(axiRemoteRead_clock),
		.axiRead_clear(axiRemoteRead_clear),
		.axiWrite_request(axiRemoteWrite_request),
		.axiWrite_response(axiRemoteWrite_response),
		.axiWrite_clock(axiRemoteWrite_clock),
		.axiWrite_clear(axiRemoteWrite_clear),
		.memory_request(memoryRemote_request),
		.memory_response(memoryRemote_response),
		.memory_clock(memoryRemote_clock),
		.memory_clear(memoryRemote_clear),
		.dump(dump),
		.clock(clock),
		.clear(clear)
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module ChipInterface (
	clock,
	resetn,
	switch,
	rgb_led,
	led,
	button,
	m00_axi_aclk,
	m00_axi_aresetn,
	m00_axi_awaddr,
	m00_axi_awprot,
	m00_axi_awvalid,
	m00_axi_awready,
	m00_axi_wdata,
	m00_axi_wstrb,
	m00_axi_wvalid,
	m00_axi_wready,
	m00_axi_bresp,
	m00_axi_bvalid,
	m00_axi_bready,
	m00_axi_araddr,
	m00_axi_arprot,
	m00_axi_arvalid,
	m00_axi_arready,
	m00_axi_rdata,
	m00_axi_rresp,
	m00_axi_rvalid,
	m00_axi_rready,
	s00_axi_aclk,
	s00_axi_aresetn,
	s00_axi_awaddr,
	s00_axi_awprot,
	s00_axi_awvalid,
	s00_axi_awready,
	s00_axi_wdata,
	s00_axi_wstrb,
	s00_axi_wvalid,
	s00_axi_wready,
	s00_axi_bresp,
	s00_axi_bvalid,
	s00_axi_bready,
	s00_axi_araddr,
	s00_axi_arprot,
	s00_axi_arvalid,
	s00_axi_arready,
	s00_axi_rdata,
	s00_axi_rresp,
	s00_axi_rvalid,
	s00_axi_rready,
	clk_pixel,
	clk_5x_pixel,
	locked,
	tmds_clk_n,
	tmds_clk_p,
	tmds_d_n,
	tmds_d_p,
	ps2_clk,
	ps2_data
);
	parameter SVO_MODE = "1280x720";
	parameter SVO_FRAMERATE = 60;
	parameter integer C_M00_AXI_ADDR_WIDTH = 32;
	parameter integer C_M00_AXI_DATA_WIDTH = 32;
	parameter integer C_M00_AXI_TRANSACTIONS_NUM = 16;
	parameter integer C_M00_PHYSICAL_ADDRESS_OFFSET = 'h8000000;
	parameter integer C_S00_AXI_DATA_WIDTH = 32;
	parameter integer C_S00_AXI_ADDR_WIDTH = 16;
	input wire clock;
	input wire resetn;
	input wire [1:0] switch;
	output reg [5:0] rgb_led;
	output reg [3:0] led;
	input wire [3:0] button;
	input wire m00_axi_aclk;
	input wire m00_axi_aresetn;
	output wire [(C_M00_AXI_ADDR_WIDTH - 1):0] m00_axi_awaddr;
	output wire [2:0] m00_axi_awprot;
	output wire m00_axi_awvalid;
	input wire m00_axi_awready;
	output wire [(C_M00_AXI_DATA_WIDTH - 1):0] m00_axi_wdata;
	output wire [((C_M00_AXI_DATA_WIDTH / 8) - 1):0] m00_axi_wstrb;
	output wire m00_axi_wvalid;
	input wire m00_axi_wready;
	input wire [1:0] m00_axi_bresp;
	input wire m00_axi_bvalid;
	output wire m00_axi_bready;
	output wire [(C_M00_AXI_ADDR_WIDTH - 1):0] m00_axi_araddr;
	output wire [2:0] m00_axi_arprot;
	output wire m00_axi_arvalid;
	input wire m00_axi_arready;
	input wire [(C_M00_AXI_DATA_WIDTH - 1):0] m00_axi_rdata;
	input wire [1:0] m00_axi_rresp;
	input wire m00_axi_rvalid;
	output wire m00_axi_rready;
	input wire s00_axi_aclk;
	input wire s00_axi_aresetn;
	input wire [(C_S00_AXI_ADDR_WIDTH - 1):0] s00_axi_awaddr;
	input wire [2:0] s00_axi_awprot;
	input wire s00_axi_awvalid;
	output wire s00_axi_awready;
	input wire [(C_S00_AXI_DATA_WIDTH - 1):0] s00_axi_wdata;
	input wire [((C_S00_AXI_DATA_WIDTH / 8) - 1):0] s00_axi_wstrb;
	input wire s00_axi_wvalid;
	output wire s00_axi_wready;
	output wire [1:0] s00_axi_bresp;
	output wire s00_axi_bvalid;
	input wire s00_axi_bready;
	input wire [(C_S00_AXI_ADDR_WIDTH - 1):0] s00_axi_araddr;
	input wire [2:0] s00_axi_arprot;
	input wire s00_axi_arvalid;
	output wire s00_axi_arready;
	output wire [(C_S00_AXI_DATA_WIDTH - 1):0] s00_axi_rdata;
	output wire [1:0] s00_axi_rresp;
	output wire s00_axi_rvalid;
	input wire s00_axi_rready;
	input wire clk_pixel;
	input wire clk_5x_pixel;
	input wire locked;
	output wire tmds_clk_n;
	output wire tmds_clk_p;
	output wire [2:0] tmds_d_n;
	output wire [2:0] tmds_d_p;
	inout wire ps2_clk;
	inout wire ps2_data;
	reg [3:0] locked_clk_q;
	reg [3:0] resetn_clk_pixel_q;
	always @(posedge clock) locked_clk_q <= {locked_clk_q, locked};
	always @(posedge clk_pixel) resetn_clk_pixel_q <= {resetn_clk_pixel_q[2:0], resetn};
	wire clk_resetn;
	assign clk_resetn = (resetn && locked_clk_q[3]);
	wire clk_pixel_resetn;
	assign clk_pixel_resetn = (locked && resetn_clk_pixel_q[3]);
	wire clearn;
	assign clearn = clk_resetn;
	wire clear;
	assign clear = (~ clearn);
	localparam SVO_BITS_PER_PIXEL = 24;
	// expanded instance: keyboard
	wire keyboard_clock;
	wire keyboard_clear;
	wire [7:0] keyboard_readch;
	wire [7:0] keyboard_scancode;
	wire keyboard_ascii_ready;
	// removed modport Controller
	// removed modport Device
	assign keyboard_clock = clock;
	assign keyboard_clear = clear;
	// expanded instance: video
	wire video_clock;
	wire video_clear;
	wire video_valid;
	wire video_ready;
	wire [23:0] video_data;
	wire [0:0] video_control;
	// removed modport Controller
	// removed modport Driver
	// expanded instance: dram
	wire [511:0] dram_writeData;
	wire [511:0] dram_readData;
	wire [33:0] dram_addressBase;
	wire dram_readEnable;
	wire dram_writeEnable;
	wire dram_requestAccepted;
	wire dram_requestCompleted;
	wire dram_requestError;
	// removed modport Chip
	// removed modport AXI
	// expanded instance: axiRemoteRead
	wire axiRemoteRead_clock;
	wire axiRemoteRead_clear;
	wire [70:0] axiRemoteRead_request;
	wire [103:0] axiRemoteRead_response;
	// removed modport Requester
	// removed modport Device
	// expanded instance: axiRemoteWrite
	wire axiRemoteWrite_clock;
	wire axiRemoteWrite_clear;
	wire [70:0] axiRemoteWrite_request;
	wire [103:0] axiRemoteWrite_response;
	// removed modport Requester
	// removed modport Device
	// expanded instance: basicIO
	wire [1:0] basicIO_switch;
	wire [5:0] basicIO_rgb_led;
	wire [3:0] basicIO_led;
	wire [3:0] basicIO_button;
	// removed modport Remote
	Chip #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) chip(
		.keyboard_clock(keyboard_clock),
		.keyboard_clear(keyboard_clear),
		.keyboard_readch(keyboard_readch),
		.keyboard_scancode(keyboard_scancode),
		.keyboard_ascii_ready(keyboard_ascii_ready),
		.video_clock(video_clock),
		.video_clear(video_clear),
		.video_valid(video_valid),
		.video_ready(video_ready),
		.video_data(video_data),
		.video_control(video_control),
		.dram_writeData(dram_writeData),
		.dram_readData(dram_readData),
		.dram_addressBase(dram_addressBase),
		.dram_readEnable(dram_readEnable),
		.dram_writeEnable(dram_writeEnable),
		.dram_requestAccepted(dram_requestAccepted),
		.dram_requestCompleted(dram_requestCompleted),
		.dram_requestError(dram_requestError),
		.axiRemoteRead_request(axiRemoteRead_request),
		.axiRemoteRead_response(axiRemoteRead_response),
		.axiRemoteRead_clock(axiRemoteRead_clock),
		.axiRemoteRead_clear(axiRemoteRead_clear),
		.axiRemoteWrite_request(axiRemoteWrite_request),
		.axiRemoteWrite_response(axiRemoteWrite_response),
		.axiRemoteWrite_clock(axiRemoteWrite_clock),
		.axiRemoteWrite_clear(axiRemoteWrite_clear),
		.basicIO_switch(basicIO_switch),
		.basicIO_button(basicIO_button),
		.basicIO_led(basicIO_led),
		.basicIO_rgb_led(basicIO_rgb_led),
		.clock(clock),
		.clear(clear)
	);
	HDMIDriver #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) hdmiDriver(
		.internalDriver_clock(video_clock),
		.internalDriver_clear(video_clear),
		.internalDriver_valid(video_valid),
		.internalDriver_ready(video_ready),
		.internalDriver_data(video_data),
		.internalDriver_control(video_control),
		.tmds_clk_n(tmds_clk_n),
		.tmds_clk_p(tmds_clk_p),
		.tmds_d_n(tmds_d_n),
		.tmds_d_p(tmds_d_p),
		.clk_pixel(clk_pixel),
		.clk_5x_pixel(clk_5x_pixel),
		.clk_pixel_resetn(clk_pixel_resetn)
	);
	RemoteAXIDriver #(
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) remoteDriver(
		.remoteRead_request(axiRemoteRead_request),
		.remoteRead_response(axiRemoteRead_response),
		.remoteRead_clock(axiRemoteRead_clock),
		.remoteRead_clear(axiRemoteRead_clear),
		.remoteWrite_request(axiRemoteWrite_request),
		.remoteWrite_response(axiRemoteWrite_response),
		.remoteWrite_clock(axiRemoteWrite_clock),
		.remoteWrite_clear(axiRemoteWrite_clear),
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);
	DRAMAXIDriver #(
		.C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
		.C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
		.C_M_TRANSACTIONS_NUM(C_M00_AXI_TRANSACTIONS_NUM),
		.PHYSICAL_ADDRESS_OFFSET(C_M00_PHYSICAL_ADDRESS_OFFSET)
	) dramDriver(
		.dram_writeData(dram_writeData),
		.dram_readData(dram_readData),
		.dram_addressBase(dram_addressBase),
		.dram_readEnable(dram_readEnable),
		.dram_writeEnable(dram_writeEnable),
		.dram_requestAccepted(dram_requestAccepted),
		.dram_requestCompleted(dram_requestCompleted),
		.dram_requestError(dram_requestError),
		.M_AXI_ACLK(m00_axi_aclk),
		.M_AXI_ARESETN(m00_axi_aresetn),
		.M_AXI_AWADDR(m00_axi_awaddr),
		.M_AXI_AWPROT(m00_axi_awprot),
		.M_AXI_AWVALID(m00_axi_awvalid),
		.M_AXI_AWREADY(m00_axi_awready),
		.M_AXI_WDATA(m00_axi_wdata),
		.M_AXI_WSTRB(m00_axi_wstrb),
		.M_AXI_WVALID(m00_axi_wvalid),
		.M_AXI_WREADY(m00_axi_wready),
		.M_AXI_BRESP(m00_axi_bresp),
		.M_AXI_BVALID(m00_axi_bvalid),
		.M_AXI_BREADY(m00_axi_bready),
		.M_AXI_ARADDR(m00_axi_araddr),
		.M_AXI_ARPROT(m00_axi_arprot),
		.M_AXI_ARVALID(m00_axi_arvalid),
		.M_AXI_ARREADY(m00_axi_arready),
		.M_AXI_RDATA(m00_axi_rdata),
		.M_AXI_RRESP(m00_axi_rresp),
		.M_AXI_RVALID(m00_axi_rvalid),
		.M_AXI_RREADY(m00_axi_rready)
	);
	PS2Driver ps2Driver(
		.ps2_clk(ps2_clk),
		.ps2_data(ps2_data),
		.keyboard_clock(keyboard_clock),
		.keyboard_clear(keyboard_clear),
		.keyboard_readch(keyboard_readch),
		.keyboard_scancode(keyboard_scancode),
		.keyboard_ascii_ready(keyboard_ascii_ready)
	);
	Sync #(2) switch_sync(
		.clock(clock),
		.sig(switch),
		.sig_s(basicIO_switch)
	);
	Sync #(4) button_sync(
		.clock(clock),
		.sig(button),
		.sig_s(basicIO_button)
	);
	always @(*) begin
		rgb_led = {basicIO_rgb_led[3+:3], basicIO_rgb_led[0+:3]};
		led = basicIO_led;
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: BTBEntry_t
module BranchPredictor (
	pc_IF,
	prediction_IF,
	prediction_BR,
	actualPC_BR,
	decode_BR,
	clock,
	clear
);
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [4:0] X1 = 5'd1;
	localparam [4:0] X5 = 5'd5;
	localparam [2:0] WEAKLY_NOT_TAKEN = 3'd0;
	localparam [2:0] STRONGLY_NOT_TAKEN = 3'd1;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [2:0] WEAKLY_TAKEN = 3'd2;
	localparam [2:0] STRONGLY_TAKEN = 3'd3;
	localparam [31:0] JUMP = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	input wire [(32 - 1):0] pc_IF;
	output reg [67:0] prediction_IF;
	input wire [67:0] prediction_BR;
	input wire [(32 - 1):0] actualPC_BR;
	input wire [447:0] decode_BR;
	input wire clock;
	input wire clear;
	localparam ADDRESS_WIDTH = $clog2(512);
	wire [(ADDRESS_WIDTH - 1):0] readAddress;
	wire [(ADDRESS_WIDTH - 1):0] writeAddress;
	assign readAddress = pc_IF[(ADDRESS_WIDTH + 1):2];
	assign writeAddress = prediction_BR[(36 + (ADDRESS_WIDTH + 1)):38];
	// removed an assertion item
	// removed an assertion item
	wire [66:0] btbPrediction_IF;
	reg [66:0] correctedPrediction_BR;
	reg writeEnable_BR;
	xilinx_bram_single #(
		.RAM_WIDTH(67),
		.RAM_DEPTH(512)
	) btb(
		.address_read(readAddress),
		.dataOut(btbPrediction_IF),
		.address_write(writeAddress),
		.dataIn(correctedPrediction_BR),
		.writeEnable(writeEnable_BR),
		.clock(clock)
	);
	wire [(32 - 1):0] pc_saved;
	Register #(.WIDTH(32)) pcSavedRegister(
		.d(pc_IF),
		.q(pc_saved),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	wire [(32 - 1):0] returnAddress_push_BR;
	wire [(32 - 1):0] returnAddress_pop_IF;
	assign returnAddress_push_BR = (prediction_BR[67:36] + 32'd4);
	reg push_valid_BR;
	reg pop_valid_BR;
	Stack #(
		.SIZE(64),
		.WIDTH(32)
	) returnAddressStack(
		.push_data(returnAddress_push_BR),
		.push_valid(push_valid_BR),
		.pop_valid(pop_valid_BR),
		.peak_data(returnAddress_pop_IF),
		.clock(clock),
		.clear(clear)
	);
	always @(*) begin
		if ((btbPrediction_IF[66:35] == pc_saved)) begin
			prediction_IF = {btbPrediction_IF[66:35], btbPrediction_IF[34:3], btbPrediction_IF[2:0], 1'b1};
			if ((btbPrediction_IF[2:0] == FUNCTION_RETURN)) begin
				prediction_IF[35:4] = returnAddress_pop_IF;
			end
		end
		else begin
			prediction_IF = {pc_saved, (pc_saved + 32'd4), WEAKLY_NOT_TAKEN, 1'b0};
		end
	end
	// removed an assertion item
	function automatic [2:0] boostConfidence;
		input reg [2:0] original;
		case (original)
			STRONGLY_NOT_TAKEN: boostConfidence = STRONGLY_NOT_TAKEN;
			WEAKLY_NOT_TAKEN: boostConfidence = STRONGLY_NOT_TAKEN;
			WEAKLY_TAKEN: boostConfidence = STRONGLY_TAKEN;
			STRONGLY_TAKEN: boostConfidence = STRONGLY_TAKEN;

		endcase
	endfunction
	// removed an assertion item
	function automatic isJumpLink;
		input reg [5:0] register;
		isJumpLink = (register[0:0] && ((register[5:1] == X1) || (register[5:1] == X5)));
	endfunction
	function automatic isSameRegister;
		input reg [5:0] r1;
		input reg [5:0] r2;
		isSameRegister = ((r1[0:0] && r2[0:0]) && (r1[5:1] == r2[5:1]));
	endfunction
	wire rdLink_BR;
	wire rs1Link_BR;
	assign rdLink_BR = isJumpLink(decode_BR[69:64]);
	assign rs1Link_BR = isJumpLink(decode_BR[147:142]);
	always @(*) begin
		correctedPrediction_BR = {prediction_BR[67:36], prediction_BR[35:4], prediction_BR[3:1]};
		writeEnable_BR = 1'b0;
		push_valid_BR = 1'b0;
		pop_valid_BR = 1'b0;
		case (decode_BR[447:416])
			JUMP: begin
				writeEnable_BR = 1'b1;
				correctedPrediction_BR[34:3] = actualPC_BR;
				if (((! rdLink_BR) && rs1Link_BR)) begin
					correctedPrediction_BR[2:0] = FUNCTION_RETURN;
					pop_valid_BR = 1'b1;
				end
				else if ((rdLink_BR && (! rs1Link_BR))) begin
					push_valid_BR = 1'b1;
					correctedPrediction_BR[2:0] = STRONGLY_TAKEN;
				end
				else if ((rdLink_BR && rs1Link_BR)) begin
					push_valid_BR = 1'b1;
					if (isSameRegister(decode_BR[69:64], decode_BR[147:109])) begin
						correctedPrediction_BR[2:0] = STRONGLY_TAKEN;
					end
					else begin
						pop_valid_BR = 1'b1;
						correctedPrediction_BR[2:0] = FUNCTION_RETURN;
					end
				end
				else begin
					correctedPrediction_BR[2:0] = STRONGLY_TAKEN;
				end
			end
			BRANCH: begin
				if ((prediction_BR[35:4] == actualPC_BR)) begin
					correctedPrediction_BR[2:0] = boostConfidence(prediction_BR[3:1]);
					writeEnable_BR = 1'b1;
				end
				else begin
					writeEnable_BR = 1'b1;
					case (prediction_BR[3:1])
						STRONGLY_NOT_TAKEN: correctedPrediction_BR[2:0] = WEAKLY_NOT_TAKEN;
						WEAKLY_NOT_TAKEN: begin
							correctedPrediction_BR[2:0] = WEAKLY_TAKEN;
							correctedPrediction_BR[34:3] = actualPC_BR;
						end
						WEAKLY_TAKEN: begin
							correctedPrediction_BR[2:0] = WEAKLY_NOT_TAKEN;
							correctedPrediction_BR[34:3] = actualPC_BR;
						end
						STRONGLY_TAKEN: correctedPrediction_BR[2:0] = WEAKLY_TAKEN;
					endcase
				end
			end
		endcase
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module BranchResolveStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	execute_BR,
	rd_BR_response,
	predictionResolution_fetch_BR,
	predictionResolution_branch_BR,
	branch_MR,
	clock,
	clear
);
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [31:0] UNPREDICTABLE = 32'd0;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [31:0] FORWARD = 32'd1;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] CORRECT_PREDICTION = 32'd1;
	localparam [31:0] BACKWARD = 32'd2;
	localparam [31:0] NORMAL = 32'd2;
	localparam [31:0] INCORRECT_PREDICTION = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] CSR = 32'd5;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] FENCE = 32'd6;
	localparam [31:0] PRIVILEGE_RAISE = 32'd7;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output wire controlFlow_ready;
	output reg controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	input wire [930:0] execute_BR;
	output wire [38:0] rd_BR_response;
	output wire [100:0] predictionResolution_fetch_BR;
	output wire [95:0] predictionResolution_branch_BR;
	output wire [1026:0] branch_MR;
	input wire clock;
	input wire clear;
	function automatic decodeGenerateFence;
		input reg [447:0] state;
		case (state[447:416])
			INVALID_INSTRUCTION: decodeGenerateFence = 1'b0;
			ILLEGAL_INSTRUCTION_FAULT: decodeGenerateFence = 1'b1;
			NORMAL: decodeGenerateFence = 1'b0;
			JUMP: decodeGenerateFence = 1'b0;
			BRANCH: decodeGenerateFence = 1'b0;
			CSR: begin
				case (state[223:212])
					SATP: decodeGenerateFence = 1'b1;
					SSTATUS: decodeGenerateFence = 1'b1;
					MSTATUS: decodeGenerateFence = 1'b1;
					default: decodeGenerateFence = 1'b0;
				endcase
			end
			FENCE: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_SRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_MRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_RAISE: decodeGenerateFence = 1'b1;
		endcase
	endfunction
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	reg [95:0] branchState;
	assign branchState[95:64] = ((branchState[31:0] >= execute_BR[929:898]) ? FORWARD : BACKWARD);
	// removed an assertion item
	always @(*) begin
		branchState[63:32] = UNPREDICTABLE;
		if (((execute_BR[550:519] != INVALID_INSTRUCTION) && (execute_BR[550:519] != ILLEGAL_INSTRUCTION_FAULT))) begin
			if ((branchState[31:0] == execute_BR[865:834])) begin
				branchState[63:32] = CORRECT_PREDICTION;
			end
			else begin
				branchState[63:32] = INCORRECT_PREDICTION;
			end
		end
	end
	// removed an assertion item
	always @(*) begin
		branchState[31:0] = (execute_BR[929:898] + 32'd4);
		case (execute_BR[550:519])
			INVALID_INSTRUCTION: branchState[31:0] = 32'HA0BADADD;
			ILLEGAL_INSTRUCTION_FAULT: branchState[31:0] = 32'HA0BADADD;
			NORMAL: branchState[31:0] = (execute_BR[929:898] + 32'd4);
			JUMP: branchState[31:0] = execute_BR[31:0];
			BRANCH: begin
				if (execute_BR[71]) begin
					branchState[31:0] = execute_BR[31:0];
				end
				else begin
					branchState[31:0] = (execute_BR[929:898] + 32'd4);
				end
			end
			CSR: branchState[31:0] = (execute_BR[929:898] + 32'd4);
			FENCE: branchState[31:0] = (execute_BR[929:898] + 32'd4);
			PRIVILEGE_RAISE: branchState[31:0] = (execute_BR[929:898] + 32'd4);
			PRIVILEGE_LOWER_SRET: begin
				branchState[31:0] = (execute_BR[929:898] + 32'd4);
			end
			PRIVILEGE_LOWER_MRET: begin
				branchState[31:0] = (execute_BR[929:898] + 32'd4);
			end
		endcase
	end
	assign predictionResolution_fetch_BR = execute_BR[930:830];
	assign predictionResolution_branch_BR = branchState;
	assign rd_BR_response = execute_BR[70:32];
	always @(*) begin
		controlFlow_fencing = 1'b0;
		if (execute_BR[930:930]) begin
			if ((hasPendingException_IW(execute_BR[829:551]) || decodeGenerateFence(execute_BR[550:103]))) begin
				controlFlow_fencing = 1'b1;
			end
		end
	end
	assign controlFlow_ready = 1'b1;
	assign controlFlow_idle = (! execute_BR[930:930]);
	assign controlFlow_decode = execute_BR[550:103];
	wire [1026:0] branchExport_BR_END;
	assign branchExport_BR_END[1026:926] = execute_BR[930:830];
	assign branchExport_BR_END[925:647] = execute_BR[829:551];
	assign branchExport_BR_END[646:199] = execute_BR[550:103];
	assign branchExport_BR_END[198:96] = execute_BR[102:0];
	assign branchExport_BR_END[95:0] = branchState;
	Register #(.WIDTH(1027)) branchPipelineRegister(
		.d(branchExport_BR_END),
		.q(branch_MR),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: ExecuteState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
// removed typedef: BackPressure_t
module ControlFlow (
	csrInterruptDelivery_deliverInterrupt,
	csrInterruptDelivery_cause,
	csrInterruptDelivery_value,
	csrInterruptDelivery_epc,
	csrInterruptDelivery_trapVector,
	csrInterruptReturn_instructionType,
	csrInterruptReturn_epc,
	csrObserver_status,
	csrObserver_currentLevel,
	csrObserver_mideleg,
	asyncInterrupt_tval,
	asyncInterrupt_cause,
	asyncInterrupt_interruptPending,
	asyncInterrupt_interruptAccepted,
	fetch_flush,
	fetch_stall,
	fetch_ready,
	fetch_fencing,
	fetch_idle,
	fetch_decode,
	fetchPrediction_pc,
	fetchPrediction_prediction,
	fetchPrediction_forcePC,
	instructionWait_flush,
	instructionWait_stall,
	instructionWait_ready,
	instructionWait_fencing,
	instructionWait_idle,
	instructionWait_decode,
	decode_flush,
	decode_stall,
	decode_ready,
	decode_fencing,
	decode_idle,
	decode_decode,
	execute_flush,
	execute_stall,
	execute_ready,
	execute_fencing,
	execute_idle,
	execute_decode,
	branch_flush,
	branch_stall,
	branch_ready,
	branch_fencing,
	branch_idle,
	branch_decode,
	branchResolve_fetch_BR,
	branchResolve_branch_BR,
	memoryRequest_flush,
	memoryRequest_stall,
	memoryRequest_ready,
	memoryRequest_fencing,
	memoryRequest_idle,
	memoryRequest_decode,
	memoryWait_flush,
	memoryWait_stall,
	memoryWait_ready,
	memoryWait_fencing,
	memoryWait_idle,
	memoryWait_decode,
	writeBack,
	remoteHalted,
	coreHalt,
	coreDebug,
	clock,
	clear
);
	localparam [1:0] USER_MODE = 2'd0;
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [1:0] SUPERVISOR_MODE = 2'd1;
	localparam [1:0] MACHINE_MODE = 2'd3;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [31:0] USER_ECALL = 32'h8;
	localparam [31:0] SUPERVISOR_ECALL = 32'h9;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] CORRECT_PREDICTION = 32'd1;
	localparam [31:0] NORMAL = 32'd2;
	localparam [31:0] INCORRECT_PREDICTION = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] CSR = 32'd5;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] FENCE = 32'd6;
	localparam [31:0] PRIVILEGE_RAISE = 32'd7;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	output reg csrInterruptDelivery_deliverInterrupt;
	output reg [(32 - 1):0] csrInterruptDelivery_cause;
	output reg [(32 - 1):0] csrInterruptDelivery_value;
	output reg [(32 - 1):0] csrInterruptDelivery_epc;
	input wire [(32 - 1):0] csrInterruptDelivery_trapVector;
	output reg [31:0] csrInterruptReturn_instructionType;
	input wire [(32 - 1):0] csrInterruptReturn_epc;
	input wire [31:0] csrObserver_status;
	input wire [1:0] csrObserver_currentLevel;
	input wire [(32 - 1):0] csrObserver_mideleg;
	input wire [(32 - 1):0] asyncInterrupt_tval;
	input wire [(32 - 1):0] asyncInterrupt_cause;
	input wire asyncInterrupt_interruptPending;
	output reg asyncInterrupt_interruptAccepted;
	output wire fetch_flush;
	output wire fetch_stall;
	input wire fetch_ready;
	input wire fetch_fencing;
	input wire fetch_idle;
	input wire [447:0] fetch_decode;
	input wire [(32 - 1):0] fetchPrediction_pc;
	output reg [67:0] fetchPrediction_prediction;
	output wire fetchPrediction_forcePC;
	output wire instructionWait_flush;
	output wire instructionWait_stall;
	input wire instructionWait_ready;
	input wire instructionWait_fencing;
	input wire instructionWait_idle;
	input wire [447:0] instructionWait_decode;
	output wire decode_flush;
	output wire decode_stall;
	input wire decode_ready;
	input wire decode_fencing;
	input wire decode_idle;
	input wire [447:0] decode_decode;
	output wire execute_flush;
	output wire execute_stall;
	input wire execute_ready;
	input wire execute_fencing;
	input wire execute_idle;
	input wire [447:0] execute_decode;
	output wire branch_flush;
	output wire branch_stall;
	input wire branch_ready;
	input wire branch_fencing;
	input wire branch_idle;
	input wire [447:0] branch_decode;
	input wire [100:0] branchResolve_fetch_BR;
	input wire [95:0] branchResolve_branch_BR;
	output wire memoryRequest_flush;
	output wire memoryRequest_stall;
	input wire memoryRequest_ready;
	input wire memoryRequest_fencing;
	input wire memoryRequest_idle;
	input wire [447:0] memoryRequest_decode;
	output wire memoryWait_flush;
	output wire memoryWait_stall;
	input wire memoryWait_ready;
	input wire memoryWait_fencing;
	input wire memoryWait_idle;
	input wire [447:0] memoryWait_decode;
	input wire [1376:0] writeBack;
	input wire remoteHalted;
	output wire coreHalt;
	output reg coreDebug;
	input wire clock;
	input wire clear;
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	function automatic decodeGenerateFence;
		input reg [447:0] state;
		case (state[447:416])
			INVALID_INSTRUCTION: decodeGenerateFence = 1'b0;
			ILLEGAL_INSTRUCTION_FAULT: decodeGenerateFence = 1'b1;
			NORMAL: decodeGenerateFence = 1'b0;
			JUMP: decodeGenerateFence = 1'b0;
			BRANCH: decodeGenerateFence = 1'b0;
			CSR: begin
				case (state[223:212])
					SATP: decodeGenerateFence = 1'b1;
					SSTATUS: decodeGenerateFence = 1'b1;
					MSTATUS: decodeGenerateFence = 1'b1;
					default: decodeGenerateFence = 1'b0;
				endcase
			end
			FENCE: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_SRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_MRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_RAISE: decodeGenerateFence = 1'b1;
		endcase
	endfunction
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	wire [(32 - 1):0] exceptionCause_WB;
	wire [(32 - 1):0] tval_WB;
	ExceptionSelection exceptionSelection(
		.writeBack(writeBack),
		.exceptionCause(exceptionCause_WB),
		.tval(tval_WB),
		.clock(clock),
		.clear(clear)
	);
	wire [67:0] prediction_IF;
	BranchPredictor branchPredictor(
		.pc_IF(fetchPrediction_pc),
		.prediction_IF(prediction_IF),
		.prediction_BR(branchResolve_fetch_BR[67:0]),
		.actualPC_BR(branchResolve_branch_BR[31:0]),
		.decode_BR(branch_decode),
		.clock(clock),
		.clear(clear)
	);
	wire pipelineEmpty;
	wire [7:0] idleState;
	wire [7:0] idleState_next;
	assign idleState_next = {fetch_idle, instructionWait_idle, decode_idle, execute_idle, branch_idle, memoryRequest_idle, memoryWait_idle, (! writeBack[1376:1376])};
	Register #(.WIDTH(8)) idleRegister(
		.d(idleState_next),
		.q(idleState),
		.enable(1'b1),
		.clock(clock),
		.clear(clear)
	);
	assign pipelineEmpty = (& idleState);
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	reg writeBackForcePC;
	reg branchForcePC;
	wire [(32 - 1):0] expectedNextRetiredPC;
	reg [(32 - 1):0] expectedNextRetiredPC_WB;
	Register #(
		.WIDTH(32),
		.CLEAR_VALUE(32'h100000)
	) nextPCRegister(
		.q(expectedNextRetiredPC),
		.d(expectedNextRetiredPC_WB),
		.enable((writeBack[1376:1376] || writeBackForcePC)),
		.clock(clock),
		.clear(clear)
	);
	// removed an assertion item
	wire csrConflictStall;
	reg [(32 - 1):0] nextPC;
	wire [(32 - 1):0] nextPC_saved;
	always @(*) begin
		expectedNextRetiredPC_WB = writeBack[381:350];
		if (writeBackForcePC) begin
			expectedNextRetiredPC_WB = nextPC;
		end
	end
	assign fetchPrediction_forcePC = (writeBackForcePC || branchForcePC);
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	reg generateCoreHalt;
	wire [1:0] faultCount;
	reg startFault;
	Counter #(.WIDTH(2)) faultCounter(
		.q(faultCount),
		.enable(startFault),
		.clock(clock),
		.clear((clear || (writeBack[1376:1376] && (! startFault))))
	);
	wire forcePC_saved;
	always @(*) begin
		fetchPrediction_prediction = prediction_IF;
		if (forcePC_saved) begin
			fetchPrediction_prediction[35:4] = nextPC_saved;
		end
	end
	Register #(.WIDTH(32)) pcSavedRegister(
		.q(nextPC_saved),
		.d(nextPC),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	Register #(.WIDTH(1)) forcePCSavedRegister(
		.d(fetchPrediction_forcePC),
		.q(forcePC_saved),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	always @(*) begin
		nextPC = prediction_IF[35:4];
		csrInterruptReturn_instructionType = INVALID_INSTRUCTION;
		csrInterruptDelivery_deliverInterrupt = 1'b0;
		csrInterruptDelivery_cause = 32'hBADF00D;
		csrInterruptDelivery_value = 32'hBADF00D;
		csrInterruptDelivery_epc = 32'HA0BADADD;
		asyncInterrupt_interruptAccepted = 1'b0;
		writeBackForcePC = 1'b0;
		branchForcePC = 1'b0;
		generateCoreHalt = 1'b0;
		coreDebug = 1'b0;
		startFault = 1'b0;
		if ((writeBack[996:965] == PRIVILEGE_RAISE)) begin
			writeBackForcePC = 1'b1;
			case (csrObserver_currentLevel)
				USER_MODE: begin
					csrInterruptDelivery_deliverInterrupt = 1'b1;
					csrInterruptDelivery_cause = USER_ECALL;
					csrInterruptDelivery_value = 32'b0;
					csrInterruptDelivery_epc = writeBack[1375:1344];
					nextPC = csrInterruptDelivery_trapVector;
				end
				SUPERVISOR_MODE: begin
					csrInterruptDelivery_deliverInterrupt = 1'b1;
					csrInterruptDelivery_cause = SUPERVISOR_ECALL;
					csrInterruptDelivery_value = 32'b0;
					csrInterruptDelivery_epc = writeBack[1375:1344];
					nextPC = csrInterruptDelivery_trapVector;
				end
				MACHINE_MODE: begin
					case (writeBack[690:659])
						32'ha: begin
							generateCoreHalt = 1'b1;
							nextPC = 32'b0;
						end
						32'hd: begin
							nextPC = writeBack[381:350];
							coreDebug = 1'b1;
						end
						default: begin
						nextPC = writeBack[381:350];
					end
					endcase
				end
			endcase
		end
		else if (hasPendingException(writeBack)) begin
			startFault = 1'b1;
			writeBackForcePC = 1'b1;
			if ((faultCount <= 2'd1)) begin
				nextPC = csrInterruptDelivery_trapVector;
				csrInterruptDelivery_deliverInterrupt = 1'b1;
				csrInterruptDelivery_cause = exceptionCause_WB;
				csrInterruptDelivery_epc = writeBack[1375:1344];
				csrInterruptDelivery_value = tval_WB;
			end
			else begin
				generateCoreHalt = 1'b1;
				nextPC = 32'd3;
			end
		end
		else if (((writeBack[996:965] == PRIVILEGE_LOWER_SRET) || (writeBack[996:965] == PRIVILEGE_LOWER_MRET))) begin
			writeBackForcePC = 1'b1;
			nextPC = csrInterruptReturn_epc;
			csrInterruptReturn_instructionType = writeBack[996:965];
		end
		else if (decodeGenerateFence(writeBack[996:549])) begin
			writeBackForcePC = 1'b1;
			nextPC = writeBack[381:350];
		end
		else if ((branchResolve_branch_BR[63:32] == INCORRECT_PREDICTION)) begin
			branchForcePC = 1'b1;
			nextPC = branchResolve_branch_BR[31:0];
		end
		else if (((pipelineEmpty && asyncInterrupt_interruptPending) && (! remoteHalted))) begin
			writeBackForcePC = 1'b1;
			nextPC = csrInterruptDelivery_trapVector;
			csrInterruptDelivery_deliverInterrupt = 1'b1;
			csrInterruptDelivery_cause = asyncInterrupt_cause;
			csrInterruptDelivery_value = asyncInterrupt_tval;
			csrInterruptDelivery_epc = expectedNextRetiredPC;
			asyncInterrupt_interruptAccepted = 1'b1;
		end
	end
	CSRConflictDetector csrConflictDetector(
		.decode_ID(decode_decode),
		.decode_EX(execute_decode),
		.decode_BR(branch_decode),
		.decode_MR(memoryRequest_decode),
		.decode_MW(memoryWait_decode),
		.decode_WB(writeBack[996:549]),
		.csrConflictStall(csrConflictStall)
	);
	// removed an assertion item
	// removed an assertion item
	wire [1:0] instructionWaitBackPressure;
	wire [1:0] decodeBackPressure;
	wire [1:0] executeBackPressure;
	wire [1:0] branchBackPressure;
	wire [1:0] memoryRequestBackPressure;
	wire [1:0] memoryWaitBackPressure;
	wire [1:0] writeBackBackPressure;
	// removed an assertion item
	reg fetchDrain;
	always @(*) begin
		fetchDrain = 1'b0;
		if (((! remoteHalted) && asyncInterrupt_interruptPending)) begin
			fetchDrain = 1'b1;
		end
	end
	PipelineControlFlow fetchControl(
		.backPressure_older(instructionWaitBackPressure),
		.fencing(fetch_fencing),
		.ready(((fetch_ready && (! fetchDrain)) && (! remoteHalted))),
		.flush(fetch_flush),
		.stall(fetch_stall),
		.backPressure()
	);
	PipelineControlFlow instructionWaitControl(
		.backPressure_older(decodeBackPressure),
		.fencing(instructionWait_fencing),
		.ready(instructionWait_ready),
		.flush(instructionWait_flush),
		.stall(instructionWait_stall),
		.backPressure(instructionWaitBackPressure)
	);
	PipelineControlFlow decodeControl(
		.backPressure_older(executeBackPressure),
		.fencing(decode_fencing),
		.ready((decode_ready && (! csrConflictStall))),
		.flush(decode_flush),
		.stall(decode_stall),
		.backPressure(decodeBackPressure)
	);
	// removed an assertion item
	PipelineControlFlow executeControl(
		.backPressure_older(branchBackPressure),
		.fencing(execute_fencing),
		.ready(execute_ready),
		.flush(execute_flush),
		.stall(execute_stall),
		.backPressure(executeBackPressure)
	);
	// removed an assertion item
	PipelineControlFlow branchControl(
		.backPressure_older(memoryRequestBackPressure),
		.fencing((branch_fencing || branchForcePC)),
		.ready(branch_ready),
		.flush(branch_flush),
		.stall(branch_stall),
		.backPressure(branchBackPressure)
	);
	PipelineControlFlow memoryRequestControl(
		.backPressure_older(memoryWaitBackPressure),
		.fencing(memoryRequest_fencing),
		.ready(memoryRequest_ready),
		.flush(memoryRequest_flush),
		.stall(memoryRequest_stall),
		.backPressure(memoryRequestBackPressure)
	);
	PipelineControlFlow memoryWaitControl(
		.backPressure_older(writeBackBackPressure),
		.fencing(memoryWait_fencing),
		.ready(memoryWait_ready),
		.flush(memoryWait_flush),
		.stall(memoryWait_stall),
		.backPressure(memoryWaitBackPressure)
	);
	assign writeBackBackPressure[1:1] = writeBackForcePC;
	assign writeBackBackPressure[0:0] = 1'b0;
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	wire savedCoredHalt;
	assign coreHalt = (savedCoredHalt || generateCoreHalt);
	Register #(.WIDTH(1)) coreHaltOneWayRegister(
		.q(savedCoredHalt),
		.d(generateCoreHalt),
		.enable(generateCoreHalt),
		.clock(clock),
		.clear(clear)
	);
endmodule
module PipelineControlFlow (
	backPressure_older,
	ready,
	fencing,
	flush,
	stall,
	backPressure
);
	input wire [1:0] backPressure_older;
	input wire ready;
	input wire fencing;
	output reg flush;
	output reg stall;
	output reg [1:0] backPressure;
	always @(*) begin
		flush = 1'b0;
		stall = 1'b0;
		backPressure[1:1] = 1'b0;
		backPressure[0:0] = 1'b0;
		if ((backPressure_older[1:1] || backPressure_older[0:0])) begin
			flush = backPressure_older[1:1];
			stall = backPressure_older[0:0];
			backPressure[1:1] = backPressure_older[1:1];
			backPressure[0:0] = backPressure_older[0:0];
		end
		else if ((! ready)) begin
			flush = 1'b1;
			backPressure[0:0] = 1'b1;
		end
		else if (fencing) begin
			backPressure[1:1] = 1'b1;
		end
	end
endmodule
module CSRConflictDetector (
	decode_ID,
	decode_EX,
	decode_BR,
	decode_MR,
	decode_MW,
	decode_WB,
	csrConflictStall
);
	localparam [31:0] NO_CSR_OPERATION = 32'd0;
	input wire [447:0] decode_ID;
	input wire [447:0] decode_EX;
	input wire [447:0] decode_BR;
	input wire [447:0] decode_MR;
	input wire [447:0] decode_MW;
	input wire [447:0] decode_WB;
	output reg csrConflictStall;
	function automatic csrMatch;
		input reg [447:0] state;
		input reg [(12 - 1):0] name;
		begin
			if ((state[211:180] == NO_CSR_OPERATION)) csrMatch = 1'b0;
			csrMatch = (state[223:212] == name);
		end
	endfunction
	always @(*) begin
		csrConflictStall = 1'b0;
		if ((decode_ID[211:180] == NO_CSR_OPERATION)) begin
			csrConflictStall = 1'b0;
		end
		else begin
			if (csrMatch(decode_EX, decode_ID[223:212])) begin
				csrConflictStall = 1'b1;
			end
			else if (csrMatch(decode_BR, decode_ID[223:212])) begin
				csrConflictStall = 1'b1;
			end
			else if (csrMatch(decode_MR, decode_ID[223:212])) begin
				csrConflictStall = 1'b1;
			end
			else if (csrMatch(decode_MW, decode_ID[223:212])) begin
				csrConflictStall = 1'b1;
			end
			else if (csrMatch(decode_WB, decode_ID[223:212])) begin
				csrConflictStall = 1'b1;
			end
		end
	end
endmodule
module ExceptionSelection (
	writeBack,
	exceptionCause,
	tval,
	clock,
	clear
);
	localparam [31:0] NO_MEMORY_OPERATION = 32'd0;
	localparam [31:0] RESERVED_INVALID_CAUSE = 32'h10;
	localparam [31:0] INSTRUCTION_ILLEGAL = 32'h2;
	localparam [31:0] INSTRUCTION_PAGE_FAULT = 32'hC;
	localparam [31:0] LOAD_PAGE_FAULT = 32'hD;
	localparam [31:0] STORE_PAGE_FAULT = 32'hF;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] LOAD_BYTE = 32'd1;
	localparam [31:0] STORE_CONDITIONAL = 32'd10;
	localparam [31:0] LOAD_HALF_WORD = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [31:0] LOAD_WORD = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] LOAD_BYTE_UNSIGNED = 32'd4;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] LOAD_HALF_WORD_UNSIGNED = 32'd5;
	localparam [31:0] STORE_BYTE = 32'd6;
	localparam [31:0] STORE_HALF_WORD = 32'd7;
	localparam [31:0] STORE_WORD = 32'd8;
	localparam [31:0] LOAD_RESERVED = 32'd9;
	input wire [1376:0] writeBack;
	output reg [(32 - 1):0] exceptionCause;
	output reg [(32 - 1):0] tval;
	input wire clock;
	input wire clear;
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	// removed an assertion item
	// removed an assertion item
	always @(*) begin
		exceptionCause = RESERVED_INVALID_CAUSE;
		tval = 32'hBADF00D;
		if ((getInstructionResponseType(writeBack) == PAGE_FAULT)) begin
			exceptionCause = INSTRUCTION_PAGE_FAULT;
			tval = writeBack[1238:1207];
		end
		else if ((getDataResponseType(writeBack) == PAGE_FAULT)) begin
			tval = writeBack[280:249];
			case (getMemoryOperation(writeBack))
				LOAD_BYTE: exceptionCause = LOAD_PAGE_FAULT;
				LOAD_HALF_WORD: exceptionCause = LOAD_PAGE_FAULT;
				LOAD_WORD: exceptionCause = LOAD_PAGE_FAULT;
				LOAD_BYTE_UNSIGNED: exceptionCause = LOAD_PAGE_FAULT;
				LOAD_HALF_WORD_UNSIGNED: exceptionCause = LOAD_PAGE_FAULT;
				STORE_BYTE: exceptionCause = STORE_PAGE_FAULT;
				STORE_HALF_WORD: exceptionCause = STORE_PAGE_FAULT;
				STORE_WORD: exceptionCause = STORE_PAGE_FAULT;
				LOAD_RESERVED: exceptionCause = STORE_PAGE_FAULT;
				STORE_CONDITIONAL: exceptionCause = STORE_PAGE_FAULT;
			endcase
		end
		else if ((writeBack[996:965] == ILLEGAL_INSTRUCTION_FAULT)) begin
			exceptionCause = INSTRUCTION_ILLEGAL;
			tval = writeBack[1028:997];
		end
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module ControlStatusRegisters (
	interruptDelivery_deliverInterrupt,
	interruptDelivery_cause,
	interruptDelivery_value,
	interruptDelivery_epc,
	interruptDelivery_trapVector,
	interruptReturn_instructionType,
	interruptReturn_epc,
	memoryControl_supervisorPagesEnabled,
	memoryControl_userPagesEnabled,
	memoryControl_virtualMemoryEnabled,
	memoryControl_rootPageTable,
	request_ID_name,
	request_ID_value,
	remote_name,
	remote_value,
	request_WB_name,
	request_WB_value,
	request_WB_writeEnable,
	observer_status,
	observer_currentLevel,
	observer_mideleg,
	instructionWait_ID,
	memoryWait_WB,
	clock,
	clear,
	halted
);
	localparam [1:0] USER_MODE = 2'd0;
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [31:0] INVALID_MEMORY_RESPONSE = 32'd0;
	localparam [31:0] NO_CONDITIONAL = 32'd0;
	localparam [31:0] NO_MEMORY_OPERATION = 32'd0;
	localparam [1:0] SUPERVISOR_MODE = 2'd1;
	localparam [1:0] MACHINE_MODE = 2'd3;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] STVEC = 12'h105;
	localparam [11:0] SSCRATCH = 12'h140;
	localparam [11:0] SEPC = 12'h141;
	localparam [11:0] SCAUSE = 12'h142;
	localparam [11:0] STVAL = 12'h143;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [11:0] MEDELEG = 12'h302;
	localparam [11:0] MIDELEG = 12'h303;
	localparam [11:0] MTVEC = 12'h305;
	localparam [11:0] MSCRATCH = 12'h340;
	localparam [11:0] MEPC = 12'h341;
	localparam [11:0] MCAUSE = 12'h342;
	localparam [11:0] MTVAL = 12'h343;
	localparam [31:0] TIMER_INTERRUPT = 32'h80000007;
	localparam [31:0] EXTERNAL_INTERRUPT = 32'h80000009;
	localparam [31:0] INVALID_TRANSLATION = 32'd0;
	localparam [31:0] FORWARD = 32'd1;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] CACHE_HIT = 32'd1;
	localparam [31:0] STORE_SUCCESS = 32'd1;
	localparam [31:0] CORRECT_PREDICTION = 32'd1;
	localparam [0:0] INTERRUPTS_DISABLED = 1'b0;
	localparam [0:0] INTERRUPTS_ENABLED = 1'b1;
	localparam [31:0] STORE_CONDITIONAL = 32'd10;
	localparam [31:0] BACKWARD = 32'd2;
	localparam [31:0] CACHE_MISS = 32'd2;
	localparam [31:0] TLB_HIT = 32'd2;
	localparam [31:0] STORE_REJECTED = 32'd2;
	localparam [31:0] INCORRECT_PREDICTION = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [31:0] VRAM_IO = 32'd3;
	localparam [31:0] TLB_MISS = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] REMOTE_IO = 32'd4;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] TLB_INVALIDATE = 32'd5;
	localparam [31:0] TRANSLATION_INVALIDATE = 32'd6;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	input wire interruptDelivery_deliverInterrupt;
	input wire [(32 - 1):0] interruptDelivery_cause;
	input wire [(32 - 1):0] interruptDelivery_value;
	input wire [(32 - 1):0] interruptDelivery_epc;
	output reg [(32 - 1):0] interruptDelivery_trapVector;
	input wire [31:0] interruptReturn_instructionType;
	output reg [(32 - 1):0] interruptReturn_epc;
	output reg memoryControl_supervisorPagesEnabled;
	output reg memoryControl_userPagesEnabled;
	output reg memoryControl_virtualMemoryEnabled;
	output reg [33:0] memoryControl_rootPageTable;
	input wire [(12 - 1):0] request_ID_name;
	output wire [(32 - 1):0] request_ID_value;
	input wire [(12 - 1):0] remote_name;
	output wire [(32 - 1):0] remote_value;
	input wire [(12 - 1):0] request_WB_name;
	input wire [(32 - 1):0] request_WB_value;
	input wire request_WB_writeEnable;
	output wire [31:0] observer_status;
	output wire [1:0] observer_currentLevel;
	output wire [(32 - 1):0] observer_mideleg;
	input wire [379:0] instructionWait_ID;
	input wire [1376:0] memoryWait_WB;
	input wire clock;
	input wire clear;
	input wire halted;
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	function automatic hasSuffientPermissions;
		input reg [(12 - 1):0] name;
		input reg [1:0] privilegeLevel;
		case (privilegeLevel)
			MACHINE_MODE: hasSuffientPermissions = 1'b1;
			SUPERVISOR_MODE: hasSuffientPermissions = (name[9:8] <= SUPERVISOR_MODE);
			USER_MODE: hasSuffientPermissions = (name[9:8] == USER_MODE);
		endcase
	endfunction
	function automatic isDelegated;
		input reg [(32 - 1):0] deleg;
		input reg [(32 - 1):0] cause;
		isDelegated = ((deleg & (1 << (cause & 32'hf))) != 32'd0);
	endfunction
	function automatic isInterruptCause;
		input reg [(32 - 1):0] cause;
		isInterruptCause = cause[31];
	endfunction
	// removed an assertion item
	// removed an assertion item
	wire [31:0] satp;
	generate
		if ((! (32 == 32))) begin
			DoesNotExist foo();
		end
	endgenerate
	SimpleControlStatusRegister #(.NAME(SATP)) satpRegister(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(satp),
		.clock(clock),
		.clear(clear)
	);
	wire [(32 - 1):0] mtvec;
	SimpleControlStatusRegister #(.NAME(MTVEC)) MTVEC_register(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(mtvec),
		.clock(clock),
		.clear(clear)
	);
	wire [(32 - 1):0] mscratch;
	SimpleControlStatusRegister #(.NAME(MSCRATCH)) MSCRATCH_register(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(mscratch),
		.clock(clock),
		.clear(clear)
	);
	wire [(32 - 1):0] mideleg;
	SimpleControlStatusRegister #(.NAME(MIDELEG)) MIDELEG_register(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(mideleg),
		.clock(clock),
		.clear(clear)
	);
	wire [(32 - 1):0] medeleg;
	SimpleControlStatusRegister #(.NAME(MEDELEG)) MEDELEG_register(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(medeleg),
		.clock(clock),
		.clear(clear)
	);
	reg [31:0] status;
	generate
		if ((! (32 == 32))) begin
			DoesNotExist foo();
		end
	endgenerate
	reg [(32 - 1):0] mepc;
	reg [(32 - 1):0] mtval;
	reg [(32 - 1):0] sepc;
	reg [(32 - 1):0] stval;
	reg [(32 - 1):0] mcause;
	reg [(32 - 1):0] scause;
	generate
		if ((! (32 == 32))) begin
			DoesNotExist foo();
		end
	endgenerate
	reg [1:0] currentLevel;
	wire [(32 - 1):0] stvec;
	SimpleControlStatusRegister #(.NAME(STVEC)) STVEC_register(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(stvec),
		.clock(clock),
		.clear(clear)
	);
	wire [(32 - 1):0] sscratch;
	SimpleControlStatusRegister #(.NAME(SSCRATCH)) SSCRATCH_register(
		.request_name(request_WB_name),
		.request_value(request_WB_value),
		.request_writeEnable(request_WB_writeEnable),
		.register(sscratch),
		.clock(clock),
		.clear(clear)
	);
	assign observer_status = status;
	assign observer_currentLevel = currentLevel;
	assign observer_mideleg = mideleg;
	always @(*) begin
		interruptDelivery_trapVector = 32'hBADF00D;
		if (interruptDelivery_deliverInterrupt) begin
			if (deliverToSupervisor(interruptDelivery_cause)) begin
				interruptDelivery_trapVector = stvec;
			end
			else begin
				interruptDelivery_trapVector = mtvec;
			end
		end
	end
	always @(*) begin
		if ((interruptReturn_instructionType == PRIVILEGE_LOWER_MRET)) begin
			interruptReturn_epc = mepc;
		end
		else if ((interruptReturn_instructionType == PRIVILEGE_LOWER_SRET)) begin
			interruptReturn_epc = sepc;
		end
		else begin
			interruptReturn_epc = 32'hBADF00D;
		end
	end
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	function automatic deliverToSupervisor;
		input reg [(32 - 1):0] cause;
		begin
			if ((currentLevel == MACHINE_MODE)) deliverToSupervisor = 1'b0;
			if (isInterruptCause(cause)) deliverToSupervisor = isDelegated(mideleg, cause);
			else deliverToSupervisor = isDelegated(medeleg, cause);
		end
	endfunction
	always @(posedge clock) begin
		if (clear) begin
			status <= {1'd0, 8'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 2'd0, 2'd0, 2'd0, 2'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0};
			mepc <= 32'b0;
			mtval <= 32'b0;
			mcause <= 32'd0;
			sepc <= 32'b0;
			stval <= 32'b0;
			scause <= 32'd0;
			currentLevel <= MACHINE_MODE;
		end
		else begin
			if (interruptDelivery_deliverInterrupt) begin
				if (deliverToSupervisor(interruptDelivery_cause)) begin
					status[1:1] <= INTERRUPTS_DISABLED;
					status[5:5] <= status[1:1];
					status[8:8] <= currentLevel[0];
					currentLevel <= SUPERVISOR_MODE;
					sepc <= interruptDelivery_epc;
					scause <= interruptDelivery_cause;
					stval <= interruptDelivery_value;
				end
				else begin
					status[3:3] <= INTERRUPTS_DISABLED;
					status[7:7] <= status[3:3];
					status[12:11] <= currentLevel;
					currentLevel <= MACHINE_MODE;
					mepc <= interruptDelivery_epc;
					mcause <= interruptDelivery_cause;
					mtval <= interruptDelivery_value;
				end
			end
			else if ((interruptReturn_instructionType == PRIVILEGE_LOWER_MRET)) begin
				status[3:3] <= status[7:7];
				status[12:11] <= USER_MODE;
				status[7:7] <= INTERRUPTS_ENABLED;
				currentLevel <= status[12:11];
			end
			else if ((interruptReturn_instructionType == PRIVILEGE_LOWER_SRET)) begin
				status[1:1] <= status[5:5];
				status[8:8] <= USER_MODE;
				status[5:5] <= INTERRUPTS_ENABLED;
				currentLevel <= status[8:8];
			end
			else if (request_WB_writeEnable) begin
				case (request_WB_name)
					MSTATUS: status <= (request_WB_value & 32'h807FF9BB);
					SSTATUS: status <= ((request_WB_value & 32'h800DE133) | (status & (~ 32'h800DE133)));
					MEPC: mepc <= request_WB_value;
					SEPC: sepc <= request_WB_value;
					MTVAL: mtval <= request_WB_value;
					STVAL: stval <= request_WB_value;
					MCAUSE: mcause <= request_WB_value;
					SCAUSE: scause <= request_WB_value;
				endcase
			end
		end
	end
	always @(*) begin
		memoryControl_supervisorPagesEnabled = ((currentLevel == SUPERVISOR_MODE) || (currentLevel == MACHINE_MODE));
		memoryControl_userPagesEnabled = ((currentLevel == USER_MODE) || (status[18:18] && memoryControl_supervisorPagesEnabled));
		memoryControl_virtualMemoryEnabled = satp[31:31];
		memoryControl_rootPageTable = {satp[21:0], 12'b0};
	end
	wire [63:0] cycleCount;
	Counter #(.WIDTH(64)) cycleCount_counter(
		.clock(clock),
		.clear(clear),
		.enable((! halted)),
		.q(cycleCount)
	);
	wire [63:0] instructionsRetired;
	Counter #(.WIDTH(64)) instructionsRetired_counter(
		.clock(clock),
		.clear(clear),
		.enable(memoryWait_WB[1376:1376]),
		.q(instructionsRetired)
	);
	wire [63:0] instructionsFetched;
	Counter #(.WIDTH(64)) instructionsFetched_counter(
		.clock(clock),
		.clear(clear),
		.enable(instructionWait_ID[379:379]),
		.q(instructionsFetched)
	);
	// removed an assertion item
	// removed an assertion item
	wire [63:0] branchesRetired;
	Counter #(.WIDTH(64)) branchesRetired_counter(
		.clock(clock),
		.clear(clear),
		.enable((isBranch(memoryWait_WB) && memoryWait_WB[1376:1376])),
		.q(branchesRetired)
	);
	wire [63:0] forwardBranchesPredictedCorrectly;
	Counter #(.WIDTH(64)) forwardBranchesPredictedCorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable((((isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == FORWARD)) && (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(forwardBranchesPredictedCorrectly)
	);
	wire [63:0] forwardBranchesPredictedIncorrectly;
	Counter #(.WIDTH(64)) forwardBranchesPredictedIncorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable((((isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == FORWARD)) && (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(forwardBranchesPredictedIncorrectly)
	);
	wire [63:0] backwardBranchesPredictedCorrectly;
	Counter #(.WIDTH(64)) backwardBranchesPredictedCorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable((((isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == BACKWARD)) && (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(backwardBranchesPredictedCorrectly)
	);
	wire [63:0] backwardBranchesPredictedIncorrectly;
	Counter #(.WIDTH(64)) backwardBranchesPredictedIncorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable((((isBranch(memoryWait_WB) && (getDirection(memoryWait_WB) == BACKWARD)) && (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(backwardBranchesPredictedIncorrectly)
	);
	// removed an assertion item
	// removed an assertion item
	wire [63:0] jumpsRetired;
	Counter #(.WIDTH(64)) jumpsRetired_counter(
		.clock(clock),
		.clear(clear),
		.enable((isJump(memoryWait_WB) && memoryWait_WB[1376:1376])),
		.q(jumpsRetired)
	);
	wire [63:0] jumpsPredictedCorrectly;
	Counter #(.WIDTH(64)) jumpsPredictedCorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable(((isJump(memoryWait_WB) && (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(jumpsPredictedCorrectly)
	);
	wire [63:0] jumpsPredictedIncorrectly;
	Counter #(.WIDTH(64)) jumpsPredictedIncorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable(((isJump(memoryWait_WB) && (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(jumpsPredictedIncorrectly)
	);
	wire [63:0] returnPredictedCorrectly;
	Counter #(.WIDTH(64)) returnPredictedCorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable((((isJump(memoryWait_WB) && isJumpReturn(memoryWait_WB)) && (getPrediction(memoryWait_WB) == CORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(returnPredictedCorrectly)
	);
	wire [63:0] returnPredictedIncorrectly;
	Counter #(.WIDTH(64)) returnPredictedIncorrectly_counter(
		.clock(clock),
		.clear(clear),
		.enable((((isJump(memoryWait_WB) && isJumpReturn(memoryWait_WB)) && (getPrediction(memoryWait_WB) == INCORRECT_PREDICTION)) && memoryWait_WB[1376:1376])),
		.q(returnPredictedIncorrectly)
	);
	wire [63:0] btbHit;
	Counter #(.WIDTH(64)) btbHit_counter(
		.clock(clock),
		.clear(clear),
		.enable(((isJump(memoryWait_WB) || isBranch(memoryWait_WB)) && isBTBHit(memoryWait_WB))),
		.q(btbHit)
	);
	// removed an assertion item
	// removed an assertion item
	wire [63:0] instructionTLBHits;
	Counter #(.WIDTH(64)) instructionTLBHits_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getInstructionTranslationType(memoryWait_WB) == TLB_HIT) && memoryWait_WB[1376:1376])),
		.q(instructionTLBHits)
	);
	wire [63:0] instructionTLBMisses;
	Counter #(.WIDTH(64)) instructionTLBMisses_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getInstructionTranslationType(memoryWait_WB) == TLB_MISS) && memoryWait_WB[1376:1376])),
		.q(instructionTLBMisses)
	);
	wire [63:0] instructionCacheHits;
	Counter #(.WIDTH(64)) instructionCacheHits_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getInstructionResponseType(memoryWait_WB) == CACHE_HIT) && memoryWait_WB[1376:1376])),
		.q(instructionCacheHits)
	);
	wire [63:0] instructionCacheMisses;
	Counter #(.WIDTH(64)) instructionCacheMisses_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getInstructionResponseType(memoryWait_WB) == CACHE_MISS) && memoryWait_WB[1376:1376])),
		.q(instructionCacheMisses)
	);
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	wire [63:0] dataTLBHits;
	Counter #(.WIDTH(64)) dataTLBHits_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getDataTranslationType(memoryWait_WB) == TLB_HIT) && memoryWait_WB[1376:1376])),
		.q(dataTLBHits)
	);
	wire [63:0] dataTLBMisses;
	Counter #(.WIDTH(64)) dataTLBMisses_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getDataTranslationType(memoryWait_WB) == TLB_MISS) && memoryWait_WB[1376:1376])),
		.q(dataTLBMisses)
	);
	wire [63:0] dataCacheHits;
	Counter #(.WIDTH(64)) dataCacheHits_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getDataResponseType(memoryWait_WB) == CACHE_HIT) && memoryWait_WB[1376:1376])),
		.q(dataCacheHits)
	);
	wire [63:0] dataCacheMisses;
	Counter #(.WIDTH(64)) dataCacheMisses_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getDataResponseType(memoryWait_WB) == CACHE_MISS) && memoryWait_WB[1376:1376])),
		.q(dataCacheMisses)
	);
	wire [63:0] dataVRAMHits;
	Counter #(.WIDTH(64)) dataVRAMHits_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getDataResponseType(memoryWait_WB) == VRAM_IO) && memoryWait_WB[1376:1376])),
		.q(dataVRAMHits)
	);
	wire [63:0] dataRemoteHits;
	Counter #(.WIDTH(64)) dataRemoteHits_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getDataResponseType(memoryWait_WB) == REMOTE_IO) && memoryWait_WB[1376:1376])),
		.q(dataRemoteHits)
	);
	// removed an assertion item
	// removed an assertion item
	wire [63:0] storeConditionalSuccesses;
	Counter #(.WIDTH(64)) storeConditionalSuccesses_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getStoreConditionalResult(memoryWait_WB) == STORE_SUCCESS) && memoryWait_WB[1376:1376])),
		.q(storeConditionalSuccesses)
	);
	wire [63:0] storeConditionalRejections;
	Counter #(.WIDTH(64)) storeConditionalRejections_counter(
		.clock(clock),
		.clear(clear),
		.enable(((getStoreConditionalResult(memoryWait_WB) == STORE_REJECTED) && memoryWait_WB[1376:1376])),
		.q(storeConditionalRejections)
	);
	wire [63:0] keyboardInterruptedUser;
	assign keyboardInterruptedUser[63:32] = 32'b0;
	// removed an assertion item
	Counter #(.WIDTH(32)) keyboardInterruptedUser_counter(
		.clock(clock),
		.clear(clear),
		.enable(((interruptDelivery_deliverInterrupt && (interruptDelivery_cause == EXTERNAL_INTERRUPT)) && (currentLevel == USER_MODE))),
		.q(keyboardInterruptedUser[31:0])
	);
	wire [63:0] keyboardInterruptedSupervisor;
	assign keyboardInterruptedSupervisor[63:32] = 32'b0;
	// removed an assertion item
	Counter #(.WIDTH(32)) keyboardInterruptedSupervisor_counter(
		.clock(clock),
		.clear(clear),
		.enable(((interruptDelivery_deliverInterrupt && (interruptDelivery_cause == EXTERNAL_INTERRUPT)) && ((currentLevel == SUPERVISOR_MODE) || (currentLevel == MACHINE_MODE)))),
		.q(keyboardInterruptedSupervisor[31:0])
	);
	wire [63:0] timerInterruptedUser;
	Counter #(.WIDTH(64)) timerInterruptedUser_counter(
		.clock(clock),
		.clear(clear),
		.enable(((interruptDelivery_deliverInterrupt && (interruptDelivery_cause == TIMER_INTERRUPT)) && (currentLevel == USER_MODE))),
		.q(timerInterruptedUser)
	);
	wire [63:0] timerInterruptedSupervisor;
	Counter #(.WIDTH(64)) timerInterruptedSupervisor_counter(
		.clock(clock),
		.clear(clear),
		.enable(((interruptDelivery_deliverInterrupt && (interruptDelivery_cause == TIMER_INTERRUPT)) && ((currentLevel == SUPERVISOR_MODE) || (currentLevel == MACHINE_MODE)))),
		.q(timerInterruptedSupervisor)
	);
	wire [63:0] exceptionsInUserMode;
	assign exceptionsInUserMode[63:32] = 32'b0;
	// removed an assertion item
	Counter #(.WIDTH(32)) exceptionsInUserMode_counter(
		.clock(clock),
		.clear(clear),
		.enable(((interruptDelivery_deliverInterrupt && ((interruptDelivery_cause != TIMER_INTERRUPT) && (interruptDelivery_cause != EXTERNAL_INTERRUPT))) && (currentLevel == USER_MODE))),
		.q(exceptionsInUserMode[31:0])
	);
	wire [63:0] exceptionsInSupervisorMode;
	assign exceptionsInSupervisorMode[63:32] = 32'b0;
	// removed an assertion item
	Counter #(.WIDTH(32)) exceptionsInSupervisorMode_counter(
		.clock(clock),
		.clear(clear),
		.enable(((interruptDelivery_deliverInterrupt && ((interruptDelivery_cause != TIMER_INTERRUPT) && (interruptDelivery_cause != EXTERNAL_INTERRUPT))) && ((currentLevel == SUPERVISOR_MODE) || (currentLevel == MACHINE_MODE)))),
		.q(exceptionsInSupervisorMode[31:0])
	);
	CSRRead csrRead_ID(
		.request_name(request_ID_name),
		.request_value(request_ID_value),
		.status(status),
		.stvec(stvec),
		.sscratch(sscratch),
		.sepc(sepc),
		.scause(scause),
		.stval(stval),
		.satp(satp),
		.medeleg(medeleg),
		.mideleg(mideleg),
		.mtvec(mtvec),
		.mscratch(mscratch),
		.mepc(mepc),
		.mcause(mcause),
		.mtval(mtval),
		.cycleCount(cycleCount),
		.instructionsRetired(instructionsRetired),
		.instructionsFetched(instructionsFetched),
		.branchesRetired(branchesRetired),
		.forwardBranchesPredictedCorrectly(forwardBranchesPredictedCorrectly),
		.forwardBranchesPredictedIncorrectly(forwardBranchesPredictedIncorrectly),
		.backwardBranchesPredictedCorrectly(backwardBranchesPredictedCorrectly),
		.backwardBranchesPredictedIncorrectly(backwardBranchesPredictedIncorrectly),
		.jumpsRetired(jumpsRetired),
		.jumpsPredictedCorrectly(jumpsPredictedCorrectly),
		.jumpsPredictedIncorrectly(jumpsPredictedIncorrectly),
		.returnPredictedCorrectly(returnPredictedCorrectly),
		.returnPredictedIncorrectly(returnPredictedIncorrectly),
		.instructionTLBHits(instructionTLBHits),
		.instructionTLBMisses(instructionTLBMisses),
		.instructionCacheHits(instructionCacheHits),
		.instructionCacheMisses(instructionCacheMisses),
		.dataTLBHits(dataTLBHits),
		.dataTLBMisses(dataTLBMisses),
		.dataCacheHits(dataCacheHits),
		.dataCacheMisses(dataCacheMisses),
		.dataVRAMHits(dataVRAMHits),
		.dataRemoteHits(dataRemoteHits),
		.storeConditionalSuccesses(storeConditionalSuccesses),
		.storeConditionalRejections(storeConditionalRejections),
		.keyboardInterruptedUser(keyboardInterruptedUser),
		.keyboardInterruptedSupervisor(keyboardInterruptedSupervisor),
		.timerInterruptedUser(timerInterruptedUser),
		.timerInterruptedSupervisor(timerInterruptedSupervisor),
		.exceptionsInUserMode(exceptionsInUserMode),
		.exceptionsInSupervisorMode(exceptionsInSupervisorMode),
		.btbHit(btbHit)
	);
	CSRRead csrRead_remote(
		.request_name(remote_name),
		.request_value(remote_value),
		.status(status),
		.stvec(stvec),
		.sscratch(sscratch),
		.sepc(sepc),
		.scause(scause),
		.stval(stval),
		.satp(satp),
		.medeleg(medeleg),
		.mideleg(mideleg),
		.mtvec(mtvec),
		.mscratch(mscratch),
		.mepc(mepc),
		.mcause(mcause),
		.mtval(mtval),
		.cycleCount(cycleCount),
		.instructionsRetired(instructionsRetired),
		.instructionsFetched(instructionsFetched),
		.branchesRetired(branchesRetired),
		.forwardBranchesPredictedCorrectly(forwardBranchesPredictedCorrectly),
		.forwardBranchesPredictedIncorrectly(forwardBranchesPredictedIncorrectly),
		.backwardBranchesPredictedCorrectly(backwardBranchesPredictedCorrectly),
		.backwardBranchesPredictedIncorrectly(backwardBranchesPredictedIncorrectly),
		.jumpsRetired(jumpsRetired),
		.jumpsPredictedCorrectly(jumpsPredictedCorrectly),
		.jumpsPredictedIncorrectly(jumpsPredictedIncorrectly),
		.returnPredictedCorrectly(returnPredictedCorrectly),
		.returnPredictedIncorrectly(returnPredictedIncorrectly),
		.instructionTLBHits(instructionTLBHits),
		.instructionTLBMisses(instructionTLBMisses),
		.instructionCacheHits(instructionCacheHits),
		.instructionCacheMisses(instructionCacheMisses),
		.dataTLBHits(dataTLBHits),
		.dataTLBMisses(dataTLBMisses),
		.dataCacheHits(dataCacheHits),
		.dataCacheMisses(dataCacheMisses),
		.dataVRAMHits(dataVRAMHits),
		.dataRemoteHits(dataRemoteHits),
		.storeConditionalSuccesses(storeConditionalSuccesses),
		.storeConditionalRejections(storeConditionalRejections),
		.keyboardInterruptedUser(keyboardInterruptedUser),
		.keyboardInterruptedSupervisor(keyboardInterruptedSupervisor),
		.timerInterruptedUser(timerInterruptedUser),
		.timerInterruptedSupervisor(timerInterruptedSupervisor),
		.exceptionsInUserMode(exceptionsInUserMode),
		.exceptionsInSupervisorMode(exceptionsInSupervisorMode),
		.btbHit(btbHit)
	);
endmodule
module CSRRead (
	request_name,
	request_value,
	status,
	stvec,
	sscratch,
	sepc,
	scause,
	stval,
	satp,
	medeleg,
	mideleg,
	mtvec,
	mscratch,
	mepc,
	mcause,
	mtval,
	cycleCount,
	instructionsRetired,
	instructionsFetched,
	branchesRetired,
	forwardBranchesPredictedCorrectly,
	forwardBranchesPredictedIncorrectly,
	backwardBranchesPredictedCorrectly,
	backwardBranchesPredictedIncorrectly,
	jumpsRetired,
	jumpsPredictedCorrectly,
	jumpsPredictedIncorrectly,
	returnPredictedCorrectly,
	returnPredictedIncorrectly,
	instructionTLBHits,
	instructionTLBMisses,
	instructionCacheHits,
	instructionCacheMisses,
	dataTLBHits,
	dataTLBMisses,
	dataCacheHits,
	dataCacheMisses,
	dataVRAMHits,
	dataRemoteHits,
	storeConditionalSuccesses,
	storeConditionalRejections,
	keyboardInterruptedUser,
	keyboardInterruptedSupervisor,
	timerInterruptedUser,
	timerInterruptedSupervisor,
	exceptionsInUserMode,
	exceptionsInSupervisorMode,
	btbHit
);
	localparam [11:0] NONE = 12'h0;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] STVEC = 12'h105;
	localparam [11:0] SSCRATCH = 12'h140;
	localparam [11:0] SEPC = 12'h141;
	localparam [11:0] SCAUSE = 12'h142;
	localparam [11:0] STVAL = 12'h143;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [11:0] MEDELEG = 12'h302;
	localparam [11:0] MIDELEG = 12'h303;
	localparam [11:0] MTVEC = 12'h305;
	localparam [11:0] MSCRATCH = 12'h340;
	localparam [11:0] MEPC = 12'h341;
	localparam [11:0] MCAUSE = 12'h342;
	localparam [11:0] MTVAL = 12'h343;
	localparam [11:0] CYCLE = 12'hC00;
	localparam [11:0] INSTRET = 12'hC02;
	localparam [11:0] HPMCOUNTER3 = 12'hC03;
	localparam [11:0] HPMCOUNTER4 = 12'hC04;
	localparam [11:0] HPMCOUNTER5 = 12'hC05;
	localparam [11:0] HPMCOUNTER6 = 12'hC06;
	localparam [11:0] HPMCOUNTER7 = 12'hC07;
	localparam [11:0] HPMCOUNTER8 = 12'hC08;
	localparam [11:0] HPMCOUNTER9 = 12'hC09;
	localparam [11:0] HPMCOUNTER10 = 12'hC0A;
	localparam [11:0] HPMCOUNTER11 = 12'hC0B;
	localparam [11:0] HPMCOUNTER12 = 12'hC0C;
	localparam [11:0] HPMCOUNTER13 = 12'hC0D;
	localparam [11:0] HPMCOUNTER14 = 12'hC0E;
	localparam [11:0] HPMCOUNTER15 = 12'hC0F;
	localparam [11:0] HPMCOUNTER16 = 12'hC10;
	localparam [11:0] HPMCOUNTER17 = 12'hC11;
	localparam [11:0] HPMCOUNTER18 = 12'hC12;
	localparam [11:0] HPMCOUNTER19 = 12'hC13;
	localparam [11:0] HPMCOUNTER20 = 12'hC14;
	localparam [11:0] HPMCOUNTER21 = 12'hC15;
	localparam [11:0] HPMCOUNTER22 = 12'hC16;
	localparam [11:0] HPMCOUNTER23 = 12'hC17;
	localparam [11:0] HPMCOUNTER24 = 12'hC18;
	localparam [11:0] HPMCOUNTER25 = 12'hC19;
	localparam [11:0] HPMCOUNTER26 = 12'hC1A;
	localparam [11:0] HPMCOUNTER27 = 12'hC1B;
	localparam [11:0] HPMCOUNTER28 = 12'hC1C;
	localparam [11:0] HPMCOUNTER29 = 12'hC1D;
	localparam [11:0] HPMCOUNTER30 = 12'hC1E;
	localparam [11:0] HPMCOUNTER31 = 12'hC1F;
	localparam [11:0] CYCLEH = 12'hC80;
	localparam [11:0] INSTRETH = 12'hC82;
	localparam [11:0] HPMCOUNTER3H = 12'hC83;
	localparam [11:0] HPMCOUNTER4H = 12'hC84;
	localparam [11:0] HPMCOUNTER5H = 12'hC85;
	localparam [11:0] HPMCOUNTER6H = 12'hC86;
	localparam [11:0] HPMCOUNTER7H = 12'hC87;
	localparam [11:0] HPMCOUNTER8H = 12'hC88;
	localparam [11:0] HPMCOUNTER9H = 12'hC89;
	localparam [11:0] HPMCOUNTER10H = 12'hC8A;
	localparam [11:0] HPMCOUNTER11H = 12'hC8B;
	localparam [11:0] HPMCOUNTER12H = 12'hC8C;
	localparam [11:0] HPMCOUNTER13H = 12'hC8D;
	localparam [11:0] HPMCOUNTER14H = 12'hC8E;
	localparam [11:0] HPMCOUNTER15H = 12'hC8F;
	localparam [11:0] HPMCOUNTER16H = 12'hC90;
	localparam [11:0] HPMCOUNTER17H = 12'hC91;
	localparam [11:0] HPMCOUNTER18H = 12'hC92;
	localparam [11:0] HPMCOUNTER19H = 12'hC93;
	localparam [11:0] HPMCOUNTER20H = 12'hC94;
	localparam [11:0] HPMCOUNTER21H = 12'hC95;
	localparam [11:0] HPMCOUNTER22H = 12'hC96;
	localparam [11:0] HPMCOUNTER23H = 12'hC97;
	localparam [11:0] HPMCOUNTER24H = 12'hC98;
	localparam [11:0] HPMCOUNTER25H = 12'hC99;
	localparam [11:0] HPMCOUNTER26H = 12'hC9A;
	localparam [11:0] HPMCOUNTER27H = 12'hC9B;
	localparam [11:0] HPMCOUNTER28H = 12'hC9C;
	localparam [11:0] HPMCOUNTER29H = 12'hC9D;
	localparam [11:0] HPMCOUNTER30H = 12'hC9E;
	localparam [11:0] HPMCOUNTER31H = 12'hC9F;
	localparam [11:0] CYCLE_COUNT = CYCLE;
	localparam [11:0] CYCLE_COUNTH = CYCLEH;
	localparam [11:0] INSTRUCTION_CACHE_HIT = HPMCOUNTER10;
	localparam [11:0] INSTRUCTION_CACHE_HITH = HPMCOUNTER10H;
	localparam [11:0] INSTRUCTION_CACHE_MISS = HPMCOUNTER11;
	localparam [11:0] INSTRUCTION_CACHE_MISSH = HPMCOUNTER11H;
	localparam [11:0] DATA_CACHE_HIT = HPMCOUNTER12;
	localparam [11:0] DATA_CACHE_HITH = HPMCOUNTER12H;
	localparam [11:0] DATA_CACHE_MISS = HPMCOUNTER13;
	localparam [11:0] DATA_CACHE_MISSH = HPMCOUNTER13H;
	localparam [11:0] DATA_VRAM_HIT = HPMCOUNTER14;
	localparam [11:0] DATA_VRAM_HITH = HPMCOUNTER14H;
	localparam [11:0] STORE_CONDITIONAL_SUCCESS = HPMCOUNTER15;
	localparam [11:0] STORE_CONDITIONAL_SUCCESSH = HPMCOUNTER15H;
	localparam [11:0] STORE_CONDITIONAL_REJECT = HPMCOUNTER16;
	localparam [11:0] STORE_CONDITIONAL_REJECTH = HPMCOUNTER16H;
	localparam [11:0] KEYBOARD_INTERRUPT_USER = HPMCOUNTER17;
	localparam [11:0] KEYBOARD_INTERRUPT_SUPERVISOR = HPMCOUNTER17H;
	localparam [11:0] BTB_HIT = HPMCOUNTER18;
	localparam [11:0] BTB_HITH = HPMCOUNTER18H;
	localparam [11:0] EXCEPTION_USER = HPMCOUNTER19;
	localparam [11:0] EXCEPTION_USERH = HPMCOUNTER19H;
	localparam [11:0] EXCEPTION_SUPERVISOR = HPMCOUNTER20;
	localparam [11:0] EXCEPTION_SUPERVISORH = HPMCOUNTER20H;
	localparam [11:0] TIMER_INTERRUPT_USER = HPMCOUNTER21;
	localparam [11:0] TIMER_INTERRUPT_USERH = HPMCOUNTER21H;
	localparam [11:0] TIMER_INTERRUPT_SUPERVISOR = HPMCOUNTER22;
	localparam [11:0] TIMER_INTERRUPT_SUPERVISORH = HPMCOUNTER22H;
	localparam [11:0] JUMP_RETIRED = HPMCOUNTER23;
	localparam [11:0] JUMP_RETIREDH = HPMCOUNTER23H;
	localparam [11:0] DATA_TLB_MISS = HPMCOUNTER24;
	localparam [11:0] DATA_TLB_MISSH = HPMCOUNTER24H;
	localparam [11:0] JUMP_PREDICTED_CORRECT = HPMCOUNTER25;
	localparam [11:0] JUMP_PREDICTED_CORRECTH = HPMCOUNTER25H;
	localparam [11:0] RETURN_PREDICTED_CORRECT = HPMCOUNTER26;
	localparam [11:0] RETURN_PREDICTED_CORRECTH = HPMCOUNTER26H;
	localparam [11:0] JUMP_PREDICTED_INCORRECT = HPMCOUNTER27;
	localparam [11:0] JUMP_PREDICTED_INCORRECTH = HPMCOUNTER27H;
	localparam [11:0] RETURN_PREDICTED_INCORRECT = HPMCOUNTER28;
	localparam [11:0] RETURN_PREDICTED_INCORRECTH = HPMCOUNTER28H;
	localparam [11:0] INSTRUCTION_TLB_HIT = HPMCOUNTER29;
	localparam [11:0] INSTRUCTION_TLB_HITH = HPMCOUNTER29H;
	localparam [11:0] INSTRUCTIONS_FETCHED = HPMCOUNTER3;
	localparam [11:0] INSTRUCTION_TLB_MISS = HPMCOUNTER30;
	localparam [11:0] INSTRUCTION_TLB_MISSH = HPMCOUNTER30H;
	localparam [11:0] DATA_REMOTE_HIT = HPMCOUNTER31;
	localparam [11:0] DATA_REMOTE_HITH = HPMCOUNTER31H;
	localparam [11:0] INSTRUCTIONS_FETCHEDH = HPMCOUNTER3H;
	localparam [11:0] BRANCH_RETIRED = HPMCOUNTER4;
	localparam [11:0] BRANCH_RETIREDH = HPMCOUNTER4H;
	localparam [11:0] DATA_TLB_HIT = HPMCOUNTER5;
	localparam [11:0] DATA_TLB_HITH = HPMCOUNTER5H;
	localparam [11:0] FORWARD_BRANCH_PREDICTED_CORRECT = HPMCOUNTER6;
	localparam [11:0] FORWARD_BRANCH_PREDICTED_CORRECTH = HPMCOUNTER6H;
	localparam [11:0] BACKWARD_BRANCH_PREDICTED_CORRECT = HPMCOUNTER7;
	localparam [11:0] BACKWARD_BRANCH_PREDICTED_CORRECTH = HPMCOUNTER7H;
	localparam [11:0] FORWARD_BRANCH_PREDICTED_INCORRECT = HPMCOUNTER8;
	localparam [11:0] FORWARD_BRANCH_PREDICTED_INCORRECTH = HPMCOUNTER8H;
	localparam [11:0] BACKWARD_BRANCH_PREDICTED_INCORRECT = HPMCOUNTER9;
	localparam [11:0] BACKWARD_BRANCH_PREDICTED_INCORRECTH = HPMCOUNTER9H;
	localparam [11:0] INSTRUCTIONS_RETIRED = INSTRET;
	localparam [11:0] INSTRUCTIONS_RETIREDH = INSTRETH;
	input wire [(12 - 1):0] request_name;
	output reg [(32 - 1):0] request_value;
	input wire [31:0] status;
	input wire [(32 - 1):0] stvec;
	input wire [(32 - 1):0] sscratch;
	input wire [(32 - 1):0] sepc;
	input wire [(32 - 1):0] scause;
	input wire [(32 - 1):0] stval;
	input wire [31:0] satp;
	input wire [(32 - 1):0] medeleg;
	input wire [(32 - 1):0] mideleg;
	input wire [(32 - 1):0] mtvec;
	input wire [(32 - 1):0] mscratch;
	input wire [(32 - 1):0] mepc;
	input wire [(32 - 1):0] mcause;
	input wire [(32 - 1):0] mtval;
	input wire [63:0] cycleCount;
	input wire [63:0] instructionsRetired;
	input wire [63:0] instructionsFetched;
	input wire [63:0] branchesRetired;
	input wire [63:0] forwardBranchesPredictedCorrectly;
	input wire [63:0] forwardBranchesPredictedIncorrectly;
	input wire [63:0] backwardBranchesPredictedCorrectly;
	input wire [63:0] backwardBranchesPredictedIncorrectly;
	input wire [63:0] jumpsRetired;
	input wire [63:0] jumpsPredictedCorrectly;
	input wire [63:0] jumpsPredictedIncorrectly;
	input wire [63:0] returnPredictedCorrectly;
	input wire [63:0] returnPredictedIncorrectly;
	input wire [63:0] instructionTLBHits;
	input wire [63:0] instructionTLBMisses;
	input wire [63:0] instructionCacheHits;
	input wire [63:0] instructionCacheMisses;
	input wire [63:0] dataTLBHits;
	input wire [63:0] dataTLBMisses;
	input wire [63:0] dataCacheHits;
	input wire [63:0] dataCacheMisses;
	input wire [63:0] dataVRAMHits;
	input wire [63:0] dataRemoteHits;
	input wire [63:0] storeConditionalSuccesses;
	input wire [63:0] storeConditionalRejections;
	input wire [63:0] keyboardInterruptedUser;
	input wire [63:0] keyboardInterruptedSupervisor;
	input wire [63:0] timerInterruptedUser;
	input wire [63:0] timerInterruptedSupervisor;
	input wire [63:0] exceptionsInUserMode;
	input wire [63:0] exceptionsInSupervisorMode;
	input wire [63:0] btbHit;
	function automatic [(32 - 1):0] lowWord;
		input reg [63:0] counter;
		lowWord = counter[31:0];
	endfunction
	function automatic [(32 - 1):0] highWord;
		input reg [63:0] counter;
		highWord = counter[63:32];
	endfunction
	wire [(12 - 1):0] performanceCounter;
	assign performanceCounter = request_name;
	always @(*) begin
		request_value = 32'b0;
		case (request_name)
			NONE: request_value = 32'b0;
			SSTATUS: request_value = (status & 32'h800DE133);
			MSTATUS: request_value = (status & 32'h807FF9BB);
			STVEC: request_value = stvec;
			SSCRATCH: request_value = sscratch;
			SEPC: request_value = sepc;
			SCAUSE: request_value = scause;
			STVAL: request_value = stval;
			SATP: request_value = satp;
			MEDELEG: request_value = medeleg;
			MIDELEG: request_value = mideleg;
			MTVEC: request_value = mtvec;
			MSCRATCH: request_value = mscratch;
			MEPC: request_value = mepc;
			MCAUSE: request_value = mcause;
			MTVAL: request_value = mtval;
			CYCLE_COUNT: request_value = lowWord(cycleCount);
			CYCLE_COUNTH: request_value = highWord(cycleCount);
			INSTRUCTIONS_RETIRED: request_value = lowWord(instructionsRetired);
			INSTRUCTIONS_RETIREDH: request_value = highWord(instructionsRetired);
			INSTRUCTIONS_FETCHED: request_value = lowWord(instructionsFetched);
			INSTRUCTIONS_FETCHEDH: request_value = highWord(instructionsFetched);
			BRANCH_RETIRED: request_value = lowWord(branchesRetired);
			BRANCH_RETIREDH: request_value = highWord(branchesRetired);
			FORWARD_BRANCH_PREDICTED_CORRECT: request_value = lowWord(forwardBranchesPredictedCorrectly);
			FORWARD_BRANCH_PREDICTED_CORRECTH: request_value = highWord(forwardBranchesPredictedCorrectly);
			BACKWARD_BRANCH_PREDICTED_CORRECT: request_value = lowWord(backwardBranchesPredictedCorrectly);
			BACKWARD_BRANCH_PREDICTED_CORRECTH: request_value = highWord(backwardBranchesPredictedCorrectly);
			FORWARD_BRANCH_PREDICTED_INCORRECT: request_value = lowWord(forwardBranchesPredictedIncorrectly);
			FORWARD_BRANCH_PREDICTED_INCORRECTH: request_value = highWord(forwardBranchesPredictedIncorrectly);
			BACKWARD_BRANCH_PREDICTED_INCORRECT: request_value = lowWord(backwardBranchesPredictedIncorrectly);
			BACKWARD_BRANCH_PREDICTED_INCORRECTH: request_value = highWord(backwardBranchesPredictedIncorrectly);
			JUMP_RETIRED: request_value = lowWord(jumpsRetired);
			JUMP_RETIREDH: request_value = highWord(jumpsRetired);
			JUMP_PREDICTED_CORRECT: request_value = lowWord(jumpsPredictedCorrectly);
			JUMP_PREDICTED_CORRECTH: request_value = highWord(jumpsPredictedCorrectly);
			JUMP_PREDICTED_INCORRECT: request_value = lowWord(jumpsPredictedIncorrectly);
			JUMP_PREDICTED_INCORRECTH: request_value = highWord(jumpsPredictedIncorrectly);
			RETURN_PREDICTED_CORRECT: request_value = lowWord(returnPredictedCorrectly);
			RETURN_PREDICTED_CORRECTH: request_value = highWord(returnPredictedCorrectly);
			RETURN_PREDICTED_INCORRECT: request_value = lowWord(returnPredictedIncorrectly);
			RETURN_PREDICTED_INCORRECTH: request_value = highWord(returnPredictedIncorrectly);
			BTB_HIT: request_value = lowWord(btbHit);
			BTB_HITH: request_value = highWord(btbHit);
			INSTRUCTION_TLB_HIT: request_value = lowWord(instructionTLBHits);
			INSTRUCTION_TLB_HITH: request_value = highWord(instructionTLBHits);
			INSTRUCTION_TLB_MISS: request_value = lowWord(instructionTLBMisses);
			INSTRUCTION_TLB_MISSH: request_value = highWord(instructionTLBMisses);
			INSTRUCTION_CACHE_HIT: request_value = lowWord(instructionCacheHits);
			INSTRUCTION_CACHE_HITH: request_value = highWord(instructionCacheHits);
			INSTRUCTION_CACHE_MISS: request_value = lowWord(instructionCacheMisses);
			INSTRUCTION_CACHE_MISSH: request_value = highWord(instructionCacheMisses);
			DATA_TLB_HIT: request_value = lowWord(dataTLBHits);
			DATA_TLB_HITH: request_value = highWord(dataTLBHits);
			DATA_TLB_MISS: request_value = lowWord(dataTLBMisses);
			DATA_TLB_MISSH: request_value = highWord(dataTLBMisses);
			DATA_CACHE_HIT: request_value = lowWord(dataCacheHits);
			DATA_CACHE_HITH: request_value = highWord(dataCacheHits);
			DATA_CACHE_MISS: request_value = lowWord(dataCacheMisses);
			DATA_CACHE_MISSH: request_value = highWord(dataCacheMisses);
			DATA_VRAM_HIT: request_value = lowWord(dataVRAMHits);
			DATA_VRAM_HITH: request_value = highWord(dataVRAMHits);
			DATA_REMOTE_HIT: request_value = lowWord(dataRemoteHits);
			DATA_REMOTE_HITH: request_value = highWord(dataRemoteHits);
			STORE_CONDITIONAL_SUCCESS: request_value = lowWord(storeConditionalSuccesses);
			STORE_CONDITIONAL_SUCCESSH: request_value = highWord(storeConditionalSuccesses);
			STORE_CONDITIONAL_REJECT: request_value = lowWord(storeConditionalRejections);
			STORE_CONDITIONAL_REJECTH: request_value = highWord(storeConditionalRejections);
			KEYBOARD_INTERRUPT_USER: request_value = lowWord(keyboardInterruptedUser);
			KEYBOARD_INTERRUPT_SUPERVISOR: request_value = lowWord(keyboardInterruptedSupervisor);
			EXCEPTION_USER: request_value = lowWord(exceptionsInUserMode);
			EXCEPTION_USERH: request_value = highWord(exceptionsInUserMode);
			EXCEPTION_SUPERVISOR: request_value = lowWord(exceptionsInSupervisorMode);
			EXCEPTION_SUPERVISORH: request_value = highWord(exceptionsInSupervisorMode);
			TIMER_INTERRUPT_USER: request_value = lowWord(timerInterruptedUser);
			TIMER_INTERRUPT_USERH: request_value = highWord(timerInterruptedUser);
			TIMER_INTERRUPT_SUPERVISOR: request_value = lowWord(timerInterruptedSupervisor);
			TIMER_INTERRUPT_SUPERVISORH: request_value = highWord(timerInterruptedSupervisor);
			default: begin
			request_value = 32'b0;
		end
		endcase
	end
endmodule
module SimpleControlStatusRegister (
	request_name,
	request_value,
	request_writeEnable,
	register,
	clock,
	clear
);
	localparam [11:0] NONE = 12'h0;
	parameter [(12 - 1):0] NAME = NONE;
	input wire [(12 - 1):0] request_name;
	input wire [(32 - 1):0] request_value;
	input wire request_writeEnable;
	output wire [(32 - 1):0] register;
	input wire clock;
	input wire clear;
	wire csrWriteEnable;
	assign csrWriteEnable = (request_writeEnable && (request_name == NAME));
	Register #(.WIDTH(32)) csr(
		.d(request_value),
		.q(register),
		.enable(csrWriteEnable),
		.clock(clock),
		.clear(clear)
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module Core (
	instruction_ready,
	instruction_request,
	instruction_response,
	data_ready,
	data_request,
	data_response,
	remote_csrName,
	remote_registerIndex,
	remote_readCSR,
	remote_readRegister,
	remote_pc,
	remote_coreHalt,
	remote_coreDebug,
	remote_halted,
	remote_dump,
	keyboard_tval,
	keyboard_interruptPending,
	keyboard_interruptAccepted,
	timer_tval,
	timer_interruptPending,
	timer_interruptAccepted,
	memoryControl_supervisorPagesEnabled,
	memoryControl_userPagesEnabled,
	memoryControl_virtualMemoryEnabled,
	memoryControl_rootPageTable,
	clock,
	clear
);
	input wire instruction_ready;
	output wire [100:0] instruction_request;
	input wire [278:0] instruction_response;
	input wire data_ready;
	output wire [100:0] data_request;
	input wire [278:0] data_response;
	input wire [(12 - 1):0] remote_csrName;
	input wire [($clog2(32) - 1):0] remote_registerIndex;
	output reg [(32 - 1):0] remote_readCSR;
	output reg [(32 - 1):0] remote_readRegister;
	output wire [(32 - 1):0] remote_pc;
	output wire remote_coreHalt;
	output wire remote_coreDebug;
	input wire remote_halted;
	input wire remote_dump;
	input wire [(32 - 1):0] keyboard_tval;
	input wire keyboard_interruptPending;
	output wire keyboard_interruptAccepted;
	input wire [(32 - 1):0] timer_tval;
	input wire timer_interruptPending;
	output wire timer_interruptAccepted;
	output wire memoryControl_supervisorPagesEnabled;
	output wire memoryControl_userPagesEnabled;
	output wire memoryControl_virtualMemoryEnabled;
	output wire [33:0] memoryControl_rootPageTable;
	input wire clock;
	input wire clear;
	wire [100:0] fetch_IW;
	wire [379:0] instructionWait_ID;
	wire [827:0] decode_EX;
	wire [930:0] execute_BR;
	wire [1026:0] branch_MR;
	wire [1058:0] memoryRequest_MW;
	wire [1376:0] memoryWait_WB;
	// expanded instance: fetchControl
	wire fetchControl_flush;
	wire fetchControl_stall;
	wire fetchControl_ready;
	wire fetchControl_fencing;
	wire fetchControl_idle;
	wire [447:0] fetchControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: instructionWaitControl
	wire instructionWaitControl_flush;
	wire instructionWaitControl_stall;
	wire instructionWaitControl_ready;
	wire instructionWaitControl_fencing;
	wire instructionWaitControl_idle;
	wire [447:0] instructionWaitControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: decodeControl
	wire decodeControl_flush;
	wire decodeControl_stall;
	wire decodeControl_ready;
	wire decodeControl_fencing;
	wire decodeControl_idle;
	wire [447:0] decodeControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: executeControl
	wire executeControl_flush;
	wire executeControl_stall;
	wire executeControl_ready;
	wire executeControl_fencing;
	wire executeControl_idle;
	wire [447:0] executeControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: branchControl
	wire branchControl_flush;
	wire branchControl_stall;
	wire branchControl_ready;
	wire branchControl_fencing;
	wire branchControl_idle;
	wire [447:0] branchControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: memoryRequestControl
	wire memoryRequestControl_flush;
	wire memoryRequestControl_stall;
	wire memoryRequestControl_ready;
	wire memoryRequestControl_fencing;
	wire memoryRequestControl_idle;
	wire [447:0] memoryRequestControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: memoryWaitControl
	wire memoryWaitControl_flush;
	wire memoryWaitControl_stall;
	wire memoryWaitControl_ready;
	wire memoryWaitControl_fencing;
	wire memoryWaitControl_idle;
	wire [447:0] memoryWaitControl_decode;
	// removed modport ControlFlow
	// removed modport Stage
	// expanded instance: fetchPrediction
	wire [(32 - 1):0] fetchPrediction_pc;
	wire [67:0] fetchPrediction_prediction;
	wire fetchPrediction_forcePC;
	// removed modport Fetch
	// removed modport ControlFlow
	// expanded instance: branchPredictionResolution
	wire [100:0] branchPredictionResolution_fetch_BR;
	wire [95:0] branchPredictionResolution_branch_BR;
	// removed modport Branch
	// removed modport ControlFlow
	// expanded instance: rs1_ID
	wire [5:0] rs1_ID_request;
	wire [38:0] rs1_ID_response;
	// removed modport Requester
	// removed modport RegisterFile
	// expanded instance: rs2_ID
	wire [5:0] rs2_ID_request;
	wire [38:0] rs2_ID_response;
	// removed modport Requester
	// removed modport RegisterFile
	// expanded instance: remoteRegisterRequest
	reg [5:0] remoteRegisterRequest_request;
	wire [38:0] remoteRegisterRequest_response;
	// removed modport Requester
	// removed modport RegisterFile
	// expanded instance: rd_EX
	wire [38:0] rd_EX_response;
	// removed modport Reporter
	// removed modport RegisterFile
	// expanded instance: rd_BR
	wire [38:0] rd_BR_response;
	// removed modport Reporter
	// removed modport RegisterFile
	// expanded instance: rd_MR
	wire [38:0] rd_MR_response;
	// removed modport Reporter
	// removed modport RegisterFile
	// expanded instance: rd_MW
	wire [38:0] rd_MW_response;
	// removed modport Reporter
	// removed modport RegisterFile
	// expanded instance: rd_WB
	wire [38:0] rd_WB_response;
	// removed modport Reporter
	// removed modport RegisterFile
	RegisterFile registerFile(
		.rs1_ID_request(rs1_ID_request),
		.rs1_ID_response(rs1_ID_response),
		.rs2_ID_request(rs2_ID_request),
		.rs2_ID_response(rs2_ID_response),
		.remote_request(remoteRegisterRequest_request),
		.remote_response(remoteRegisterRequest_response),
		.rd_EX_response(rd_EX_response),
		.rd_BR_response(rd_BR_response),
		.rd_MR_response(rd_MR_response),
		.rd_MW_response(rd_MW_response),
		.rd_WB_response(rd_WB_response),
		.clock(clock),
		.clear(clear)
	);
	// expanded instance: controlFlow_InterruptDelivery
	wire controlFlow_InterruptDelivery_deliverInterrupt;
	wire [(32 - 1):0] controlFlow_InterruptDelivery_cause;
	wire [(32 - 1):0] controlFlow_InterruptDelivery_value;
	wire [(32 - 1):0] controlFlow_InterruptDelivery_epc;
	wire [(32 - 1):0] controlFlow_InterruptDelivery_trapVector;
	// removed modport ControlStatusRegister
	// removed modport ControlFlow
	// expanded instance: controlFlow_InterruptReturn
	wire [31:0] controlFlow_InterruptReturn_instructionType;
	wire [(32 - 1):0] controlFlow_InterruptReturn_epc;
	// removed modport ControlStatusRegister
	// removed modport ControlFlow
	// expanded instance: csr_ID
	wire [(12 - 1):0] csr_ID_name;
	wire [(32 - 1):0] csr_ID_value;
	// removed modport ControlStatusRegister
	// removed modport Requester
	// expanded instance: csr_remote
	reg [(12 - 1):0] csr_remote_name;
	wire [(32 - 1):0] csr_remote_value;
	// removed modport ControlStatusRegister
	// removed modport Requester
	// expanded instance: csr_WB
	wire [(12 - 1):0] csr_WB_name;
	wire [(32 - 1):0] csr_WB_value;
	wire csr_WB_writeEnable;
	// removed modport ControlStatusRegister
	// removed modport Requester
	// expanded instance: csrCoreObserver
	wire [31:0] csrCoreObserver_status;
	wire [1:0] csrCoreObserver_currentLevel;
	wire [(32 - 1):0] csrCoreObserver_mideleg;
	// removed modport ControlStatusRegister
	// removed modport Observer
	ControlStatusRegisters csrs(
		.interruptDelivery_deliverInterrupt(controlFlow_InterruptDelivery_deliverInterrupt),
		.interruptDelivery_cause(controlFlow_InterruptDelivery_cause),
		.interruptDelivery_value(controlFlow_InterruptDelivery_value),
		.interruptDelivery_epc(controlFlow_InterruptDelivery_epc),
		.interruptDelivery_trapVector(controlFlow_InterruptDelivery_trapVector),
		.interruptReturn_instructionType(controlFlow_InterruptReturn_instructionType),
		.interruptReturn_epc(controlFlow_InterruptReturn_epc),
		.memoryControl_supervisorPagesEnabled(memoryControl_supervisorPagesEnabled),
		.memoryControl_userPagesEnabled(memoryControl_userPagesEnabled),
		.memoryControl_virtualMemoryEnabled(memoryControl_virtualMemoryEnabled),
		.memoryControl_rootPageTable(memoryControl_rootPageTable),
		.observer_status(csrCoreObserver_status),
		.observer_currentLevel(csrCoreObserver_currentLevel),
		.observer_mideleg(csrCoreObserver_mideleg),
		.request_ID_name(csr_ID_name),
		.request_ID_value(csr_ID_value),
		.remote_name(csr_remote_name),
		.remote_value(csr_remote_value),
		.request_WB_name(csr_WB_name),
		.request_WB_value(csr_WB_value),
		.request_WB_writeEnable(csr_WB_writeEnable),
		.instructionWait_ID(instructionWait_ID),
		.memoryWait_WB(memoryWait_WB),
		.clock(clock),
		.clear(clear),
		.halted(remote_halted)
	);
	// expanded instance: controlFlowAsyncInterrupt
	wire [(32 - 1):0] controlFlowAsyncInterrupt_tval;
	wire [(32 - 1):0] controlFlowAsyncInterrupt_cause;
	wire controlFlowAsyncInterrupt_interruptPending;
	wire controlFlowAsyncInterrupt_interruptAccepted;
	// removed modport ControlFlow
	// removed modport InterruptController
	InterruptController interruptController(
		.keyboard_tval(keyboard_tval),
		.keyboard_interruptPending(keyboard_interruptPending),
		.keyboard_interruptAccepted(keyboard_interruptAccepted),
		.timer_tval(timer_tval),
		.timer_interruptPending(timer_interruptPending),
		.timer_interruptAccepted(timer_interruptAccepted),
		.controlFlow_tval(controlFlowAsyncInterrupt_tval),
		.controlFlow_cause(controlFlowAsyncInterrupt_cause),
		.controlFlow_interruptPending(controlFlowAsyncInterrupt_interruptPending),
		.controlFlow_interruptAccepted(controlFlowAsyncInterrupt_interruptAccepted),
		.csr_status(csrCoreObserver_status),
		.csr_currentLevel(csrCoreObserver_currentLevel),
		.csr_mideleg(csrCoreObserver_mideleg),
		.clock(clock),
		.clear(clear)
	);
	ControlFlow controlFlow(
		.csrInterruptDelivery_deliverInterrupt(controlFlow_InterruptDelivery_deliverInterrupt),
		.csrInterruptDelivery_cause(controlFlow_InterruptDelivery_cause),
		.csrInterruptDelivery_value(controlFlow_InterruptDelivery_value),
		.csrInterruptDelivery_epc(controlFlow_InterruptDelivery_epc),
		.csrInterruptDelivery_trapVector(controlFlow_InterruptDelivery_trapVector),
		.csrInterruptReturn_instructionType(controlFlow_InterruptReturn_instructionType),
		.csrInterruptReturn_epc(controlFlow_InterruptReturn_epc),
		.csrObserver_status(csrCoreObserver_status),
		.csrObserver_currentLevel(csrCoreObserver_currentLevel),
		.csrObserver_mideleg(csrCoreObserver_mideleg),
		.asyncInterrupt_tval(controlFlowAsyncInterrupt_tval),
		.asyncInterrupt_cause(controlFlowAsyncInterrupt_cause),
		.asyncInterrupt_interruptPending(controlFlowAsyncInterrupt_interruptPending),
		.asyncInterrupt_interruptAccepted(controlFlowAsyncInterrupt_interruptAccepted),
		.fetch_flush(fetchControl_flush),
		.fetch_stall(fetchControl_stall),
		.fetch_ready(fetchControl_ready),
		.fetch_fencing(fetchControl_fencing),
		.fetch_idle(fetchControl_idle),
		.fetch_decode(fetchControl_decode),
		.instructionWait_flush(instructionWaitControl_flush),
		.instructionWait_stall(instructionWaitControl_stall),
		.instructionWait_ready(instructionWaitControl_ready),
		.instructionWait_fencing(instructionWaitControl_fencing),
		.instructionWait_idle(instructionWaitControl_idle),
		.instructionWait_decode(instructionWaitControl_decode),
		.decode_flush(decodeControl_flush),
		.decode_stall(decodeControl_stall),
		.decode_ready(decodeControl_ready),
		.decode_fencing(decodeControl_fencing),
		.decode_idle(decodeControl_idle),
		.decode_decode(decodeControl_decode),
		.execute_flush(executeControl_flush),
		.execute_stall(executeControl_stall),
		.execute_ready(executeControl_ready),
		.execute_fencing(executeControl_fencing),
		.execute_idle(executeControl_idle),
		.execute_decode(executeControl_decode),
		.branch_flush(branchControl_flush),
		.branch_stall(branchControl_stall),
		.branch_ready(branchControl_ready),
		.branch_fencing(branchControl_fencing),
		.branch_idle(branchControl_idle),
		.branch_decode(branchControl_decode),
		.memoryRequest_flush(memoryRequestControl_flush),
		.memoryRequest_stall(memoryRequestControl_stall),
		.memoryRequest_ready(memoryRequestControl_ready),
		.memoryRequest_fencing(memoryRequestControl_fencing),
		.memoryRequest_idle(memoryRequestControl_idle),
		.memoryRequest_decode(memoryRequestControl_decode),
		.memoryWait_flush(memoryWaitControl_flush),
		.memoryWait_stall(memoryWaitControl_stall),
		.memoryWait_ready(memoryWaitControl_ready),
		.memoryWait_fencing(memoryWaitControl_fencing),
		.memoryWait_idle(memoryWaitControl_idle),
		.memoryWait_decode(memoryWaitControl_decode),
		.fetchPrediction_pc(fetchPrediction_pc),
		.fetchPrediction_prediction(fetchPrediction_prediction),
		.fetchPrediction_forcePC(fetchPrediction_forcePC),
		.branchResolve_fetch_BR(branchPredictionResolution_fetch_BR),
		.branchResolve_branch_BR(branchPredictionResolution_branch_BR),
		.writeBack(memoryWait_WB),
		.remoteHalted(remote_halted),
		.coreHalt(remote_coreHalt),
		.coreDebug(remote_coreDebug),
		.clock(clock),
		.clear(clear)
	);
	InstructionFetchStage fetch(
		.controlFlow_flush(fetchControl_flush),
		.controlFlow_stall(fetchControl_stall),
		.controlFlow_ready(fetchControl_ready),
		.controlFlow_fencing(fetchControl_fencing),
		.controlFlow_idle(fetchControl_idle),
		.controlFlow_decode(fetchControl_decode),
		.prediction_pc(fetchPrediction_pc),
		.prediction_prediction(fetchPrediction_prediction),
		.prediction_forcePC(fetchPrediction_forcePC),
		.memoryRequest(instruction_request),
		.memoryReady(instruction_ready),
		.fetch_IW(fetch_IW),
		.clock(clock),
		.clear(clear),
		.halted(remote_halted)
	);
	InstructionWaitStage instructionWait(
		.controlFlow_flush(instructionWaitControl_flush),
		.controlFlow_stall(instructionWaitControl_stall),
		.controlFlow_ready(instructionWaitControl_ready),
		.controlFlow_fencing(instructionWaitControl_fencing),
		.controlFlow_idle(instructionWaitControl_idle),
		.controlFlow_decode(instructionWaitControl_decode),
		.fetch_IW(fetch_IW),
		.memoryResponse(instruction_response),
		.instructionWait_ID(instructionWait_ID),
		.clock(clock),
		.clear(clear)
	);
	InstructionDecodeStage decode(
		.controlFlow_flush(decodeControl_flush),
		.controlFlow_stall(decodeControl_stall),
		.controlFlow_ready(decodeControl_ready),
		.controlFlow_fencing(decodeControl_fencing),
		.controlFlow_idle(decodeControl_idle),
		.controlFlow_decode(decodeControl_decode),
		.instructionWait_ID(instructionWait_ID),
		.csr_name(csr_ID_name),
		.csr_value(csr_ID_value),
		.rs1_request(rs1_ID_request),
		.rs1_response(rs1_ID_response),
		.rs2_request(rs2_ID_request),
		.rs2_response(rs2_ID_response),
		.csrObserver_status(csrCoreObserver_status),
		.csrObserver_currentLevel(csrCoreObserver_currentLevel),
		.csrObserver_mideleg(csrCoreObserver_mideleg),
		.decode_EX(decode_EX),
		.clock(clock),
		.clear(clear)
	);
	ExecuteStage execute(
		.controlFlow_flush(executeControl_flush),
		.controlFlow_stall(executeControl_stall),
		.controlFlow_ready(executeControl_ready),
		.controlFlow_fencing(executeControl_fencing),
		.controlFlow_idle(executeControl_idle),
		.controlFlow_decode(executeControl_decode),
		.decode_EX(decode_EX),
		.rd_EX_response(rd_EX_response),
		.execute_BR(execute_BR),
		.clock(clock),
		.clear(clear)
	);
	BranchResolveStage branch(
		.controlFlow_flush(branchControl_flush),
		.controlFlow_stall(branchControl_stall),
		.controlFlow_ready(branchControl_ready),
		.controlFlow_fencing(branchControl_fencing),
		.controlFlow_idle(branchControl_idle),
		.controlFlow_decode(branchControl_decode),
		.execute_BR(execute_BR),
		.rd_BR_response(rd_BR_response),
		.predictionResolution_fetch_BR(branchPredictionResolution_fetch_BR),
		.predictionResolution_branch_BR(branchPredictionResolution_branch_BR),
		.branch_MR(branch_MR),
		.clock(clock),
		.clear(clear)
	);
	MemoryRequestStage memoryRequest(
		.controlFlow_flush(memoryRequestControl_flush),
		.controlFlow_stall(memoryRequestControl_stall),
		.controlFlow_ready(memoryRequestControl_ready),
		.controlFlow_fencing(memoryRequestControl_fencing),
		.controlFlow_idle(memoryRequestControl_idle),
		.controlFlow_decode(memoryRequestControl_decode),
		.branch_MR(branch_MR),
		.memoryRequest(data_request),
		.memoryReady(data_ready),
		.rd_MR_response(rd_MR_response),
		.memoryRequest_MW(memoryRequest_MW),
		.clock(clock),
		.clear(clear)
	);
	MemoryWaitStage memoryWait(
		.controlFlow_flush(memoryWaitControl_flush),
		.controlFlow_stall(memoryWaitControl_stall),
		.controlFlow_ready(memoryWaitControl_ready),
		.controlFlow_fencing(memoryWaitControl_fencing),
		.controlFlow_idle(memoryWaitControl_idle),
		.controlFlow_decode(memoryWaitControl_decode),
		.memoryRequest_MW(memoryRequest_MW),
		.memoryResponse(data_response),
		.rd_MW_response(rd_MW_response),
		.memoryWait_WB(memoryWait_WB),
		.clock(clock),
		.clear(clear)
	);
	WriteBackStage writeBack(
		.memoryWait_WB(memoryWait_WB),
		.rd_WB_response(rd_WB_response),
		.csr_name(csr_WB_name),
		.csr_value(csr_WB_value),
		.csr_writeEnable(csr_WB_writeEnable),
		.lastPCRetired(remote_pc),
		.clock(clock),
		.clear(clear)
	);
	// removed an assertion item
	always @(*) begin
		remoteRegisterRequest_request = {remote_registerIndex, 1'b1};
		remote_readRegister = remoteRegisterRequest_response[32:1];
	end
	always @(*) begin
		csr_remote_name = remote_csrName;
		remote_readCSR = csr_remote_value;
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module ExecuteStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	decode_EX,
	rd_EX_response,
	execute_BR,
	clock,
	clear
);
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [31:0] ALU_SELECT_ZERO = 32'd0;
	localparam [31:0] JUMP_SELECT_ZERO = 32'd0;
	localparam [31:0] ALU_SELECT_REGISTER = 32'd1;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] JUMP_SELECT_PC = 32'd1;
	localparam [31:0] WRITE_BACK_SELECT_ALU = 32'd1;
	localparam [31:0] NORMAL = 32'd2;
	localparam [31:0] JUMP_SELECT_RS1 = 32'd2;
	localparam [31:0] WRITE_BACK_SELECT_PC_PLUS_4 = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] WRITE_BACK_SELECT_CSR = 32'd4;
	localparam [31:0] CSR = 32'd5;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] FENCE = 32'd6;
	localparam [31:0] PRIVILEGE_RAISE = 32'd7;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output wire controlFlow_ready;
	output reg controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	input wire [827:0] decode_EX;
	output wire [38:0] rd_EX_response;
	output wire [930:0] execute_BR;
	input wire clock;
	input wire clear;
	function automatic decodeGenerateFence;
		input reg [447:0] state;
		case (state[447:416])
			INVALID_INSTRUCTION: decodeGenerateFence = 1'b0;
			ILLEGAL_INSTRUCTION_FAULT: decodeGenerateFence = 1'b1;
			NORMAL: decodeGenerateFence = 1'b0;
			JUMP: decodeGenerateFence = 1'b0;
			BRANCH: decodeGenerateFence = 1'b0;
			CSR: begin
				case (state[223:212])
					SATP: decodeGenerateFence = 1'b1;
					SSTATUS: decodeGenerateFence = 1'b1;
					MSTATUS: decodeGenerateFence = 1'b1;
					default: decodeGenerateFence = 1'b0;
				endcase
			end
			FENCE: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_SRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_MRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_RAISE: decodeGenerateFence = 1'b1;
		endcase
	endfunction
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	reg [102:0] executeState;
	wire [(32 - 1):0] aluOperand1;
	wire [(32 - 1):0] aluOperand2;
	// removed an assertion item
	// removed an assertion item
	ArithmeticLogicUnitOperandSelector aluOperandSelector1(
		.decode(decode_EX),
		.registerValue(decode_EX[141:110]),
		.operandSelect(decode_EX[351:320]),
		.aluOperand(aluOperand1)
	);
	// removed an assertion item
	// removed an assertion item
	ArithmeticLogicUnitOperandSelector aluOperandSelector2(
		.decode(decode_EX),
		.registerValue(decode_EX[102:71]),
		.operandSelect(decode_EX[319:288]),
		.aluOperand(aluOperand2)
	);
	ArithmeticLogicUnit alu(
		.aluOperand1(aluOperand1),
		.aluOperand2(aluOperand2),
		.operation(decode_EX[383:352]),
		.aluResult(executeState[102:71])
	);
	reg [(32 - 1):0] jumpBase;
	always @(*) begin
		jumpBase = 32'b0;
		case (decode_EX[287:256])
			JUMP_SELECT_ZERO: jumpBase = 32'b0;
			JUMP_SELECT_PC: jumpBase = decode_EX[826:795];
			JUMP_SELECT_RS1: jumpBase = decode_EX[141:110];
		endcase
	end
	assign executeState[31:0] = (jumpBase + decode_EX[31:0]);
	always @(*) begin
		executeState[70:32] = {decode_EX[69:64], 32'hBADF00D, 1'b0};
		case (decode_EX[255:224])
			WRITE_BACK_SELECT_ALU: begin
				executeState[64:33] = executeState[102:71];
				executeState[32:32] = 1'b1;
			end
			WRITE_BACK_SELECT_PC_PLUS_4: begin
				executeState[64:33] = (decode_EX[826:795] + 32'd4);
				executeState[32:32] = 1'b1;
			end
			WRITE_BACK_SELECT_CSR: begin
				executeState[64:33] = decode_EX[179:148];
				executeState[32:32] = 1'b1;
			end
		endcase
	end
	assign rd_EX_response = executeState[70:32];
	always @(*) begin
		controlFlow_fencing = 1'b0;
		if (decode_EX[827:827]) begin
			if ((hasPendingException_IW(decode_EX[726:448]) || decodeGenerateFence(decode_EX[447:0]))) begin
				controlFlow_fencing = 1'b1;
			end
		end
	end
	assign controlFlow_ready = 1'b1;
	assign controlFlow_idle = (! decode_EX[827:827]);
	assign controlFlow_decode = decode_EX[447:0];
	wire [930:0] executeExport_EX_END;
	assign executeExport_EX_END[930:830] = decode_EX[827:727];
	assign executeExport_EX_END[829:551] = decode_EX[726:448];
	assign executeExport_EX_END[550:103] = decode_EX[447:0];
	assign executeExport_EX_END[102:0] = executeState;
	Register #(.WIDTH(931)) executePipelineRegister(
		.d(executeExport_EX_END),
		.q(execute_BR),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
endmodule
module ArithmeticLogicUnitOperandSelector (
	decode,
	registerValue,
	operandSelect,
	aluOperand
);
	localparam [31:0] ALU_SELECT_ZERO = 32'd0;
	localparam [31:0] ALU_SELECT_REGISTER = 32'd1;
	localparam [31:0] ALU_SELECT_PC = 32'd2;
	localparam [31:0] ALU_SELECT_IMMEDIATE = 32'd3;
	localparam [31:0] ALU_SELECT_CSR = 32'd4;
	input wire [827:0] decode;
	input wire [(32 - 1):0] registerValue;
	input wire [31:0] operandSelect;
	output reg [(32 - 1):0] aluOperand;
	always @(*) begin
		aluOperand = 32'hBADF00D;
		case (operandSelect)
			ALU_SELECT_ZERO: aluOperand = 32'b0;
			ALU_SELECT_REGISTER: aluOperand = registerValue;
			ALU_SELECT_PC: aluOperand = decode[826:795];
			ALU_SELECT_IMMEDIATE: aluOperand = decode[31:0];
			ALU_SELECT_CSR: aluOperand = decode[179:148];
		endcase
	end
endmodule
module ArithmeticLogicUnit (
	aluOperand1,
	aluOperand2,
	operation,
	aluResult
);
	localparam [31:0] ALU_ADD = 32'd0;
	localparam [31:0] ALU_SUBTRACT = 32'd1;
	localparam [31:0] ALU_NOT_EQUAL = 32'd10;
	localparam [31:0] ALU_LESS_THAN_SIGNED = 32'd11;
	localparam [31:0] ALU_LESS_THAN_UNSIGNED = 32'd12;
	localparam [31:0] ALU_GREATER_THAN_OR_EQUAL_SIGNED = 32'd13;
	localparam [31:0] ALU_GREATER_THAN_OR_EQUAL_UNSIGNED = 32'd14;
	localparam [31:0] ALU_OR = 32'd2;
	localparam [31:0] ALU_XOR = 32'd3;
	localparam [31:0] ALU_AND = 32'd4;
	localparam [31:0] ALU_CLEAR_MASK = 32'd5;
	localparam [31:0] ALU_SHIFT_LEFT = 32'd6;
	localparam [31:0] ALU_SHIFT_RIGHT_LOGICAL = 32'd7;
	localparam [31:0] ALU_SHIFT_RIGHT_ARITHMETIC = 32'd8;
	localparam [31:0] ALU_EQUAL = 32'd9;
	input wire [(32 - 1):0] aluOperand1;
	input wire [(32 - 1):0] aluOperand2;
	input wire [31:0] operation;
	output reg [(32 - 1):0] aluResult;
	always @(*) begin
		aluResult = 32'hBADF00D;
		case (operation)
			ALU_ADD: aluResult = (aluOperand1 + aluOperand2);
			ALU_SUBTRACT: aluResult = (aluOperand1 - aluOperand2);
			ALU_OR: aluResult = (aluOperand1 | aluOperand2);
			ALU_XOR: aluResult = (aluOperand1 ^ aluOperand2);
			ALU_AND: aluResult = (aluOperand1 & aluOperand2);
			ALU_CLEAR_MASK: aluResult = ((~ aluOperand1) & aluOperand2);
			ALU_SHIFT_LEFT: aluResult = (aluOperand1 << aluOperand2[4:0]);
			ALU_SHIFT_RIGHT_LOGICAL: aluResult = ($unsigned(aluOperand1) >> aluOperand2[4:0]);
			ALU_SHIFT_RIGHT_ARITHMETIC: aluResult = ($signed(aluOperand1) >>> aluOperand2[4:0]);
			ALU_EQUAL: aluResult = (aluOperand1 == aluOperand2);
			ALU_NOT_EQUAL: aluResult = (aluOperand1 != aluOperand2);
			ALU_LESS_THAN_SIGNED: aluResult = ($signed(aluOperand1) < $signed(aluOperand2));
			ALU_LESS_THAN_UNSIGNED: aluResult = ($unsigned(aluOperand1) < $unsigned(aluOperand2));
			ALU_GREATER_THAN_OR_EQUAL_SIGNED: aluResult = ($signed(aluOperand1) >= $signed(aluOperand2));
			ALU_GREATER_THAN_OR_EQUAL_UNSIGNED: aluResult = ($unsigned(aluOperand1) >= $unsigned(aluOperand2));
		endcase
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module InstructionDecodeStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	instructionWait_ID,
	csr_name,
	csr_value,
	rs1_request,
	rs1_response,
	rs2_request,
	rs2_response,
	csrObserver_status,
	csrObserver_currentLevel,
	csrObserver_mideleg,
	decode_EX,
	clock,
	clear
);
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [31:0] NO_WRITE_BACK = 32'd0;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] JUMP = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] PAGE_FAULT = 32'd5;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output reg controlFlow_ready;
	output reg controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	input wire [379:0] instructionWait_ID;
	output wire [(12 - 1):0] csr_name;
	input wire [(32 - 1):0] csr_value;
	output wire [5:0] rs1_request;
	input wire [38:0] rs1_response;
	output wire [5:0] rs2_request;
	input wire [38:0] rs2_response;
	input wire [31:0] csrObserver_status;
	input wire [1:0] csrObserver_currentLevel;
	input wire [(32 - 1):0] csrObserver_mideleg;
	output wire [827:0] decode_EX;
	input wire clock;
	input wire clear;
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	wire [447:0] decodeState_decoder;
	reg [447:0] decodeState_selected;
	RISCVDecoder decoder(
		.instruction(instructionWait_ID[31:0]),
		.decodeState(decodeState_decoder),
		.csr_name(csr_name),
		.csr_value(csr_value),
		.rs1_request(rs1_request),
		.rs1_response(rs1_response),
		.rs2_request(rs2_request),
		.rs2_response(rs2_response),
		.csrObserver_status(csrObserver_status),
		.csrObserver_currentLevel(csrObserver_currentLevel),
		.csrObserver_mideleg(csrObserver_mideleg),
		.clock(clock),
		.clear(clear)
	);
	always @(*) begin
		controlFlow_ready = 1'b1;
		controlFlow_fencing = 1'b0;
		decodeState_selected = {32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 12'd0, 32'd0, 32'd0, 39'd0, 39'd0, 6'd0, 64'd0};
		if (instructionWait_ID[379:379]) begin
			if (hasPendingException_IW(instructionWait_ID[278:0])) begin
				controlFlow_ready = 1'b1;
				controlFlow_fencing = 1'b1;
				decodeState_selected[447:416] = ILLEGAL_INSTRUCTION_FAULT;
			end
			else if ((decodeState_decoder[447:416] == ILLEGAL_INSTRUCTION_FAULT)) begin
				controlFlow_ready = 1'b1;
				controlFlow_fencing = 1'b1;
				decodeState_selected = decodeState_decoder;
			end
			else if (((decodeState_decoder[142:142] && (! rs1_response[0:0])) || (decodeState_decoder[103:103] && (! rs2_response[0:0])))) begin
				controlFlow_ready = 1'b0;
				decodeState_selected = decodeState_decoder;
			end
			else begin
				controlFlow_ready = 1'b1;
				decodeState_selected = decodeState_decoder;
			end
		end
	end
	assign controlFlow_idle = (! instructionWait_ID[379:379]);
	assign controlFlow_decode = decodeState_selected;
	wire [827:0] decodeExport_ID_END;
	assign decodeExport_ID_END[827:727] = instructionWait_ID[379:279];
	assign decodeExport_ID_END[726:448] = instructionWait_ID[278:0];
	assign decodeExport_ID_END[447:0] = decodeState_selected;
	Register #(.WIDTH(828)) decodePipelineRegister(
		.d(decodeExport_ID_END),
		.q(decode_EX),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module InstructionFetchStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	prediction_pc,
	prediction_prediction,
	prediction_forcePC,
	memoryRequest,
	memoryReady,
	fetch_IW,
	clock,
	clear,
	halted
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] PAGE_FAULT = 32'd5;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output wire controlFlow_ready;
	output wire controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	output wire [(32 - 1):0] prediction_pc;
	input wire [67:0] prediction_prediction;
	input wire prediction_forcePC;
	output wire [100:0] memoryRequest;
	input wire memoryReady;
	output reg [100:0] fetch_IW;
	input wire clock;
	input wire clear;
	input wire halted;
	function automatic [33:0] translateVirtualAddress;
		input reg [(32 - 1):0] virtualAddress;
		input reg [47:0] translation;
		translateVirtualAddress = {translation[27:6], virtualAddress[11:0]};
	endfunction
	function automatic [113:0] addressTranslationResponse;
		input reg [(32 - 1):0] virtualAddress;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		reg [33:0] physicalAddress;
		begin
			physicalAddress = translateVirtualAddress(virtualAddress, translation);
			addressTranslationResponse = {physicalAddress, translationType, translation};
		end
	endfunction
	function automatic [278:0] memoryResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		input reg [31:0] responseType;
		input reg [(32 - 1):0] readData;
		reg [113:0] addressTranslation;
		begin
			addressTranslation = addressTranslationResponse(request[95:64], translationType, translation);
			memoryResponse = {responseType, request, addressTranslation, readData};
		end
	endfunction
	function automatic [278:0] memoryFaultResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		memoryFaultResponse = memoryResponse(request, translationType, translation, PAGE_FAULT, 32'hBADF00D);
	endfunction
	function automatic [70:0] simpleMemoryRead;
		input reg [33:0] physicalAddress;
		simpleMemoryRead = {physicalAddress, 1'b1, 4'b0, 32'hBADF00D};
	endfunction
	function automatic isInVRAM;
		input reg [33:0] address;
		isInVRAM = ((32'h1000 <= address) && (address < (32'h1000 + 'd4096)));
	endfunction
	function automatic isInRemote;
		input reg [33:0] address;
		isInRemote = ((32'h2000 <= address) && (address < (32'h2000 + 'd4096)));
	endfunction
	function automatic [70:0] simpleMemoryWrite;
		input reg [33:0] physicalAddress;
		input reg [3:0] writeEnable;
		input reg [(32 - 1):0] writeData;
		simpleMemoryWrite = {physicalAddress, 1'b0, writeEnable, writeData};
	endfunction
	function automatic [100:0] memoryReadRequest;
		input reg [(32 - 1):0] address;
		input reg readEnable;
		memoryReadRequest = {readEnable, 4'b0, address, 32'hBADF00D, KEEP};
	endfunction
	reg [(32 - 1):0] pc;
	wire [(32 - 1):0] pc_saved;
	wire [(32 - 1):0] pc_new;
	wire pc_enable;
	wire pc_enable_saved;
	assign pc_new = prediction_prediction[35:4];
	assign pc_enable = (((! controlFlow_stall) && (! controlFlow_flush)) || prediction_forcePC);
	Register #(.WIDTH(1)) pcEnableRegister(
		.q(pc_enable_saved),
		.d(pc_enable),
		.enable(1'b1),
		.clock(clock),
		.clear(clear)
	);
	Register #(
		.WIDTH(32),
		.CLEAR_VALUE(32'h100000)
	) pcRegister(
		.q(pc_saved),
		.d(pc_new),
		.enable(pc_enable_saved),
		.clock(clock),
		.clear(clear)
	);
	always @(*) begin
		if (pc_enable_saved) begin
			pc = pc_new;
		end
		else begin
			pc = pc_saved;
		end
	end
	assign controlFlow_ready = memoryReady;
	assign controlFlow_idle = controlFlow_flush;
	assign controlFlow_fencing = 1'b0;
	assign controlFlow_decode = {32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 12'd0, 32'd0, 32'd0, 39'd0, 39'd0, 6'd0, 64'd0};
	assign prediction_pc = pc;
	assign memoryRequest = memoryReadRequest(pc, (! halted));
	wire [100:0] fetchState_IF_END;
	assign fetchState_IF_END = {(memoryRequest[100:100] && controlFlow_ready), pc, prediction_prediction};
	wire [100:0] fetchExport_IF_END;
	wire [100:0] fetchExport_IW_START;
	assign fetchExport_IF_END[100:0] = fetchState_IF_END;
	Register #(.WIDTH(101)) fetchPipelineRegister(
		.d(fetchExport_IF_END),
		.q(fetchExport_IW_START),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
	wire didStall;
	Register #(.WIDTH(1)) didStallRegister(
		.d(controlFlow_stall),
		.q(didStall),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	wire [67:0] prediction_last;
	Register #(.WIDTH(68)) predictionLastRegister(
		.d(fetch_IW[67:0]),
		.q(prediction_last),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	always @(*) begin
		fetch_IW = fetchExport_IW_START;
		fetch_IW[67:0] = prediction_prediction;
		if (didStall) begin
			fetch_IW[67:0] = prediction_last;
		end
	end
	// removed an assertion item
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module InstructionWaitStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	fetch_IW,
	memoryResponse,
	instructionWait_ID,
	clock,
	clear
);
	localparam [31:0] INVALID_MEMORY_RESPONSE = 32'd0;
	localparam [31:0] CACHE_HIT = 32'd1;
	localparam [31:0] CACHE_MISS = 32'd2;
	localparam [31:0] PAGE_FAULT = 32'd5;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output reg controlFlow_ready;
	output reg controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	input wire [100:0] fetch_IW;
	input wire [278:0] memoryResponse;
	output wire [379:0] instructionWait_ID;
	input wire clock;
	input wire clear;
	// removed an assertion item
	function automatic isValidMemoryResponse;
		input reg [278:0] response;
		input reg [(32 - 1):0] expectedAddress;
		isValidMemoryResponse = ((response[278:247] != INVALID_MEMORY_RESPONSE) && (response[241:210] == expectedAddress));
	endfunction
	wire [278:0] memoryResponse_IF;
	wire validMemoryResponse_IF;
	assign validMemoryResponse_IF = isValidMemoryResponse(memoryResponse, fetch_IW[99:68]);
	Register #(.WIDTH(279)) memoryResponseRegister(
		.q(memoryResponse_IF),
		.d(memoryResponse),
		.clock(clock),
		.clear(clear),
		.enable(validMemoryResponse_IF)
	);
	reg [278:0] memoryResponse_selected;
	always @(*) begin
		memoryResponse_selected = {32'd0, 101'd0, 114'd0, 32'd0};
		if (fetch_IW[100:100]) begin
			if (isValidMemoryResponse(memoryResponse_IF, fetch_IW[99:68])) begin
				memoryResponse_selected = memoryResponse_IF;
			end
			else begin
				memoryResponse_selected = memoryResponse;
			end
		end
	end
	wire [278:0] waitState_IW_END;
	assign waitState_IW_END[278:0] = memoryResponse_selected;
	always @(*) begin
		controlFlow_ready = 1'b0;
		controlFlow_fencing = 1'b0;
		if (fetch_IW[100:100]) begin
			controlFlow_ready = isValidMemoryResponse(memoryResponse_selected, fetch_IW[99:68]);
			controlFlow_fencing = ((memoryResponse_selected[278:247] == PAGE_FAULT) && controlFlow_ready);
		end
		else begin
			controlFlow_ready = 1'b1;
		end
	end
	assign controlFlow_idle = (! fetch_IW[100:100]);
	assign controlFlow_decode = {32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 32'd0, 12'd0, 32'd0, 32'd0, 39'd0, 39'd0, 6'd0, 64'd0};
	wire [379:0] waitExport_IW_END;
	assign waitExport_IW_END[379:279] = fetch_IW[100:0];
	assign waitExport_IW_END[278:0] = waitState_IW_END;
	Register #(.WIDTH(380)) instructionWaitPipelineRegister(
		.d(waitExport_IW_END),
		.q(instructionWait_ID),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
module InterruptController (
	keyboard_tval,
	keyboard_interruptPending,
	keyboard_interruptAccepted,
	timer_tval,
	timer_interruptPending,
	timer_interruptAccepted,
	controlFlow_tval,
	controlFlow_cause,
	controlFlow_interruptPending,
	controlFlow_interruptAccepted,
	csr_status,
	csr_currentLevel,
	csr_mideleg,
	clock,
	clear
);
	localparam [1:0] USER_MODE = 2'd0;
	localparam [1:0] SUPERVISOR_MODE = 2'd1;
	localparam [1:0] MACHINE_MODE = 2'd3;
	localparam [31:0] RESERVED_INVALID_CAUSE = 32'h10;
	localparam [31:0] TIMER_INTERRUPT = 32'h80000007;
	localparam [31:0] EXTERNAL_INTERRUPT = 32'h80000009;
	input wire [(32 - 1):0] keyboard_tval;
	input wire keyboard_interruptPending;
	output reg keyboard_interruptAccepted;
	input wire [(32 - 1):0] timer_tval;
	input wire timer_interruptPending;
	output reg timer_interruptAccepted;
	output reg [(32 - 1):0] controlFlow_tval;
	output reg [(32 - 1):0] controlFlow_cause;
	output reg controlFlow_interruptPending;
	input wire controlFlow_interruptAccepted;
	input wire [31:0] csr_status;
	input wire [1:0] csr_currentLevel;
	input wire [(32 - 1):0] csr_mideleg;
	input wire clock;
	input wire clear;
	function automatic hasSuffientPermissions;
		input reg [(12 - 1):0] name;
		input reg [1:0] privilegeLevel;
		case (privilegeLevel)
			MACHINE_MODE: hasSuffientPermissions = 1'b1;
			SUPERVISOR_MODE: hasSuffientPermissions = (name[9:8] <= SUPERVISOR_MODE);
			USER_MODE: hasSuffientPermissions = (name[9:8] == USER_MODE);
		endcase
	endfunction
	function automatic isDelegated;
		input reg [(32 - 1):0] deleg;
		input reg [(32 - 1):0] cause;
		isDelegated = ((deleg & (1 << (cause & 32'hf))) != 32'd0);
	endfunction
	function automatic isInterruptCause;
		input reg [(32 - 1):0] cause;
		isInterruptCause = cause[31];
	endfunction
	// removed an assertion item
	function automatic interruptEnabled;
		input reg [(32 - 1):0] cause;
		case (csr_currentLevel)
			USER_MODE: interruptEnabled = 1'b1;
			SUPERVISOR_MODE: begin
				if (isDelegated(csr_mideleg, cause)) begin
					interruptEnabled = csr_status[1:1];
				end
				else begin
					interruptEnabled = 1'b1;
				end
			end
			MACHINE_MODE: interruptEnabled = csr_status[3:3];
		endcase
	endfunction
	always @(*) begin
		timer_interruptAccepted = 1'b0;
		keyboard_interruptAccepted = 1'b0;
		controlFlow_interruptPending = 1'b0;
		controlFlow_tval = 32'hBADF00D;
		controlFlow_cause = RESERVED_INVALID_CAUSE;
		if ((interruptEnabled(EXTERNAL_INTERRUPT) && keyboard_interruptPending)) begin
			controlFlow_interruptPending = 1'b1;
			controlFlow_tval = keyboard_tval;
			controlFlow_cause = EXTERNAL_INTERRUPT;
			if (controlFlow_interruptAccepted) begin
				keyboard_interruptAccepted = 1'b1;
			end
		end
		else if ((interruptEnabled(TIMER_INTERRUPT) && timer_interruptPending)) begin
			controlFlow_interruptPending = 1'b1;
			controlFlow_tval = timer_tval;
			controlFlow_cause = TIMER_INTERRUPT;
			if (controlFlow_interruptAccepted) begin
				timer_interruptAccepted = 1'b1;
			end
		end
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
// removed typedef: Reservation_t
module MemoryRequestStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	branch_MR,
	memoryRequest,
	memoryReady,
	rd_MR_response,
	memoryRequest_MW,
	clock,
	clear
);
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] NO_CONDITIONAL = 32'd0;
	localparam [31:0] NO_MEMORY_OPERATION = 32'd0;
	localparam [4:0] X0 = 5'd0;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] INVALIDATE_PAGE = 32'd1;
	localparam [31:0] STORE_SUCCESS = 32'd1;
	localparam [31:0] LOAD_BYTE = 32'd1;
	localparam [31:0] STORE_CONDITIONAL = 32'd10;
	localparam [31:0] FLUSH_TRANSLATION = 32'd11;
	localparam [31:0] NORMAL = 32'd2;
	localparam [31:0] INVALIDATE_ALL = 32'd2;
	localparam [31:0] STORE_REJECTED = 32'd2;
	localparam [31:0] LOAD_HALF_WORD = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [31:0] LOAD_WORD = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] LOAD_BYTE_UNSIGNED = 32'd4;
	localparam [31:0] CSR = 32'd5;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] LOAD_HALF_WORD_UNSIGNED = 32'd5;
	localparam [31:0] FENCE = 32'd6;
	localparam [31:0] STORE_BYTE = 32'd6;
	localparam [31:0] PRIVILEGE_RAISE = 32'd7;
	localparam [31:0] STORE_HALF_WORD = 32'd7;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] STORE_WORD = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	localparam [31:0] LOAD_RESERVED = 32'd9;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output reg controlFlow_ready;
	output reg controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	input wire [1026:0] branch_MR;
	output reg [100:0] memoryRequest;
	input wire memoryReady;
	output wire [38:0] rd_MR_response;
	output wire [1058:0] memoryRequest_MW;
	input wire clock;
	input wire clear;
	function automatic decodeGenerateFence;
		input reg [447:0] state;
		case (state[447:416])
			INVALID_INSTRUCTION: decodeGenerateFence = 1'b0;
			ILLEGAL_INSTRUCTION_FAULT: decodeGenerateFence = 1'b1;
			NORMAL: decodeGenerateFence = 1'b0;
			JUMP: decodeGenerateFence = 1'b0;
			BRANCH: decodeGenerateFence = 1'b0;
			CSR: begin
				case (state[223:212])
					SATP: decodeGenerateFence = 1'b1;
					SSTATUS: decodeGenerateFence = 1'b1;
					MSTATUS: decodeGenerateFence = 1'b1;
					default: decodeGenerateFence = 1'b0;
				endcase
			end
			FENCE: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_SRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_MRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_RAISE: decodeGenerateFence = 1'b1;
		endcase
	endfunction
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	// removed an assertion item
	reg [31:0] requestState;
	wire [(32 - 1):0] address;
	assign address = branch_MR[198:167];
	reg [(32 - 1):0] writeData_aligned;
	always @(*) begin
		case (address[1:0])
			2'd0: writeData_aligned = branch_MR[301:270];
			2'd1: writeData_aligned = (branch_MR[301:270] << 5'd8);
			2'd2: writeData_aligned = (branch_MR[301:270] << 5'd16);
			2'd3: writeData_aligned = (branch_MR[301:270] << 5'd24);
		endcase
	end
	wire [32:0] reservation;
	wire [32:0] reservation_next;
	reg reservation_clear;
	reg reservation_load;
	assign reservation_next = {reservation_load, address};
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	Register #(.WIDTH(33)) reservationRegister(
		.d(reservation_next),
		.q(reservation),
		.enable(reservation_load),
		.clear((clear || ((reservation_clear && (! controlFlow_stall)) && (! controlFlow_flush)))),
		.clock(clock)
	);
	always @(*) begin : requestGeneration
		memoryRequest = {1'b0, 4'b0, {address[31:2], 2'b0}, writeData_aligned, KEEP};
		reservation_clear = 1'b0;
		reservation_load = 1'b0;
		requestState[31:0] = NO_CONDITIONAL;
		case (branch_MR[614:583])
			NO_MEMORY_OPERATION: begin
				case (branch_MR[646:615])
					INVALID_INSTRUCTION: reservation_clear = 1'b0;
					ILLEGAL_INSTRUCTION_FAULT: begin
						reservation_clear = 1'b1;
					end
					NORMAL: reservation_clear = 1'b0;
					JUMP: reservation_clear = 1'b0;
					BRANCH: reservation_clear = 1'b0;
					CSR: begin
						reservation_clear = 1'b0;
					end
					FENCE: begin
						reservation_clear = 1'b0;
					end
					PRIVILEGE_RAISE: reservation_clear = 1'b1;
					PRIVILEGE_LOWER_SRET: reservation_clear = 1'b1;
					PRIVILEGE_LOWER_MRET: reservation_clear = 1'b1;
				endcase
			end
			LOAD_BYTE: begin
				reservation_clear = 1'b1;
				memoryRequest[100:100] = 1'b1;
			end
			LOAD_HALF_WORD: begin
				reservation_clear = 1'b1;
				memoryRequest[100:100] = 1'b1;
			end
			LOAD_WORD: begin
				reservation_clear = 1'b1;
				memoryRequest[100:100] = 1'b1;
			end
			LOAD_BYTE_UNSIGNED: begin
				reservation_clear = 1'b1;
				memoryRequest[100:100] = 1'b1;
			end
			LOAD_HALF_WORD_UNSIGNED: begin
				reservation_clear = 1'b1;
				memoryRequest[100:100] = 1'b1;
			end
			STORE_BYTE: begin
				reservation_clear = 1'b1;
				memoryRequest[99:96] = (4'b0001 << address[1:0]);
			end
			STORE_HALF_WORD: begin
				reservation_clear = 1'b1;
				memoryRequest[99:96] = (4'b0011 << address[1:0]);
			end
			STORE_WORD: begin
				reservation_clear = 1'b1;
				memoryRequest[99:96] = 4'b1111;
			end
			LOAD_RESERVED: begin
				reservation_load = 1'b1;
				memoryRequest[100:100] = 1'b1;
			end
			STORE_CONDITIONAL: begin
				if ((reservation[32:32] && (reservation[31:0] == address))) begin
					memoryRequest[99:96] = 4'b1111;
					requestState[31:0] = STORE_SUCCESS;
				end
				else begin
					requestState[31:0] = STORE_REJECTED;
				end
				reservation_clear = 1'b1;
			end
			FLUSH_TRANSLATION: begin
				if ((branch_MR[346:342] == X0)) begin
					memoryRequest[31:0] = INVALIDATE_ALL;
				end
				else begin
					memoryRequest[31:0] = INVALIDATE_PAGE;
				end
				reservation_clear = 1'b1;
			end
		endcase
	end
	assign rd_MR_response = branch_MR[166:128];
	always @(*) begin
		controlFlow_fencing = 1'b0;
		controlFlow_ready = 1'b1;
		if (branch_MR[1026:1026]) begin
			controlFlow_fencing = decodeGenerateFence(memoryRequest_MW[678:231]);
			if (hasPendingException_IW(branch_MR[925:647])) begin
				controlFlow_fencing = 1'b1;
				controlFlow_ready = 1'b1;
			end
			else if ((branch_MR[614:583] == NO_MEMORY_OPERATION)) begin
				controlFlow_ready = 1'b1;
			end
			else begin
				controlFlow_ready = memoryReady;
			end
		end
	end
	assign controlFlow_idle = (! branch_MR[1026:1026]);
	assign controlFlow_decode = branch_MR[646:199];
	wire [1058:0] memoryRequest_MR_END;
	assign memoryRequest_MR_END[1058:958] = branch_MR[1026:926];
	assign memoryRequest_MR_END[957:679] = branch_MR[925:647];
	assign memoryRequest_MR_END[678:231] = branch_MR[646:199];
	assign memoryRequest_MR_END[230:128] = branch_MR[198:96];
	assign memoryRequest_MR_END[127:32] = branch_MR[95:0];
	assign memoryRequest_MR_END[31:0] = requestState;
	Register #(.WIDTH(1059)) memoryRequestPipelineRegister(
		.d(memoryRequest_MR_END),
		.q(memoryRequest_MW),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module MemoryWaitStage (
	controlFlow_flush,
	controlFlow_stall,
	controlFlow_ready,
	controlFlow_fencing,
	controlFlow_idle,
	controlFlow_decode,
	memoryRequest_MW,
	memoryResponse,
	rd_MW_response,
	memoryWait_WB,
	clock,
	clear
);
	localparam [31:0] INVALID_INSTRUCTION = 32'd0;
	localparam [31:0] INVALID_MEMORY_RESPONSE = 32'd0;
	localparam [31:0] NO_MEMORY_OPERATION = 32'd0;
	localparam [11:0] SSTATUS = 12'h100;
	localparam [11:0] SATP = 12'h180;
	localparam [11:0] MSTATUS = 12'h300;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] CACHE_HIT = 32'd1;
	localparam [31:0] STORE_SUCCESS = 32'd1;
	localparam [31:0] LOAD_BYTE = 32'd1;
	localparam [31:0] STORE_CONDITIONAL = 32'd10;
	localparam [31:0] NORMAL = 32'd2;
	localparam [31:0] CACHE_MISS = 32'd2;
	localparam [31:0] STORE_REJECTED = 32'd2;
	localparam [31:0] LOAD_HALF_WORD = 32'd2;
	localparam [31:0] JUMP = 32'd3;
	localparam [31:0] VRAM_IO = 32'd3;
	localparam [31:0] LOAD_WORD = 32'd3;
	localparam [31:0] WRITE_BACK_SELECT_MEMORY = 32'd3;
	localparam [2:0] FUNCTION_RETURN = 3'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] REMOTE_IO = 32'd4;
	localparam [31:0] LOAD_BYTE_UNSIGNED = 32'd4;
	localparam [31:0] CSR = 32'd5;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] LOAD_HALF_WORD_UNSIGNED = 32'd5;
	localparam [31:0] FENCE = 32'd6;
	localparam [31:0] TRANSLATION_INVALIDATE = 32'd6;
	localparam [31:0] STORE_BYTE = 32'd6;
	localparam [31:0] PRIVILEGE_RAISE = 32'd7;
	localparam [31:0] STORE_HALF_WORD = 32'd7;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] STORE_WORD = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	localparam [31:0] LOAD_RESERVED = 32'd9;
	input wire controlFlow_flush;
	input wire controlFlow_stall;
	output reg controlFlow_ready;
	output reg controlFlow_fencing;
	output wire controlFlow_idle;
	output wire [447:0] controlFlow_decode;
	input wire [1058:0] memoryRequest_MW;
	input wire [278:0] memoryResponse;
	output wire [38:0] rd_MW_response;
	output wire [1376:0] memoryWait_WB;
	input wire clock;
	input wire clear;
	function automatic decodeGenerateFence;
		input reg [447:0] state;
		case (state[447:416])
			INVALID_INSTRUCTION: decodeGenerateFence = 1'b0;
			ILLEGAL_INSTRUCTION_FAULT: decodeGenerateFence = 1'b1;
			NORMAL: decodeGenerateFence = 1'b0;
			JUMP: decodeGenerateFence = 1'b0;
			BRANCH: decodeGenerateFence = 1'b0;
			CSR: begin
				case (state[223:212])
					SATP: decodeGenerateFence = 1'b1;
					SSTATUS: decodeGenerateFence = 1'b1;
					MSTATUS: decodeGenerateFence = 1'b1;
					default: decodeGenerateFence = 1'b0;
				endcase
			end
			FENCE: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_SRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_LOWER_MRET: decodeGenerateFence = 1'b1;
			PRIVILEGE_RAISE: decodeGenerateFence = 1'b1;
		endcase
	endfunction
	function automatic isBranch;
		input reg [1376:0] memoryWaitExport;
		isBranch = (memoryWaitExport[996:965] == BRANCH);
	endfunction
	function automatic [31:0] getDirection;
		input reg [1376:0] memoryWaitExport;
		getDirection = memoryWaitExport[445:414];
	endfunction
	function automatic isJump;
		input reg [1376:0] memoryWaitExport;
		isJump = (memoryWaitExport[996:965] == JUMP);
	endfunction
	function automatic [31:0] getPrediction;
		input reg [1376:0] memoryWaitExport;
		getPrediction = memoryWaitExport[413:382];
	endfunction
	function automatic isBTBHit;
		input reg [1376:0] memoryWaitExport;
		isBTBHit = memoryWaitExport[1276:1276];
	endfunction
	function automatic isJumpReturn;
		input reg [1376:0] memoryWaitExport;
		isJumpReturn = (memoryWaitExport[1279:1277] == FUNCTION_RETURN);
	endfunction
	function automatic [31:0] getInstructionTranslationType;
		input reg [1376:0] memoryWaitExport;
		getInstructionTranslationType = memoryWaitExport[1108:1077];
	endfunction
	function automatic [31:0] getInstructionResponseType;
		input reg [1376:0] memoryWaitExport;
		getInstructionResponseType = memoryWaitExport[1275:1244];
	endfunction
	function automatic [31:0] getDataTranslationType;
		input reg [1376:0] memoryWaitExport;
		getDataTranslationType = memoryWaitExport[150:119];
	endfunction
	function automatic [31:0] getDataResponseType;
		input reg [1376:0] memoryWaitExport;
		getDataResponseType = memoryWaitExport[317:286];
	endfunction
	function automatic [31:0] getMemoryOperation;
		input reg [1376:0] memoryWaitExport;
		getMemoryOperation = memoryWaitExport[964:933];
	endfunction
	function automatic [31:0] getStoreConditionalResult;
		input reg [1376:0] memoryWaitExport;
		getStoreConditionalResult = memoryWaitExport[349:318];
	endfunction
	function automatic hasPendingException;
		input reg [1376:0] memoryWaitExport;
		hasPendingException = (((getDataResponseType(memoryWaitExport) == PAGE_FAULT) || (getInstructionResponseType(memoryWaitExport) == PAGE_FAULT)) || (memoryWaitExport[996:965] == ILLEGAL_INSTRUCTION_FAULT));
	endfunction
	function automatic hasPendingException_IW;
		input reg [278:0] instructionWait;
		hasPendingException_IW = (instructionWait[278:247] == PAGE_FAULT);
	endfunction
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	reg [31:0] requestState;
	always @(*) begin
		requestState = memoryRequest_MW[31:0];
		if (((requestState[31:0] == STORE_SUCCESS) && (memoryResponse[278:247] == PAGE_FAULT))) begin
			requestState[31:0] = STORE_REJECTED;
		end
	end
	// removed an assertion item
	reg [317:0] waitState;
	assign waitState[317:39] = memoryResponse;
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	wire [(32 - 1):0] address;
	assign address = memoryRequest_MW[230:199];
	reg [(32 - 1):0] readData_aligned;
	always @(*) begin
		case (address[1:0])
			2'd0: readData_aligned = memoryResponse[31:0];
			2'd1: readData_aligned = (memoryResponse[31:0] >> 5'd8);
			2'd2: readData_aligned = (memoryResponse[31:0] >> 5'd16);
			2'd3: readData_aligned = (memoryResponse[31:0] >> 5'd24);
		endcase
	end
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	reg memoryHasResponse;
	always @(*) begin
		memoryHasResponse = 1'b0;
		case (memoryResponse[278:247])
			INVALID_MEMORY_RESPONSE: memoryHasResponse = 1'b0;
			CACHE_HIT: memoryHasResponse = 1'b1;
			CACHE_MISS: memoryHasResponse = 1'b1;
			VRAM_IO: memoryHasResponse = 1'b1;
			REMOTE_IO: memoryHasResponse = 1'b1;
			PAGE_FAULT: memoryHasResponse = 1'b0;
			TRANSLATION_INVALIDATE: memoryHasResponse = 1'b0;
		endcase
	end
	always @(*) begin
		waitState[38:0] = memoryRequest_MW[198:160];
		case (memoryRequest_MW[646:615])
			LOAD_BYTE: begin
				waitState[32:1] = {{24 {readData_aligned[7]}}, readData_aligned[7:0]};
				waitState[0:0] = memoryHasResponse;
			end
			LOAD_HALF_WORD: begin
				waitState[32:1] = {{16 {readData_aligned[15]}}, readData_aligned[15:0]};
				waitState[0:0] = memoryHasResponse;
			end
			LOAD_WORD: begin
				waitState[32:1] = memoryResponse[31:0];
				waitState[0:0] = memoryHasResponse;
			end
			LOAD_BYTE_UNSIGNED: begin
				waitState[32:1] = {24'h0, readData_aligned[7:0]};
				;
				waitState[0:0] = memoryHasResponse;
			end
			LOAD_HALF_WORD_UNSIGNED: begin
				waitState[32:1] = {16'h0, readData_aligned[15:0]};
				waitState[0:0] = memoryHasResponse;
			end
			LOAD_RESERVED: begin
				waitState[32:1] = memoryResponse[31:0];
				waitState[0:0] = memoryHasResponse;
			end
			STORE_CONDITIONAL: begin
				if ((requestState[31:0] == STORE_SUCCESS)) begin
					waitState[32:1] = 32'b0;
					waitState[0:0] = memoryHasResponse;
				end
				else begin
					waitState[0:0] = 1'b1;
					waitState[32:1] = 32'b1;
				end
			end
		endcase
	end
	assign rd_MW_response = waitState[38:0];
	always @(*) begin
		controlFlow_fencing = 1'b0;
		controlFlow_ready = 1'b1;
		if (memoryRequest_MW[1058:1058]) begin
			controlFlow_fencing = decodeGenerateFence(memoryRequest_MW[678:231]);
			if (hasPendingException_IW(memoryRequest_MW[957:679])) begin
				controlFlow_fencing = 1'b1;
				controlFlow_ready = 1'b1;
			end
			else if ((memoryResponse[278:247] == PAGE_FAULT)) begin
				controlFlow_fencing = 1'b1;
				controlFlow_ready = 1'b1;
			end
			else if ((memoryRequest_MW[646:615] == NO_MEMORY_OPERATION)) begin
				controlFlow_ready = 1'b1;
			end
			else if (((memoryResponse[278:247] == INVALID_MEMORY_RESPONSE) && (memoryRequest_MW[31:0] != STORE_REJECTED))) begin
				controlFlow_ready = 1'b0;
			end
		end
	end
	assign controlFlow_idle = (! memoryRequest_MW[1058:1058]);
	assign controlFlow_decode = memoryRequest_MW[678:231];
	wire [1376:0] memoryWait_MW_END;
	assign memoryWait_MW_END[1376:1276] = memoryRequest_MW[1058:958];
	assign memoryWait_MW_END[1275:997] = memoryRequest_MW[957:679];
	assign memoryWait_MW_END[996:549] = memoryRequest_MW[678:231];
	assign memoryWait_MW_END[548:446] = memoryRequest_MW[230:128];
	assign memoryWait_MW_END[445:350] = memoryRequest_MW[127:32];
	assign memoryWait_MW_END[349:318] = requestState;
	assign memoryWait_MW_END[317:0] = waitState;
	Register #(.WIDTH(1377)) memoryWaitPipelineRegister(
		.d(memoryWait_MW_END),
		.q(memoryWait_WB),
		.enable((! controlFlow_stall)),
		.clock(clock),
		.clear((clear || controlFlow_flush))
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
module RegisterFile (
	rs1_ID_request,
	rs1_ID_response,
	rs2_ID_request,
	rs2_ID_response,
	remote_request,
	remote_response,
	rd_EX_response,
	rd_BR_response,
	rd_MR_response,
	rd_MW_response,
	rd_WB_response,
	clock,
	clear
);
	input wire [5:0] rs1_ID_request;
	output wire [38:0] rs1_ID_response;
	input wire [5:0] rs2_ID_request;
	output wire [38:0] rs2_ID_response;
	input wire [5:0] remote_request;
	output reg [38:0] remote_response;
	input wire [38:0] rd_EX_response;
	input wire [38:0] rd_BR_response;
	input wire [38:0] rd_MR_response;
	input wire [38:0] rd_MW_response;
	input wire [38:0] rd_WB_response;
	input wire clock;
	input wire clear;
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	reg [38:0] rs1_RF;
	reg [38:0] rs2_RF;
	SimpleRegisterFile #(
		.NUM_REGS(32),
		.WIDTH(32)
	) registerFile(
		.rs1(rs1_ID_request[5:1]),
		.rs1_data(rs1_RF[32:1]),
		.rs2(rs2_ID_request[5:1]),
		.rs2_data(rs2_RF[32:1]),
		.rs3(remote_request[5:1]),
		.rs3_data(remote_response[32:1]),
		.rd(rd_WB_response[38:34]),
		.rd_data(rd_WB_response[32:1]),
		.rd_write(rd_WB_response[0:0]),
		.clock(clock),
		.clear(clear)
	);
	always @(*) begin
		remote_response[0:0] = 1'b1;
		remote_response[38:33] = remote_request;
	end
	always @(*) begin
		rs1_RF[38:33] = rs1_ID_request;
		rs2_RF[38:33] = rs2_ID_request;
		rs1_RF[0:0] = rs1_ID_request[0:0];
		rs2_RF[0:0] = rs2_ID_request[0:0];
	end
	ForwardingRegister rs1Forwarding(
		.register_request(rs1_ID_request),
		.register_response(rs1_ID_response),
		.rd_RF(rs1_RF),
		.rd_EX(rd_EX_response),
		.rd_BR(rd_BR_response),
		.rd_MR(rd_MR_response),
		.rd_MW(rd_MW_response),
		.rd_WB(rd_WB_response),
		.clock(clock),
		.clear(clear)
	);
	ForwardingRegister rs2Forwarding(
		.register_request(rs2_ID_request),
		.register_response(rs2_ID_response),
		.rd_RF(rs2_RF),
		.rd_EX(rd_EX_response),
		.rd_BR(rd_BR_response),
		.rd_MR(rd_MR_response),
		.rd_MW(rd_MW_response),
		.rd_WB(rd_WB_response),
		.clock(clock),
		.clear(clear)
	);
endmodule
module ForwardingRegister (
	register_request,
	register_response,
	rd_EX,
	rd_BR,
	rd_MR,
	rd_MW,
	rd_WB,
	rd_RF,
	clock,
	clear
);
	input wire [5:0] register_request;
	output reg [38:0] register_response;
	input wire [38:0] rd_EX;
	input wire [38:0] rd_BR;
	input wire [38:0] rd_MR;
	input wire [38:0] rd_MW;
	input wire [38:0] rd_WB;
	input wire [38:0] rd_RF;
	input wire clock;
	input wire clear;
	function automatic registerMatch;
		input reg [5:0] a;
		input reg [5:0] b;
		registerMatch = ((a[0:0] && b[0:0]) && (a[5:1] == b[5:1]));
	endfunction
	// removed an assertion item
	// removed an assertion item
	always @(*) begin
		register_response = {register_request, 32'hBADF00D, 1'b0};
		if (((register_request[5:1] == 5'd0) || (! register_request[0:0]))) begin
			register_response[32:1] = 32'b0;
			register_response[0:0] = register_request[0:0];
		end
		else begin
			if (registerMatch(register_request, rd_EX[38:33])) begin
				if (rd_EX[0:0]) begin
					register_response[32:1] = rd_EX[32:1];
					register_response[0:0] = 1'b1;
				end
			end
			else if (registerMatch(register_request, rd_BR[38:33])) begin
				if (rd_BR[0:0]) begin
					register_response[32:1] = rd_BR[32:1];
					register_response[0:0] = 1'b1;
				end
			end
			else if (registerMatch(register_request, rd_MR[38:33])) begin
				if (rd_MR[0:0]) begin
					register_response[32:1] = rd_MR[32:1];
					register_response[0:0] = 1'b1;
				end
			end
			else if (registerMatch(register_request, rd_MW[38:33])) begin
				if (rd_MW[0:0]) begin
					register_response[32:1] = rd_MW[32:1];
					register_response[0:0] = 1'b1;
				end
			end
			else if (registerMatch(register_request, rd_WB[38:33])) begin
				register_response[32:1] = rd_WB[32:1];
				register_response[0:0] = 1'b1;
			end
			else begin
				register_response[32:1] = rd_RF[32:1];
				register_response[0:0] = 1'b1;
			end
		end
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
module RISCVDecoder (
	instruction,
	csr_name,
	csr_value,
	rs1_request,
	rs1_response,
	rs2_request,
	rs2_response,
	csrObserver_status,
	csrObserver_currentLevel,
	csrObserver_mideleg,
	decodeState,
	clock,
	clear
);
	localparam [1:0] USER_MODE = 2'd0;
	localparam [31:0] NO_CSR_OPERATION = 32'd0;
	localparam [31:0] NO_MEMORY_OPERATION = 32'd0;
	localparam [31:0] NO_WRITE_BACK = 32'd0;
	localparam [4:0] X0 = 5'd0;
	localparam [1:0] SUPERVISOR_MODE = 2'd1;
	localparam [1:0] MACHINE_MODE = 2'd3;
	localparam [2:0] FUNCT3_ADDI = 3'h0;
	localparam [2:0] FUNCT3_ADD_SUB = 3'h0;
	localparam [2:0] FUNCT3_BEQ = 3'h0;
	localparam [2:0] FUNCT3_LB = 3'h0;
	localparam [2:0] FUNCT3_PRIV = 3'h0;
	localparam [2:0] FUNCT3_SB = 3'h0;
	localparam [6:0] FUNCT7_INT = 7'h00;
	localparam [11:0] FUNCT12_ECALL = 12'h000;
	localparam [6:0] OP_LOAD = 7'h03;
	localparam [2:0] FUNCT3_SLLI = 3'h1;
	localparam [2:0] FUNCT3_SLL = 3'h1;
	localparam [2:0] FUNCT3_BNE = 3'h1;
	localparam [2:0] FUNCT3_LH = 3'h1;
	localparam [2:0] FUNCT3_CSRRW = 3'h1;
	localparam [2:0] FUNCT3_SH = 3'h1;
	localparam [11:0] FUNCT12_SRET = 12'h102;
	localparam [6:0] OP_IMM = 7'h13;
	localparam [6:0] OP_AUIPC = 7'h17;
	localparam [2:0] FUNCT3_SLTI = 3'h2;
	localparam [2:0] FUNCT3_SLT = 3'h2;
	localparam [2:0] FUNCT3_LW = 3'h2;
	localparam [2:0] FUNCT3_CSRRS = 3'h2;
	localparam [2:0] FUNCT3_SW = 3'h2;
	localparam [4:0] FUNCT5_LR = 5'h2;
	localparam [6:0] FUNCT7_ALT_INT = 7'h20;
	localparam [6:0] OP_STORE = 7'h23;
	localparam [6:0] OP_AMO = 7'h2F;
	localparam [2:0] FUNCT3_SLTIU = 3'h3;
	localparam [2:0] FUNCT3_SLTU = 3'h3;
	localparam [2:0] FUNCT3_CSRRC = 3'h3;
	localparam [4:0] FUNCT5_SC = 5'h3;
	localparam [11:0] FUNCT12_MRET = 12'h302;
	localparam [6:0] OP_OP = 7'h33;
	localparam [6:0] OP_LUI = 7'h37;
	localparam [2:0] FUNCT3_XORI = 3'h4;
	localparam [2:0] FUNCT3_XOR = 3'h4;
	localparam [2:0] FUNCT3_BLT = 3'h4;
	localparam [2:0] FUNCT3_LBU = 3'h4;
	localparam [2:0] FUNCT3_SRLI_SRAI = 3'h5;
	localparam [2:0] FUNCT3_SRL_SRA = 3'h5;
	localparam [2:0] FUNCT3_BGE = 3'h5;
	localparam [2:0] FUNCT3_LHU = 3'h5;
	localparam [2:0] FUNCT3_CSRRWI = 3'h5;
	localparam [2:0] FUNCT3_ORI = 3'h6;
	localparam [2:0] FUNCT3_OR = 3'h6;
	localparam [2:0] FUNCT3_BLTU = 3'h6;
	localparam [2:0] FUNCT3_CSRRSI = 3'h6;
	localparam [6:0] OP_BRANCH = 7'h63;
	localparam [6:0] OP_JALR = 7'h67;
	localparam [6:0] OP_JAL = 7'h6F;
	localparam [2:0] FUNCT3_ANDI = 3'h7;
	localparam [2:0] FUNCT3_AND = 3'h7;
	localparam [2:0] FUNCT3_BGEU = 3'h7;
	localparam [2:0] FUNCT3_CSRRCI = 3'h7;
	localparam [6:0] OP_SYSTEM = 7'h73;
	localparam [31:0] ALU_ADD = 32'd0;
	localparam [31:0] ALU_SELECT_ZERO = 32'd0;
	localparam [31:0] JUMP_SELECT_ZERO = 32'd0;
	localparam [31:0] NO_IMMEDIATE = 32'd0;
	localparam [31:0] ALU_SUBTRACT = 32'd1;
	localparam [31:0] ALU_SELECT_REGISTER = 32'd1;
	localparam [31:0] ILLEGAL_INSTRUCTION_FAULT = 32'd1;
	localparam [31:0] JUMP_SELECT_PC = 32'd1;
	localparam [31:0] CSR_WRITE = 32'd1;
	localparam [31:0] I_TYPE = 32'd1;
	localparam [31:0] LOAD_BYTE = 32'd1;
	localparam [31:0] WRITE_BACK_SELECT_ALU = 32'd1;
	localparam [31:0] ALU_NOT_EQUAL = 32'd10;
	localparam [31:0] STORE_CONDITIONAL = 32'd10;
	localparam [31:0] ALU_LESS_THAN_SIGNED = 32'd11;
	localparam [31:0] FLUSH_TRANSLATION = 32'd11;
	localparam [31:0] ALU_LESS_THAN_UNSIGNED = 32'd12;
	localparam [31:0] ALU_GREATER_THAN_OR_EQUAL_SIGNED = 32'd13;
	localparam [31:0] ALU_GREATER_THAN_OR_EQUAL_UNSIGNED = 32'd14;
	localparam [31:0] ALU_OR = 32'd2;
	localparam [31:0] ALU_SELECT_PC = 32'd2;
	localparam [31:0] NORMAL = 32'd2;
	localparam [31:0] JUMP_SELECT_RS1 = 32'd2;
	localparam [31:0] CSR_X0 = 32'd2;
	localparam [31:0] U_TYPE = 32'd2;
	localparam [31:0] LOAD_HALF_WORD = 32'd2;
	localparam [31:0] WRITE_BACK_SELECT_PC_PLUS_4 = 32'd2;
	localparam [31:0] ALU_XOR = 32'd3;
	localparam [31:0] ALU_SELECT_IMMEDIATE = 32'd3;
	localparam [31:0] JUMP = 32'd3;
	localparam [31:0] SB_TYPE = 32'd3;
	localparam [31:0] LOAD_WORD = 32'd3;
	localparam [31:0] WRITE_BACK_SELECT_MEMORY = 32'd3;
	localparam [31:0] ALU_AND = 32'd4;
	localparam [31:0] ALU_SELECT_CSR = 32'd4;
	localparam [31:0] BRANCH = 32'd4;
	localparam [31:0] UJ_TYPE = 32'd4;
	localparam [31:0] LOAD_BYTE_UNSIGNED = 32'd4;
	localparam [31:0] WRITE_BACK_SELECT_CSR = 32'd4;
	localparam [31:0] ALU_CLEAR_MASK = 32'd5;
	localparam [31:0] CSR = 32'd5;
	localparam [31:0] S_TYPE = 32'd5;
	localparam [31:0] LOAD_HALF_WORD_UNSIGNED = 32'd5;
	localparam [31:0] ALU_SHIFT_LEFT = 32'd6;
	localparam [31:0] FENCE = 32'd6;
	localparam [31:0] CSR_TYPE = 32'd6;
	localparam [31:0] STORE_BYTE = 32'd6;
	localparam [31:0] ALU_SHIFT_RIGHT_LOGICAL = 32'd7;
	localparam [31:0] PRIVILEGE_RAISE = 32'd7;
	localparam [31:0] STORE_HALF_WORD = 32'd7;
	localparam [31:0] ALU_SHIFT_RIGHT_ARITHMETIC = 32'd8;
	localparam [31:0] PRIVILEGE_LOWER_SRET = 32'd8;
	localparam [31:0] STORE_WORD = 32'd8;
	localparam [31:0] ALU_EQUAL = 32'd9;
	localparam [31:0] PRIVILEGE_LOWER_MRET = 32'd9;
	localparam [31:0] LOAD_RESERVED = 32'd9;
	input wire [(32 - 1):0] instruction;
	output wire [(12 - 1):0] csr_name;
	input wire [(32 - 1):0] csr_value;
	output wire [5:0] rs1_request;
	input wire [38:0] rs1_response;
	output wire [5:0] rs2_request;
	input wire [38:0] rs2_response;
	input wire [31:0] csrObserver_status;
	input wire [1:0] csrObserver_currentLevel;
	input wire [(32 - 1):0] csrObserver_mideleg;
	output reg [447:0] decodeState;
	input wire clock;
	input wire clear;
	function automatic hasSuffientPermissions;
		input reg [(12 - 1):0] name;
		input reg [1:0] privilegeLevel;
		case (privilegeLevel)
			MACHINE_MODE: hasSuffientPermissions = 1'b1;
			SUPERVISOR_MODE: hasSuffientPermissions = (name[9:8] <= SUPERVISOR_MODE);
			USER_MODE: hasSuffientPermissions = (name[9:8] == USER_MODE);
		endcase
	endfunction
	function automatic isDelegated;
		input reg [(32 - 1):0] deleg;
		input reg [(32 - 1):0] cause;
		isDelegated = ((deleg & (1 << (cause & 32'hf))) != 32'd0);
	endfunction
	function automatic isInterruptCause;
		input reg [(32 - 1):0] cause;
		isInterruptCause = cause[31];
	endfunction
	wire [63:0] immediate;
	assign decodeState[179:148] = csr_value;
	assign decodeState[147:109] = rs1_response;
	assign decodeState[108:70] = rs2_response;
	assign decodeState[63:0] = immediate;
	reg [5:0] rs1Request;
	reg [5:0] rs2Request;
	assign csr_name = decodeState[223:212];
	assign rs1_request = rs1Request;
	assign rs2_request = rs2Request;
	reg [31:0] immediateType;
	ImmediateDecoder immediateDecoder(
		.instruction(instruction),
		.immediateType(immediateType),
		.immediate(immediate)
	);
	wire [(7 - 1):0] opcode;
	wire [(7 - 1):0] funct7;
	wire [(3 - 1):0] rtype_funct3;
	wire [(3 - 1):0] rtype_m_funct3;
	wire [(3 - 1):0] itype_int_funct3;
	wire [(3 - 1):0] itype_load_funct3;
	wire [(12 - 1):0] itype_funct12;
	wire [(3 - 1):0] stype_funct3;
	wire [(5 - 1):0] amo_funct5;
	wire [(3 - 1):0] sbtype_funct3;
	wire [(3 - 1):0] itype_system_funct3;
	assign opcode = instruction[6:0];
	assign funct7 = instruction[31:25];
	assign rtype_funct3 = instruction[14:12];
	assign rtype_m_funct3 = instruction[14:12];
	assign itype_int_funct3 = instruction[14:12];
	assign itype_load_funct3 = instruction[14:12];
	assign itype_funct12 = instruction[31:20];
	assign stype_funct3 = instruction[14:12];
	assign amo_funct5 = instruction[31:27];
	assign sbtype_funct3 = instruction[14:12];
	assign itype_system_funct3 = instruction[14:12];
	wire [($clog2(32) - 1):0] rs1Index;
	wire [($clog2(32) - 1):0] rs2Index;
	wire [($clog2(32) - 1):0] rdIndex;
	assign rs1Index = instruction[19:15];
	assign rs2Index = instruction[24:20];
	assign rdIndex = instruction[11:7];
	assign decodeState[69:64] = {rdIndex, (decodeState[255:224] != NO_WRITE_BACK)};
	assign decodeState[223:212] = instruction[31:20];
	always @(*) begin
		decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
		decodeState[415:384] = NO_MEMORY_OPERATION;
		decodeState[383:352] = ALU_ADD;
		decodeState[351:320] = ALU_SELECT_ZERO;
		decodeState[319:288] = ALU_SELECT_ZERO;
		decodeState[287:256] = JUMP_SELECT_ZERO;
		decodeState[255:224] = NO_WRITE_BACK;
		decodeState[211:180] = NO_CSR_OPERATION;
		rs1Request = {rs1Index, 1'b0};
		rs2Request = {rs2Index, 1'b0};
		immediateType = NO_IMMEDIATE;
		case (opcode)
			OP_OP: begin
				decodeState[447:416] = NORMAL;
				decodeState[351:320] = ALU_SELECT_REGISTER;
				decodeState[319:288] = ALU_SELECT_REGISTER;
				rs1Request[0:0] = 1'b1;
				rs2Request[0:0] = 1'b1;
				decodeState[255:224] = WRITE_BACK_SELECT_ALU;
				case (funct7)
					FUNCT7_INT: begin
						case (rtype_funct3)
							FUNCT3_ADD_SUB: decodeState[383:352] = ALU_ADD;
							FUNCT3_SLL: decodeState[383:352] = ALU_SHIFT_LEFT;
							FUNCT3_SLT: decodeState[383:352] = ALU_LESS_THAN_SIGNED;
							FUNCT3_SLTU: decodeState[383:352] = ALU_LESS_THAN_UNSIGNED;
							FUNCT3_XOR: decodeState[383:352] = ALU_XOR;
							FUNCT3_SRL_SRA: decodeState[383:352] = ALU_SHIFT_RIGHT_LOGICAL;
							FUNCT3_OR: decodeState[383:352] = ALU_OR;
							FUNCT3_AND: decodeState[383:352] = ALU_AND;
							default: begin
							decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
							decodeState[255:224] = NO_WRITE_BACK;
						end
						endcase
					end
					FUNCT7_ALT_INT: begin
						case (rtype_funct3)
							FUNCT3_ADD_SUB: decodeState[383:352] = ALU_SUBTRACT;
							FUNCT3_SRL_SRA: decodeState[383:352] = ALU_SHIFT_RIGHT_ARITHMETIC;
							default: begin
							decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
							decodeState[255:224] = NO_WRITE_BACK;
						end
						endcase
					end
					default: begin
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
					decodeState[255:224] = NO_WRITE_BACK;
				end
				endcase
			end
			OP_IMM: begin
				decodeState[447:416] = NORMAL;
				decodeState[351:320] = ALU_SELECT_REGISTER;
				decodeState[319:288] = ALU_SELECT_IMMEDIATE;
				rs1Request[0:0] = 1'b1;
				decodeState[255:224] = WRITE_BACK_SELECT_ALU;
				immediateType = I_TYPE;
				case (itype_int_funct3)
					FUNCT3_ADDI: decodeState[383:352] = ALU_ADD;
					FUNCT3_SLTI: decodeState[383:352] = ALU_LESS_THAN_SIGNED;
					FUNCT3_SLTIU: decodeState[383:352] = ALU_LESS_THAN_UNSIGNED;
					FUNCT3_XORI: decodeState[383:352] = ALU_XOR;
					FUNCT3_ORI: decodeState[383:352] = ALU_OR;
					FUNCT3_ANDI: decodeState[383:352] = ALU_AND;
					FUNCT3_SLLI: decodeState[383:352] = ALU_SHIFT_LEFT;
					FUNCT3_SRLI_SRAI: begin
						case (funct7)
							FUNCT7_INT: decodeState[383:352] = ALU_SHIFT_RIGHT_LOGICAL;
							FUNCT7_ALT_INT: decodeState[383:352] = ALU_SHIFT_RIGHT_ARITHMETIC;
							default: begin
							decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
							decodeState[255:224] = NO_WRITE_BACK;
						end
						endcase
					end
					default: begin
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
					decodeState[255:224] = NO_WRITE_BACK;
				end
				endcase
			end
			OP_LOAD: begin
				decodeState[447:416] = NORMAL;
				decodeState[383:352] = ALU_ADD;
				decodeState[351:320] = ALU_SELECT_REGISTER;
				decodeState[319:288] = ALU_SELECT_IMMEDIATE;
				rs1Request[0:0] = 1'b1;
				decodeState[255:224] = WRITE_BACK_SELECT_MEMORY;
				immediateType = I_TYPE;
				case (itype_load_funct3)
					FUNCT3_LB: decodeState[415:384] = LOAD_BYTE;
					FUNCT3_LH: decodeState[415:384] = LOAD_HALF_WORD;
					FUNCT3_LW: decodeState[415:384] = LOAD_WORD;
					FUNCT3_LBU: decodeState[415:384] = LOAD_BYTE_UNSIGNED;
					FUNCT3_LHU: decodeState[415:384] = LOAD_HALF_WORD_UNSIGNED;
					default: begin
					decodeState[255:224] = NO_WRITE_BACK;
					decodeState[415:384] = NO_MEMORY_OPERATION;
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
				end
				endcase
			end
			OP_STORE: begin
				decodeState[447:416] = NORMAL;
				decodeState[383:352] = ALU_ADD;
				decodeState[351:320] = ALU_SELECT_REGISTER;
				decodeState[319:288] = ALU_SELECT_IMMEDIATE;
				rs1Request[0:0] = 1'b1;
				rs2Request[0:0] = 1'b1;
				immediateType = S_TYPE;
				case (stype_funct3)
					FUNCT3_SB: decodeState[415:384] = STORE_BYTE;
					FUNCT3_SH: decodeState[415:384] = STORE_HALF_WORD;
					FUNCT3_SW: decodeState[415:384] = STORE_WORD;
					default: begin
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
					decodeState[415:384] = NO_MEMORY_OPERATION;
				end
				endcase
			end
			OP_AMO: begin
				decodeState[447:416] = NORMAL;
				decodeState[383:352] = ALU_ADD;
				decodeState[351:320] = ALU_SELECT_REGISTER;
				decodeState[319:288] = ALU_SELECT_ZERO;
				rs1Request[0:0] = 1'b1;
				decodeState[255:224] = WRITE_BACK_SELECT_MEMORY;
				case (amo_funct5)
					FUNCT5_LR: begin
						decodeState[415:384] = LOAD_RESERVED;
					end
					FUNCT5_SC: begin
						rs2Request[0:0] = 1'b1;
						decodeState[415:384] = STORE_CONDITIONAL;
					end
					default: begin
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
					decodeState[415:384] = NO_MEMORY_OPERATION;
					decodeState[255:224] = NO_WRITE_BACK;
				end
				endcase
			end
			OP_LUI: begin
				decodeState[447:416] = NORMAL;
				immediateType = U_TYPE;
				decodeState[383:352] = ALU_ADD;
				decodeState[351:320] = ALU_SELECT_ZERO;
				decodeState[319:288] = ALU_SELECT_IMMEDIATE;
				decodeState[255:224] = WRITE_BACK_SELECT_ALU;
			end
			OP_AUIPC: begin
				decodeState[447:416] = NORMAL;
				immediateType = U_TYPE;
				decodeState[383:352] = ALU_ADD;
				decodeState[351:320] = ALU_SELECT_PC;
				decodeState[319:288] = ALU_SELECT_IMMEDIATE;
				decodeState[255:224] = WRITE_BACK_SELECT_ALU;
			end
			OP_JAL: begin
				decodeState[447:416] = JUMP;
				immediateType = UJ_TYPE;
				decodeState[287:256] = JUMP_SELECT_PC;
				decodeState[255:224] = WRITE_BACK_SELECT_PC_PLUS_4;
			end
			OP_JALR: begin
				decodeState[447:416] = JUMP;
				immediateType = I_TYPE;
				decodeState[287:256] = JUMP_SELECT_RS1;
				decodeState[255:224] = WRITE_BACK_SELECT_PC_PLUS_4;
				rs1Request[0:0] = 1'b1;
			end
			OP_BRANCH: begin
				decodeState[447:416] = BRANCH;
				decodeState[351:320] = ALU_SELECT_REGISTER;
				decodeState[319:288] = ALU_SELECT_REGISTER;
				immediateType = SB_TYPE;
				rs1Request[0:0] = 1'b1;
				rs2Request[0:0] = 1'b1;
				decodeState[287:256] = JUMP_SELECT_PC;
				case (sbtype_funct3)
					FUNCT3_BEQ: decodeState[383:352] = ALU_EQUAL;
					FUNCT3_BNE: decodeState[383:352] = ALU_NOT_EQUAL;
					FUNCT3_BLT: decodeState[383:352] = ALU_LESS_THAN_SIGNED;
					FUNCT3_BGE: decodeState[383:352] = ALU_GREATER_THAN_OR_EQUAL_SIGNED;
					FUNCT3_BLTU: decodeState[383:352] = ALU_LESS_THAN_UNSIGNED;
					FUNCT3_BGEU: decodeState[383:352] = ALU_GREATER_THAN_OR_EQUAL_UNSIGNED;
					default: begin
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
				end
				endcase
			end
			OP_SYSTEM: begin
				case (itype_system_funct3)
					FUNCT3_PRIV: begin
						casez (itype_funct12)
							FUNCT12_ECALL: begin
								rs1Request[5:1] = 5'ha;
								rs1Request[0:0] = 1'b1;
								decodeState[447:416] = PRIVILEGE_RAISE;
							end
							FUNCT12_MRET: begin
								if ((csrObserver_currentLevel == MACHINE_MODE)) begin
									decodeState[447:416] = PRIVILEGE_LOWER_MRET;
								end
								else begin
									decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
								end
							end
							FUNCT12_SRET: begin
								if (((csrObserver_currentLevel == MACHINE_MODE) || (csrObserver_currentLevel == SUPERVISOR_MODE))) begin
									decodeState[447:416] = PRIVILEGE_LOWER_SRET;
								end
								else begin
									decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
								end
							end
							12'b0001001?????: begin
								rs1Request[0:0] = 1'b1;
								rs2Request[0:0] = 1'b1;
								decodeState[415:384] = FLUSH_TRANSLATION;
								decodeState[383:352] = ALU_ADD;
								decodeState[351:320] = ALU_SELECT_REGISTER;
								decodeState[319:288] = ALU_SELECT_ZERO;
								case (csrObserver_currentLevel)
									MACHINE_MODE: decodeState[447:416] = FENCE;
									SUPERVISOR_MODE: decodeState[447:416] = FENCE;
									USER_MODE: begin
										decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
										decodeState[415:384] = NO_MEMORY_OPERATION;
									end
								endcase
							end
							default: begin
							decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
							decodeState[415:384] = NO_MEMORY_OPERATION;
						end
						endcase
					end
					FUNCT3_CSRRW: begin
						decodeState[447:416] = CSR;
						decodeState[211:180] = CSR_WRITE;
						rs1Request[0:0] = 1'b1;
						decodeState[351:320] = ALU_SELECT_REGISTER;
						decodeState[319:288] = ALU_SELECT_ZERO;
						decodeState[383:352] = ALU_ADD;
						decodeState[255:224] = WRITE_BACK_SELECT_CSR;
					end
					FUNCT3_CSRRS: begin
						decodeState[447:416] = CSR;
						decodeState[211:180] = ((rs1Request[5:1] == X0) ? CSR_X0 : CSR_WRITE);
						rs1Request[0:0] = 1'b1;
						decodeState[351:320] = ALU_SELECT_REGISTER;
						decodeState[319:288] = ALU_SELECT_CSR;
						decodeState[383:352] = ALU_OR;
						decodeState[255:224] = WRITE_BACK_SELECT_CSR;
					end
					FUNCT3_CSRRC: begin
						decodeState[447:416] = CSR;
						decodeState[211:180] = ((rs1Request[5:1] == X0) ? CSR_X0 : CSR_WRITE);
						rs1Request[0:0] = 1'b1;
						decodeState[351:320] = ALU_SELECT_REGISTER;
						decodeState[319:288] = ALU_SELECT_CSR;
						decodeState[383:352] = ALU_CLEAR_MASK;
						decodeState[255:224] = WRITE_BACK_SELECT_CSR;
					end
					FUNCT3_CSRRWI: begin
						decodeState[447:416] = CSR;
						decodeState[211:180] = CSR_WRITE;
						immediateType = CSR_TYPE;
						decodeState[351:320] = ALU_SELECT_IMMEDIATE;
						decodeState[319:288] = ALU_SELECT_ZERO;
						decodeState[383:352] = ALU_ADD;
						decodeState[255:224] = WRITE_BACK_SELECT_CSR;
					end
					FUNCT3_CSRRSI: begin
						decodeState[447:416] = CSR;
						decodeState[211:180] = ((rs1Request[5:1] == X0) ? CSR_X0 : CSR_WRITE);
						immediateType = CSR_TYPE;
						decodeState[351:320] = ALU_SELECT_IMMEDIATE;
						decodeState[319:288] = ALU_SELECT_CSR;
						decodeState[383:352] = ALU_OR;
						decodeState[255:224] = WRITE_BACK_SELECT_CSR;
					end
					FUNCT3_CSRRCI: begin
						decodeState[447:416] = CSR;
						decodeState[211:180] = ((rs1Request[5:1] == X0) ? CSR_X0 : CSR_WRITE);
						immediateType = CSR_TYPE;
						decodeState[351:320] = ALU_SELECT_IMMEDIATE;
						decodeState[319:288] = ALU_SELECT_CSR;
						decodeState[383:352] = ALU_CLEAR_MASK;
						decodeState[255:224] = WRITE_BACK_SELECT_CSR;
					end
					default: begin
					decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
					decodeState[255:224] = NO_WRITE_BACK;
					decodeState[211:180] = NO_CSR_OPERATION;
					decodeState[415:384] = NO_MEMORY_OPERATION;
				end
				endcase
			end
			default: begin
			decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
		end
		endcase
		if (((decodeState[447:416] == CSR) && (! hasSuffientPermissions(decodeState[223:212], csrObserver_currentLevel)))) begin
			decodeState[447:416] = ILLEGAL_INSTRUCTION_FAULT;
			decodeState[255:224] = NO_WRITE_BACK;
			decodeState[211:180] = NO_CSR_OPERATION;
		end
		if ((decodeState[447:416] == ILLEGAL_INSTRUCTION_FAULT)) begin
			decodeState[415:384] = NO_MEMORY_OPERATION;
			decodeState[255:224] = NO_WRITE_BACK;
			decodeState[211:180] = NO_CSR_OPERATION;
		end
	end
	// removed an assertion item
	// removed an assertion item
endmodule
module ImmediateDecoder (
	instruction,
	immediateType,
	immediate
);
	localparam [31:0] NO_IMMEDIATE = 32'd0;
	localparam [31:0] I_TYPE = 32'd1;
	localparam [31:0] U_TYPE = 32'd2;
	localparam [31:0] SB_TYPE = 32'd3;
	localparam [31:0] UJ_TYPE = 32'd4;
	localparam [31:0] S_TYPE = 32'd5;
	localparam [31:0] CSR_TYPE = 32'd6;
	input wire [(32 - 1):0] instruction;
	input wire [31:0] immediateType;
	output wire [63:0] immediate;
	reg [(32 - 1):0] value;
	assign immediate = {immediateType, value};
	always @(*) begin
		value = 32'hBADF00D;
		case (immediateType)
			NO_IMMEDIATE: value = 32'd0;
			I_TYPE: value = {{21 {instruction[31]}}, instruction[30:20]};
			U_TYPE: value = {instruction[31:12], 12'd0};
			SB_TYPE: value = {{20 {instruction[31]}}, instruction[7], instruction[30:25], instruction[11:8], 1'b0};
			UJ_TYPE: value = {{12 {instruction[31]}}, instruction[19:12], instruction[20], instruction[30:21], 1'b0};
			S_TYPE: value = {{21 {instruction[31]}}, instruction[30:25], instruction[11:7]};
			CSR_TYPE: value = {27'd0, instruction[19:15]};
		endcase
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
module SimpleRegisterFile (
	rs1,
	rs2,
	rs3,
	rd,
	rd_write,
	rd_data,
	rs1_data,
	rs2_data,
	rs3_data,
	clock,
	clear
);
	parameter NUM_REGS = 32;
	parameter WIDTH = 32;
	input wire [($clog2(NUM_REGS) - 1):0] rs1;
	input wire [($clog2(NUM_REGS) - 1):0] rs2;
	input wire [($clog2(NUM_REGS) - 1):0] rs3;
	input wire [($clog2(NUM_REGS) - 1):0] rd;
	input wire rd_write;
	input wire [(WIDTH - 1):0] rd_data;
	output reg [(WIDTH - 1):0] rs1_data;
	output reg [(WIDTH - 1):0] rs2_data;
	output reg [(WIDTH - 1):0] rs3_data;
	input wire clock;
	input wire clear;
	reg [(NUM_REGS * WIDTH) - 1:0] registers;
	always @(posedge clock) begin
		if (clear) begin
			registers <= {(NUM_REGS * WIDTH){1'b0}};
		end
		else begin
			if ((rd_write && (rd != 'd0))) begin
				registers[(rd * WIDTH)+:WIDTH] <= rd_data;
			end
		end
	end
	always @(*) begin
		rs1_data = registers[(rs1 * WIDTH)+:WIDTH];
		rs2_data = registers[(rs2 * WIDTH)+:WIDTH];
		rs3_data = registers[(rs3 * WIDTH)+:WIDTH];
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: CounterState_t
// removed typedef: Prediction_t
// removed typedef: InstructionFetchState_t
// removed typedef: InstructionWaitState_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: InstructionType_t
// removed typedef: MemoryOperation_t
// removed typedef: CSROperation_t
// removed typedef: ArithmeticOperation_t
// removed typedef: ArithmeticOperandSelect_t
// removed typedef: JumpBaseSelect_t
// removed typedef: WriteBackSelect_t
// removed typedef: ImmediateType_t
// removed typedef: Immediate_t
// removed typedef: InstructionDecodeState_t
// removed typedef: ExecuteState_t
// removed typedef: Direction_t
// removed typedef: PredictionResult_t
// removed typedef: BranchResolveState_t
// removed typedef: StoreConditionalResult_t
// removed typedef: MemoryRequestState_t
// removed typedef: MemoryWaitState_t
// removed typedef: InstructionFetchExport_t
// removed typedef: InstructionWaitExport_t
// removed typedef: InstructionDecodeExport_t
// removed typedef: ExecuteExport_t
// removed typedef: BranchResolveExport_t
// removed typedef: MemoryRequestExport_t
// removed typedef: MemoryWaitExport_t
module WriteBackStage (
	memoryWait_WB,
	rd_WB_response,
	csr_name,
	csr_value,
	csr_writeEnable,
	lastPCRetired,
	clock,
	clear
);
	localparam [31:0] INVALID_MEMORY_RESPONSE = 32'd0;
	localparam [31:0] NO_MEMORY_OPERATION = 32'd0;
	localparam [31:0] NO_WRITE_BACK = 32'd0;
	localparam [31:0] CSR_WRITE = 32'd1;
	localparam [31:0] WRITE_BACK_SELECT_ALU = 32'd1;
	localparam [31:0] STORE_REJECTED = 32'd2;
	localparam [31:0] WRITE_BACK_SELECT_PC_PLUS_4 = 32'd2;
	localparam [31:0] WRITE_BACK_SELECT_MEMORY = 32'd3;
	localparam [31:0] PAGE_FAULT = 32'd5;
	localparam [31:0] TRANSLATION_INVALIDATE = 32'd6;
	input wire [1376:0] memoryWait_WB;
	output reg [38:0] rd_WB_response;
	output wire [(12 - 1):0] csr_name;
	output wire [(32 - 1):0] csr_value;
	output wire csr_writeEnable;
	output wire [(32 - 1):0] lastPCRetired;
	input wire clock;
	input wire clear;
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	always @(*) begin
		rd_WB_response = memoryWait_WB[38:0];
		if ((memoryWait_WB[317:286] == PAGE_FAULT)) begin
			rd_WB_response = {6'd0, 32'd0, 1'd0};
		end
	end
	assign csr_name = memoryWait_WB[772:761];
	assign csr_value = memoryWait_WB[548:517];
	assign csr_writeEnable = (memoryWait_WB[760:729] == CSR_WRITE);
	Register #(.WIDTH(32)) lastPC(
		.q(lastPCRetired),
		.d(memoryWait_WB[1375:1344]),
		.enable(memoryWait_WB[1376:1376]),
		.clock(clock),
		.clear(clear)
	);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
module KeyboardController (
	keyboard_clock,
	keyboard_clear,
	keyboard_readch,
	keyboard_scancode,
	keyboard_ascii_ready,
	interruptController_tval,
	interruptController_interruptPending,
	interruptController_interruptAccepted
);
	input wire keyboard_clock;
	input wire keyboard_clear;
	input wire [7:0] keyboard_readch;
	input wire [7:0] keyboard_scancode;
	input wire keyboard_ascii_ready;
	output wire [(32 - 1):0] interruptController_tval;
	output wire interruptController_interruptPending;
	input wire interruptController_interruptAccepted;
	wire [7:0] keyboardData;
	wire [7:0] interruptData;
	wire isFull;
	wire isEmpty;
	FIFO #(
		.SIZE(64),
		.WIDTH(8)
	) keyboardQueue(
		.producerData(keyboardData),
		.enqueueValid((keyboard_ascii_ready & (~ isFull))),
		.consumerData(interruptData),
		.dequeueValid(interruptController_interruptAccepted),
		.isEmpty(isEmpty),
		.isFull(isFull),
		.clock(keyboard_clock),
		.clear(keyboard_clear)
	);
	assign keyboardData = keyboard_readch;
	wire dataPending;
	Register #(.WIDTH(1)) pendingRegister(
		.d((! isEmpty)),
		.q(dataPending),
		.clock(keyboard_clock),
		.clear(keyboard_clear),
		.enable(1'b1)
	);
	assign interruptController_interruptPending = dataPending;
	assign interruptController_tval = {24'b0, interruptData};
endmodule
module ps2 (
	clk,
	rst,
	rx_ascii_read,
	PS2_K_CLK_IO,
	PS2_K_DATA_IO,
	PS2_M_CLK_IO,
	PS2_M_DATA_IO,
	ascii_code,
	ascii_data_ready_out,
	rx_translated_scan_code_out
);
	input wire clk;
	input wire rst;
	input wire rx_ascii_read;
	inout wire PS2_K_CLK_IO;
	inout wire PS2_K_DATA_IO;
	inout wire PS2_M_CLK_IO;
	inout wire PS2_M_DATA_IO;
	output wire ascii_data_ready_out;
	output wire [7:0] rx_translated_scan_code_out;
	output reg [6:0] ascii_code;
	reg [7:0] kcode;
	wire ps2_k_clk_en_o_;
	wire ps2_k_data_en_o_;
	wire ps2_k_clk_i;
	wire ps2_k_data_i;
	wire rx_released;
	wire [7:0] rx_scan_code;
	wire rx_scan_read;
	wire rx_scan_ready;
	reg [7:0] tx_data;
	reg tx_write;
	wire tx_write_ack_o;
	wire tx_error_no_keyboard_ack;
	reg translate;
	reg shift;
	reg ctrl;
	wire [15:0] divide_reg = 13000;
	reg ascii_data_ready;
	reg data_ready_dly;
	wire [7:0] rx_translated_scan_code;
	wire [7:0] rx_ascii_code;
	wire rx_translated_data_ready;
	wire rx_ascii_data_ready;
	assign rx_ascii_code = ascii_code;
	assign rx_ascii_data_ready = ascii_data_ready;
	assign ascii_data_ready_out = ascii_data_ready;
	assign rx_translated_scan_code_out = rx_translated_scan_code;
	function [6:0] ASCII_code;
		input reg [6:0] scancode;
		input reg shift;
		begin
			case (scancode)
				7'H0: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H1: ASCII_code = (shift ? 7'H1B : 7'H1B);
				7'H2: ASCII_code = (shift ? 7'H21 : 7'H31);
				7'H3: ASCII_code = (shift ? 7'H22 : 7'H32);
				7'H4: ASCII_code = (shift ? 7'H23 : 7'H33);
				7'H5: ASCII_code = (shift ? 7'H24 : 7'H34);
				7'H6: ASCII_code = (shift ? 7'H25 : 7'H35);
				7'H7: ASCII_code = (shift ? 7'H5E : 7'H36);
				7'H8: ASCII_code = (shift ? 7'H26 : 7'H37);
				7'H9: ASCII_code = (shift ? 7'H2A : 7'H38);
				7'HA: ASCII_code = (shift ? 7'H28 : 7'H39);
				7'HB: ASCII_code = (shift ? 7'H29 : 7'H30);
				7'HC: ASCII_code = (shift ? 7'H5F : 7'H2D);
				7'HD: ASCII_code = (shift ? 7'H2B : 7'H3D);
				7'HE: ASCII_code = (shift ? 7'H8 : 7'H8);
				7'HF: ASCII_code = (shift ? 7'H9 : 7'H9);
				7'H10: ASCII_code = (shift ? 7'H51 : 7'H71);
				7'H11: ASCII_code = (shift ? 7'H57 : 7'H77);
				7'H12: ASCII_code = (shift ? 7'H45 : 7'H65);
				7'H13: ASCII_code = (shift ? 7'H52 : 7'H72);
				7'H14: ASCII_code = (shift ? 7'H54 : 7'H74);
				7'H15: ASCII_code = (shift ? 7'H59 : 7'H79);
				7'H16: ASCII_code = (shift ? 7'H55 : 7'H75);
				7'H17: ASCII_code = (shift ? 7'H49 : 7'H69);
				7'H18: ASCII_code = (shift ? 7'H4F : 7'H6F);
				7'H19: ASCII_code = (shift ? 7'H50 : 7'H70);
				7'H1A: ASCII_code = (shift ? 7'H7B : 7'H5B);
				7'H1B: ASCII_code = (shift ? 7'H7D : 7'H5D);
				7'H1C: ASCII_code = (shift ? 7'HA : 7'HA);
				7'H1D: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H1E: ASCII_code = (shift ? 7'H41 : 7'H61);
				7'H1F: ASCII_code = (shift ? 7'H53 : 7'H73);
				7'H20: ASCII_code = (shift ? 7'H44 : 7'H64);
				7'H21: ASCII_code = (shift ? 7'H46 : 7'H66);
				7'H22: ASCII_code = (shift ? 7'H47 : 7'H67);
				7'H23: ASCII_code = (shift ? 7'H48 : 7'H68);
				7'H24: ASCII_code = (shift ? 7'H4A : 7'H6A);
				7'H25: ASCII_code = (shift ? 7'H4B : 7'H6B);
				7'H26: ASCII_code = (shift ? 7'H4C : 7'H6C);
				7'H27: ASCII_code = (shift ? 7'H3A : 7'H3B);
				7'H28: ASCII_code = (shift ? 7'H40 : 7'H27);
				7'H29: ASCII_code = (shift ? 7'H60 : 7'H60);
				7'H2A: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H2B: ASCII_code = (shift ? 7'H7C : 7'H5C);
				7'H2C: ASCII_code = (shift ? 7'H5A : 7'H7A);
				7'H2D: ASCII_code = (shift ? 7'H58 : 7'H78);
				7'H2E: ASCII_code = (shift ? 7'H43 : 7'H63);
				7'H2F: ASCII_code = (shift ? 7'H56 : 7'H76);
				7'H30: ASCII_code = (shift ? 7'H42 : 7'H62);
				7'H31: ASCII_code = (shift ? 7'H4E : 7'H6E);
				7'H32: ASCII_code = (shift ? 7'H4D : 7'H6D);
				7'H33: ASCII_code = (shift ? 7'H3C : 7'H2C);
				7'H34: ASCII_code = (shift ? 7'H3E : 7'H2E);
				7'H35: ASCII_code = (shift ? 7'H3F : 7'H2F);
				7'H36: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H37: ASCII_code = (shift ? 7'H2A : 7'H2A);
				7'H38: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H39: ASCII_code = (shift ? 7'H20 : 7'H20);
				7'H3A: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H3B: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H3C: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H3D: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H3E: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H3F: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H40: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H41: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H42: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H43: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H44: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H45: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H46: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H47: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H48: ASCII_code = (shift ? 7'H57 : 7'H57);
				7'H49: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H4A: ASCII_code = (shift ? 7'H5F : 7'H2D);
				7'H4B: ASCII_code = (shift ? 7'H41 : 7'H41);
				7'H4C: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H4D: ASCII_code = (shift ? 7'H44 : 7'H44);
				7'H4E: ASCII_code = (shift ? 7'H2B : 7'H2B);
				7'H4F: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H50: ASCII_code = (shift ? 7'H53 : 7'H53);
				7'H51: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H52: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H53: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H54: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H55: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H56: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H57: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H58: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H59: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H5A: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H5B: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H5C: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H5D: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H5E: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H5F: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H60: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H61: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H62: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H63: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H64: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H65: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H66: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H67: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H68: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H69: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H6A: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H6B: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H6C: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H6D: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H6E: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H6F: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H70: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H71: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H72: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H73: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H74: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H75: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H76: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H77: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H78: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H79: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H7A: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H7B: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H7C: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H7D: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H7E: ASCII_code = (shift ? 7'H0 : 7'H0);
				7'H7F: ASCII_code = (shift ? 7'H0 : 7'H0);
			endcase
		end
	endfunction
	always @(posedge clk) if (rst) begin
		translate = 1;
		tx_write = 0;
		tx_data = 0;
		kcode = 0;
		shift = 0;
		ctrl = 0;
		ascii_data_ready = 0;
		ascii_code = 0;
		data_ready_dly <= 0;
	end
	else begin
		data_ready_dly <= rx_translated_data_ready;
		if (rx_ascii_read) ascii_data_ready = 0;
		if ((rx_translated_data_ready & (~ data_ready_dly))) begin
			kcode = 0;
			case (rx_translated_scan_code[6:0])
				7'H2A, 7'H36: shift = (~ rx_translated_scan_code[7]);
				7'H1D: ctrl = (~ rx_translated_scan_code[7]);
				default: kcode = ASCII_code(rx_translated_scan_code, (shift | ctrl));
			endcase
			if ((kcode && (! rx_translated_scan_code[7]))) begin
				if (((ctrl && (kcode >= "A")) && (kcode <= "]"))) begin
					kcode = (kcode & (~ 7'H40));
				end
				ascii_code = kcode;
				ascii_data_ready = 1;
			end
		end
	end
	IOBUF #(
		.DRIVE(12),
		.IOSTANDARD("DEFAULT"),
		.SLEW("SLOW")
	) IOBUF_k_clk(
		.O(ps2_k_clk_i),
		.IO(PS2_K_CLK_IO),
		.I(ps2_k_clk_en_o_),
		.T(ps2_k_clk_en_o_)
	);
	IOBUF #(
		.DRIVE(12),
		.IOSTANDARD("DEFAULT"),
		.SLEW("SLOW")
	) IOBUF_k_data(
		.O(ps2_k_data_i),
		.IO(PS2_K_DATA_IO),
		.I(ps2_k_data_en_o_),
		.T(ps2_k_data_en_o_)
	);
	ps2_keyboard key1(
		.clock_i(clk),
		.reset_i(rst),
		.ps2_clk_en_o_(ps2_k_clk_en_o_),
		.ps2_data_en_o_(ps2_k_data_en_o_),
		.ps2_clk_i(ps2_k_clk_i),
		.ps2_data_i(ps2_k_data_i),
		.rx_released(rx_released),
		.rx_scan_code(rx_scan_code),
		.rx_data_ready(rx_scan_ready),
		.rx_read(rx_scan_read),
		.tx_data(tx_data),
		.tx_write(tx_write),
		.tx_write_ack_o(tx_write_ack_o),
		.tx_error_no_keyboard_ack(tx_error_no_keyboard_ack),
		.translate(translate),
		.divide_reg_i(divide_reg)
	);
	ps2_translation_table i_ps2_translation_table(
		.reset_i(rst),
		.clock_i(clk),
		.translate_i(translate),
		.code_i(rx_scan_code),
		.code_o(rx_translated_scan_code),
		.address_i(8'h00),
		.data_i(8'h00),
		.we_i(1'b0),
		.re_i(1'b0),
		.data_o(),
		.rx_data_ready_i(rx_scan_ready),
		.rx_translated_data_ready_o(rx_translated_data_ready),
		.rx_read_i(rx_translated_data_ready),
		.rx_read_o(rx_scan_read),
		.rx_released_i(rx_released)
	);
	wire ps2_m_clk_en_o_;
	wire ps2_m_data_en_o_;
	wire ps2_m_clk_i;
	wire ps2_m_data_i;
	IOBUF #(
		.DRIVE(12),
		.IOSTANDARD("DEFAULT"),
		.SLEW("SLOW")
	) IOBUF_m_clk(
		.O(ps2_m_clk_i),
		.IO(PS2_M_CLK_IO),
		.I(ps2_m_clk_en_o_),
		.T(ps2_m_clk_en_o_)
	);
	IOBUF #(
		.DRIVE(12),
		.IOSTANDARD("DEFAULT"),
		.SLEW("SLOW")
	) IOBUF_m_data(
		.O(ps2_m_data_i),
		.IO(PS2_M_DATA_IO),
		.I(ps2_m_data_en_o_),
		.T(ps2_m_data_en_o_)
	);
endmodule
module PS2Driver (
	ps2_clk,
	ps2_data,
	keyboard_clock,
	keyboard_clear,
	keyboard_readch,
	keyboard_scancode,
	keyboard_ascii_ready
);
	inout wire ps2_clk;
	inout wire ps2_data;
	input wire keyboard_clock;
	input wire keyboard_clear;
	output wire [7:0] keyboard_readch;
	output wire [7:0] keyboard_scancode;
	output wire keyboard_ascii_ready;
	assign keyboard_readch[7] = 1'b0;
	ps2 keyb_mouse(
		.clk(keyboard_clock),
		.rst(keyboard_clear),
		.PS2_K_CLK_IO(ps2_clk),
		.PS2_K_DATA_IO(ps2_data),
		.PS2_M_CLK_IO(),
		.PS2_M_DATA_IO(),
		.ascii_code(keyboard_readch[6:0]),
		.ascii_data_ready_out(keyboard_ascii_ready),
		.rx_translated_scan_code_out(keyboard_scancode),
		.rx_ascii_read(keyboard_ascii_ready)
	);
endmodule
module ps2_keyboard (
	clock_i,
	reset_i,
	ps2_clk_en_o_,
	ps2_data_en_o_,
	ps2_clk_i,
	ps2_data_i,
	rx_released,
	rx_scan_code,
	rx_data_ready,
	rx_read,
	tx_data,
	tx_write,
	tx_write_ack_o,
	tx_error_no_keyboard_ack,
	translate,
	divide_reg_i
);
	parameter TIMER_60USEC_VALUE_PP = 3000;
	parameter TIMER_60USEC_BITS_PP = $clog2(TIMER_60USEC_VALUE_PP);
	parameter TIMER_5USEC_VALUE_PP = 250;
	parameter TIMER_5USEC_BITS_PP = $clog2(TIMER_5USEC_VALUE_PP);
	parameter m1_rx_clk_h = 1;
	parameter m1_rx_clk_l = 0;
	parameter m1_rx_falling_edge_marker = 13;
	parameter m1_rx_rising_edge_marker = 14;
	parameter m1_tx_force_clk_l = 3;
	parameter m1_tx_first_wait_clk_h = 10;
	parameter m1_tx_first_wait_clk_l = 11;
	parameter m1_tx_reset_timer = 12;
	parameter m1_tx_wait_clk_h = 2;
	parameter m1_tx_clk_h = 4;
	parameter m1_tx_clk_l = 5;
	parameter m1_tx_wait_keyboard_ack = 6;
	parameter m1_tx_done_recovery = 7;
	parameter m1_tx_error_no_keyboard_ack = 8;
	parameter m1_tx_rising_edge_marker = 9;
	parameter m2_rx_data_ready = 1;
	parameter m2_rx_data_ready_ack = 0;
	input wire clock_i;
	input wire reset_i;
	output wire ps2_clk_en_o_;
	output wire ps2_data_en_o_;
	input wire ps2_clk_i;
	input wire ps2_data_i;
	output reg rx_released;
	output reg [7:0] rx_scan_code;
	output reg rx_data_ready;
	input wire rx_read;
	input wire [7:0] tx_data;
	input wire tx_write;
	output wire tx_write_ack_o;
	output reg tx_error_no_keyboard_ack;
	input wire translate;
	input wire [15:0] divide_reg_i;
	wire timer_60usec_done;
	wire timer_5usec_done;
	wire released;
	wire rx_output_event;
	wire rx_output_strobe;
	wire tx_parity_bit;
	wire rx_shifting_done;
	wire tx_shifting_done;
	reg [(11 - 1):0] q;
	reg [3:0] m1_state;
	reg [3:0] m1_next_state;
	reg m2_state;
	reg m2_next_state;
	reg [3:0] bit_count;
	reg enable_timer_60usec;
	reg enable_timer_5usec;
	reg [(TIMER_60USEC_BITS_PP - 1):0] timer_60usec_count;
	reg [(TIMER_5USEC_BITS_PP - 1):0] timer_5usec_count;
	reg hold_released;
	reg ps2_clk_s;
	reg ps2_data_s;
	reg ps2_clk_hi_z;
	reg ps2_data_hi_z;
	reg ps2_clk_ms;
	reg ps2_data_ms;
	reg [15:0] timer_5usec;
	reg timer_done;
	assign ps2_clk_en_o_ = ps2_clk_hi_z;
	assign ps2_data_en_o_ = ps2_data_hi_z;
	always @(posedge clock_i) begin
		ps2_clk_ms <= #(1) ps2_clk_i;
		ps2_data_ms <= #(1) ps2_data_i;
		ps2_clk_s <= #(1) ps2_clk_ms;
		ps2_data_s <= #(1) ps2_data_ms;
	end
	always @(posedge clock_i) begin : m1_state_register
		if (reset_i) m1_state <= #(1) m1_rx_clk_h;
		else m1_state <= #(1) m1_next_state;
	end
	always @(m1_state or q or tx_shifting_done or tx_write or ps2_clk_s or ps2_data_s or timer_60usec_done or timer_5usec_done) begin : m1_state_logic
		ps2_clk_hi_z <= #(1) 1;
		ps2_data_hi_z <= #(1) 1;
		tx_error_no_keyboard_ack <= #(1) 1'b0;
		enable_timer_60usec <= #(1) 0;
		enable_timer_5usec <= #(1) 0;
		case (m1_state)
			m1_rx_clk_h: begin
				enable_timer_60usec <= #(1) 1;
				if (tx_write) m1_next_state <= #(1) m1_tx_reset_timer;
				else if ((~ ps2_clk_s)) m1_next_state <= #(1) m1_rx_falling_edge_marker;
				else m1_next_state <= #(1) m1_rx_clk_h;
			end
			m1_rx_falling_edge_marker: begin
				enable_timer_60usec <= #(1) 0;
				m1_next_state <= #(1) m1_rx_clk_l;
			end
			m1_rx_rising_edge_marker: begin
				enable_timer_60usec <= #(1) 0;
				m1_next_state <= #(1) m1_rx_clk_h;
			end
			m1_rx_clk_l: begin
				enable_timer_60usec <= #(1) 1;
				if (tx_write) m1_next_state <= #(1) m1_tx_reset_timer;
				else if (ps2_clk_s) m1_next_state <= #(1) m1_rx_rising_edge_marker;
				else m1_next_state <= #(1) m1_rx_clk_l;
			end
			m1_tx_reset_timer: begin
				enable_timer_60usec <= #(1) 0;
				m1_next_state <= #(1) m1_tx_force_clk_l;
			end
			m1_tx_force_clk_l: begin
				enable_timer_60usec <= #(1) 1;
				ps2_clk_hi_z <= #(1) 0;
				if (timer_60usec_done) m1_next_state <= #(1) m1_tx_first_wait_clk_h;
				else m1_next_state <= #(1) m1_tx_force_clk_l;
			end
			m1_tx_first_wait_clk_h: begin
				enable_timer_5usec <= #(1) 1;
				ps2_data_hi_z <= #(1) 0;
				if (((~ ps2_clk_s) && timer_5usec_done)) m1_next_state <= #(1) m1_tx_clk_l;
				else m1_next_state <= #(1) m1_tx_first_wait_clk_h;
			end
			m1_tx_first_wait_clk_l: begin
				ps2_data_hi_z <= #(1) 0;
				if ((~ ps2_clk_s)) m1_next_state <= #(1) m1_tx_clk_l;
				else m1_next_state <= #(1) m1_tx_first_wait_clk_l;
			end
			m1_tx_wait_clk_h: begin
				enable_timer_5usec <= #(1) 1;
				ps2_data_hi_z <= #(1) q[0];
				if ((ps2_clk_s && timer_5usec_done)) m1_next_state <= #(1) m1_tx_rising_edge_marker;
				else m1_next_state <= #(1) m1_tx_wait_clk_h;
			end
			m1_tx_rising_edge_marker: begin
				ps2_data_hi_z <= #(1) q[0];
				m1_next_state <= #(1) m1_tx_clk_h;
			end
			m1_tx_clk_h: begin
				ps2_data_hi_z <= #(1) q[0];
				if (tx_shifting_done) m1_next_state <= #(1) m1_tx_wait_keyboard_ack;
				else if ((~ ps2_clk_s)) m1_next_state <= #(1) m1_tx_clk_l;
				else m1_next_state <= #(1) m1_tx_clk_h;
			end
			m1_tx_clk_l: begin
				ps2_data_hi_z <= #(1) q[0];
				if (ps2_clk_s) m1_next_state <= #(1) m1_tx_wait_clk_h;
				else m1_next_state <= #(1) m1_tx_clk_l;
			end
			m1_tx_wait_keyboard_ack: begin
				if (((~ ps2_clk_s) && ps2_data_s)) m1_next_state <= #(1) m1_tx_error_no_keyboard_ack;
				else if (((~ ps2_clk_s) && (~ ps2_data_s))) m1_next_state <= #(1) m1_tx_done_recovery;
				else m1_next_state <= #(1) m1_tx_wait_keyboard_ack;
			end
			m1_tx_done_recovery: begin
				if ((ps2_clk_s && ps2_data_s)) m1_next_state <= #(1) m1_rx_clk_h;
				else m1_next_state <= #(1) m1_tx_done_recovery;
			end
			m1_tx_error_no_keyboard_ack: begin
				tx_error_no_keyboard_ack <= #(1) 1;
				if ((ps2_clk_s && ps2_data_s)) m1_next_state <= #(1) m1_rx_clk_h;
				else m1_next_state <= #(1) m1_tx_error_no_keyboard_ack;
			end
			default: m1_next_state <= #(1) m1_rx_clk_h;
		endcase
	end
	always @(posedge clock_i) begin : m2_state_register
		if (reset_i) m2_state <= #(1) m2_rx_data_ready_ack;
		else m2_state <= #(1) m2_next_state;
	end
	always @(m2_state or rx_output_strobe or rx_read) begin : m2_state_logic
		case (m2_state)
			m2_rx_data_ready_ack: begin
				rx_data_ready <= #(1) 1'b0;
				if (rx_output_strobe) m2_next_state <= #(1) m2_rx_data_ready;
				else m2_next_state <= #(1) m2_rx_data_ready_ack;
			end
			m2_rx_data_ready: begin
				rx_data_ready <= #(1) 1'b1;
				if (rx_read) m2_next_state <= #(1) m2_rx_data_ready_ack;
				else m2_next_state <= #(1) m2_rx_data_ready;
			end
			default: m2_next_state <= #(1) m2_rx_data_ready_ack;
		endcase
	end
	always @(posedge clock_i) begin
		if (reset_i) bit_count <= #(1) 0;
		else if ((rx_shifting_done || (m1_state == m1_tx_wait_keyboard_ack))) bit_count <= #(1) 0;
		else if (((timer_60usec_done && (m1_state == m1_rx_clk_h)) && ps2_clk_s)) bit_count <= #(1) 0;
		else if (((m1_state == m1_rx_falling_edge_marker) || (m1_state == m1_tx_rising_edge_marker))) bit_count <= #(1) (bit_count + 1);
	end
	assign rx_shifting_done = (bit_count == 11);
	assign tx_shifting_done = (bit_count == (11 - 1));
	assign tx_write_ack_o = ((tx_write && (m1_state == m1_rx_clk_h)) || (tx_write && (m1_state == m1_rx_clk_l)));
	assign tx_parity_bit = (~^ tx_data);
	always @(posedge clock_i) begin
		if (reset_i) q <= #(1) 0;
		else if (tx_write_ack_o) q <= #(1) {1'b1, tx_parity_bit, tx_data, 1'b0};
		else if (((m1_state == m1_rx_falling_edge_marker) || (m1_state == m1_tx_rising_edge_marker))) q <= #(1) {ps2_data_s, q[(11 - 1):1]};
	end
	always @(posedge clock_i) begin
		if ((~ enable_timer_60usec)) timer_60usec_count <= #(1) 0;
		else if ((timer_done && (! timer_60usec_done))) timer_60usec_count <= #(1) (timer_60usec_count + 1);
	end
	assign timer_60usec_done = (timer_60usec_count == TIMER_60USEC_VALUE_PP);
	always @(posedge clock_i) if (reset_i) timer_5usec <= #(1) 1;
	else if ((! enable_timer_60usec)) timer_5usec <= #(1) 1;
	else if ((timer_5usec == divide_reg_i)) begin
		timer_5usec <= #(1) 1;
		timer_done <= #(1) 1;
	end
	else begin
		timer_5usec <= #(1) (timer_5usec + 1);
		timer_done <= #(1) 0;
	end
	always @(posedge clock_i) begin
		if ((~ enable_timer_5usec)) timer_5usec_count <= #(1) 0;
		else if ((~ timer_5usec_done)) timer_5usec_count <= #(1) (timer_5usec_count + 1);
	end
	assign timer_5usec_done = (timer_5usec_count == (divide_reg_i - 1));
	assign released = (((q[8:1] == 16'hF0) && rx_shifting_done) && translate);
	always @(posedge clock_i) begin
		if (reset_i) hold_released <= #(1) 0;
		else if (rx_output_event) begin
			hold_released <= #(1) 0;
		end
		else begin
			if ((rx_shifting_done && released)) hold_released <= #(1) 1;
		end
	end
	always @(posedge clock_i) begin
		if (reset_i) begin
			rx_released <= #(1) 0;
			rx_scan_code <= #(1) 0;
		end
		else if (rx_output_strobe) begin
			rx_released <= #(1) hold_released;
			rx_scan_code <= #(1) q[8:1];
		end
	end
	assign rx_output_event = (rx_shifting_done && (~ released));
	assign rx_output_strobe = (rx_shifting_done && (~ released));
endmodule
module ps2_translation_table (
	reset_i,
	clock_i,
	translate_i,
	code_i,
	code_o,
	address_i,
	data_i,
	we_i,
	re_i,
	data_o,
	rx_data_ready_i,
	rx_translated_data_ready_o,
	rx_read_i,
	rx_read_o,
	rx_released_i
);
	input reset_i;
	input clock_i;
	input translate_i;
	input [7:0] code_i;
	output [7:0] code_o;
	input [7:0] address_i;
	input [7:0] data_i;
	input we_i;
	input re_i;
	output [7:0] data_o;
	input rx_data_ready_i;
	input rx_read_i;
	output rx_translated_data_ready_o;
	output rx_read_o;
	input rx_released_i;
   wire         translation_table_write_enable;
  assign translation_table_write_enable = we_i && (!translate_i || !rx_data_ready_i) ;
   wire [7:0]   translation_table_address;
   assign translation_table_address = ((we_i || re_i) && (!rx_data_ready_i || !translate_i)) ? address_i : code_i ;
   wire         translation_table_enable;
   assign translation_table_enable        = we_i || re_i || (translate_i && rx_data_ready_i) ;
	reg rx_translated_data_ready;
	always @(posedge clock_i) begin
		if (reset_i) rx_translated_data_ready <= #(1) 1'b0;
		else if ((rx_read_i || (! translate_i))) rx_translated_data_ready <= #(1) 1'b0;
		else rx_translated_data_ready <= #(1) rx_data_ready_i;
	end
	reg [((32 * 8) - 1):0] ps2_32byte_constant;
	reg [7:0] ram_out;
	always @(translation_table_address) begin
		case (translation_table_address[7:5])
			3'b000: ps2_32byte_constant = 256'h5b03111e1f2c71665a02101d702a386559290f3e40424464583c3b3d3f4143ff;
			3'b001: ps2_32byte_constant = 256'h5f0908162432726a5e071522233031695d061314212f39685c040512202d2e67;
			3'b010: ps2_32byte_constant = 256'h76632b751b1c363a6e620d1a7428736d610c19272635346c600a0b181725336b;
			3'b011: ps2_32byte_constant = 256'h544649374a514e574501484d4c5053526f7f7e474b7d4f7c7b0e7a7978775655;
			3'b100: ps2_32byte_constant = 256'h9f9e9d9c9b9a999897969594939291908f8e8d8c8b8a89888786855441828180;
			3'b101: ps2_32byte_constant = 256'hbfbebdbcbbbab9b8b7b6b5b4b3b2b1b0afaeadacabaaa9a8a7a6a5a4a3a2a1a0;
			3'b110: ps2_32byte_constant = 256'hdfdedddcdbdad9d8d7d6d5d4d3d2d1d0cfcecdcccbcac9c8c7c6c5c4c3c2c1c0;
			3'b111: ps2_32byte_constant = 256'hfffefdfcfbfaf9f8f7f6f5f4f3f2f1f0efeeedecebeae9e8e7e6e5e4e3e2e1e0;
		endcase
	end
	always @(posedge clock_i) begin
		if (reset_i) ram_out <= #(1) 8'h0;
		else if (translation_table_enable) begin : get_dat_out
			reg [7:0] bit_num;
			bit_num = (translation_table_address[4:0] << 3);
			repeat (8) begin
				ram_out[(bit_num % 8)] <= #(1) ps2_32byte_constant[bit_num];
				bit_num = (bit_num + 1'b1);
			end
		end
	end
	assign data_o = ram_out;
	assign code_o = (translate_i ? {(rx_released_i | ram_out[7]), ram_out[6:0]} : code_i);
	assign rx_translated_data_ready_o = (translate_i ? rx_translated_data_ready : rx_data_ready_i);
	assign rx_read_o = rx_read_i;
endmodule
module ArrayOrReduction (
	inputs,
	result
);
	parameter SIZE = 16;
	parameter WIDTH = 32;
	input wire [(SIZE * WIDTH) - 1:0] inputs;
	output wire [(WIDTH - 1):0] result;
	generate
		if ((SIZE <= 0)) begin : error_case
			DoesNotExit foo();
		end
		else if ((SIZE == 1)) begin : base_case_1
			assign result = inputs;
		end
		else if ((SIZE == 2)) begin : base_case_2
			assign result = (inputs[(SIZE * WIDTH)-1:WIDTH] | inputs[WIDTH-1:0]);
		end
		else begin : recursive_case
			wire [(WIDTH*2)-1:0] subResults;
			ArrayOrReduction #(
				.WIDTH(WIDTH),
				.SIZE((SIZE / 2))
			) top(
				.inputs(inputs[(SIZE * WIDTH)-1:(SIZE*WIDTH/2)]),
				.result(subResults[(WIDTH*2)-1:WIDTH])
			);
			ArrayOrReduction #(
				.WIDTH(WIDTH),
				.SIZE((SIZE / 2))
			) bot(
				.inputs(inputs[(SIZE*WIDTH/2)-1:0]),
				.result(subResults[WIDTH-1:0])
			);
			assign result = subResults[(WIDTH*2)-1:WIDTH] | subResults[WIDTH-1:0];
		end
	endgenerate
endmodule
module Array (
	index,
	element,
	array,
	clock,
	clear,
	enable
);
	parameter ELEMENTS = 16;
	parameter WIDTH = 32;
	input wire [($clog2(ELEMENTS) - 1):0] index;
	input wire [(WIDTH - 1):0] element;
	output reg [(((ELEMENTS - 1) >= 0) ? (((WIDTH - 1) >= 0) ? (((((ELEMENTS - 1) >= 0) ? ELEMENTS : ((0 - (ELEMENTS - 1)) + 1)) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + -1) : (((((ELEMENTS - 1) >= 0) ? ELEMENTS : ((0 - (ELEMENTS - 1)) + 1)) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + ((WIDTH - 1) - 1))) : (((WIDTH - 1) >= 0) ? ((((0 >= (ELEMENTS - 1)) ? ((0 - (ELEMENTS - 1)) + 1) : ELEMENTS) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + (((ELEMENTS - 1) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) - 1)) : ((((0 >= (ELEMENTS - 1)) ? ((0 - (ELEMENTS - 1)) + 1) : ELEMENTS) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + (((WIDTH - 1) + ((ELEMENTS - 1) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH))) - 1)))):(((ELEMENTS - 1) >= 0) ? (((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) : (((WIDTH - 1) >= 0) ? ((ELEMENTS - 1) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) : ((WIDTH - 1) + ((ELEMENTS - 1) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)))))] array;
	input wire clock;
	input wire clear;
	input wire enable;
	localparam ZERO_ELEMENT = {WIDTH {1'b0}};
	always @(posedge clock) if (clear) array <= {ELEMENTS {ZERO_ELEMENT}};
	else if (enable) array[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((ELEMENTS - 1) >= 0) ? index : (0 - (index - (ELEMENTS - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))] <= element;
endmodule
module Counter (
	clock,
	clear,
	enable,
	q
);
	parameter WIDTH = 8;
	parameter INCREMENT = 1;
	input wire clock;
	input wire clear;
	input wire enable;
	output reg [(WIDTH - 1):0] q;
	always @(posedge clock) if (clear) q <= 0;
	else if (enable) q <= (q + INCREMENT);
endmodule
module EdgeTrigger (
	signal,
	isEdge,
	clock,
	clear
);
	input wire signal;
	output wire isEdge;
	input wire clock;
	input wire clear;
	reg current;
	reg next;
	assign isEdge = ((! next) && current);
	always @(posedge clock) begin
		if (clear) begin
			current <= 1'b0;
			next <= 1'b0;
		end
		else begin
			current <= signal;
			next <= current;
		end
	end
endmodule
module NotMostRecentlyUsedPolicy (
	select,
	index,
	recentValid,
	recent,
	clock,
	clear
);
	parameter WIDTH = 32;
	parameter RECENT = 2;
	input wire select;
	output wire [(WIDTH - 1):0] index;
	input wire [(RECENT - 1):0] recentValid;
	input wire [(((RECENT - 1) >= 0) ? (((WIDTH - 1) >= 0) ? (((((RECENT - 1) >= 0) ? RECENT : ((0 - (RECENT - 1)) + 1)) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + -1) : (((((RECENT - 1) >= 0) ? RECENT : ((0 - (RECENT - 1)) + 1)) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + ((WIDTH - 1) - 1))) : (((WIDTH - 1) >= 0) ? ((((0 >= (RECENT - 1)) ? ((0 - (RECENT - 1)) + 1) : RECENT) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + (((RECENT - 1) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) - 1)) : ((((0 >= (RECENT - 1)) ? ((0 - (RECENT - 1)) + 1) : RECENT) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + (((WIDTH - 1) + ((RECENT - 1) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH))) - 1)))):(((RECENT - 1) >= 0) ? (((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) : (((WIDTH - 1) >= 0) ? ((RECENT - 1) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) : ((WIDTH - 1) + ((RECENT - 1) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)))))] recent;
	input wire clock;
	input wire clear;
	reg [(WIDTH - 1):0] index_next;
	Register #(.WIDTH(WIDTH)) lastRegister(
		.q(index),
		.d(index_next),
		.enable(select),
		.clock(clock),
		.clear(clear)
	);
	wire [(((RECENT - 1) >= 0) ? (((WIDTH - 1) >= 0) ? (((((RECENT - 1) >= 0) ? RECENT : ((0 - (RECENT - 1)) + 1)) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + -1) : (((((RECENT - 1) >= 0) ? RECENT : ((0 - (RECENT - 1)) + 1)) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + ((WIDTH - 1) - 1))) : (((WIDTH - 1) >= 0) ? ((((0 >= (RECENT - 1)) ? ((0 - (RECENT - 1)) + 1) : RECENT) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + (((RECENT - 1) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) - 1)) : ((((0 >= (RECENT - 1)) ? ((0 - (RECENT - 1)) + 1) : RECENT) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + (((WIDTH - 1) + ((RECENT - 1) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH))) - 1)))):(((RECENT - 1) >= 0) ? (((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) : (((WIDTH - 1) >= 0) ? ((RECENT - 1) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) : ((WIDTH - 1) + ((RECENT - 1) * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)))))] recent_saved;
	generate
		genvar recentIndex;
		for (recentIndex = 0; (recentIndex < RECENT); recentIndex = (recentIndex + 1)) begin : recent_latch
			Register #(.WIDTH(WIDTH)) recentRegister(
				.q(recent_saved[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((RECENT - 1) >= 0) ? recentIndex : (0 - (recentIndex - (RECENT - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))]),
				.d(recent[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((RECENT - 1) >= 0) ? recentIndex : (0 - (recentIndex - (RECENT - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))]),
				.enable(recentValid[recentIndex]),
				.clock(clock),
				.clear(clear)
			);
		end
	endgenerate
	generate
		if ((! (RECENT == 2))) begin
			DoesNotExist foo();
		end
	endgenerate
	wire [(WIDTH - 1):0] index_plus1;
	wire [(WIDTH - 1):0] index_plus2;
	assign index_plus1 = (index + 2'd1);
	assign index_plus2 = (index + 2'd2);
	always @(*) begin
		if (((index_plus1 != recent_saved[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((RECENT - 1) >= 0) ? 0 : (0 - (0 - (RECENT - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))]) && (index_plus1 != recent_saved[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((RECENT - 1) >= 0) ? 1 : (0 - (1 - (RECENT - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))]))) begin
			index_next = index_plus1;
		end
		else if (((index_plus2 != recent_saved[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((RECENT - 1) >= 0) ? 0 : (0 - (0 - (RECENT - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))]) && (index_plus2 != recent_saved[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + ((((RECENT - 1) >= 0) ? 1 : (0 - (1 - (RECENT - 1)))) * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))]))) begin
			index_next = index_plus2;
		end
		else begin
			index_next = (index + 2'd3);
		end
	end
endmodule
module FIFO (
	producerData,
	enqueueValid,
	consumerData,
	dequeueValid,
	isEmpty,
	isFull,
	clock,
	clear
);
	parameter SIZE = 64;
	parameter WIDTH = 32;
	input wire [(WIDTH - 1):0] producerData;
	input wire enqueueValid;
	output wire [(WIDTH - 1):0] consumerData;
	input wire dequeueValid;
	output wire isEmpty;
	output wire isFull;
	input wire clock;
	input wire clear;
	// removed an assertion item
	// removed an assertion item
	wire [($clog2(SIZE) - 1):0] producerIndex;
	wire [($clog2(SIZE) - 1):0] consumerIndex;
	xilinx_bram #(
		.RAM_WIDTH(WIDTH),
		.RAM_DEPTH(SIZE)
	) bram(
		.addressA(producerIndex),
		.dataInA(producerData),
		.writeEnableA(enqueueValid),
		.dataOutA(),
		.addressB(consumerIndex),
		.dataInB({WIDTH {1'b0}}),
		.writeEnableB(1'b0),
		.dataOutB(consumerData),
		.clock(clock)
	);
	Counter #(.WIDTH(((($clog2(SIZE) - 1) >= 0) ? $clog2(SIZE) : ((0 - ($clog2(SIZE) - 1)) + 1)))) producerIndex_register(
		.clock(clock),
		.clear(clear),
		.enable(enqueueValid),
		.q(producerIndex)
	);
	Counter #(.WIDTH(((($clog2(SIZE) - 1) >= 0) ? $clog2(SIZE) : ((0 - ($clog2(SIZE) - 1)) + 1)))) consumerIndex_register(
		.clock(clock),
		.clear(clear),
		.enable(dequeueValid),
		.q(consumerIndex)
	);
	assign isEmpty = (producerIndex == consumerIndex);
	assign isFull = (((producerIndex + 32'd1) % SIZE) == consumerIndex);
endmodule
module Register (
	q,
	d,
	clock,
	enable,
	clear
);
	parameter WIDTH = 32;
	parameter CLEAR_VALUE = 0;
	output reg [(WIDTH - 1):0] q;
	input wire [(WIDTH - 1):0] d;
	input wire clock;
	input wire enable;
	input wire clear;
	always @(posedge clock) if (clear) q <= CLEAR_VALUE;
	else if (enable) q <= d;
endmodule
module Stack (
	push_data,
	push_valid,
	pop_valid,
	peak_data,
	clock,
	clear
);
	parameter SIZE = 64;
	parameter WIDTH = 32;
	input wire [(WIDTH - 1):0] push_data;
	input wire push_valid;
	input wire pop_valid;
	output wire [(WIDTH - 1):0] peak_data;
	input wire clock;
	input wire clear;
	reg [($clog2(SIZE) - 1):0] stack_top;
	wire [($clog2(SIZE) - 1):0] stack_next;
	assign stack_next = (pop_valid ? stack_top : (stack_top + 1'd1));
	xilinx_bram_single #(
		.RAM_WIDTH(WIDTH),
		.RAM_DEPTH(SIZE)
	) bram(
		.address_read(stack_top),
		.dataOut(peak_data),
		.address_write(stack_next),
		.dataIn(push_data),
		.writeEnable(push_valid),
		.clock(clock)
	);
	always @(posedge clock) if (clear) stack_top <= {WIDTH {1'b0}};
	else if ((push_valid && pop_valid)) stack_top <= stack_top;
	else if (push_valid) stack_top <= (stack_top + 1'd1);
	else if (pop_valid) stack_top <= (stack_top - 1'd1);
endmodule
module Sync (
	clock,
	sig,
	sig_s
);
	parameter WIDTH = 1;
	input wire clock;
	input wire [(WIDTH - 1):0] sig;
	output wire [(WIDTH - 1):0] sig_s;
	reg [(((WIDTH - 1) >= 0) ? ((4 * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) + -1) : ((4 * ((0 >= (WIDTH - 1)) ? ((0 - (WIDTH - 1)) + 1) : WIDTH)) + ((WIDTH - 1) - 1))):(((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1))] sig_i;
	always @(posedge clock) begin
		sig_i <= {sig_i[((3 * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))) - 1):0], sig};
	end
	assign sig_s = sig_i[((((WIDTH - 1) >= 0) ? 0 : (WIDTH - 1)) + (3 * (((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))))+:(((WIDTH - 1) >= 0) ? WIDTH : ((0 - (WIDTH - 1)) + 1))];
endmodule
module xilinx_bram (
	addressA,
	addressB,
	dataInA,
	dataInB,
	clock,
	writeEnableA,
	writeEnableB,
	dataOutA,
	dataOutB
);
	parameter RAM_WIDTH = 32;
	parameter RAM_DEPTH = 1024;
	parameter INIT_FILE = "";
	input wire [($clog2(RAM_DEPTH) - 1):0] addressA;
	input wire [($clog2(RAM_DEPTH) - 1):0] addressB;
	input wire [(RAM_WIDTH - 1):0] dataInA;
	input wire [(RAM_WIDTH - 1):0] dataInB;
	input wire clock;
	input wire writeEnableA;
	input wire writeEnableB;
	output wire [(RAM_WIDTH - 1):0] dataOutA;
	output wire [(RAM_WIDTH - 1):0] dataOutB;
	(* ram_style = "block" *) reg [(RAM_WIDTH - 1):0] bram [(RAM_DEPTH - 1):0];
	reg [(RAM_WIDTH - 1):0] bram_dataA = {RAM_WIDTH {1'b0}};
	reg [(RAM_WIDTH - 1):0] bram_dataB = {RAM_WIDTH {1'b0}};
	generate
		if ((INIT_FILE != "")) begin : use_init_file
			initial $readmemh(INIT_FILE, bram, 0, (RAM_DEPTH - 1));
		end
		else begin : init_bram_to_zero
			integer ram_index;
			initial for (ram_index = 0; (ram_index < RAM_DEPTH); ram_index = (ram_index + 1))
				bram[ram_index] = {RAM_WIDTH {1'b0}};
		end
	endgenerate
	always @(posedge clock) if (writeEnableA) bram[addressA] <= dataInA;
	else bram_dataA <= bram[addressA];
	always @(posedge clock) if (writeEnableB) bram[addressB] <= dataInB;
	else bram_dataB <= bram[addressB];
	assign dataOutA = bram_dataA;
	assign dataOutB = bram_dataB;
endmodule
module xilinx_bram_byte_write (
	addra,
	addrb,
	dina,
	dinb,
	clka,
	wea,
	web,
	douta,
	doutb
);
	parameter NB_COL = 4;
	parameter COL_WIDTH = 9;
	parameter RAM_DEPTH = 1024;
	parameter INIT_FILE = "";
	input [($clog2(RAM_DEPTH) - 1):0] addra;
	input [($clog2(RAM_DEPTH) - 1):0] addrb;
	input [((NB_COL * COL_WIDTH) - 1):0] dina;
	input [((NB_COL * COL_WIDTH) - 1):0] dinb;
	input clka;
	input [(NB_COL - 1):0] wea;
	input [(NB_COL - 1):0] web;
	output [((NB_COL * COL_WIDTH) - 1):0] douta;
	output [((NB_COL * COL_WIDTH) - 1):0] doutb;
	reg [((NB_COL * COL_WIDTH) - 1):0] BRAM [(RAM_DEPTH - 1):0];
	reg [((NB_COL * COL_WIDTH) - 1):0] ram_data_a = {(NB_COL * COL_WIDTH) {1'b0}};
	reg [((NB_COL * COL_WIDTH) - 1):0] ram_data_b = {(NB_COL * COL_WIDTH) {1'b0}};
	generate
		if ((INIT_FILE != "")) begin : use_init_file
			initial $readmemh(INIT_FILE, BRAM, 0, (RAM_DEPTH - 1));
		end
		else begin : init_bram_to_zero
			integer ram_index;
			initial for (ram_index = 0; (ram_index < RAM_DEPTH); ram_index = (ram_index + 1))
				BRAM[ram_index] = {(NB_COL * COL_WIDTH) {1'b0}};
		end
	endgenerate
	always @(posedge clka) ram_data_a <= BRAM[addra];
	always @(posedge clka) ram_data_b <= BRAM[addrb];
	generate
		genvar i;
		for (i = 0; (i < NB_COL); i = (i + 1)) begin : byte_write
			always @(posedge clka) if (wea[i]) BRAM[addra][(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)] <= dina[(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)];
			always @(posedge clka) if (web[i]) BRAM[addrb][(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)] <= dinb[(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)];
		end
	endgenerate
	assign douta = ram_data_a;
	assign doutb = ram_data_b;
endmodule
module xilinx_bram_double_clock (
	addra,
	addrb,
	dina,
	dinb,
	clka,
	clkb,
	wea,
	web,
	douta,
	doutb
);
	parameter NB_COL = 4;
	parameter COL_WIDTH = 8;
	parameter RAM_DEPTH = 1024;
	parameter INIT_FILE = "";
	input [($clog2(RAM_DEPTH) - 1):0] addra;
	input [($clog2(RAM_DEPTH) - 1):0] addrb;
	input [((NB_COL * COL_WIDTH) - 1):0] dina;
	input [((NB_COL * COL_WIDTH) - 1):0] dinb;
	input clka;
	input clkb;
	input [(NB_COL - 1):0] wea;
	input [(NB_COL - 1):0] web;
	output [((NB_COL * COL_WIDTH) - 1):0] douta;
	output [((NB_COL * COL_WIDTH) - 1):0] doutb;
	reg [((NB_COL * COL_WIDTH) - 1):0] BRAM [(RAM_DEPTH - 1):0];
	reg [((NB_COL * COL_WIDTH) - 1):0] ram_data_a = {(NB_COL * COL_WIDTH) {1'b0}};
	reg [((NB_COL * COL_WIDTH) - 1):0] ram_data_b = {(NB_COL * COL_WIDTH) {1'b0}};
	generate
		if ((INIT_FILE != "")) begin : use_init_file
			initial $readmemh(INIT_FILE, BRAM, 0, (RAM_DEPTH - 1));
		end
		else begin : init_bram_to_zero
			integer ram_index;
			initial for (ram_index = 0; (ram_index < RAM_DEPTH); ram_index = (ram_index + 1))
				BRAM[ram_index] = {(NB_COL * COL_WIDTH) {1'b0}};
		end
	endgenerate
	always @(posedge clka) begin
		ram_data_a <= BRAM[addra];
	end
	always @(posedge clkb) begin
		ram_data_b <= BRAM[addrb];
	end
	generate
		genvar i;
		for (i = 0; (i < NB_COL); i = (i + 1)) begin : byte_write
			always @(posedge clka) if (wea[i]) BRAM[addra][(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)] <= dina[(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)];
			always @(posedge clkb) if (web[i]) BRAM[addrb][(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)] <= dinb[(((i + 1) * COL_WIDTH) - 1):(i * COL_WIDTH)];
		end
	endgenerate
	assign douta = ram_data_a;
	assign doutb = ram_data_b;
endmodule
module xilinx_bram_single (
	address_read,
	dataOut,
	address_write,
	dataIn,
	writeEnable,
	clock
);
	parameter RAM_WIDTH = 18;
	parameter RAM_DEPTH = 1024;
	parameter INIT_FILE = "";
	input [($clog2(RAM_DEPTH) - 1):0] address_read;
	output [(RAM_WIDTH - 1):0] dataOut;
	input [($clog2(RAM_DEPTH) - 1):0] address_write;
	input [(RAM_WIDTH - 1):0] dataIn;
	input writeEnable;
	input clock;
	(* ram_style = "block" *) reg [(RAM_WIDTH - 1):0] BRAM [(RAM_DEPTH - 1):0];
	reg [(RAM_WIDTH - 1):0] ram_data = {RAM_WIDTH {1'b0}};
	generate
		if ((INIT_FILE != "")) begin : use_init_file
			initial $readmemh(INIT_FILE, BRAM, 0, (RAM_DEPTH - 1));
		end
		else begin : init_bram_to_zero
			integer ram_index;
			initial for (ram_index = 0; (ram_index < RAM_DEPTH); ram_index = (ram_index + 1))
				BRAM[ram_index] = {RAM_WIDTH {1'b0}};
		end
	endgenerate
	always @(posedge clock) begin
		if (writeEnable) begin
			BRAM[address_write] <= dataIn;
		end
		ram_data <= BRAM[address_read];
	end
	assign dataOut = ram_data;
endmodule
module xilinx_sram (
	clock,
	clear,
	writeEnable,
	readAddress,
	writeAddress,
	writeData,
	readData
);
	parameter NUM_WORDS = 128;
	parameter WORD_WIDTH = 62;
	input wire clock;
	input wire clear;
	input wire writeEnable;
	input wire [($clog2(NUM_WORDS) - 1):0] readAddress;
	input wire [($clog2(NUM_WORDS) - 1):0] writeAddress;
	input wire [(WORD_WIDTH - 1):0] writeData;
	output wire [(WORD_WIDTH - 1):0] readData;
	reg [(NUM_WORDS * WORD_WIDTH) - 1:0] memory;
	always @(posedge clock) begin
		if (clear) memory <= {(NUM_WORDS * WORD_WIDTH) {1'b0}};
		else if (writeEnable) memory[(writeAddress * WORD_WIDTH)+:WORD_WIDTH] <= writeData;
	end
	assign readData = memory[(readAddress * WORD_WIDTH)+:WORD_WIDTH];
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: PageOffset_t
// removed typedef: VirtualPageNumber_t
// removed typedef: PhysicalPageNumber_t
// removed typedef: TableWalkResult_t
// removed typedef: PageTableEntry_t
// removed typedef: MemorySelect_t
// removed typedef: MemoryLock_t
// removed typedef: MemoryMode_t
module DRAMAXIDriver (
	dram_writeData,
	dram_readData,
	dram_addressBase,
	dram_readEnable,
	dram_writeEnable,
	dram_requestAccepted,
	dram_requestCompleted,
	dram_requestError,
	M_AXI_ACLK,
	M_AXI_ARESETN,
	M_AXI_AWADDR,
	M_AXI_AWPROT,
	M_AXI_AWVALID,
	M_AXI_AWREADY,
	M_AXI_WDATA,
	M_AXI_WSTRB,
	M_AXI_WVALID,
	M_AXI_WREADY,
	M_AXI_BRESP,
	M_AXI_BVALID,
	M_AXI_BREADY,
	M_AXI_ARADDR,
	M_AXI_ARPROT,
	M_AXI_ARVALID,
	M_AXI_ARREADY,
	M_AXI_RDATA,
	M_AXI_RRESP,
	M_AXI_RVALID,
	M_AXI_RREADY
);
	localparam [31:0] IDLE = 32'd0;
	localparam [31:0] WRITE = 32'd1;
	localparam [31:0] READ = 32'd2;
	parameter integer C_M_AXI_ADDR_WIDTH = 32;
	parameter integer C_M_AXI_DATA_WIDTH = 32;
	parameter integer C_M_TRANSACTIONS_NUM = 16;
	parameter integer PHYSICAL_ADDRESS_OFFSET = 'h8000000;
	input wire [511:0] dram_writeData;
	output wire [511:0] dram_readData;
	input wire [33:0] dram_addressBase;
	input wire dram_readEnable;
	input wire dram_writeEnable;
	output reg dram_requestAccepted;
	output reg dram_requestCompleted;
	output reg dram_requestError;
	input wire M_AXI_ACLK;
	input wire M_AXI_ARESETN;
	output wire [(C_M_AXI_ADDR_WIDTH - 1):0] M_AXI_AWADDR;
	output wire [2:0] M_AXI_AWPROT;
	output wire M_AXI_AWVALID;
	input wire M_AXI_AWREADY;
	output wire [(C_M_AXI_DATA_WIDTH - 1):0] M_AXI_WDATA;
	output wire [((C_M_AXI_DATA_WIDTH / 8) - 1):0] M_AXI_WSTRB;
	output wire M_AXI_WVALID;
	input wire M_AXI_WREADY;
	input wire [1:0] M_AXI_BRESP;
	input wire M_AXI_BVALID;
	output wire M_AXI_BREADY;
	output wire [(C_M_AXI_ADDR_WIDTH - 1):0] M_AXI_ARADDR;
	output wire [2:0] M_AXI_ARPROT;
	output wire M_AXI_ARVALID;
	input wire M_AXI_ARREADY;
	input wire [(C_M_AXI_DATA_WIDTH - 1):0] M_AXI_RDATA;
	input wire [1:0] M_AXI_RRESP;
	input wire M_AXI_RVALID;
	output wire M_AXI_RREADY;
	wire readEdge;
	wire writeEdge;
	EdgeTrigger readTrigger(
		.signal(dram_readEnable),
		.isEdge(readEdge),
		.clock(M_AXI_ACLK),
		.clear((~ M_AXI_ARESETN))
	);
	EdgeTrigger writeTrigger(
		.signal(dram_writeEnable),
		.isEdge(writeEdge),
		.clock(M_AXI_ACLK),
		.clear((~ M_AXI_ARESETN))
	);
	wire [(C_M_AXI_ADDR_WIDTH - 1):0] savedAddressBase;
	wire [(((C_M_TRANSACTIONS_NUM - 1) >= 0) ? (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? (((((C_M_TRANSACTIONS_NUM - 1) >= 0) ? C_M_TRANSACTIONS_NUM : ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1)) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) + -1) : (((((C_M_TRANSACTIONS_NUM - 1) >= 0) ? C_M_TRANSACTIONS_NUM : ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1)) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH)) + ((C_M_AXI_DATA_WIDTH - 1) - 1))) : (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? ((((0 >= (C_M_TRANSACTIONS_NUM - 1)) ? ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1) : C_M_TRANSACTIONS_NUM) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) + (((C_M_TRANSACTIONS_NUM - 1) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) - 1)) : ((((0 >= (C_M_TRANSACTIONS_NUM - 1)) ? ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1) : C_M_TRANSACTIONS_NUM) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH)) + (((C_M_AXI_DATA_WIDTH - 1) + ((C_M_TRANSACTIONS_NUM - 1) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH))) - 1)))):(((C_M_TRANSACTIONS_NUM - 1) >= 0) ? (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? 0 : (C_M_AXI_DATA_WIDTH - 1)) : (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? ((C_M_TRANSACTIONS_NUM - 1) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) : ((C_M_AXI_DATA_WIDTH - 1) + ((C_M_TRANSACTIONS_NUM - 1) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH)))))] savedWriteData;
	reg writes_done;
	reg axi_bready;
	wire write_resp_error;
	wire write_error_cumulative;
	reg last_write;
	reg [$clog2(C_M_TRANSACTIONS_NUM):0] write_index;
	reg start_single_write;
	reg write_issued;
	reg reads_done;
	wire read_resp_error;
	wire read_error_cumulative;
	reg last_read;
	reg [$clog2(C_M_TRANSACTIONS_NUM):0] read_index;
	reg start_single_read;
	reg read_issued;
	reg axi_awvalid;
	reg axi_wvalid;
	reg axi_rready;
	reg axi_arvalid;
	reg [(C_M_AXI_ADDR_WIDTH - 1):0] axi_awaddr;
	reg [(C_M_AXI_DATA_WIDTH - 1):0] axi_wdata;
	reg [(C_M_AXI_ADDR_WIDTH - 1):0] axi_araddr;
	reg [(((C_M_TRANSACTIONS_NUM - 1) >= 0) ? (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? (((((C_M_TRANSACTIONS_NUM - 1) >= 0) ? C_M_TRANSACTIONS_NUM : ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1)) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) + -1) : (((((C_M_TRANSACTIONS_NUM - 1) >= 0) ? C_M_TRANSACTIONS_NUM : ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1)) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH)) + ((C_M_AXI_DATA_WIDTH - 1) - 1))) : (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? ((((0 >= (C_M_TRANSACTIONS_NUM - 1)) ? ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1) : C_M_TRANSACTIONS_NUM) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) + (((C_M_TRANSACTIONS_NUM - 1) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) - 1)) : ((((0 >= (C_M_TRANSACTIONS_NUM - 1)) ? ((0 - (C_M_TRANSACTIONS_NUM - 1)) + 1) : C_M_TRANSACTIONS_NUM) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH)) + (((C_M_AXI_DATA_WIDTH - 1) + ((C_M_TRANSACTIONS_NUM - 1) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH))) - 1)))):(((C_M_TRANSACTIONS_NUM - 1) >= 0) ? (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? 0 : (C_M_AXI_DATA_WIDTH - 1)) : (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? ((C_M_TRANSACTIONS_NUM - 1) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))) : ((C_M_AXI_DATA_WIDTH - 1) + ((C_M_TRANSACTIONS_NUM - 1) * ((0 >= (C_M_AXI_DATA_WIDTH - 1)) ? ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1) : C_M_AXI_DATA_WIDTH)))))] readDataArray;
	assign dram_readData = readDataArray;
	wire doSetup;
	reg [31:0] state;
	assign doSetup = ((readEdge || writeEdge) && (state == IDLE));
	Register #(.WIDTH(C_M_AXI_ADDR_WIDTH)) address_reg(
		.q(savedAddressBase),
		.d(dram_addressBase[31:0]),
		.clock(M_AXI_ACLK),
		.enable(doSetup),
		.clear((~ M_AXI_ARESETN))
	);
	Register #(.WIDTH(512)) data_reg(
		.q(savedWriteData),
		.d(dram_writeData),
		.clock(M_AXI_ACLK),
		.enable(doSetup),
		.clear((~ M_AXI_ARESETN))
	);
	Register #(.WIDTH(1)) writeError_reg(
		.q(write_error_cumulative),
		.d(1'b1),
		.clock(M_AXI_ACLK),
		.enable(write_resp_error),
		.clear((doSetup || (~ M_AXI_ARESETN)))
	);
	Register #(.WIDTH(1)) readError_reg(
		.q(read_error_cumulative),
		.d(1'b1),
		.clock(M_AXI_ACLK),
		.enable(read_resp_error),
		.clear((doSetup || (~ M_AXI_ARESETN)))
	);
	assign M_AXI_BREADY = axi_bready;
	assign M_AXI_AWVALID = axi_awvalid;
	assign M_AXI_WVALID = axi_wvalid;
	assign M_AXI_WSTRB = 4'b1111;
	assign M_AXI_AWADDR = ((PHYSICAL_ADDRESS_OFFSET + savedAddressBase) + axi_awaddr);
	assign M_AXI_WDATA = axi_wdata;
	assign M_AXI_AWPROT = 3'b000;
	assign M_AXI_RREADY = axi_rready;
	assign M_AXI_ARADDR = ((PHYSICAL_ADDRESS_OFFSET + savedAddressBase) + axi_araddr);
	assign M_AXI_ARVALID = axi_arvalid;
	assign M_AXI_ARPROT = 3'b001;
	always @(posedge M_AXI_ACLK) begin
		if ((~ M_AXI_ARESETN)) begin
			state <= IDLE;
			dram_requestError <= 1'b0;
			dram_requestCompleted <= 1'b0;
			dram_requestAccepted <= 1'b0;
			start_single_write <= 1'b0;
			write_issued <= 1'b0;
			start_single_read <= 1'b0;
			read_issued <= 1'b0;
		end
		else begin
			case (state)
				IDLE: begin
					if (readEdge) begin
						state <= READ;
						dram_requestError <= 1'b0;
						dram_requestCompleted <= 1'b0;
						dram_requestAccepted <= 1'b1;
					end
					else if (writeEdge) begin
						state <= WRITE;
						dram_requestError <= 1'b0;
						dram_requestCompleted <= 1'b0;
						dram_requestAccepted <= 1'b1;
					end
				end
				WRITE: begin
					dram_requestAccepted <= 1'b0;
					if (writes_done) begin
						state <= IDLE;
						dram_requestCompleted <= 1'b1;
						dram_requestError <= write_error_cumulative;
					end
					else begin
						if (((((((~ axi_awvalid) && (~ axi_wvalid)) && (~ M_AXI_BVALID)) && (~ last_write)) && (~ start_single_write)) && (~ write_issued))) begin
							start_single_write <= 1'b1;
							write_issued <= 1'b1;
						end
						else if (axi_bready) begin
							write_issued <= 1'b0;
						end
						else begin
							start_single_write <= 1'b0;
						end
					end
				end
				READ: begin
					dram_requestAccepted <= 1'b0;
					if (reads_done) begin
						state <= IDLE;
						dram_requestCompleted <= 1'b1;
						dram_requestError <= read_error_cumulative;
					end
					else begin
						if ((((((~ axi_arvalid) && (~ M_AXI_RVALID)) && (~ last_read)) && (~ start_single_read)) && (~ read_issued))) begin
							start_single_read <= 1'b1;
							read_issued <= 1'b1;
						end
						else if (axi_rready) begin
							read_issued <= 1'b0;
						end
						else begin
							start_single_read <= 1'b0;
						end
					end
				end
				default: state <= IDLE;
			endcase
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_awaddr <= 0;
		end
		else if ((M_AXI_AWREADY && axi_awvalid)) begin
			axi_awaddr <= (axi_awaddr + 32'h00000004);
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_wdata <= dram_writeData[0+:32];
		end
		else if ((M_AXI_WREADY && axi_wvalid)) begin
			axi_wdata <= savedWriteData[((((C_M_AXI_DATA_WIDTH - 1) >= 0) ? 0 : (C_M_AXI_DATA_WIDTH - 1)) + ((((C_M_TRANSACTIONS_NUM - 1) >= 0) ? write_index : (0 - (write_index - (C_M_TRANSACTIONS_NUM - 1)))) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))))+:(((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))];
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_bready <= 1'b0;
		end
		else if ((M_AXI_BVALID && (~ axi_bready))) begin
			axi_bready <= 1'b1;
		end
		else if (axi_bready) begin
			axi_bready <= 1'b0;
		end
		else begin
			axi_bready <= axi_bready;
		end
	end
	assign write_resp_error = ((axi_bready & M_AXI_BVALID) & M_AXI_BRESP[1]);
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) writes_done <= 1'b0;
		else if (((last_write && M_AXI_BVALID) && axi_bready)) writes_done <= 1'b1;
		else writes_done <= writes_done;
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) last_write <= 1'b0;
		else if (((write_index == C_M_TRANSACTIONS_NUM) && M_AXI_AWREADY)) begin
			last_write <= 1'b1;
		end
		else last_write <= last_write;
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			write_index <= 0;
		end
		else if (start_single_write) begin
			write_index <= (write_index + 1);
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_awvalid <= 1'b0;
		end
		else begin
			if (start_single_write) begin
				axi_awvalid <= 1'b1;
			end
			else if ((M_AXI_AWREADY && axi_awvalid)) begin
				axi_awvalid <= 1'b0;
			end
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_wvalid <= 1'b0;
		end
		else if (start_single_write) begin
			axi_wvalid <= 1'b1;
		end
		else if ((M_AXI_WREADY && axi_wvalid)) begin
			axi_wvalid <= 1'b0;
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) reads_done <= 1'b0;
		else if (((last_read && M_AXI_RVALID) && axi_rready)) begin
			reads_done <= 1'b1;
		end
		else reads_done <= reads_done;
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_rready <= 1'b0;
		end
		else if ((M_AXI_RVALID && (~ axi_rready))) begin
			axi_rready <= 1'b1;
		end
		else if (axi_rready) begin
			axi_rready <= 1'b0;
		end
	end
	assign read_resp_error = ((axi_rready & M_AXI_RVALID) & M_AXI_RRESP[1]);
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) last_read <= 1'b0;
		else if (((read_index == C_M_TRANSACTIONS_NUM) && M_AXI_ARREADY)) begin
			last_read <= 1'b1;
		end
		else last_read <= last_read;
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			read_index <= 0;
		end
		else if (start_single_read) begin
			read_index <= (read_index + 1);
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_arvalid <= 1'b0;
		end
		else if (start_single_read) begin
			axi_arvalid <= 1'b1;
		end
		else if ((M_AXI_ARREADY && axi_arvalid)) begin
			axi_arvalid <= 1'b0;
		end
	end
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			axi_araddr <= 0;
		end
		else if ((M_AXI_ARREADY && axi_arvalid)) begin
			axi_araddr <= (axi_araddr + 32'h00000004);
		end
	end
	localparam CLEAR_LITTERAL = {C_M_AXI_DATA_WIDTH {1'b0}};
	always @(posedge M_AXI_ACLK) begin
		if (((M_AXI_ARESETN == 0) || doSetup)) begin
			readDataArray <= {C_M_TRANSACTIONS_NUM {CLEAR_LITTERAL}};
		end
		else if ((M_AXI_RVALID && axi_rready)) begin
			readDataArray[((((C_M_AXI_DATA_WIDTH - 1) >= 0) ? 0 : (C_M_AXI_DATA_WIDTH - 1)) + ((((C_M_TRANSACTIONS_NUM - 1) >= 0) ? (read_index - 1) : (0 - ((read_index - 1) - (C_M_TRANSACTIONS_NUM - 1)))) * (((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))))+:(((C_M_AXI_DATA_WIDTH - 1) >= 0) ? C_M_AXI_DATA_WIDTH : ((0 - (C_M_AXI_DATA_WIDTH - 1)) + 1))] <= M_AXI_RDATA;
		end
	end
endmodule

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: MemorySelect_t
// removed typedef: MemoryLock_t
module DRAMWrapper (
	memory_physicalAddress,
	memory_readEnable,
	memory_writeEnable,
	memory_writeData,
	memory_responseValid,
	memory_readData,
	memory_transactionComplete,
	axi_writeData,
	axi_readData,
	axi_addressBase,
	axi_readEnable,
	axi_writeEnable,
	axi_requestAccepted,
	axi_requestCompleted,
	axi_requestError,
	remote_readError,
	remote_writeError,
	remote_writeEnableMissing,
	remote_index,
	remote_writeArray,
	remote_addressBase,
	clock,
	clear
);
	localparam [31:0] IDLE = 32'd0;
	localparam [31:0] READ_REQUEST = 32'd1;
	localparam [31:0] READ_RESPONSE = 32'd2;
	localparam [31:0] READ_UNLOAD = 32'd3;
	localparam [31:0] WRITE_LOAD = 32'd4;
	localparam [31:0] WRITE_REQUEST = 32'd5;
	localparam [31:0] WRITE_WAIT = 32'd6;
	localparam [31:0] WRITE_RESPONSE = 32'd7;
	localparam [31:0] FATAL_ERROR = 32'd8;
	input wire [33:0] memory_physicalAddress;
	input wire memory_readEnable;
	input wire memory_writeEnable;
	input wire [(32 - 1):0] memory_writeData;
	output reg memory_responseValid;
	output reg [(32 - 1):0] memory_readData;
	output reg memory_transactionComplete;
	output wire [511:0] axi_writeData;
	input wire [511:0] axi_readData;
	output wire [33:0] axi_addressBase;
	output reg axi_readEnable;
	output reg axi_writeEnable;
	input wire axi_requestAccepted;
	input wire axi_requestCompleted;
	input wire axi_requestError;
	output reg remote_readError;
	output reg remote_writeError;
	output reg remote_writeEnableMissing;
	output wire [5:0] remote_index;
	output wire [511:0] remote_writeArray;
	output wire [33:0] remote_addressBase;
	input wire clock;
	input wire clear;
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	reg loadReadArray;
	reg loadWriteArray;
	wire [511:0] writeArray;
	wire [511:0] readArray;
	wire [$clog2('d16):0] index;
	reg counterIncrement;
	reg counterClear;
	Counter #(.WIDTH(5)) indexCounter(
		.clock(clock),
		.clear((clear | counterClear)),
		.enable(counterIncrement),
		.q(index)
	);
	reg loadAddress;
	Register #(.WIDTH(34)) addressRegister(
		.q(axi_addressBase),
		.d(memory_physicalAddress),
		.clock(clock),
		.enable(loadAddress),
		.clear(clear)
	);
	assign remote_index = index;
	assign remote_writeArray = writeArray;
	assign remote_addressBase = axi_addressBase;
	// removed an assertion item
	assign axi_writeData = writeArray;
	Register #(.WIDTH(512)) readArrayRegister(
		.q(readArray),
		.d(axi_readData),
		.enable(loadReadArray),
		.clock(clock),
		.clear(clear)
	);
	generate
		if ((! (5 >= 4))) begin
			DoesNotExist foo();
		end
	endgenerate
	generate
		if ((! ($clog2('d16) == 4))) begin
			DoesNotExist foo();
		end
	endgenerate
	Array #(
		.WIDTH(32),
		.ELEMENTS('d16)
	) writeArrayArray(
		.index(index[3:0]),
		.element(memory_writeData),
		.array(writeArray),
		.clock(clock),
		.clear(clear),
		.enable(loadWriteArray)
	);
	reg [31:0] state;
	reg [31:0] nextState;
	always @(posedge clock) if (clear) state <= IDLE;
	else state <= nextState;
	// removed an assertion item
	// removed an assertion item
	always @(*) begin
		nextState = state;
		counterIncrement = 1'b0;
		counterClear = 1'b0;
		loadAddress = 1'b0;
		loadReadArray = 1'b0;
		loadWriteArray = 1'b0;
		axi_readEnable = 1'b0;
		axi_writeEnable = 1'b0;
		remote_writeError = 1'b0;
		remote_readError = 1'b0;
		remote_writeEnableMissing = 1'b0;
		memory_responseValid = 1'b0;
		memory_transactionComplete = 1'b0;
		memory_readData = 32'hBADF00D;
		case (state)
			IDLE: begin
				counterClear = 1'b1;
				if (memory_readEnable) begin
					loadAddress = 1'b1;
					nextState = READ_REQUEST;
				end
				else if (memory_writeEnable) begin
					loadAddress = 1'b1;
					nextState = WRITE_LOAD;
					loadWriteArray = 1'b1;
					counterIncrement = 1'b1;
					counterClear = 1'b0;
				end
			end
			READ_REQUEST: begin
				axi_readEnable = 1'b1;
				if (axi_requestAccepted) nextState = READ_RESPONSE;
			end
			READ_RESPONSE: begin
				if (axi_requestCompleted) begin
					loadReadArray = 1'b1;
					counterClear = 1'b1;
					if (axi_requestError) begin
						nextState = FATAL_ERROR;
						remote_readError = 1'b1;
					end
					else begin
						nextState = READ_UNLOAD;
					end
				end
			end
			READ_UNLOAD: begin
				memory_responseValid = 1'b1;
				memory_readData = readArray[(index * 32)+:32];
				counterIncrement = 1'b1;
				if ((index == ('d16 - 1))) begin
					nextState = IDLE;
					memory_transactionComplete = 1'b1;
					counterClear = 1'b1;
				end
			end
			WRITE_LOAD: begin
				memory_responseValid = 1'b1;
				counterIncrement = 1'b1;
				loadWriteArray = 1'b1;
				if ((index == ('d16 - 1))) nextState = WRITE_REQUEST;
				if ((! memory_writeEnable)) begin
					nextState = FATAL_ERROR;
					remote_writeEnableMissing = 1'b1;
				end
			end
			WRITE_REQUEST: begin
				memory_responseValid = 1'b1;
				nextState = WRITE_WAIT;
			end
			WRITE_WAIT: begin
				axi_writeEnable = 1'b1;
				if (axi_requestAccepted) nextState = WRITE_RESPONSE;
			end
			WRITE_RESPONSE: begin
				if (axi_requestCompleted) begin
					if (axi_requestError) begin
						nextState = FATAL_ERROR;
						remote_writeError = 1'b1;
					end
					else begin
						counterClear = 1'b1;
						memory_transactionComplete = 1'b1;
						nextState = IDLE;
					end
				end
			end
			FATAL_ERROR: begin
				nextState = FATAL_ERROR;
			end
		endcase
	end
endmodule

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: Index_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: PageOffset_t
// removed typedef: VirtualPageNumber_t
// removed typedef: PhysicalPageNumber_t
// removed typedef: TableWalkResult_t
// removed typedef: PageTableEntry_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: MemorySelect_t
// removed typedef: MemoryLock_t
// removed typedef: MemoryMode_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: MemorySelect_t
// removed typedef: MemoryLock_t
// removed typedef: PageOffset_t
// removed typedef: VirtualPageNumber_t
// removed typedef: PhysicalPageNumber_t
// removed typedef: TableWalkResult_t
// removed typedef: PageTableEntry_t

module Memory (
	instruction_ready,
	instruction_request,
	instruction_response,
	data_ready,
	data_request,
	data_response,
	dram_physicalAddress,
	dram_readEnable,
	dram_writeEnable,
	dram_writeData,
	dram_responseValid,
	dram_readData,
	dram_transactionComplete,
	vram_request,
	vram_response,
	vram_clock,
	vram_clear,
	remote_request,
	remote_response,
	remote_clock,
	remote_clear,
	control_supervisorPagesEnabled,
	control_userPagesEnabled,
	control_virtualMemoryEnabled,
	control_rootPageTable,
	clock,
	clear
);
	output logic instruction_ready;
	input logic [100:0] instruction_request;
	output logic [278:0] instruction_response;
	output logic data_ready;
	input logic [100:0] data_request;
	output logic [278:0] data_response;
	output logic [33:0] dram_physicalAddress;
	output logic dram_readEnable;
	output logic dram_writeEnable;
	output logic [(32 - 1):0] dram_writeData;
	input logic dram_responseValid;
	input logic [(32 - 1):0] dram_readData;
	input logic dram_transactionComplete;
	output logic [70:0] vram_request;
	input logic [103:0] vram_response;
	output logic vram_clock;
	output logic vram_clear;
	output logic [70:0] remote_request;
	input logic [103:0] remote_response;
	output logic remote_clock;
	output logic remote_clear;
	input logic control_supervisorPagesEnabled;
	input logic control_userPagesEnabled;
	input logic control_virtualMemoryEnabled;
	input logic [33:0] control_rootPageTable;
	input logic clock;
	input logic clear;

	MemoryInterface instruction_interface();
	MemoryInterface data_interface();
	DRAMInterface dram_interface();
	MemoryMappedIOInterface vram_interface();
	MemoryMappedIOInterface remote_interface();
	MemoryControl control_interface();
	always_comb begin
		instruction_interface.request = instruction_request;
		instruction_response = instruction_interface.response;
		instruction_ready = instruction_interface.ready;

		data_interface.request = data_request;
		data_response = data_interface.response;
		data_ready = data_interface.ready;

		dram_physicalAddress = dram_interface.physicalAddress;
		dram_readEnable = dram_interface.readEnable;
		dram_writeEnable = dram_interface.writeEnable;
		dram_writeData = dram_interface.writeData;
		dram_interface.responseValid = dram_responseValid;
		dram_interface.readData = dram_readData;
		dram_interface.transactionComplete = dram_transactionComplete;

		vram_request = vram_interface.request;
		vram_interface.response = vram_response;
		vram_clock = vram_interface.clock;
		vram_clear = vram_interface.clear;

		remote_request = remote_interface.request;
		remote_interface.response = remote_response;
		remote_clock = remote_interface.clock;
		remote_clear = remote_interface.clear;

		control_interface.supervisorPagesEnabled = control_supervisorPagesEnabled;
		control_interface.userPagesEnabled = control_userPagesEnabled;
		control_interface.virtualMemoryEnabled = control_virtualMemoryEnabled;
		control_interface.rootPageTable = control_rootPageTable;
	end

	Memory_SV memory_sv(
		.instruction(instruction_interface.Memory),
		.data(data_interface.Memory),
		.dram(dram_interface.Memory),
		.vram(vram_interface.Requester),
		.remote(remote_interface.Requester),
		.control(control_interface.Memory),
		.clock(clock),
		.clear(clear)
	);

endmodule

module TranslationLookasideBuffer (
	instruction_virtualTag,
	instruction_virtualTagValid,
	instruction_flushMode,
	instruction_translation,
	data_virtualTag,
	data_virtualTagValid,
	data_flushMode,
	data_translation,
	translationFill_newTranslation,
	translationFill_instructionResponse,
	translationFill_dataResponse,
	clock,
	clear
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] INVALIDATE_PAGE = 32'd1;
	localparam [31:0] INVALIDATE_ALL = 32'd2;
	input wire [19:0] instruction_virtualTag;
	input wire instruction_virtualTagValid;
	input wire [31:0] instruction_flushMode;
	output wire [51:0] instruction_translation;
	input wire [19:0] data_virtualTag;
	input wire data_virtualTagValid;
	input wire [31:0] data_flushMode;
	output wire [51:0] data_translation;
	input wire [51:0] translationFill_newTranslation;
	output wire [51:0] translationFill_instructionResponse;
	output wire [51:0] translationFill_dataResponse;
	input wire clock;
	input wire clear;
	reg [767:0] translationEntries;
	TranslationBlock instructionBlock(
		.request_virtualTag(instruction_virtualTag),
		.request_virtualTagValid(instruction_virtualTagValid),
		.request_flushMode(instruction_flushMode),
		.request_translation(instruction_translation),
		.translationEntries(translationEntries),
		.clock(clock),
		.clear(clear)
	);
	TranslationBlock dataBlock(
		.request_virtualTag(data_virtualTag),
		.request_virtualTagValid(data_virtualTagValid),
		.request_flushMode(data_flushMode),
		.request_translation(data_translation),
		.translationEntries(translationEntries),
		.clock(clock),
		.clear(clear)
	);
	// removed an assertion item
	assign translationFill_dataResponse = data_translation;
	assign translationFill_instructionResponse = instruction_translation;
	wire [31:0] dataFlushMode_last;
	Register #(.WIDTH(32)) dataFlushModeRegister(
		.q(dataFlushMode_last),
		.d(data_flushMode),
		.enable(1'b1),
		.clock(clock),
		.clear(clear)
	);
	always @(posedge clock) begin
		if (clear) translationEntries <= {16 {{20'd0, 22'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0}}};
		else begin
			if ((data_flushMode == INVALIDATE_ALL)) translationEntries <= {16 {{20'd0, 22'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0}}};
			else if (translationFill_newTranslation[9:9]) translationEntries[(translationFill_newTranslation[3:0] * 48)+:48] <= translationFill_newTranslation[51:4];
			else if ((data_translation[9:9] && (dataFlushMode_last == INVALIDATE_PAGE))) begin
				translationEntries[(data_translation[3:0] * 48)+:48] <= {20'd0, 22'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0, 1'd0};
			end
		end
	end
endmodule

module TranslationBlock (
	request_virtualTag,
	request_virtualTagValid,
	request_flushMode,
	request_translation,
	translationEntries,
	clock,
	clear
);
	input logic [19:0] request_virtualTag;
	input logic request_virtualTagValid;
	input logic [31:0] request_flushMode;
	output logic [51:0] request_translation;
	input logic [767:0] translationEntries;
	input logic clock;
	input logic clear;

	TranslationInterface ti();
	always_comb begin
		ti.virtualTag = request_virtualTag;
		ti.virtualTagValid = request_virtualTagValid;
		ti.flushMode = request_flushMode;
		request_translation = ti.translation;
	end

	TranslationBlock_SV tb(
		.request(ti.TLB),
		.translationEntries(translationEntries),
		.clock(clock),
		.clear(clear)
	);

endmodule


module TranslationBlock_SV(
    TranslationInterface.TLB request,
    input TranslationEntry_t [`TLB_ENTRIES-1:0] translationEntries,
    input logic clock, clear
);

    // First we go through all of the entries in the TLB, if an entry is valid
    // then we check if the tag matches the tag we are trying to match again, if
    // it does, then we know that
    TranslationResponse_t[`TLB_ENTRIES/2-1:0] subResults, subResults_sync;

    generate
        genvar i;
        for(i = 0; i < `TLB_ENTRIES; i = i + 2) begin : translation_check
            always_comb begin
                unique if(translationEntries[i].isValid && (translationEntries[i].virtualTag == request.virtualTag))
                    subResults[i/2] = '{
                        entry:  translationEntries[i],
                        index: i
                    };
                else if(translationEntries[i+1].isValid && (translationEntries[i+1].virtualTag == request.virtualTag))
                    subResults[i/2] = '{
                        entry: translationEntries[i+1],
                        index: i+1
                    };
                else
                    subResults[i/2] = '{default:0};
            end
        end
    endgenerate

    Register #(.WIDTH($bits(subResults))) subResultsRegister(
        .q(subResults_sync),
        .d(subResults),
        .clock,
        .clear(clear || !request.virtualTagValid),
        .enable(1'b1)
    );


    ArrayOrReduction #(.WIDTH($bits(TranslationResponse_t)), .SIZE(`TLB_ENTRIES/2)) reduction(
        .inputs(subResults_sync),
        .result(request.translation)
    );

endmodule

// removed typedef: MemorySelect_t
// removed typedef: MemoryLock_t
// removed typedef: MemoryLock_t
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
// removed typedef: MemoryMode_t
// removed typedef: PageOffset_t
// removed typedef: VirtualPageNumber_t
// removed typedef: PhysicalPageNumber_t
// removed typedef: TableWalkResult_t
// removed typedef: PageTableEntry_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: PageOffset_t
// removed typedef: VirtualPageNumber_t
// removed typedef: PhysicalPageNumber_t
// removed typedef: TableWalkResult_t
// removed typedef: PageTableEntry_t
// removed typedef: MemorySelect_t
// removed typedef: MemoryLock_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t

// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module Remote (
	core_csrName,
	core_registerIndex,
	core_readCSR,
	core_readRegister,
	core_pc,
	core_coreHalt,
	core_coreDebug,
	core_halted,
	core_dump,
	dram_readError,
	dram_writeError,
	dram_writeEnableMissing,
	dram_index,
	dram_writeArray,
	dram_addressBase,
	timer_halted,
	timer_currentTime,
	timer_nextTickTime,
	basicIO_switch,
	basicIO_button,
	basicIO_led,
	basicIO_rgb_led,
	axiRead_request,
	axiRead_response,
	axiRead_clock,
	axiRead_clear,
	axiWrite_request,
	axiWrite_response,
	axiWrite_clock,
	axiWrite_clear,
	memory_request,
	memory_response,
	memory_clock,
	memory_clear,
	dump,
	clock,
	clear
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] PAGE_FAULT = 32'd5;
	output reg [(12 - 1):0] core_csrName;
	output reg [($clog2(32) - 1):0] core_registerIndex;
	input wire [(32 - 1):0] core_readCSR;
	input wire [(32 - 1):0] core_readRegister;
	input wire [(32 - 1):0] core_pc;
	input wire core_coreHalt;
	input wire core_coreDebug;
	output wire core_halted;
	output wire core_dump;
	input wire dram_readError;
	input wire dram_writeError;
	input wire dram_writeEnableMissing;
	input wire [5:0] dram_index;
	input wire [511:0] dram_writeArray;
	input wire [33:0] dram_addressBase;
	output wire timer_halted;
	input wire [63:0] timer_currentTime;
	input wire [63:0] timer_nextTickTime;
	input wire [1:0] basicIO_switch;
	input wire [3:0] basicIO_button;
	output wire [3:0] basicIO_led;
	output wire [5:0] basicIO_rgb_led;
	input wire [70:0] axiRead_request;
	output reg [103:0] axiRead_response;
	input wire axiRead_clock;
	input wire axiRead_clear;
	input wire [70:0] axiWrite_request;
	output wire [103:0] axiWrite_response;
	input wire axiWrite_clock;
	input wire axiWrite_clear;
	input wire [70:0] memory_request;
	output wire [103:0] memory_response;
	input wire memory_clock;
	input wire memory_clear;
	output wire dump;
	input wire clock;
	input wire clear;
	function automatic [33:0] translateVirtualAddress;
		input reg [(32 - 1):0] virtualAddress;
		input reg [47:0] translation;
		translateVirtualAddress = {translation[27:6], virtualAddress[11:0]};
	endfunction
	function automatic [113:0] addressTranslationResponse;
		input reg [(32 - 1):0] virtualAddress;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		reg [33:0] physicalAddress;
		begin
			physicalAddress = translateVirtualAddress(virtualAddress, translation);
			addressTranslationResponse = {physicalAddress, translationType, translation};
		end
	endfunction
	function automatic [278:0] memoryResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		input reg [31:0] responseType;
		input reg [(32 - 1):0] readData;
		reg [113:0] addressTranslation;
		begin
			addressTranslation = addressTranslationResponse(request[95:64], translationType, translation);
			memoryResponse = {responseType, request, addressTranslation, readData};
		end
	endfunction
	function automatic [278:0] memoryFaultResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		memoryFaultResponse = memoryResponse(request, translationType, translation, PAGE_FAULT, 32'hBADF00D);
	endfunction
	function automatic [70:0] simpleMemoryRead;
		input reg [33:0] physicalAddress;
		simpleMemoryRead = {physicalAddress, 1'b1, 4'b0, 32'hBADF00D};
	endfunction
	function automatic isInVRAM;
		input reg [33:0] address;
		isInVRAM = ((32'h1000 <= address) && (address < (32'h1000 + 'd4096)));
	endfunction
	function automatic isInRemote;
		input reg [33:0] address;
		isInRemote = ((32'h2000 <= address) && (address < (32'h2000 + 'd4096)));
	endfunction
	function automatic [70:0] simpleMemoryWrite;
		input reg [33:0] physicalAddress;
		input reg [3:0] writeEnable;
		input reg [(32 - 1):0] writeData;
		simpleMemoryWrite = {physicalAddress, 1'b0, writeEnable, writeData};
	endfunction
	function automatic [100:0] memoryReadRequest;
		input reg [(32 - 1):0] address;
		input reg readEnable;
		memoryReadRequest = {readEnable, 4'b0, address, 32'hBADF00D, KEEP};
	endfunction
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// removed an assertion item
	// expanded instance: axiMemory
	wire axiMemory_clock;
	wire axiMemory_clear;
	reg [70:0] axiMemory_request;
	wire [103:0] axiMemory_response;
	// removed modport Requester
	// removed modport Device
	RemoteMemory remoteMemory(
		.axi_request(axiMemory_request),
		.axi_response(axiMemory_response),
		.axi_clock(axiMemory_clock),
		.axi_clear(axiMemory_clear),
		.memory_request(memory_request),
		.memory_response(memory_response),
		.memory_clock(memory_clock),
		.memory_clear(memory_clear),
		.clock(clock),
		.clear(clear)
	);
	reg halted_n;
	reg debug_n;
	reg [31:0] errorCode;
	reg [(32 - 1):0] nonMemoryResult;
	assign core_halted = (~ halted_n);
	assign timer_halted = (~ halted_n);
	assign core_dump = dump;
	always @(*) begin
		core_csrName = axiRead_request[50:39];
		core_registerIndex = axiRead_request[43:39];
		nonMemoryResult = 32'b0;
		casez (axiRead_request[52:49])
			4'h0: begin
				casez (axiRead_request[48:39])
					10'h1: nonMemoryResult = {30'b0, (~ debug_n), (~ halted_n)};
					10'h2: nonMemoryResult = errorCode;
					10'h4: nonMemoryResult = core_pc;
					10'h10: nonMemoryResult = timer_currentTime[31:0];
					10'h11: nonMemoryResult = timer_currentTime[63:32];
					10'h12: nonMemoryResult = timer_nextTickTime[31:0];
					10'h13: nonMemoryResult = timer_nextTickTime[63:32];
					10'b10?????: nonMemoryResult = core_readRegister;
					10'h60: nonMemoryResult = dram_addressBase;
					10'h61: nonMemoryResult = {26'b0, dram_index};
					10'b111????: begin
						nonMemoryResult = dram_writeArray[(axiRead_request[42:39] * 32)+:32];
					end
				endcase
			end
			4'h2: begin
				nonMemoryResult = 32'hBADF00D;
			end
			4'b11??: begin
				nonMemoryResult = core_readCSR;
			end
		endcase
	end
	assign basicIO_led = {clear, (~ clear), (~ halted_n), (~ debug_n)};
	assign basicIO_rgb_led[0+:3] = errorCode[2:0];
	assign basicIO_rgb_led[3+:3] = {(~ halted_n), halted_n, 1'b0};
	reg oneShotDump;
	always @(posedge clock) begin
		if (clear) begin
			halted_n <= 1'b0;
			debug_n <= 1'b1;
			errorCode <= 32'b0;
			oneShotDump <= 1'b0;
		end
		else begin
			if (dram_readError) errorCode[5'd1] <= 1'b1;
			if (dram_writeError) errorCode[5'd0] <= 1'b1;
			if (dram_writeEnableMissing) errorCode[5'd2] <= 1'b1;
			if ((axiWrite_request[32] && (axiWrite_request[52:37] == {4'h0, 10'h1, 2'b0}))) begin
				{debug_n, halted_n} <= (~ axiWrite_request[1:0]);
			end
			else begin
				if ((core_coreHalt || core_coreDebug)) halted_n <= 1'b0;
				if (core_coreDebug) begin
					debug_n <= 1'b0;
					halted_n <= 1'b0;
				end
			end
			if (core_coreHalt) begin
				oneShotDump <= 1'b1;
			end
		end
	end
	assign dump = (core_coreHalt && (! oneShotDump));
	always @(*) begin
		axiMemory_request = {34'd0, 1'd0, 4'd0, 32'd0};
		if (axiRead_request[36:36]) begin
			axiMemory_request = simpleMemoryRead((32'h2000 + {axiRead_request[48:39], 2'b0}));
		end
		else if (axiWrite_request[35:32]) begin
			axiMemory_request = simpleMemoryWrite((32'h2000 + {axiWrite_request[48:39], 2'b0}), 4'hf, axiWrite_request[31:0]);
		end
	end
	assign axiMemory_clock = clock;
	assign axiMemory_clear = clear;
	wire [(32 - 1):0] nonMemoryResult_sync;
	Register #(.WIDTH(32)) nonMemoryResultRegister(
		.d(nonMemoryResult),
		.q(nonMemoryResult_sync),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	wire [70:0] axiRead_saved;
	Register #(.WIDTH(71)) axiReadRegister(
		.d(axiRead_request),
		.q(axiRead_saved),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	wire [70:0] axiWrite_saved;
	Register #(.WIDTH(71)) axiWriteRegister(
		.d(axiWrite_request),
		.q(axiWrite_saved),
		.clock(clock),
		.clear(clear),
		.enable(1'b1)
	);
	assign axiWrite_response = {axiWrite_saved, 32'hBADF00D, (| axiWrite_saved[35:32])};
	always @(*) begin
		if ((axiRead_saved[52:49] == 4'h2)) begin
			axiRead_response = axiMemory_response;
		end
		else begin
			axiRead_response[103:33] = axiRead_saved;
			axiRead_response[0:0] = axiRead_saved[36:36];
			axiRead_response[32:1] = nonMemoryResult_sync;
		end
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module RemoteAXIDriver (
	remoteRead_request,
	remoteRead_response,
	remoteRead_clock,
	remoteRead_clear,
	remoteWrite_request,
	remoteWrite_response,
	remoteWrite_clock,
	remoteWrite_clear,
	S_AXI_ACLK,
	S_AXI_ARESETN,
	S_AXI_AWADDR,
	S_AXI_AWPROT,
	S_AXI_AWVALID,
	S_AXI_AWREADY,
	S_AXI_WDATA,
	S_AXI_WSTRB,
	S_AXI_WVALID,
	S_AXI_WREADY,
	S_AXI_BRESP,
	S_AXI_BVALID,
	S_AXI_BREADY,
	S_AXI_ARADDR,
	S_AXI_ARPROT,
	S_AXI_ARVALID,
	S_AXI_ARREADY,
	S_AXI_RDATA,
	S_AXI_RRESP,
	S_AXI_RVALID,
	S_AXI_RREADY
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] PAGE_FAULT = 32'd5;
	parameter integer C_S_AXI_DATA_WIDTH = 32;
	parameter integer C_S_AXI_ADDR_WIDTH = 16;
	output reg [70:0] remoteRead_request;
	input wire [103:0] remoteRead_response;
	output wire remoteRead_clock;
	output wire remoteRead_clear;
	output reg [70:0] remoteWrite_request;
	input wire [103:0] remoteWrite_response;
	output wire remoteWrite_clock;
	output wire remoteWrite_clear;
	input wire S_AXI_ACLK;
	input wire S_AXI_ARESETN;
	input wire [(C_S_AXI_ADDR_WIDTH - 1):0] S_AXI_AWADDR;
	input wire [2:0] S_AXI_AWPROT;
	input wire S_AXI_AWVALID;
	output wire S_AXI_AWREADY;
	input wire [(C_S_AXI_DATA_WIDTH - 1):0] S_AXI_WDATA;
	input wire [((C_S_AXI_DATA_WIDTH / 8) - 1):0] S_AXI_WSTRB;
	input wire S_AXI_WVALID;
	output wire S_AXI_WREADY;
	output wire [1:0] S_AXI_BRESP;
	output wire S_AXI_BVALID;
	input wire S_AXI_BREADY;
	input wire [(C_S_AXI_ADDR_WIDTH - 1):0] S_AXI_ARADDR;
	input wire [2:0] S_AXI_ARPROT;
	input wire S_AXI_ARVALID;
	output wire S_AXI_ARREADY;
	output wire [(C_S_AXI_DATA_WIDTH - 1):0] S_AXI_RDATA;
	output wire [1:0] S_AXI_RRESP;
	output wire S_AXI_RVALID;
	input wire S_AXI_RREADY;
	function automatic [33:0] translateVirtualAddress;
		input reg [(32 - 1):0] virtualAddress;
		input reg [47:0] translation;
		translateVirtualAddress = {translation[27:6], virtualAddress[11:0]};
	endfunction
	function automatic [113:0] addressTranslationResponse;
		input reg [(32 - 1):0] virtualAddress;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		reg [33:0] physicalAddress;
		begin
			physicalAddress = translateVirtualAddress(virtualAddress, translation);
			addressTranslationResponse = {physicalAddress, translationType, translation};
		end
	endfunction
	function automatic [278:0] memoryResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		input reg [31:0] responseType;
		input reg [(32 - 1):0] readData;
		reg [113:0] addressTranslation;
		begin
			addressTranslation = addressTranslationResponse(request[95:64], translationType, translation);
			memoryResponse = {responseType, request, addressTranslation, readData};
		end
	endfunction
	function automatic [278:0] memoryFaultResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		memoryFaultResponse = memoryResponse(request, translationType, translation, PAGE_FAULT, 32'hBADF00D);
	endfunction
	function automatic [70:0] simpleMemoryRead;
		input reg [33:0] physicalAddress;
		simpleMemoryRead = {physicalAddress, 1'b1, 4'b0, 32'hBADF00D};
	endfunction
	function automatic isInVRAM;
		input reg [33:0] address;
		isInVRAM = ((32'h1000 <= address) && (address < (32'h1000 + 'd4096)));
	endfunction
	function automatic isInRemote;
		input reg [33:0] address;
		isInRemote = ((32'h2000 <= address) && (address < (32'h2000 + 'd4096)));
	endfunction
	function automatic [70:0] simpleMemoryWrite;
		input reg [33:0] physicalAddress;
		input reg [3:0] writeEnable;
		input reg [(32 - 1):0] writeData;
		simpleMemoryWrite = {physicalAddress, 1'b0, writeEnable, writeData};
	endfunction
	function automatic [100:0] memoryReadRequest;
		input reg [(32 - 1):0] address;
		input reg readEnable;
		memoryReadRequest = {readEnable, 4'b0, address, 32'hBADF00D, KEEP};
	endfunction
	reg [(C_S_AXI_ADDR_WIDTH - 1):0] axi_awaddr;
	reg axi_awready;
	reg axi_wready;
	reg [1:0] axi_bresp;
	reg axi_bvalid;
	reg [(C_S_AXI_ADDR_WIDTH - 1):0] axi_araddr;
	reg axi_arready;
	wire [(C_S_AXI_DATA_WIDTH - 1):0] axi_rdata;
	reg [1:0] axi_rresp;
	reg axi_rvalid;
	wire slv_reg_rden;
	wire slv_reg_wren;
	wire [(C_S_AXI_DATA_WIDTH - 1):0] reg_data_out;
	reg aw_en;
	assign S_AXI_AWREADY = axi_awready;
	assign S_AXI_WREADY = axi_wready;
	assign S_AXI_BRESP = axi_bresp;
	assign S_AXI_BVALID = axi_bvalid;
	assign S_AXI_ARREADY = axi_arready;
	assign S_AXI_RDATA = axi_rdata;
	assign S_AXI_RRESP = axi_rresp;
	assign S_AXI_RVALID = axi_rvalid;
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			axi_awready <= 1'b0;
			aw_en <= 1'b1;
		end
		else begin
			if (((((~ axi_awready) && S_AXI_AWVALID) && S_AXI_WVALID) && aw_en)) begin
				axi_awready <= 1'b1;
				aw_en <= 1'b0;
			end
			else if ((S_AXI_BREADY && axi_bvalid)) begin
				aw_en <= 1'b1;
				axi_awready <= 1'b0;
			end
			else begin
				axi_awready <= 1'b0;
			end
		end
	end
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			axi_awaddr <= 0;
		end
		else begin
			if (((((~ axi_awready) && S_AXI_AWVALID) && S_AXI_WVALID) && aw_en)) begin
				axi_awaddr <= S_AXI_AWADDR;
			end
		end
	end
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			axi_wready <= 1'b0;
		end
		else begin
			if (((((~ axi_wready) && S_AXI_WVALID) && S_AXI_AWVALID) && aw_en)) begin
				axi_wready <= 1'b1;
			end
			else begin
				axi_wready <= 1'b0;
			end
		end
	end
	assign slv_reg_wren = (((axi_wready && S_AXI_WVALID) && axi_awready) && S_AXI_AWVALID);
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			remoteWrite_request = {34'd0, 1'd0, 4'd0, 32'd0};
		end
		else begin
			if (slv_reg_wren) begin
				remoteWrite_request = simpleMemoryWrite({18'b0, axi_awaddr}, S_AXI_WSTRB, S_AXI_WDATA);
			end
			else begin
				remoteWrite_request = {34'd0, 1'd0, 4'd0, 32'd0};
			end
		end
	end
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			axi_bvalid <= 0;
			axi_bresp <= 2'b0;
		end
		else begin
			if (((((axi_awready && S_AXI_AWVALID) && (~ axi_bvalid)) && axi_wready) && S_AXI_WVALID)) begin
				axi_bvalid <= 1'b1;
				axi_bresp <= 2'b0;
			end
			else begin
				if ((S_AXI_BREADY && axi_bvalid)) begin
					axi_bvalid <= 1'b0;
				end
			end
		end
	end
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			axi_arready <= 1'b0;
			axi_araddr <= 32'b0;
		end
		else begin
			if (((~ axi_arready) && S_AXI_ARVALID)) begin
				axi_arready <= 1'b1;
				axi_araddr <= S_AXI_ARADDR;
			end
			else begin
				axi_arready <= 1'b0;
			end
		end
	end
	always @(posedge S_AXI_ACLK) begin
		if ((S_AXI_ARESETN == 1'b0)) begin
			axi_rvalid <= 0;
			axi_rresp <= 0;
		end
		else begin
			if (((axi_arready && S_AXI_ARVALID) && (~ axi_rvalid))) begin
				axi_rvalid <= 1'b1;
				axi_rresp <= 2'b0;
			end
			else if ((axi_rvalid && S_AXI_RREADY)) begin
				axi_rvalid <= 1'b0;
			end
		end
	end
	always @(*) begin
		remoteRead_request = simpleMemoryRead(axi_araddr);
	end
	assign axi_rdata = remoteRead_response[32:1];
	assign remoteRead_clock = 1'b0;
	assign remoteRead_clear = 1'b1;
	assign remoteWrite_clock = 1'b0;
	assign remoteWrite_clear = 1'b1;
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module RemoteMemory (
	axi_request,
	axi_response,
	axi_clock,
	axi_clear,
	memory_request,
	memory_response,
	memory_clock,
	memory_clear,
	clock,
	clear
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] PAGE_FAULT = 32'd5;
	input wire [70:0] axi_request;
	output reg [103:0] axi_response;
	input wire axi_clock;
	input wire axi_clear;
	input wire [70:0] memory_request;
	output reg [103:0] memory_response;
	input wire memory_clock;
	input wire memory_clear;
	input wire clock;
	input wire clear;
	function automatic [33:0] translateVirtualAddress;
		input reg [(32 - 1):0] virtualAddress;
		input reg [47:0] translation;
		translateVirtualAddress = {translation[27:6], virtualAddress[11:0]};
	endfunction
	function automatic [113:0] addressTranslationResponse;
		input reg [(32 - 1):0] virtualAddress;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		reg [33:0] physicalAddress;
		begin
			physicalAddress = translateVirtualAddress(virtualAddress, translation);
			addressTranslationResponse = {physicalAddress, translationType, translation};
		end
	endfunction
	function automatic [278:0] memoryResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		input reg [31:0] responseType;
		input reg [(32 - 1):0] readData;
		reg [113:0] addressTranslation;
		begin
			addressTranslation = addressTranslationResponse(request[95:64], translationType, translation);
			memoryResponse = {responseType, request, addressTranslation, readData};
		end
	endfunction
	function automatic [278:0] memoryFaultResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		memoryFaultResponse = memoryResponse(request, translationType, translation, PAGE_FAULT, 32'hBADF00D);
	endfunction
	function automatic [70:0] simpleMemoryRead;
		input reg [33:0] physicalAddress;
		simpleMemoryRead = {physicalAddress, 1'b1, 4'b0, 32'hBADF00D};
	endfunction
	function automatic isInVRAM;
		input reg [33:0] address;
		isInVRAM = ((32'h1000 <= address) && (address < (32'h1000 + 'd4096)));
	endfunction
	function automatic isInRemote;
		input reg [33:0] address;
		isInRemote = ((32'h2000 <= address) && (address < (32'h2000 + 'd4096)));
	endfunction
	function automatic [70:0] simpleMemoryWrite;
		input reg [33:0] physicalAddress;
		input reg [3:0] writeEnable;
		input reg [(32 - 1):0] writeData;
		simpleMemoryWrite = {physicalAddress, 1'b0, writeEnable, writeData};
	endfunction
	function automatic [100:0] memoryReadRequest;
		input reg [(32 - 1):0] address;
		input reg readEnable;
		memoryReadRequest = {readEnable, 4'b0, address, 32'hBADF00D, KEEP};
	endfunction
	generate
		if ((! ('d4096 == 'd4096))) begin
			DoesNotExist foo();
		end
	endgenerate
	generate
		if ((! ('d4096 == ((2 ** 10) * (32 / 8))))) begin
			DoesNotExist foo();
		end
	endgenerate
	// removed an assertion item
	// removed an assertion item
	localparam NB_COL = 4;
	localparam COL_WIDTH = 8;
	generate
		if ((! (32 == (NB_COL * COL_WIDTH)))) begin
			DoesNotExist foo();
		end
	endgenerate
	xilinx_bram_byte_write #(
		.NB_COL(NB_COL),
		.COL_WIDTH(COL_WIDTH),
		.RAM_DEPTH(1024)
	) remoteBRAM(
		.addra(memory_request[48:39]),
		.dina(memory_request[31:0]),
		.wea(memory_request[35:32]),
		.douta(memory_response[32:1]),
		.addrb(axi_request[48:39]),
		.dinb(axi_request[31:0]),
		.web(axi_request[35:32]),
		.doutb(axi_response[32:1]),
		.clka(clock)
	);
	wire [70:0] memoryRequest_saved;
	wire [70:0] axiRequest_saved;
	Register #(.WIDTH(71)) memoryRequestRegister(
		.q(memoryRequest_saved),
		.d(memory_request),
		.enable(1'b1),
		.clock(clock),
		.clear(clear)
	);
	Register #(.WIDTH(71)) axiRequestRegister(
		.q(axiRequest_saved),
		.d(axi_request),
		.enable(1'b1),
		.clock(clock),
		.clear(clear)
	);
	always @(*) begin
		memory_response[103:33] = memoryRequest_saved;
		axi_response[103:33] = axiRequest_saved;
		memory_response[0:0] = isInRemote(memoryRequest_saved[70:37]);
		axi_response[0:0] = isInRemote(axiRequest_saved[70:37]);
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: opcode_t
// removed typedef: funct7_t
// removed typedef: rtype_funct3_t
// removed typedef: rtype_m_funct3_t
// removed typedef: itype_int_funct3_t
// removed typedef: itype_load_funct3_t
// removed typedef: itype_funct12_t
// removed typedef: itype_system_funct3_t
// removed typedef: stype_funct3_t
// removed typedef: amo_funct5_t
// removed typedef: sbtype_funct3_t
// removed typedef: isa_reg_t
// removed typedef: Register_t
// removed typedef: RegisterResponse_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
module TimerController (
	interruptController_tval,
	interruptController_interruptPending,
	interruptController_interruptAccepted,
	remote_halted,
	remote_currentTime,
	remote_nextTickTime,
	clock,
	clear
);
	output wire [(32 - 1):0] interruptController_tval;
	output wire interruptController_interruptPending;
	input wire interruptController_interruptAccepted;
	input wire remote_halted;
	output wire [63:0] remote_currentTime;
	output wire [63:0] remote_nextTickTime;
	input wire clock;
	input wire clear;
	wire [63:0] nextTimerInterrupt;
	wire [63:0] currentTime;
	Counter #(.WIDTH(64)) timerCounter(
		.clock(clock),
		.clear(clear),
		.enable((~ remote_halted)),
		.q(currentTime)
	);
	Register #(
		.WIDTH(64),
		.CLEAR_VALUE(32'd1000000)
	) nextTickRegister(
		.q(nextTimerInterrupt),
		.d((nextTimerInterrupt + 32'd1000000)),
		.clock(clock),
		.clear(clear),
		.enable(interruptController_interruptAccepted)
	);
	wire [(32 - 1):0] tickCount;
	Counter #(.WIDTH(32)) tickCounter(
		.clock(clock),
		.clear(clear),
		.enable(interruptController_interruptAccepted),
		.q(tickCount)
	);
	assign interruptController_interruptPending = (currentTime >= nextTimerInterrupt);
	assign interruptController_tval = tickCount;
	assign remote_currentTime = currentTime;
	assign remote_nextTickTime = nextTimerInterrupt;
endmodule

module charmap (
	clk,
	ascii_code,
	row,
	col,
	pixel
);
	input wire clk;
	input wire [6:0] ascii_code;
	input wire [3:0] row;
	input wire [2:0] col;
	output wire pixel;
	RAMB16_S1 BRAM_PC_VGA_0(
		.CLK(clk),
		.EN(1'b1),
		.WE(1'b0),
		.ADDR({ascii_code[6:1], (~ ascii_code[0]), row, (~ col)}),
		.SSR(1'b0),
		.DI(1'b0),
		.DO(pixel)
	);
	defparam BRAM_PC_VGA_0.INIT_00 = 256'h00000000000000000000000000000000000000007e818199bd8181a5817e0000;
	defparam BRAM_PC_VGA_0.INIT_01 = 256'h000000007effffe7c3ffffdbff7e00000000000010387cfefefefe6c00000000;
	defparam BRAM_PC_VGA_0.INIT_02 = 256'h000000000010387cfe7c381000000000000000003c1818e7e7e73c3c18000000;
	defparam BRAM_PC_VGA_0.INIT_03 = 256'h000000003c18187effff7e3c18000000000000000000183c3c18000000000000;
	defparam BRAM_PC_VGA_0.INIT_04 = 256'hffffffffffffe7c3c3e7ffffffffffff00000000003c664242663c0000000000;
	defparam BRAM_PC_VGA_0.INIT_05 = 256'hffffffffffc399bdbd99c3ffffffffff0000000078cccccccc78321a0e1e0000;
	defparam BRAM_PC_VGA_0.INIT_06 = 256'h0000000018187e183c666666663c000000000000e0f070303030303f333f0000;
	defparam BRAM_PC_VGA_0.INIT_07 = 256'h000000c0e6e767636363637f637f0000000000001818db3ce73cdb1818000000;
	defparam BRAM_PC_VGA_0.INIT_08 = 256'h0000000080c0e0f0f8fef8f0e0c080000000000002060e1e3efe3e1e0e060200;
	defparam BRAM_PC_VGA_0.INIT_09 = 256'h0000000000183c7e1818187e3c18000000000000666600666666666666660000;
	defparam BRAM_PC_VGA_0.INIT_0A = 256'h000000001b1b1b1b1b7bdbdbdb7f00000000007cc60c386cc6c66c3860c67c00;
	defparam BRAM_PC_VGA_0.INIT_0B = 256'h00000000fefefefe0000000000000000000000007e183c7e1818187e3c180000;
	defparam BRAM_PC_VGA_0.INIT_0C = 256'h00000000181818181818187e3c18000000000000183c7e181818181818180000;
	defparam BRAM_PC_VGA_0.INIT_0D = 256'h000000000000180cfe0c1800000000000000000000003060fe60300000000000;
	defparam BRAM_PC_VGA_0.INIT_0E = 256'h000000000000fec0c0c00000000000000000000000002466ff66240000000000;
	defparam BRAM_PC_VGA_0.INIT_0F = 256'h0000000000fefe7c7c3838100000000000000000001038387c7cfefe00000000;
	defparam BRAM_PC_VGA_0.INIT_10 = 256'h00000000000000000000000000000000000000001818001818183c3c3c180000;
	defparam BRAM_PC_VGA_0.INIT_11 = 256'h00000000000000000000002466666600000000006c6cfe6c6c6cfe6c6c000000;
	defparam BRAM_PC_VGA_0.INIT_12 = 256'h000018187cc68606067cc0c2c67c18180000000086c66030180cc6c200000000;
	defparam BRAM_PC_VGA_0.INIT_13 = 256'h0000000076ccccccdc76386c6c38000000000000000000000000006030303000;
	defparam BRAM_PC_VGA_0.INIT_14 = 256'h000000000c18303030303030180c00000000000030180c0c0c0c0c0c18300000;
	defparam BRAM_PC_VGA_0.INIT_15 = 256'h000000000000663cff3c66000000000000000000000018187e18180000000000;
	defparam BRAM_PC_VGA_0.INIT_16 = 256'h0000003018181800000000000000000000000000000000007e00000000000000;
	defparam BRAM_PC_VGA_0.INIT_17 = 256'h000000001818000000000000000000000000000080c06030180c060200000000;
	defparam BRAM_PC_VGA_0.INIT_18 = 256'h000000007cc6c6e6f6decec6c67c0000000000007e1818181818187838180000;
	defparam BRAM_PC_VGA_0.INIT_19 = 256'h00000000fec6c06030180c06c67c0000000000007cc60606063c0606c67c0000;
	defparam BRAM_PC_VGA_0.INIT_1A = 256'h000000001e0c0c0cfecc6c3c1c0c0000000000007cc6060606fcc0c0c0fe0000;
	defparam BRAM_PC_VGA_0.INIT_1B = 256'h000000007cc6c6c6c6fcc0c0603800000000000030303030180c0606c6fe0000;
	defparam BRAM_PC_VGA_0.INIT_1C = 256'h000000007cc6c6c6c67cc6c6c67c000000000000780c0606067ec6c6c67c0000;
	defparam BRAM_PC_VGA_0.INIT_1D = 256'h0000000000181800000018180000000000000000301818000000181800000000;
	defparam BRAM_PC_VGA_0.INIT_1E = 256'h00000000060c18306030180c06000000000000000000007e00007e0000000000;
	defparam BRAM_PC_VGA_0.INIT_1F = 256'h000000006030180c060c183060000000000000001818001818180cc6c67c0000;
	defparam BRAM_PC_VGA_0.INIT_20 = 256'h000000007cc0dcdededec6c6c67c000000000000c6c6c6c6fec6c66c38100000;
	defparam BRAM_PC_VGA_0.INIT_21 = 256'h00000000fc666666667c666666fc0000000000003c66c2c0c0c0c0c2663c0000;
	defparam BRAM_PC_VGA_0.INIT_22 = 256'h00000000f86c6666666666666cf8000000000000fe6662606878686266fe0000;
	defparam BRAM_PC_VGA_0.INIT_23 = 256'h00000000f06060606878686266fe0000000000003a66c6c6dec0c0c2663c0000;
	defparam BRAM_PC_VGA_0.INIT_24 = 256'h00000000c6c6c6c6c6fec6c6c6c60000000000003c18181818181818183c0000;
	defparam BRAM_PC_VGA_0.INIT_25 = 256'h0000000078cccccc0c0c0c0c0c1e000000000000e666666c78786c6666e60000;
	defparam BRAM_PC_VGA_0.INIT_26 = 256'h00000000fe6662606060606060f0000000000000c3c3c3c3c3dbffffe7c30000;
	defparam BRAM_PC_VGA_0.INIT_27 = 256'h00000000c6c6c6c6cedefef6e6c60000000000007cc6c6c6c6c6c6c6c67c0000;
	defparam BRAM_PC_VGA_0.INIT_28 = 256'h00000000f0606060607c666666fc000000000e0c7cded6c6c6c6c6c6c67c0000;
	defparam BRAM_PC_VGA_0.INIT_29 = 256'h00000000e66666666c7c666666fc0000000000007cc6c6060c3860c6c67c0000;
	defparam BRAM_PC_VGA_0.INIT_2A = 256'h000000003c18181818181899dbff0000000000007cc6c6c6c6c6c6c6c6c60000;
	defparam BRAM_PC_VGA_0.INIT_2B = 256'h00000000183c66c3c3c3c3c3c3c30000000000006666ffdbdbc3c3c3c3c30000;
	defparam BRAM_PC_VGA_0.INIT_2C = 256'h00000000c3c3663c18183c66c3c30000000000003c181818183c66c3c3c30000;
	defparam BRAM_PC_VGA_0.INIT_2D = 256'h00000000ffc3c16030180c86c3ff0000000000003c30303030303030303c0000;
	defparam BRAM_PC_VGA_0.INIT_2E = 256'h0000000002060e1c3870e0c080000000000000003c0c0c0c0c0c0c0c0c3c0000;
	defparam BRAM_PC_VGA_0.INIT_2F = 256'h000000000000000000000000c66c38100000ff00000000000000000000000000;
	defparam BRAM_PC_VGA_0.INIT_30 = 256'h000000000000000000000000001830300000000076cccccc7c0c780000000000;
	defparam BRAM_PC_VGA_0.INIT_31 = 256'h000000007c666666666c786060e00000000000007cc6c0c0c0c67c0000000000;
	defparam BRAM_PC_VGA_0.INIT_32 = 256'h0000000076cccccccc6c3c0c0c1c0000000000007cc6c0c0fec67c0000000000;
	defparam BRAM_PC_VGA_0.INIT_33 = 256'h00000000f060606060f060646c3800000078cc0c7ccccccccccc760000000000;
	defparam BRAM_PC_VGA_0.INIT_34 = 256'h00000000e666666666766c6060e00000000000003c1818181818380018180000;
	defparam BRAM_PC_VGA_0.INIT_35 = 256'h003c66660606060606060e000606000000000000e6666c78786c666060e00000;
	defparam BRAM_PC_VGA_0.INIT_36 = 256'h000000003c181818181818181838000000000000dbdbdbdbdbffe60000000000;
	defparam BRAM_PC_VGA_0.INIT_37 = 256'h00000000666666666666dc0000000000000000007cc6c6c6c6c67c0000000000;
	defparam BRAM_PC_VGA_0.INIT_38 = 256'h00f060607c6666666666dc0000000000001e0c0c7ccccccccccc760000000000;
	defparam BRAM_PC_VGA_0.INIT_39 = 256'h00000000f06060606676dc0000000000000000007cc60c3860c67c0000000000;
	defparam BRAM_PC_VGA_0.INIT_3A = 256'h000000001c3630303030fc30301000000000000076cccccccccccc0000000000;
	defparam BRAM_PC_VGA_0.INIT_3B = 256'h00000000183c66c3c3c3c300000000000000000066ffdbdbc3c3c30000000000;
	defparam BRAM_PC_VGA_0.INIT_3C = 256'h00000000c3663c183c66c3000000000000f80c067ec6c6c6c6c6c60000000000;
	defparam BRAM_PC_VGA_0.INIT_3D = 256'h00000000fec6603018ccfe0000000000000000000e18181818701818180e0000;
	defparam BRAM_PC_VGA_0.INIT_3E = 256'h000000001818181818001818181800000000000070181818180e181818700000;
	defparam BRAM_PC_VGA_0.INIT_3F = 256'h000000000000000000000000dc7600000000000000fec6c6c66c381000000000;
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module DisplayController (
	videoMemory_request,
	videoMemory_response,
	videoMemory_clock,
	videoMemory_clear,
	videoInternal_clock,
	videoInternal_clear,
	videoInternal_valid,
	videoInternal_ready,
	videoInternal_data,
	videoInternal_control
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	output wire [70:0] videoMemory_request;
	input wire [103:0] videoMemory_response;
	output wire videoMemory_clock;
	output wire videoMemory_clear;
	input wire videoInternal_clock;
	input wire videoInternal_clear;
	output wire videoInternal_valid;
	input wire videoInternal_ready;
	output wire [23:0] videoInternal_data;
	output wire [0:0] videoInternal_control;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	localparam CONSOLE_HOR_PIXELS = 640;
	localparam CONSOLE_VER_PIXELS = 400;
	localparam CONSOLE_X1 = ((SVO_HOR_PIXELS - CONSOLE_HOR_PIXELS) / 2);
	localparam CONSOLE_Y1 = ((SVO_VER_PIXELS - CONSOLE_VER_PIXELS) / 2);
	localparam CONSOLE_X2 = (CONSOLE_X1 + CONSOLE_HOR_PIXELS);
	localparam CONSOLE_Y2 = (CONSOLE_Y1 + CONSOLE_VER_PIXELS);
	wire display_border_tvalid;
	wire display_border_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] display_border_tdata;
	wire [2:0] display_border_tuser;
	svo_rect #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) display_border(
		.clk(videoInternal_clock),
		.resetn((~ videoInternal_clear)),
		.x1(14'd0),
		.y1(14'd0),
		.x2(svo_coordinate((SVO_HOR_PIXELS - 1))),
		.y2(svo_coordinate((SVO_VER_PIXELS - 1))),
		.border(svo_rgb(8'hFF, 8'h00, 8'h00)),
		.fill(svo_rgb(8'h00, 8'h00, 8'h00)),
		.out_axis_tvalid(display_border_tvalid),
		.out_axis_tready(display_border_tready),
		.out_axis_tdata(display_border_tdata),
		.out_axis_tuser(display_border_tuser)
	);
	wire console_border_tvalid;
	wire console_border_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] console_border_tdata;
	wire [2:0] console_border_tuser;
	svo_rect #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) console_border(
		.clk(videoInternal_clock),
		.resetn((~ videoInternal_clear)),
		.x1(svo_coordinate((CONSOLE_X1 - 1))),
		.y1(svo_coordinate((CONSOLE_Y1 - 1))),
		.x2(svo_coordinate(CONSOLE_X2)),
		.y2(svo_coordinate(CONSOLE_Y2)),
		.border(svo_rgb(8'h00, 8'hFF, 8'h00)),
		.fill(svo_rgb(8'hFF, 8'hFF, 8'hFF)),
		.out_axis_tvalid(console_border_tvalid),
		.out_axis_tready(console_border_tready),
		.out_axis_tdata(console_border_tdata),
		.out_axis_tuser(console_border_tuser)
	);
	wire video_border_tvalid;
	wire video_border_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] video_border_tdata;
	wire [0:0] video_border_tuser;
	svo_overlay #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) border_overlay(
		.clk(videoInternal_clock),
		.resetn((~ videoInternal_clear)),
		.enable(1'b1),
		.in_axis_tvalid(display_border_tvalid),
		.in_axis_tready(display_border_tready),
		.in_axis_tdata(display_border_tdata),
		.in_axis_tuser(display_border_tuser[0]),
		.over_axis_tvalid(console_border_tvalid),
		.over_axis_tready(console_border_tready),
		.over_axis_tdata(console_border_tdata),
		.over_axis_tuser({(| console_border_tuser[2:1]), console_border_tuser[0]}),
		.out_axis_tvalid(video_border_tvalid),
		.out_axis_tready(video_border_tready),
		.out_axis_tdata(video_border_tdata),
		.out_axis_tuser(video_border_tuser)
	);
	wire text_tvalid;
	wire text_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] text_tdata;
	wire [1:0] text_tuser;
	text_console #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL),
		.CONSOLE_HOR_PIXELS(CONSOLE_HOR_PIXELS),
		.CONSOLE_VER_PIXELS(CONSOLE_VER_PIXELS),
		.CONSOLE_ABS_X1(CONSOLE_X1),
		.CONSOLE_ABS_Y1(CONSOLE_Y1),
		.CONSOLE_ABS_X2(CONSOLE_X2),
		.CONSOLE_ABS_Y2(CONSOLE_Y2)
	) text_console(
		.videoMemory_request(videoMemory_request),
		.videoMemory_response(videoMemory_response),
		.videoMemory_clock(videoMemory_clock),
		.videoMemory_clear(videoMemory_clear),
		.clock(videoInternal_clock),
		.reset_n((~ videoInternal_clear)),
		.out_axis_tvalid(text_tvalid),
		.out_axis_tready(text_tready),
		.out_axis_tdata(text_tdata),
		.out_axis_tuser(text_tuser)
	);
	svo_overlay #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) final_overlay(
		.clk(videoInternal_clock),
		.resetn((~ videoInternal_clear)),
		.enable(1'b1),
		.in_axis_tvalid(video_border_tvalid),
		.in_axis_tready(video_border_tready),
		.in_axis_tdata(video_border_tdata),
		.in_axis_tuser(video_border_tuser[0]),
		.over_axis_tvalid(text_tvalid),
		.over_axis_tready(text_tready),
		.over_axis_tdata(text_tdata),
		.over_axis_tuser(text_tuser),
		.out_axis_tvalid(videoInternal_valid),
		.out_axis_tready(videoInternal_ready),
		.out_axis_tdata(videoInternal_data),
		.out_axis_tuser(videoInternal_control)
	);
endmodule
module HDMIDriver (
	internalDriver_clock,
	internalDriver_clear,
	internalDriver_valid,
	internalDriver_ready,
	internalDriver_data,
	internalDriver_control,
	tmds_clk_n,
	tmds_clk_p,
	tmds_d_n,
	tmds_d_p,
	clk_pixel,
	clk_5x_pixel,
	clk_pixel_resetn
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	output wire internalDriver_clock;
	output wire internalDriver_clear;
	input wire internalDriver_valid;
	output wire internalDriver_ready;
	input wire [23:0] internalDriver_data;
	input wire [0:0] internalDriver_control;
	output wire tmds_clk_n;
	output wire tmds_clk_p;
	output wire [2:0] tmds_d_n;
	output wire [2:0] tmds_d_p;
	input wire clk_pixel;
	input wire clk_5x_pixel;
	input wire clk_pixel_resetn;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	assign internalDriver_clock = clk_pixel;
	assign internalDriver_clear = (! clk_pixel_resetn);
	wire video_enc_tvalid;
	wire video_enc_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] video_enc_tdata;
	wire [3:0] video_enc_tuser;
	svo_enc #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) svo_enc(
		.clk(clk_pixel),
		.resetn(clk_pixel_resetn),
		.in_axis_tvalid(internalDriver_valid),
		.in_axis_tready(internalDriver_ready),
		.in_axis_tdata(internalDriver_data),
		.in_axis_tuser(internalDriver_control[0]),
		.out_axis_tvalid(video_enc_tvalid),
		.out_axis_tready(video_enc_tready),
		.out_axis_tdata(video_enc_tdata),
		.out_axis_tuser(video_enc_tuser)
	);
	assign video_enc_tready = 1;
	wire [2:0] tmds_d;
	wire [2:0] tmds_serdes_shift1;
	wire [2:0] tmds_serdes_shift2;
	wire [2:0] tmds_d0;
	wire [2:0] tmds_d1;
	wire [2:0] tmds_d2;
	wire [2:0] tmds_d3;
	wire [2:0] tmds_d4;
	wire [2:0] tmds_d5;
	wire [2:0] tmds_d6;
	wire [2:0] tmds_d7;
	wire [2:0] tmds_d8;
	wire [2:0] tmds_d9;
	OBUFDS tmds_bufds [3:0] (
		.I({clk_pixel, tmds_d}),
		.O({tmds_clk_p, tmds_d_p}),
		.OB({tmds_clk_n, tmds_d_n})
	);
	OSERDESE2 #(
		.DATA_RATE_OQ("DDR"),
		.DATA_RATE_TQ("SDR"),
		.DATA_WIDTH(10),
		.INIT_OQ(1'b0),
		.INIT_TQ(1'b0),
		.SERDES_MODE("MASTER"),
		.SRVAL_OQ(1'b0),
		.SRVAL_TQ(1'b0),
		.TBYTE_CTL("FALSE"),
		.TBYTE_SRC("FALSE"),
		.TRISTATE_WIDTH(1)
	) tmds_serdes_lo [2:0] (
		.OFB(),
		.OQ(tmds_d),
		.SHIFTOUT1(),
		.SHIFTOUT2(),
		.TBYTEOUT(),
		.TFB(),
		.TQ(),
		.CLK(clk_5x_pixel),
		.CLKDIV(clk_pixel),
		.D1(tmds_d0),
		.D2(tmds_d1),
		.D3(tmds_d2),
		.D4(tmds_d3),
		.D5(tmds_d4),
		.D6(tmds_d5),
		.D7(tmds_d6),
		.D8(tmds_d7),
		.OCE(1'b1),
		.RST((~ clk_pixel_resetn)),
		.SHIFTIN1(tmds_serdes_shift1),
		.SHIFTIN2(tmds_serdes_shift2),
		.T1(1'b0),
		.T2(1'b0),
		.T3(1'b0),
		.T4(1'b0),
		.TBYTEIN(1'b0),
		.TCE(1'b0)
	);
	OSERDESE2 #(
		.DATA_RATE_OQ("DDR"),
		.DATA_RATE_TQ("SDR"),
		.DATA_WIDTH(10),
		.INIT_OQ(1'b0),
		.INIT_TQ(1'b0),
		.SERDES_MODE("SLAVE"),
		.SRVAL_OQ(1'b0),
		.SRVAL_TQ(1'b0),
		.TBYTE_CTL("FALSE"),
		.TBYTE_SRC("FALSE"),
		.TRISTATE_WIDTH(1)
	) tmds_serdes_hi [2:0] (
		.OFB(),
		.OQ(),
		.SHIFTOUT1(tmds_serdes_shift1),
		.SHIFTOUT2(tmds_serdes_shift2),
		.TBYTEOUT(),
		.TFB(),
		.TQ(),
		.CLK(clk_5x_pixel),
		.CLKDIV(clk_pixel),
		.D1(1'b0),
		.D2(1'b0),
		.D3(tmds_d8),
		.D4(tmds_d9),
		.D5(1'b0),
		.D6(1'b0),
		.D7(1'b0),
		.D8(1'b0),
		.OCE(1'b1),
		.RST((~ clk_pixel_resetn)),
		.SHIFTIN1(1'b0),
		.SHIFTIN2(1'b0),
		.T1(1'b0),
		.T2(1'b0),
		.T3(1'b0),
		.T4(1'b0),
		.TBYTEIN(1'b0),
		.TCE(1'b0)
	);
	svo_tmds svo_tmds_0(
		.clk(clk_pixel),
		.resetn(clk_pixel_resetn),
		.de((! video_enc_tuser[3])),
		.ctrl(video_enc_tuser[2:1]),
		.din(video_enc_tdata[23:16]),
		.dout({tmds_d9[0], tmds_d8[0], tmds_d7[0], tmds_d6[0], tmds_d5[0], tmds_d4[0], tmds_d3[0], tmds_d2[0], tmds_d1[0], tmds_d0[0]})
	);
	svo_tmds svo_tmds_1(
		.clk(clk_pixel),
		.resetn(clk_pixel_resetn),
		.de((! video_enc_tuser[3])),
		.ctrl(2'b0),
		.din(video_enc_tdata[15:8]),
		.dout({tmds_d9[1], tmds_d8[1], tmds_d7[1], tmds_d6[1], tmds_d5[1], tmds_d4[1], tmds_d3[1], tmds_d2[1], tmds_d1[1], tmds_d0[1]})
	);
	svo_tmds svo_tmds_2(
		.clk(clk_pixel),
		.resetn(clk_pixel_resetn),
		.de((! video_enc_tuser[3])),
		.ctrl(2'b0),
		.din(video_enc_tdata[7:0]),
		.dout({tmds_d9[2], tmds_d8[2], tmds_d7[2], tmds_d6[2], tmds_d5[2], tmds_d4[2], tmds_d3[2], tmds_d2[2], tmds_d1[2], tmds_d0[2]})
	);
endmodule
module svo_enc (
	clk,
	resetn,
	in_axis_tvalid,
	in_axis_tready,
	in_axis_tdata,
	in_axis_tuser,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	input clk;
	input resetn;
	input in_axis_tvalid;
	output reg in_axis_tready;
	input [(SVO_BITS_PER_PIXEL - 1):0] in_axis_tdata;
	input [0:0] in_axis_tuser;
	output reg out_axis_tvalid;
	input out_axis_tready;
	output reg [(SVO_BITS_PER_PIXEL - 1):0] out_axis_tdata;
	output reg [3:0] out_axis_tuser;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	reg [(14 - 1):0] hcursor;
	reg [(14 - 1):0] vcursor;
	reg [3:0] ctrl_fifo [0:3];
	reg [1:0] ctrl_fifo_wraddr;
	reg [1:0] ctrl_fifo_rdaddr;
	reg [SVO_BITS_PER_PIXEL:0] pixel_fifo [0:7];
	reg [2:0] pixel_fifo_wraddr;
	reg [2:0] pixel_fifo_rdaddr;
	reg [(SVO_BITS_PER_PIXEL + 3):0] out_fifo [0:3];
	reg [1:0] out_fifo_wraddr;
	reg [1:0] out_fifo_rdaddr;
	wire [1:0] ctrl_fifo_fill = (ctrl_fifo_wraddr - ctrl_fifo_rdaddr);
	wire [2:0] pixel_fifo_fill = (pixel_fifo_wraddr - pixel_fifo_rdaddr);
	wire [1:0] out_fifo_fill = (out_fifo_wraddr - out_fifo_rdaddr);
	reg is_hsync;
	reg is_vsync;
	reg is_blank;
	always @(posedge clk) begin
		if ((! resetn)) begin
			ctrl_fifo_wraddr <= 0;
			hcursor = 0;
			vcursor = 0;
		end
		else if (((ctrl_fifo_wraddr + 2'd1) != ctrl_fifo_rdaddr)) begin
			is_blank = 0;
			is_hsync = 0;
			is_vsync = 0;
			if ((hcursor < SVO_HOR_FRONT_PORCH)) begin
				is_blank = 1;
			end
			else if ((hcursor < (SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC))) begin
				is_blank = 1;
				is_hsync = 1;
			end
			else if ((hcursor < ((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH))) begin
				is_blank = 1;
			end
			if ((vcursor < SVO_VER_FRONT_PORCH)) begin
				is_blank = 1;
			end
			else if ((vcursor < (SVO_VER_FRONT_PORCH + SVO_VER_SYNC))) begin
				is_blank = 1;
				is_vsync = 1;
			end
			else if ((vcursor < ((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH))) begin
				is_blank = 1;
			end
			ctrl_fifo[ctrl_fifo_wraddr] <= {is_blank, is_vsync, is_hsync, ((! hcursor) && (! vcursor))};
			ctrl_fifo_wraddr <= (ctrl_fifo_wraddr + 1);
			if ((hcursor == (SVO_HOR_TOTAL - 1))) begin
				hcursor = 0;
				vcursor = ((vcursor == (SVO_VER_TOTAL - 1)) ? 0 : (vcursor + 1));
			end
			else begin
				hcursor = (hcursor + 1);
			end
		end
	end
	always @(posedge clk) begin
		if ((! resetn)) begin
			pixel_fifo_wraddr <= 0;
			in_axis_tready <= 0;
		end
		else begin
			if ((in_axis_tvalid && in_axis_tready)) begin
				pixel_fifo[pixel_fifo_wraddr] <= {in_axis_tuser, in_axis_tdata};
				pixel_fifo_wraddr <= (pixel_fifo_wraddr + 1);
			end
			in_axis_tready <= (((pixel_fifo_wraddr + 3'd2) != pixel_fifo_rdaddr) && ((pixel_fifo_wraddr + 3'd1) != pixel_fifo_rdaddr));
		end
	end
	always @(posedge clk) begin
		if ((! resetn)) begin
			ctrl_fifo_rdaddr <= 0;
			pixel_fifo_rdaddr <= 0;
			out_fifo_wraddr <= 0;
		end
		else begin
			if ((((ctrl_fifo_rdaddr != ctrl_fifo_wraddr) && (pixel_fifo_rdaddr != pixel_fifo_wraddr)) && ((out_fifo_wraddr + 2'd1) != out_fifo_rdaddr))) begin
				if ((ctrl_fifo[ctrl_fifo_rdaddr][0] && (! pixel_fifo[pixel_fifo_rdaddr][SVO_BITS_PER_PIXEL]))) begin
					pixel_fifo_rdaddr <= (pixel_fifo_rdaddr + 1);
				end
				else if (ctrl_fifo[ctrl_fifo_rdaddr][3]) begin
					out_fifo[out_fifo_wraddr] <= {ctrl_fifo[ctrl_fifo_rdaddr], {SVO_BITS_PER_PIXEL {1'b0}}};
					out_fifo_wraddr <= (out_fifo_wraddr + 1);
					ctrl_fifo_rdaddr <= (ctrl_fifo_rdaddr + 1);
				end
				else begin
					out_fifo[out_fifo_wraddr] <= {ctrl_fifo[ctrl_fifo_rdaddr], pixel_fifo[pixel_fifo_rdaddr][(SVO_BITS_PER_PIXEL - 1):0]};
					out_fifo_wraddr <= (out_fifo_wraddr + 1);
					ctrl_fifo_rdaddr <= (ctrl_fifo_rdaddr + 1);
					pixel_fifo_rdaddr <= (pixel_fifo_rdaddr + 1);
				end
			end
		end
	end
	reg [1:0] next_out_fifo_rdaddr;
	reg [1:0] wait_for_fifos;
	always @(posedge clk) begin
		if ((! resetn)) begin
			wait_for_fifos <= 0;
			out_fifo_rdaddr <= 0;
			out_axis_tvalid <= 0;
			out_axis_tdata <= 0;
			out_axis_tuser <= 0;
		end
		else if (((wait_for_fifos < 3) || (out_fifo_fill == 0))) begin
			if ((((ctrl_fifo_fill < 3) || (pixel_fifo_fill < 6)) || (out_fifo_fill < 3))) wait_for_fifos <= 0;
			else wait_for_fifos <= (wait_for_fifos + 1);
		end
		else begin
			next_out_fifo_rdaddr = out_fifo_rdaddr;
			if ((out_axis_tvalid && out_axis_tready)) next_out_fifo_rdaddr = (next_out_fifo_rdaddr + 1);
			out_axis_tvalid <= (next_out_fifo_rdaddr != out_fifo_wraddr);
			{out_axis_tuser, out_axis_tdata} <= out_fifo[next_out_fifo_rdaddr];
			out_fifo_rdaddr <= next_out_fifo_rdaddr;
		end
	end
endmodule
module svo_tmds (
	clk,
	resetn,
	de,
	ctrl,
	din,
	dout
);
	input clk;
	input resetn;
	input de;
	input [1:0] ctrl;
	input [7:0] din;
	output reg [9:0] dout;
	function [3:0] count_set_bits;
		input reg [9:0] bits;
		integer i;
		begin
			count_set_bits = 0;
			for (i = 0; (i < 9); i = (i + 1))
				count_set_bits = (count_set_bits + bits[i]);
		end
	endfunction
	function [3:0] count_transitions;
		input reg [7:0] bits;
		integer i;
		begin
			count_transitions = 0;
			for (i = 0; (i < 7); i = (i + 1))
				count_transitions = (count_transitions + (bits[i] != bits[(i + 1)]));
		end
	endfunction
	wire [7:0] din_xor;
	assign din_xor[0] = din[0];
	assign din_xor[1] = (din[1] ^ din_xor[0]);
	assign din_xor[2] = (din[2] ^ din_xor[1]);
	assign din_xor[3] = (din[3] ^ din_xor[2]);
	assign din_xor[4] = (din[4] ^ din_xor[3]);
	assign din_xor[5] = (din[5] ^ din_xor[4]);
	assign din_xor[6] = (din[6] ^ din_xor[5]);
	assign din_xor[7] = (din[7] ^ din_xor[6]);
	wire [7:0] din_xnor;
	assign din_xnor[0] = din[0];
	assign din_xnor[1] = (din[1] ~^ din_xnor[0]);
	assign din_xnor[2] = (din[2] ~^ din_xnor[1]);
	assign din_xnor[3] = (din[3] ~^ din_xnor[2]);
	assign din_xnor[4] = (din[4] ~^ din_xnor[3]);
	assign din_xnor[5] = (din[5] ~^ din_xnor[4]);
	assign din_xnor[6] = (din[6] ~^ din_xnor[5]);
	assign din_xnor[7] = (din[7] ~^ din_xnor[6]);
	reg signed [7:0] cnt;
	reg [9:0] dout_buf;
	reg [9:0] dout_buf2;
	reg [9:0] m;
	always @(posedge clk) begin
		if ((! resetn)) begin
			cnt <= 0;
		end
		else if ((! de)) begin
			cnt <= 0;
			case (ctrl)
				2'b00: dout_buf <= 10'b1101010100;
				2'b01: dout_buf <= 10'b0010101011;
				2'b10: dout_buf <= 10'b0101010100;
				2'b11: dout_buf <= 10'b1010101011;
			endcase
		end
		else begin
			m = ((count_transitions(din_xor) < count_transitions(din_xnor)) ? {2'b01, din_xor} : {2'b00, din_xnor});
			if (((count_set_bits(m[7:0]) > 4) == (cnt > 0))) begin
				m = {1'b1, m[8], (~ m[7:0])};
			end
			cnt <= ((cnt + count_set_bits(m)) - 5);
			dout_buf <= m;
		end
		dout_buf2 <= dout_buf;
		dout <= dout_buf2;
	end
endmodule
module svo_axis_pipe (
	clk,
	resetn,
	in_axis_tvalid,
	in_axis_tready,
	in_axis_tdata,
	in_axis_tuser,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser,
	pipe_in_tdata,
	pipe_out_tdata,
	pipe_in_tuser,
	pipe_out_tuser,
	pipe_in_tvalid,
	pipe_out_tvalid,
	pipe_enable
);
	parameter TDATA_WIDTH = 8;
	parameter TUSER_WIDTH = 1;
	input clk;
	input resetn;
	input in_axis_tvalid;
	output in_axis_tready;
	input [(TDATA_WIDTH - 1):0] in_axis_tdata;
	input [(TUSER_WIDTH - 1):0] in_axis_tuser;
	output out_axis_tvalid;
	input out_axis_tready;
	output [(TDATA_WIDTH - 1):0] out_axis_tdata;
	output [(TUSER_WIDTH - 1):0] out_axis_tuser;
	output [(TDATA_WIDTH - 1):0] pipe_in_tdata;
	input [(TDATA_WIDTH - 1):0] pipe_out_tdata;
	output [(TUSER_WIDTH - 1):0] pipe_in_tuser;
	input [(TUSER_WIDTH - 1):0] pipe_out_tuser;
	output pipe_in_tvalid;
	input pipe_out_tvalid;
	output pipe_enable;
	reg tvalid_q0;
	reg tvalid_q1;
	reg [(TDATA_WIDTH - 1):0] tdata_q0;
	reg [(TDATA_WIDTH - 1):0] tdata_q1;
	reg [(TUSER_WIDTH - 1):0] tuser_q0;
	reg [(TUSER_WIDTH - 1):0] tuser_q1;
	assign in_axis_tready = (! tvalid_q1);
	assign out_axis_tvalid = (tvalid_q0 || tvalid_q1);
	assign out_axis_tdata = (tvalid_q1 ? tdata_q1 : tdata_q0);
	assign out_axis_tuser = (tvalid_q1 ? tuser_q1 : tuser_q0);
	assign pipe_enable = (in_axis_tvalid && in_axis_tready);
	assign pipe_in_tdata = in_axis_tdata;
	assign pipe_in_tuser = in_axis_tuser;
	assign pipe_in_tvalid = in_axis_tvalid;
	always @(posedge clk) begin
		if ((! resetn)) begin
			tvalid_q0 <= 0;
			tvalid_q1 <= 0;
		end
		else begin
			if (pipe_enable) begin
				tdata_q0 <= pipe_out_tdata;
				tdata_q1 <= tdata_q0;
				tuser_q0 <= pipe_out_tuser;
				tuser_q1 <= tuser_q0;
				tvalid_q0 <= pipe_out_tvalid;
				tvalid_q1 <= (tvalid_q0 && (! out_axis_tready));
			end
			else if (out_axis_tready) begin
				if (tvalid_q1) tvalid_q1 <= 0;
				else tvalid_q0 <= 0;
			end
		end
	end
endmodule
module svo_buf (
	clk,
	resetn,
	in_axis_tvalid,
	in_axis_tready,
	in_axis_tdata,
	in_axis_tuser,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser
);
	parameter TUSER_WIDTH = 1;
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	input clk;
	input resetn;
	input in_axis_tvalid;
	output in_axis_tready;
	input [(SVO_BITS_PER_PIXEL - 1):0] in_axis_tdata;
	input [(TUSER_WIDTH - 1):0] in_axis_tuser;
	output out_axis_tvalid;
	input out_axis_tready;
	output [(SVO_BITS_PER_PIXEL - 1):0] out_axis_tdata;
	output [(TUSER_WIDTH - 1):0] out_axis_tuser;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	wire [(SVO_BITS_PER_PIXEL - 1):0] pipe_in_tdata;
	reg [(SVO_BITS_PER_PIXEL - 1):0] pipe_out_tdata;
	wire [(TUSER_WIDTH - 1):0] pipe_in_tuser;
	reg [(TUSER_WIDTH - 1):0] pipe_out_tuser;
	wire pipe_in_tvalid;
	reg pipe_out_tvalid;
	wire pipe_enable;
	always @(posedge clk) begin
		if ((! resetn)) begin
			pipe_out_tvalid <= 0;
		end
		else if (pipe_enable) begin
			pipe_out_tdata <= pipe_in_tdata;
			pipe_out_tuser <= pipe_in_tuser;
			pipe_out_tvalid <= pipe_in_tvalid;
		end
	end
	svo_axis_pipe #(
		.TDATA_WIDTH(SVO_BITS_PER_PIXEL),
		.TUSER_WIDTH(TUSER_WIDTH)
	) svo_axis_pipe(
		.clk(clk),
		.resetn(resetn),
		.in_axis_tvalid(in_axis_tvalid),
		.in_axis_tready(in_axis_tready),
		.in_axis_tdata(in_axis_tdata),
		.in_axis_tuser(in_axis_tuser),
		.out_axis_tvalid(out_axis_tvalid),
		.out_axis_tready(out_axis_tready),
		.out_axis_tdata(out_axis_tdata),
		.out_axis_tuser(out_axis_tuser),
		.pipe_in_tdata(pipe_in_tdata),
		.pipe_out_tdata(pipe_out_tdata),
		.pipe_in_tuser(pipe_in_tuser),
		.pipe_out_tuser(pipe_out_tuser),
		.pipe_in_tvalid(pipe_in_tvalid),
		.pipe_out_tvalid(pipe_out_tvalid),
		.pipe_enable(pipe_enable)
	);
endmodule
module svo_dim (
	clk,
	resetn,
	enable,
	in_axis_tvalid,
	in_axis_tready,
	in_axis_tdata,
	in_axis_tuser,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	input clk;
	input resetn;
	input enable;
	input in_axis_tvalid;
	output in_axis_tready;
	input [(SVO_BITS_PER_PIXEL - 1):0] in_axis_tdata;
	input [0:0] in_axis_tuser;
	output out_axis_tvalid;
	input out_axis_tready;
	output [(SVO_BITS_PER_PIXEL - 1):0] out_axis_tdata;
	output [0:0] out_axis_tuser;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	wire [(SVO_BITS_PER_PIXEL - 1):0] pipe_in_tdata;
	reg [(SVO_BITS_PER_PIXEL - 1):0] pipe_out_tdata;
	wire pipe_in_tuser;
	reg pipe_out_tuser;
	wire pipe_in_tvalid;
	reg pipe_out_tvalid;
	wire pipe_enable;
	always @(posedge clk) begin
		if ((! resetn)) begin
			pipe_out_tvalid <= 0;
		end
		else if (pipe_enable) begin
			pipe_out_tdata <= (enable ? svo_rgba((svo_r(pipe_in_tdata) >> 1), (svo_g(pipe_in_tdata) >> 1), (svo_b(pipe_in_tdata) >> 1), svo_a(pipe_in_tdata)) : pipe_in_tdata);
			pipe_out_tuser <= pipe_in_tuser;
			pipe_out_tvalid <= pipe_in_tvalid;
		end
	end
	svo_axis_pipe #(
		.TDATA_WIDTH(SVO_BITS_PER_PIXEL),
		.TUSER_WIDTH(1)
	) svo_axis_pipe(
		.clk(clk),
		.resetn(resetn),
		.in_axis_tvalid(in_axis_tvalid),
		.in_axis_tready(in_axis_tready),
		.in_axis_tdata(in_axis_tdata),
		.in_axis_tuser(in_axis_tuser),
		.out_axis_tvalid(out_axis_tvalid),
		.out_axis_tready(out_axis_tready),
		.out_axis_tdata(out_axis_tdata),
		.out_axis_tuser(out_axis_tuser),
		.pipe_in_tdata(pipe_in_tdata),
		.pipe_out_tdata(pipe_out_tdata),
		.pipe_in_tuser(pipe_in_tuser),
		.pipe_out_tuser(pipe_out_tuser),
		.pipe_in_tvalid(pipe_in_tvalid),
		.pipe_out_tvalid(pipe_out_tvalid),
		.pipe_enable(pipe_enable)
	);
endmodule
module svo_overlay (
	clk,
	resetn,
	enable,
	in_axis_tvalid,
	in_axis_tready,
	in_axis_tdata,
	in_axis_tuser,
	over_axis_tvalid,
	over_axis_tready,
	over_axis_tdata,
	over_axis_tuser,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	input clk;
	input resetn;
	input enable;
	input in_axis_tvalid;
	output in_axis_tready;
	input [(SVO_BITS_PER_PIXEL - 1):0] in_axis_tdata;
	input [0:0] in_axis_tuser;
	input over_axis_tvalid;
	output over_axis_tready;
	input [(SVO_BITS_PER_PIXEL - 1):0] over_axis_tdata;
	input [1:0] over_axis_tuser;
	output out_axis_tvalid;
	input out_axis_tready;
	output [(SVO_BITS_PER_PIXEL - 1):0] out_axis_tdata;
	output [0:0] out_axis_tuser;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	wire buf_in_axis_tvalid;
	wire buf_in_axis_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] buf_in_axis_tdata;
	wire [0:0] buf_in_axis_tuser;
	wire buf_over_axis_tvalid;
	wire buf_over_axis_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] buf_over_axis_tdata;
	wire [1:0] buf_over_axis_tuser;
	wire buf_out_axis_tvalid;
	wire buf_out_axis_tready;
	wire [(SVO_BITS_PER_PIXEL - 1):0] buf_out_axis_tdata;
	wire [0:0] buf_out_axis_tuser;
	wire active = (buf_in_axis_tvalid && buf_over_axis_tvalid);
	wire skip_in = ((! buf_in_axis_tuser[0]) && buf_over_axis_tuser[0]);
	wire skip_over = (buf_in_axis_tuser[0] && (! buf_over_axis_tuser[0]));
	assign buf_in_axis_tready = (active && (skip_in || ((! skip_over) && buf_out_axis_tready)));
	assign buf_over_axis_tready = (active && (skip_over || ((! skip_in) && buf_out_axis_tready)));
	assign buf_out_axis_tvalid = ((active && (! skip_in)) && (! skip_over));
	assign buf_out_axis_tdata = ((enable && buf_over_axis_tuser[1]) ? buf_over_axis_tdata : buf_in_axis_tdata);
	assign buf_out_axis_tuser = ((enable && buf_over_axis_tuser[1]) ? buf_over_axis_tuser[0] : buf_in_axis_tuser);
	svo_buf #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) svo_buf_in(
		.clk(clk),
		.resetn(resetn),
		.in_axis_tvalid(in_axis_tvalid),
		.in_axis_tready(in_axis_tready),
		.in_axis_tdata(in_axis_tdata),
		.in_axis_tuser(in_axis_tuser),
		.out_axis_tvalid(buf_in_axis_tvalid),
		.out_axis_tready(buf_in_axis_tready),
		.out_axis_tdata(buf_in_axis_tdata),
		.out_axis_tuser(buf_in_axis_tuser)
	);
	svo_buf #(
		.TUSER_WIDTH(2),
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) svo_buf_over(
		.clk(clk),
		.resetn(resetn),
		.in_axis_tvalid(over_axis_tvalid),
		.in_axis_tready(over_axis_tready),
		.in_axis_tdata(over_axis_tdata),
		.in_axis_tuser(over_axis_tuser),
		.out_axis_tvalid(buf_over_axis_tvalid),
		.out_axis_tready(buf_over_axis_tready),
		.out_axis_tdata(buf_over_axis_tdata),
		.out_axis_tuser(buf_over_axis_tuser)
	);
	svo_buf #(
		.SVO_MODE(SVO_MODE),
		.SVO_FRAMERATE(SVO_FRAMERATE),
		.SVO_BITS_PER_PIXEL(SVO_BITS_PER_PIXEL)
	) svo_buf_out(
		.clk(clk),
		.resetn(resetn),
		.in_axis_tvalid(buf_out_axis_tvalid),
		.in_axis_tready(buf_out_axis_tready),
		.in_axis_tdata(buf_out_axis_tdata),
		.in_axis_tuser(buf_out_axis_tuser),
		.out_axis_tvalid(out_axis_tvalid),
		.out_axis_tready(out_axis_tready),
		.out_axis_tdata(out_axis_tdata),
		.out_axis_tuser(out_axis_tuser)
	);
endmodule
module svo_rect (
	clk,
	resetn,
	x1,
	y1,
	x2,
	y2,
	border,
	fill,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser
);
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	input clk;
	input resetn;
	input [(14 - 1):0] x1;
	input [(14 - 1):0] y1;
	input [(14 - 1):0] x2;
	input [(14 - 1):0] y2;
	input [(SVO_BITS_PER_PIXEL - 1):0] border;
	input [(SVO_BITS_PER_PIXEL - 1):0] fill;
	output reg out_axis_tvalid;
	input out_axis_tready;
	output reg [(SVO_BITS_PER_PIXEL - 1):0] out_axis_tdata;
	output reg [2:0] out_axis_tuser;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	reg [(14 - 1):0] x;
	reg [(14 - 1):0] y;
	reg on_x;
	reg on_y;
	reg in_x;
	reg in_y;
	reg on_border;
	always @(posedge clk) begin
		if ((! resetn)) begin
			x = 0;
			y = 0;
			in_x = 0;
			in_y = 0;
		end
		else begin
			if ((out_axis_tvalid && out_axis_tready)) begin
				if ((x == (SVO_HOR_PIXELS - 1))) begin
					x = 0;
					y = ((y == (SVO_VER_PIXELS - 1)) ? 0 : (y + 1));
				end
				else begin
					x = (x + 1);
				end
			end
			if ((x == x1)) in_x = 1;
			if ((y == y1)) in_y = 1;
			on_x = ((x == x1) || (x == x2));
			on_y = ((y == y1) || (y == y2));
			on_border = ((in_x && in_y) && (on_x || on_y));
			out_axis_tvalid <= 1;
			if (on_border) out_axis_tdata <= border;
			else out_axis_tdata <= fill;
			out_axis_tuser[0] <= ((! x) && (! y));
			out_axis_tuser[1] <= (in_x && in_y);
			out_axis_tuser[2] <= on_border;
			if ((x == x2)) in_x = 0;
			if (((y == y2) && (x == x2))) in_y = 0;
		end
	end
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module text_console (
	videoMemory_request,
	videoMemory_response,
	videoMemory_clock,
	videoMemory_clear,
	clock,
	reset_n,
	out_axis_tvalid,
	out_axis_tready,
	out_axis_tdata,
	out_axis_tuser
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] PAGE_FAULT = 32'd5;
	parameter SVO_MODE = "640x480";
	parameter SVO_FRAMERATE = 60;
	parameter SVO_BITS_PER_PIXEL = 24;
	parameter CONSOLE_HOR_PIXELS = 640;
	parameter CONSOLE_VER_PIXELS = 400;
	parameter CONSOLE_ABS_X1 = 0;
	parameter CONSOLE_ABS_Y1 = 0;
	parameter CONSOLE_ABS_X2 = (CONSOLE_ABS_X1 + CONSOLE_HOR_PIXELS);
	parameter CONSOLE_ABS_Y2 = (CONSOLE_ABS_Y1 + CONSOLE_VER_PIXELS);
	output reg [70:0] videoMemory_request;
	input wire [103:0] videoMemory_response;
	output wire videoMemory_clock;
	output wire videoMemory_clear;
	input wire clock;
	input wire reset_n;
	output reg out_axis_tvalid;
	input wire out_axis_tready;
	output wire [(SVO_BITS_PER_PIXEL - 1):0] out_axis_tdata;
	output wire [1:0] out_axis_tuser;
	localparam SVO_BITS_PER_RED = 8;
	localparam SVO_BITS_PER_GREEN = 8;
	localparam SVO_BITS_PER_BLUE = 8;
	localparam SVO_BITS_PER_ALPHA = 0;
	localparam SVO_HOR_PIXELS = ((SVO_MODE == "768x576") ? 768 : ((SVO_MODE == "1280x854R") ? 1280 : ((SVO_MODE == "2560x2048R") ? 2560 : ((SVO_MODE == "1920x1200") ? 1920 : ((SVO_MODE == "480x320R") ? 480 : ((SVO_MODE == "1280x768R") ? 1280 : ((SVO_MODE == "2560x1440R") ? 2560 : ((SVO_MODE == "2048x1536") ? 2048 : ((SVO_MODE == "1024x576") ? 1024 : ((SVO_MODE == "320x200") ? 320 : ((SVO_MODE == "384x288R") ? 384 : ((SVO_MODE == "1280x1024R") ? 1280 : ((SVO_MODE == "768x576R") ? 768 : ((SVO_MODE == "2048x1536R") ? 2048 : ((SVO_MODE == "1024x576R") ? 1024 : ((SVO_MODE == "1680x1050R") ? 1680 : ((SVO_MODE == "1280x854") ? 1280 : ((SVO_MODE == "2560x2048") ? 2560 : ((SVO_MODE == "1440x900R") ? 1440 : ((SVO_MODE == "2048x1080") ? 2048 : ((SVO_MODE == "1152x768R") ? 1152 : ((SVO_MODE == "4096x2160") ? 4096 : ((SVO_MODE == "4096x2160R") ? 4096 : ((SVO_MODE == "800x480") ? 800 : ((SVO_MODE == "2560x1080R") ? 2560 : ((SVO_MODE == "1440x1080R") ? 1440 : ((SVO_MODE == "854x480") ? 854 : ((SVO_MODE == "640x480") ? 640 : ((SVO_MODE == "480x320") ? 480 : ((SVO_MODE == "1920x1200R") ? 1920 : ((SVO_MODE == "3840x2160") ? 3840 : ((SVO_MODE == "1400x1050") ? 1400 : ((SVO_MODE == "854x480R") ? 854 : ((SVO_MODE == "1680x1050") ? 1680 : ((SVO_MODE == "320x200R") ? 320 : ((SVO_MODE == "1920x1080R") ? 1920 : ((SVO_MODE == "1920x1080") ? 1920 : ((SVO_MODE == "2560x1440") ? 2560 : ((SVO_MODE == "1440x900") ? 1440 : ((SVO_MODE == "1024x600") ? 1024 : ((SVO_MODE == "1400x1050R") ? 1400 : ((SVO_MODE == "1366x768") ? 1366 : ((SVO_MODE == "1440x1080") ? 1440 : ((SVO_MODE == "1600x900") ? 1600 : ((SVO_MODE == "64x48T") ? 64 : ((SVO_MODE == "640x480R") ? 640 : ((SVO_MODE == "352x288R") ? 352 : ((SVO_MODE == "1024x768") ? 1024 : ((SVO_MODE == "800x600") ? 800 : ((SVO_MODE == "1280x960") ? 1280 : ((SVO_MODE == "1024x768R") ? 1024 : ((SVO_MODE == "1280x960R") ? 1280 : ((SVO_MODE == "1600x900R") ? 1600 : ((SVO_MODE == "800x600R") ? 800 : ((SVO_MODE == "1280x800") ? 1280 : ((SVO_MODE == "384x288") ? 384 : ((SVO_MODE == "352x288") ? 352 : ((SVO_MODE == "800x480R") ? 800 : ((SVO_MODE == "1440x960") ? 1440 : ((SVO_MODE == "3840x2160R") ? 3840 : ((SVO_MODE == "2048x1080R") ? 2048 : ((SVO_MODE == "1280x800R") ? 1280 : ((SVO_MODE == "1366x768R") ? 1366 : ((SVO_MODE == "1600x1200R") ? 1600 : ((SVO_MODE == "2560x1600") ? 2560 : ((SVO_MODE == "1600x1200") ? 1600 : ((SVO_MODE == "320x240") ? 320 : ((SVO_MODE == "1152x864") ? 1152 : ((SVO_MODE == "1440x960R") ? 1440 : ((SVO_MODE == "2560x1080") ? 2560 : ((SVO_MODE == "1152x768") ? 1152 : ((SVO_MODE == "1280x720") ? 1280 : ((SVO_MODE == "1152x864R") ? 1152 : ((SVO_MODE == "1024x600R") ? 1024 : ((SVO_MODE == "1280x1024") ? 1280 : ((SVO_MODE == "1280x768") ? 1280 : ((SVO_MODE == "1280x720R") ? 1280 : ((SVO_MODE == "2560x1600R") ? 2560 : ((SVO_MODE == "320x240R") ? 320 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_PIXELS = ((SVO_MODE == "768x576") ? 576 : ((SVO_MODE == "1280x854R") ? 854 : ((SVO_MODE == "2560x2048R") ? 2048 : ((SVO_MODE == "1920x1200") ? 1200 : ((SVO_MODE == "480x320R") ? 320 : ((SVO_MODE == "1280x768R") ? 768 : ((SVO_MODE == "2560x1440R") ? 1440 : ((SVO_MODE == "2048x1536") ? 1536 : ((SVO_MODE == "1024x576") ? 576 : ((SVO_MODE == "320x200") ? 200 : ((SVO_MODE == "384x288R") ? 288 : ((SVO_MODE == "1280x1024R") ? 1024 : ((SVO_MODE == "768x576R") ? 576 : ((SVO_MODE == "2048x1536R") ? 1536 : ((SVO_MODE == "1024x576R") ? 576 : ((SVO_MODE == "1680x1050R") ? 1050 : ((SVO_MODE == "1280x854") ? 854 : ((SVO_MODE == "2560x2048") ? 2048 : ((SVO_MODE == "1440x900R") ? 900 : ((SVO_MODE == "2048x1080") ? 1080 : ((SVO_MODE == "1152x768R") ? 768 : ((SVO_MODE == "4096x2160") ? 2160 : ((SVO_MODE == "4096x2160R") ? 2160 : ((SVO_MODE == "800x480") ? 480 : ((SVO_MODE == "2560x1080R") ? 1080 : ((SVO_MODE == "1440x1080R") ? 1080 : ((SVO_MODE == "854x480") ? 480 : ((SVO_MODE == "640x480") ? 480 : ((SVO_MODE == "480x320") ? 320 : ((SVO_MODE == "1920x1200R") ? 1200 : ((SVO_MODE == "3840x2160") ? 2160 : ((SVO_MODE == "1400x1050") ? 1050 : ((SVO_MODE == "854x480R") ? 480 : ((SVO_MODE == "1680x1050") ? 1050 : ((SVO_MODE == "320x200R") ? 200 : ((SVO_MODE == "1920x1080R") ? 1080 : ((SVO_MODE == "1920x1080") ? 1080 : ((SVO_MODE == "2560x1440") ? 1440 : ((SVO_MODE == "1440x900") ? 900 : ((SVO_MODE == "1024x600") ? 600 : ((SVO_MODE == "1400x1050R") ? 1050 : ((SVO_MODE == "1366x768") ? 768 : ((SVO_MODE == "1440x1080") ? 1080 : ((SVO_MODE == "1600x900") ? 900 : ((SVO_MODE == "64x48T") ? 48 : ((SVO_MODE == "640x480R") ? 480 : ((SVO_MODE == "352x288R") ? 288 : ((SVO_MODE == "1024x768") ? 768 : ((SVO_MODE == "800x600") ? 600 : ((SVO_MODE == "1280x960") ? 960 : ((SVO_MODE == "1024x768R") ? 768 : ((SVO_MODE == "1280x960R") ? 960 : ((SVO_MODE == "1600x900R") ? 900 : ((SVO_MODE == "800x600R") ? 600 : ((SVO_MODE == "1280x800") ? 800 : ((SVO_MODE == "384x288") ? 288 : ((SVO_MODE == "352x288") ? 288 : ((SVO_MODE == "800x480R") ? 480 : ((SVO_MODE == "1440x960") ? 960 : ((SVO_MODE == "3840x2160R") ? 2160 : ((SVO_MODE == "2048x1080R") ? 1080 : ((SVO_MODE == "1280x800R") ? 800 : ((SVO_MODE == "1366x768R") ? 768 : ((SVO_MODE == "1600x1200R") ? 1200 : ((SVO_MODE == "2560x1600") ? 1600 : ((SVO_MODE == "1600x1200") ? 1200 : ((SVO_MODE == "320x240") ? 240 : ((SVO_MODE == "1152x864") ? 864 : ((SVO_MODE == "1440x960R") ? 960 : ((SVO_MODE == "2560x1080") ? 1080 : ((SVO_MODE == "1152x768") ? 768 : ((SVO_MODE == "1280x720") ? 720 : ((SVO_MODE == "1152x864R") ? 864 : ((SVO_MODE == "1024x600R") ? 600 : ((SVO_MODE == "1280x1024") ? 1024 : ((SVO_MODE == "1280x768") ? 768 : ((SVO_MODE == "1280x720R") ? 720 : ((SVO_MODE == "2560x1600R") ? 1600 : ((SVO_MODE == "320x240R") ? 240 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_FRONT_PORCH = ((SVO_MODE == "768x576") ? 32 : ((SVO_MODE == "1280x854R") ? 48 : ((SVO_MODE == "2560x2048R") ? 48 : ((SVO_MODE == "1920x1200") ? 136 : ((SVO_MODE == "480x320R") ? 48 : ((SVO_MODE == "1280x768R") ? 48 : ((SVO_MODE == "2560x1440R") ? 48 : ((SVO_MODE == "2048x1536") ? 160 : ((SVO_MODE == "1024x576") ? 40 : ((SVO_MODE == "320x200") ? 16 : ((SVO_MODE == "384x288R") ? 48 : ((SVO_MODE == "1280x1024R") ? 48 : ((SVO_MODE == "768x576R") ? 48 : ((SVO_MODE == "2048x1536R") ? 48 : ((SVO_MODE == "1024x576R") ? 48 : ((SVO_MODE == "1680x1050R") ? 48 : ((SVO_MODE == "1280x854") ? 72 : ((SVO_MODE == "2560x2048") ? 208 : ((SVO_MODE == "1440x900R") ? 48 : ((SVO_MODE == "2048x1080") ? 128 : ((SVO_MODE == "1152x768R") ? 48 : ((SVO_MODE == "4096x2160") ? 336 : ((SVO_MODE == "4096x2160R") ? 48 : ((SVO_MODE == "800x480") ? 24 : ((SVO_MODE == "2560x1080R") ? 48 : ((SVO_MODE == "1440x1080R") ? 48 : ((SVO_MODE == "854x480") ? 24 : ((SVO_MODE == "640x480") ? 24 : ((SVO_MODE == "480x320") ? 16 : ((SVO_MODE == "1920x1200R") ? 48 : ((SVO_MODE == "3840x2160") ? 320 : ((SVO_MODE == "1400x1050") ? 88 : ((SVO_MODE == "854x480R") ? 48 : ((SVO_MODE == "1680x1050") ? 104 : ((SVO_MODE == "320x200R") ? 48 : ((SVO_MODE == "1920x1080R") ? 48 : ((SVO_MODE == "1920x1080") ? 128 : ((SVO_MODE == "2560x1440") ? 192 : ((SVO_MODE == "1440x900") ? 88 : ((SVO_MODE == "1024x600") ? 48 : ((SVO_MODE == "1400x1050R") ? 48 : ((SVO_MODE == "1366x768") ? 72 : ((SVO_MODE == "1440x1080") ? 88 : ((SVO_MODE == "1600x900") ? 96 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 48 : ((SVO_MODE == "352x288R") ? 48 : ((SVO_MODE == "1024x768") ? 48 : ((SVO_MODE == "800x600") ? 32 : ((SVO_MODE == "1280x960") ? 80 : ((SVO_MODE == "1024x768R") ? 48 : ((SVO_MODE == "1280x960R") ? 48 : ((SVO_MODE == "1600x900R") ? 48 : ((SVO_MODE == "800x600R") ? 48 : ((SVO_MODE == "1280x800") ? 72 : ((SVO_MODE == "384x288") ? 16 : ((SVO_MODE == "352x288") ? 8 : ((SVO_MODE == "800x480R") ? 48 : ((SVO_MODE == "1440x960") ? 88 : ((SVO_MODE == "3840x2160R") ? 48 : ((SVO_MODE == "2048x1080R") ? 48 : ((SVO_MODE == "1280x800R") ? 48 : ((SVO_MODE == "1366x768R") ? 48 : ((SVO_MODE == "1600x1200R") ? 48 : ((SVO_MODE == "2560x1600") ? 200 : ((SVO_MODE == "1600x1200") ? 112 : ((SVO_MODE == "320x240") ? 16 : ((SVO_MODE == "1152x864") ? 64 : ((SVO_MODE == "1440x960R") ? 48 : ((SVO_MODE == "2560x1080") ? 160 : ((SVO_MODE == "1152x768") ? 64 : ((SVO_MODE == "1280x720") ? 64 : ((SVO_MODE == "1152x864R") ? 48 : ((SVO_MODE == "1024x600R") ? 48 : ((SVO_MODE == "1280x1024") ? 88 : ((SVO_MODE == "1280x768") ? 64 : ((SVO_MODE == "1280x720R") ? 48 : ((SVO_MODE == "2560x1600R") ? 48 : ((SVO_MODE == "320x240R") ? 48 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_SYNC = ((SVO_MODE == "768x576") ? 72 : ((SVO_MODE == "1280x854R") ? 32 : ((SVO_MODE == "2560x2048R") ? 32 : ((SVO_MODE == "1920x1200") ? 200 : ((SVO_MODE == "480x320R") ? 32 : ((SVO_MODE == "1280x768R") ? 32 : ((SVO_MODE == "2560x1440R") ? 32 : ((SVO_MODE == "2048x1536") ? 216 : ((SVO_MODE == "1024x576") ? 96 : ((SVO_MODE == "320x200") ? 24 : ((SVO_MODE == "384x288R") ? 32 : ((SVO_MODE == "1280x1024R") ? 32 : ((SVO_MODE == "768x576R") ? 32 : ((SVO_MODE == "2048x1536R") ? 32 : ((SVO_MODE == "1024x576R") ? 32 : ((SVO_MODE == "1680x1050R") ? 32 : ((SVO_MODE == "1280x854") ? 128 : ((SVO_MODE == "2560x2048") ? 280 : ((SVO_MODE == "1440x900R") ? 32 : ((SVO_MODE == "2048x1080") ? 216 : ((SVO_MODE == "1152x768R") ? 32 : ((SVO_MODE == "4096x2160") ? 448 : ((SVO_MODE == "4096x2160R") ? 32 : ((SVO_MODE == "800x480") ? 72 : ((SVO_MODE == "2560x1080R") ? 32 : ((SVO_MODE == "1440x1080R") ? 32 : ((SVO_MODE == "854x480") ? 80 : ((SVO_MODE == "640x480") ? 56 : ((SVO_MODE == "480x320") ? 40 : ((SVO_MODE == "1920x1200R") ? 32 : ((SVO_MODE == "3840x2160") ? 416 : ((SVO_MODE == "1400x1050") ? 144 : ((SVO_MODE == "854x480R") ? 32 : ((SVO_MODE == "1680x1050") ? 176 : ((SVO_MODE == "320x200R") ? 32 : ((SVO_MODE == "1920x1080R") ? 32 : ((SVO_MODE == "1920x1080") ? 200 : ((SVO_MODE == "2560x1440") ? 272 : ((SVO_MODE == "1440x900") ? 144 : ((SVO_MODE == "1024x600") ? 96 : ((SVO_MODE == "1400x1050R") ? 32 : ((SVO_MODE == "1366x768") ? 136 : ((SVO_MODE == "1440x1080") ? 152 : ((SVO_MODE == "1600x900") ? 160 : ((SVO_MODE == "64x48T") ? 4 : ((SVO_MODE == "640x480R") ? 32 : ((SVO_MODE == "352x288R") ? 32 : ((SVO_MODE == "1024x768") ? 104 : ((SVO_MODE == "800x600") ? 80 : ((SVO_MODE == "1280x960") ? 128 : ((SVO_MODE == "1024x768R") ? 32 : ((SVO_MODE == "1280x960R") ? 32 : ((SVO_MODE == "1600x900R") ? 32 : ((SVO_MODE == "800x600R") ? 32 : ((SVO_MODE == "1280x800") ? 128 : ((SVO_MODE == "384x288") ? 32 : ((SVO_MODE == "352x288") ? 32 : ((SVO_MODE == "800x480R") ? 32 : ((SVO_MODE == "1440x960") ? 144 : ((SVO_MODE == "3840x2160R") ? 32 : ((SVO_MODE == "2048x1080R") ? 32 : ((SVO_MODE == "1280x800R") ? 32 : ((SVO_MODE == "1366x768R") ? 32 : ((SVO_MODE == "1600x1200R") ? 32 : ((SVO_MODE == "2560x1600") ? 272 : ((SVO_MODE == "1600x1200") ? 168 : ((SVO_MODE == "320x240") ? 24 : ((SVO_MODE == "1152x864") ? 120 : ((SVO_MODE == "1440x960R") ? 32 : ((SVO_MODE == "2560x1080") ? 272 : ((SVO_MODE == "1152x768") ? 112 : ((SVO_MODE == "1280x720") ? 128 : ((SVO_MODE == "1152x864R") ? 32 : ((SVO_MODE == "1024x600R") ? 32 : ((SVO_MODE == "1280x1024") ? 128 : ((SVO_MODE == "1280x768") ? 128 : ((SVO_MODE == "1280x720R") ? 32 : ((SVO_MODE == "2560x1600R") ? 32 : ((SVO_MODE == "320x240R") ? 32 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_HOR_BACK_PORCH = ((SVO_MODE == "768x576") ? 104 : ((SVO_MODE == "1280x854R") ? 80 : ((SVO_MODE == "2560x2048R") ? 80 : ((SVO_MODE == "1920x1200") ? 336 : ((SVO_MODE == "480x320R") ? 80 : ((SVO_MODE == "1280x768R") ? 80 : ((SVO_MODE == "2560x1440R") ? 80 : ((SVO_MODE == "2048x1536") ? 376 : ((SVO_MODE == "1024x576") ? 136 : ((SVO_MODE == "320x200") ? 40 : ((SVO_MODE == "384x288R") ? 80 : ((SVO_MODE == "1280x1024R") ? 80 : ((SVO_MODE == "768x576R") ? 80 : ((SVO_MODE == "2048x1536R") ? 80 : ((SVO_MODE == "1024x576R") ? 80 : ((SVO_MODE == "1680x1050R") ? 80 : ((SVO_MODE == "1280x854") ? 200 : ((SVO_MODE == "2560x2048") ? 488 : ((SVO_MODE == "1440x900R") ? 80 : ((SVO_MODE == "2048x1080") ? 344 : ((SVO_MODE == "1152x768R") ? 80 : ((SVO_MODE == "4096x2160") ? 784 : ((SVO_MODE == "4096x2160R") ? 80 : ((SVO_MODE == "800x480") ? 96 : ((SVO_MODE == "2560x1080R") ? 80 : ((SVO_MODE == "1440x1080R") ? 80 : ((SVO_MODE == "854x480") ? 104 : ((SVO_MODE == "640x480") ? 80 : ((SVO_MODE == "480x320") ? 56 : ((SVO_MODE == "1920x1200R") ? 80 : ((SVO_MODE == "3840x2160") ? 736 : ((SVO_MODE == "1400x1050") ? 232 : ((SVO_MODE == "854x480R") ? 80 : ((SVO_MODE == "1680x1050") ? 280 : ((SVO_MODE == "320x200R") ? 80 : ((SVO_MODE == "1920x1080R") ? 80 : ((SVO_MODE == "1920x1080") ? 328 : ((SVO_MODE == "2560x1440") ? 464 : ((SVO_MODE == "1440x900") ? 232 : ((SVO_MODE == "1024x600") ? 144 : ((SVO_MODE == "1400x1050R") ? 80 : ((SVO_MODE == "1366x768") ? 208 : ((SVO_MODE == "1440x1080") ? 240 : ((SVO_MODE == "1600x900") ? 256 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 80 : ((SVO_MODE == "352x288R") ? 80 : ((SVO_MODE == "1024x768") ? 152 : ((SVO_MODE == "800x600") ? 112 : ((SVO_MODE == "1280x960") ? 208 : ((SVO_MODE == "1024x768R") ? 80 : ((SVO_MODE == "1280x960R") ? 80 : ((SVO_MODE == "1600x900R") ? 80 : ((SVO_MODE == "800x600R") ? 80 : ((SVO_MODE == "1280x800") ? 200 : ((SVO_MODE == "384x288") ? 48 : ((SVO_MODE == "352x288") ? 40 : ((SVO_MODE == "800x480R") ? 80 : ((SVO_MODE == "1440x960") ? 232 : ((SVO_MODE == "3840x2160R") ? 80 : ((SVO_MODE == "2048x1080R") ? 80 : ((SVO_MODE == "1280x800R") ? 80 : ((SVO_MODE == "1366x768R") ? 80 : ((SVO_MODE == "1600x1200R") ? 80 : ((SVO_MODE == "2560x1600") ? 472 : ((SVO_MODE == "1600x1200") ? 280 : ((SVO_MODE == "320x240") ? 40 : ((SVO_MODE == "1152x864") ? 184 : ((SVO_MODE == "1440x960R") ? 80 : ((SVO_MODE == "2560x1080") ? 432 : ((SVO_MODE == "1152x768") ? 176 : ((SVO_MODE == "1280x720") ? 192 : ((SVO_MODE == "1152x864R") ? 80 : ((SVO_MODE == "1024x600R") ? 80 : ((SVO_MODE == "1280x1024") ? 216 : ((SVO_MODE == "1280x768") ? 192 : ((SVO_MODE == "1280x720R") ? 80 : ((SVO_MODE == "2560x1600R") ? 80 : ((SVO_MODE == "320x240R") ? 80 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_FRONT_PORCH = ((SVO_MODE == "768x576") ? 3 : ((SVO_MODE == "1280x854R") ? 3 : ((SVO_MODE == "2560x2048R") ? 3 : ((SVO_MODE == "1920x1200") ? 3 : ((SVO_MODE == "480x320R") ? 3 : ((SVO_MODE == "1280x768R") ? 3 : ((SVO_MODE == "2560x1440R") ? 3 : ((SVO_MODE == "2048x1536") ? 3 : ((SVO_MODE == "1024x576") ? 3 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 3 : ((SVO_MODE == "1280x1024R") ? 3 : ((SVO_MODE == "768x576R") ? 3 : ((SVO_MODE == "2048x1536R") ? 3 : ((SVO_MODE == "1024x576R") ? 3 : ((SVO_MODE == "1680x1050R") ? 3 : ((SVO_MODE == "1280x854") ? 3 : ((SVO_MODE == "2560x2048") ? 3 : ((SVO_MODE == "1440x900R") ? 3 : ((SVO_MODE == "2048x1080") ? 3 : ((SVO_MODE == "1152x768R") ? 3 : ((SVO_MODE == "4096x2160") ? 3 : ((SVO_MODE == "4096x2160R") ? 3 : ((SVO_MODE == "800x480") ? 3 : ((SVO_MODE == "2560x1080R") ? 3 : ((SVO_MODE == "1440x1080R") ? 3 : ((SVO_MODE == "854x480") ? 3 : ((SVO_MODE == "640x480") ? 3 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 3 : ((SVO_MODE == "3840x2160") ? 3 : ((SVO_MODE == "1400x1050") ? 3 : ((SVO_MODE == "854x480R") ? 3 : ((SVO_MODE == "1680x1050") ? 3 : ((SVO_MODE == "320x200R") ? 3 : ((SVO_MODE == "1920x1080R") ? 3 : ((SVO_MODE == "1920x1080") ? 3 : ((SVO_MODE == "2560x1440") ? 3 : ((SVO_MODE == "1440x900") ? 3 : ((SVO_MODE == "1024x600") ? 3 : ((SVO_MODE == "1400x1050R") ? 3 : ((SVO_MODE == "1366x768") ? 3 : ((SVO_MODE == "1440x1080") ? 3 : ((SVO_MODE == "1600x900") ? 3 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 3 : ((SVO_MODE == "352x288R") ? 3 : ((SVO_MODE == "1024x768") ? 3 : ((SVO_MODE == "800x600") ? 3 : ((SVO_MODE == "1280x960") ? 3 : ((SVO_MODE == "1024x768R") ? 3 : ((SVO_MODE == "1280x960R") ? 3 : ((SVO_MODE == "1600x900R") ? 3 : ((SVO_MODE == "800x600R") ? 3 : ((SVO_MODE == "1280x800") ? 3 : ((SVO_MODE == "384x288") ? 3 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 3 : ((SVO_MODE == "1440x960") ? 3 : ((SVO_MODE == "3840x2160R") ? 3 : ((SVO_MODE == "2048x1080R") ? 3 : ((SVO_MODE == "1280x800R") ? 3 : ((SVO_MODE == "1366x768R") ? 3 : ((SVO_MODE == "1600x1200R") ? 3 : ((SVO_MODE == "2560x1600") ? 3 : ((SVO_MODE == "1600x1200") ? 3 : ((SVO_MODE == "320x240") ? 3 : ((SVO_MODE == "1152x864") ? 3 : ((SVO_MODE == "1440x960R") ? 3 : ((SVO_MODE == "2560x1080") ? 3 : ((SVO_MODE == "1152x768") ? 3 : ((SVO_MODE == "1280x720") ? 3 : ((SVO_MODE == "1152x864R") ? 3 : ((SVO_MODE == "1024x600R") ? 3 : ((SVO_MODE == "1280x1024") ? 3 : ((SVO_MODE == "1280x768") ? 3 : ((SVO_MODE == "1280x720R") ? 3 : ((SVO_MODE == "2560x1600R") ? 3 : ((SVO_MODE == "320x240R") ? 3 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_SYNC = ((SVO_MODE == "768x576") ? 4 : ((SVO_MODE == "1280x854R") ? 10 : ((SVO_MODE == "2560x2048R") ? 7 : ((SVO_MODE == "1920x1200") ? 6 : ((SVO_MODE == "480x320R") ? 10 : ((SVO_MODE == "1280x768R") ? 10 : ((SVO_MODE == "2560x1440R") ? 5 : ((SVO_MODE == "2048x1536") ? 4 : ((SVO_MODE == "1024x576") ? 5 : ((SVO_MODE == "320x200") ? 6 : ((SVO_MODE == "384x288R") ? 4 : ((SVO_MODE == "1280x1024R") ? 7 : ((SVO_MODE == "768x576R") ? 4 : ((SVO_MODE == "2048x1536R") ? 4 : ((SVO_MODE == "1024x576R") ? 5 : ((SVO_MODE == "1680x1050R") ? 6 : ((SVO_MODE == "1280x854") ? 10 : ((SVO_MODE == "2560x2048") ? 7 : ((SVO_MODE == "1440x900R") ? 6 : ((SVO_MODE == "2048x1080") ? 10 : ((SVO_MODE == "1152x768R") ? 10 : ((SVO_MODE == "4096x2160") ? 10 : ((SVO_MODE == "4096x2160R") ? 10 : ((SVO_MODE == "800x480") ? 10 : ((SVO_MODE == "2560x1080R") ? 10 : ((SVO_MODE == "1440x1080R") ? 4 : ((SVO_MODE == "854x480") ? 10 : ((SVO_MODE == "640x480") ? 4 : ((SVO_MODE == "480x320") ? 10 : ((SVO_MODE == "1920x1200R") ? 6 : ((SVO_MODE == "3840x2160") ? 5 : ((SVO_MODE == "1400x1050") ? 4 : ((SVO_MODE == "854x480R") ? 10 : ((SVO_MODE == "1680x1050") ? 6 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 5 : ((SVO_MODE == "1920x1080") ? 5 : ((SVO_MODE == "2560x1440") ? 5 : ((SVO_MODE == "1440x900") ? 6 : ((SVO_MODE == "1024x600") ? 10 : ((SVO_MODE == "1400x1050R") ? 4 : ((SVO_MODE == "1366x768") ? 10 : ((SVO_MODE == "1440x1080") ? 4 : ((SVO_MODE == "1600x900") ? 5 : ((SVO_MODE == "64x48T") ? 2 : ((SVO_MODE == "640x480R") ? 4 : ((SVO_MODE == "352x288R") ? 10 : ((SVO_MODE == "1024x768") ? 4 : ((SVO_MODE == "800x600") ? 4 : ((SVO_MODE == "1280x960") ? 4 : ((SVO_MODE == "1024x768R") ? 4 : ((SVO_MODE == "1280x960R") ? 4 : ((SVO_MODE == "1600x900R") ? 5 : ((SVO_MODE == "800x600R") ? 4 : ((SVO_MODE == "1280x800") ? 6 : ((SVO_MODE == "384x288") ? 4 : ((SVO_MODE == "352x288") ? 10 : ((SVO_MODE == "800x480R") ? 10 : ((SVO_MODE == "1440x960") ? 10 : ((SVO_MODE == "3840x2160R") ? 5 : ((SVO_MODE == "2048x1080R") ? 10 : ((SVO_MODE == "1280x800R") ? 6 : ((SVO_MODE == "1366x768R") ? 10 : ((SVO_MODE == "1600x1200R") ? 4 : ((SVO_MODE == "2560x1600") ? 6 : ((SVO_MODE == "1600x1200") ? 4 : ((SVO_MODE == "320x240") ? 4 : ((SVO_MODE == "1152x864") ? 4 : ((SVO_MODE == "1440x960R") ? 10 : ((SVO_MODE == "2560x1080") ? 10 : ((SVO_MODE == "1152x768") ? 10 : ((SVO_MODE == "1280x720") ? 5 : ((SVO_MODE == "1152x864R") ? 4 : ((SVO_MODE == "1024x600R") ? 10 : ((SVO_MODE == "1280x1024") ? 7 : ((SVO_MODE == "1280x768") ? 10 : ((SVO_MODE == "1280x720R") ? 5 : ((SVO_MODE == "2560x1600R") ? 6 : ((SVO_MODE == "320x240R") ? 4 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	localparam SVO_VER_BACK_PORCH = ((SVO_MODE == "768x576") ? 16 : ((SVO_MODE == "1280x854R") ? 12 : ((SVO_MODE == "2560x2048R") ? 49 : ((SVO_MODE == "1920x1200") ? 36 : ((SVO_MODE == "480x320R") ? 6 : ((SVO_MODE == "1280x768R") ? 9 : ((SVO_MODE == "2560x1440R") ? 33 : ((SVO_MODE == "2048x1536") ? 49 : ((SVO_MODE == "1024x576") ? 15 : ((SVO_MODE == "320x200") ? 3 : ((SVO_MODE == "384x288R") ? 6 : ((SVO_MODE == "1280x1024R") ? 20 : ((SVO_MODE == "768x576R") ? 10 : ((SVO_MODE == "2048x1536R") ? 37 : ((SVO_MODE == "1024x576R") ? 9 : ((SVO_MODE == "1680x1050R") ? 21 : ((SVO_MODE == "1280x854") ? 20 : ((SVO_MODE == "2560x2048") ? 63 : ((SVO_MODE == "1440x900R") ? 17 : ((SVO_MODE == "2048x1080") ? 27 : ((SVO_MODE == "1152x768R") ? 9 : ((SVO_MODE == "4096x2160") ? 64 : ((SVO_MODE == "4096x2160R") ? 49 : ((SVO_MODE == "800x480") ? 7 : ((SVO_MODE == "2560x1080R") ? 18 : ((SVO_MODE == "1440x1080R") ? 24 : ((SVO_MODE == "854x480") ? 7 : ((SVO_MODE == "640x480") ? 13 : ((SVO_MODE == "480x320") ? 3 : ((SVO_MODE == "1920x1200R") ? 26 : ((SVO_MODE == "3840x2160") ? 69 : ((SVO_MODE == "1400x1050") ? 32 : ((SVO_MODE == "854x480R") ? 6 : ((SVO_MODE == "1680x1050") ? 30 : ((SVO_MODE == "320x200R") ? 6 : ((SVO_MODE == "1920x1080R") ? 23 : ((SVO_MODE == "1920x1080") ? 32 : ((SVO_MODE == "2560x1440") ? 45 : ((SVO_MODE == "1440x900") ? 25 : ((SVO_MODE == "1024x600") ? 11 : ((SVO_MODE == "1400x1050R") ? 23 : ((SVO_MODE == "1366x768") ? 17 : ((SVO_MODE == "1440x1080") ? 33 : ((SVO_MODE == "1600x900") ? 26 : ((SVO_MODE == "64x48T") ? 1 : ((SVO_MODE == "640x480R") ? 7 : ((SVO_MODE == "352x288R") ? 6 : ((SVO_MODE == "1024x768") ? 23 : ((SVO_MODE == "800x600") ? 17 : ((SVO_MODE == "1280x960") ? 29 : ((SVO_MODE == "1024x768R") ? 15 : ((SVO_MODE == "1280x960R") ? 21 : ((SVO_MODE == "1600x900R") ? 18 : ((SVO_MODE == "800x600R") ? 11 : ((SVO_MODE == "1280x800") ? 22 : ((SVO_MODE == "384x288") ? 6 : ((SVO_MODE == "352x288") ? 3 : ((SVO_MODE == "800x480R") ? 6 : ((SVO_MODE == "1440x960") ? 23 : ((SVO_MODE == "3840x2160R") ? 54 : ((SVO_MODE == "2048x1080R") ? 18 : ((SVO_MODE == "1280x800R") ? 14 : ((SVO_MODE == "1366x768R") ? 9 : ((SVO_MODE == "1600x1200R") ? 28 : ((SVO_MODE == "2560x1600") ? 49 : ((SVO_MODE == "1600x1200") ? 38 : ((SVO_MODE == "320x240") ? 5 : ((SVO_MODE == "1152x864") ? 26 : ((SVO_MODE == "1440x960R") ? 15 : ((SVO_MODE == "2560x1080") ? 27 : ((SVO_MODE == "1152x768") ? 17 : ((SVO_MODE == "1280x720") ? 20 : ((SVO_MODE == "1152x864R") ? 18 : ((SVO_MODE == "1024x600R") ? 6 : ((SVO_MODE == "1280x1024") ? 29 : ((SVO_MODE == "1280x768") ? 17 : ((SVO_MODE == "1280x720R") ? 13 : ((SVO_MODE == "2560x1600R") ? 37 : ((SVO_MODE == "320x240R") ? 6 : 'bx)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
	function integer svo_clog2;
		input integer v;
		begin
			if ((v > 0)) v = (v - 1);
			svo_clog2 = 0;
			while (v) begin
				v = (v >> 1);
				svo_clog2 = (svo_clog2 + 1);
			end
		end
	endfunction
	function integer svo_max;
		input integer a;
		input integer b;
		begin
			svo_max = ((a > b) ? a : b);
		end
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_r;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_r = rgba[0+:SVO_BITS_PER_RED];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_g;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_g = rgba[SVO_BITS_PER_RED+:SVO_BITS_PER_GREEN];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_b;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_b = rgba[(SVO_BITS_PER_RED + SVO_BITS_PER_GREEN)+:SVO_BITS_PER_BLUE];
	endfunction
	function [(SVO_BITS_PER_RED - 1):0] svo_a;
		input reg [(SVO_BITS_PER_PIXEL - 1):0] rgba;
		svo_a = rgba[(SVO_BITS_PER_ALPHA ? ((SVO_BITS_PER_RED + SVO_BITS_PER_GREEN) + SVO_BITS_PER_BLUE) : 0)+:svo_max(SVO_BITS_PER_ALPHA, 1)];
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgba;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		input reg [(SVO_BITS_PER_ALPHA - 1):0] a;
		svo_rgba = {a, b, g, r};
	endfunction
	function [(SVO_BITS_PER_PIXEL - 1):0] svo_rgb;
		input reg [(SVO_BITS_PER_RED - 1):0] r;
		input reg [(SVO_BITS_PER_GREEN - 1):0] g;
		input reg [(SVO_BITS_PER_BLUE - 1):0] b;
		svo_rgb = svo_rgba(r, g, b, 0);
	endfunction
	function [(14 - 1):0] svo_coordinate;
		input reg [31:0] val32;
		svo_coordinate = val32[(14 - 1):0];
	endfunction
	localparam SVO_HOR_TOTAL = (((SVO_HOR_FRONT_PORCH + SVO_HOR_SYNC) + SVO_HOR_BACK_PORCH) + SVO_HOR_PIXELS);
	localparam SVO_VER_TOTAL = (((SVO_VER_FRONT_PORCH + SVO_VER_SYNC) + SVO_VER_BACK_PORCH) + SVO_VER_PIXELS);
	initial if ((SVO_HOR_PIXELS === 'bx)) begin
		$display("Invalid SVO_MODE value: %0s", SVO_MODE);
		$finish();
	end
	function automatic [33:0] translateVirtualAddress;
		input reg [(32 - 1):0] virtualAddress;
		input reg [47:0] translation;
		translateVirtualAddress = {translation[27:6], virtualAddress[11:0]};
	endfunction
	function automatic [113:0] addressTranslationResponse;
		input reg [(32 - 1):0] virtualAddress;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		reg [33:0] physicalAddress;
		begin
			physicalAddress = translateVirtualAddress(virtualAddress, translation);
			addressTranslationResponse = {physicalAddress, translationType, translation};
		end
	endfunction
	function automatic [278:0] memoryResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		input reg [31:0] responseType;
		input reg [(32 - 1):0] readData;
		reg [113:0] addressTranslation;
		begin
			addressTranslation = addressTranslationResponse(request[95:64], translationType, translation);
			memoryResponse = {responseType, request, addressTranslation, readData};
		end
	endfunction
	function automatic [278:0] memoryFaultResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		memoryFaultResponse = memoryResponse(request, translationType, translation, PAGE_FAULT, 32'hBADF00D);
	endfunction
	function automatic [70:0] simpleMemoryRead;
		input reg [33:0] physicalAddress;
		simpleMemoryRead = {physicalAddress, 1'b1, 4'b0, 32'hBADF00D};
	endfunction
	function automatic isInVRAM;
		input reg [33:0] address;
		isInVRAM = ((32'h1000 <= address) && (address < (32'h1000 + 'd4096)));
	endfunction
	function automatic isInRemote;
		input reg [33:0] address;
		isInRemote = ((32'h2000 <= address) && (address < (32'h2000 + 'd4096)));
	endfunction
	function automatic [70:0] simpleMemoryWrite;
		input reg [33:0] physicalAddress;
		input reg [3:0] writeEnable;
		input reg [(32 - 1):0] writeData;
		simpleMemoryWrite = {physicalAddress, 1'b0, writeEnable, writeData};
	endfunction
	function automatic [100:0] memoryReadRequest;
		input reg [(32 - 1):0] address;
		input reg readEnable;
		memoryReadRequest = {readEnable, 4'b0, address, 32'hBADF00D, KEEP};
	endfunction
	assign videoMemory_clock = clock;
	assign videoMemory_clear = (~ reset_n);
	reg [(14 - 1):0] x;
	reg [(14 - 1):0] y;
	wire in_x;
	wire in_y;
	assign in_x = ((CONSOLE_ABS_X1 <= x) && (x < CONSOLE_ABS_X2));
	assign in_y = ((CONSOLE_ABS_Y1 <= y) && (y < CONSOLE_ABS_Y2));
	assign out_axis_tuser = {(in_x && in_y), ((! x) && (! y))};
	always @(posedge clock) begin
		if ((~ reset_n)) begin
			x <= 0;
			y <= 0;
			out_axis_tvalid <= 1'b0;
		end
		else begin
			out_axis_tvalid <= 1'b1;
			if ((out_axis_tvalid && out_axis_tready)) begin
				if ((x == (SVO_HOR_PIXELS - 1))) begin
					x <= 0;
					y <= ((y == (SVO_VER_PIXELS - 1)) ? 0 : (y + 1));
				end
				else begin
					x <= (x + 1);
				end
			end
		end
	end
	reg [(14 - 1):0] virtual_x;
	reg [(14 - 1):0] virtual_y;
	always @(*) begin
		virtual_x = (~ 0);
		virtual_y = (~ 0);
		if ((((CONSOLE_ABS_X1 - 2) <= x) && (x < CONSOLE_ABS_X2))) virtual_x = ((x - CONSOLE_ABS_X1) + 2);
		if (((CONSOLE_ABS_Y1 <= y) && (y < CONSOLE_ABS_Y2))) virtual_y = (y - CONSOLE_ABS_Y1);
	end
	localparam CHAR_WIDTH = 8;
	localparam CHAR_HEIGHT = 16;
	localparam CONSOLE_SIZE = (80 * 25);
	reg [($clog2(80) - 1):0] character_cell_x;
	reg [($clog2(25) - 1):0] character_cell_y;
	reg [10:0] character_cell_index;
	wire [33:0] character_address;
	generate
		if ((! (11 >= $clog2(CONSOLE_SIZE)))) begin
			DoesNotExist foo();
		end
	endgenerate
	assign character_address = ({character_cell_index[10:1], 2'b0} + 32'h1000);
	always @(*) begin
		character_cell_x = (virtual_x / CHAR_WIDTH);
		character_cell_y = (virtual_y / CHAR_HEIGHT);
		character_cell_index = (character_cell_x + (80 * character_cell_y));
		videoMemory_request = simpleMemoryRead(character_address);
	end
	wire [15:0] character;
	wire [15:0] color;
	reg [10:0] character_cell_index_AG;
	reg [(14 - 1):0] virtual_x_AG;
	reg [(14 - 1):0] virtual_y_AG;
	always @(posedge clock) begin
		character_cell_index_AG <= character_cell_index;
		virtual_x_AG <= virtual_x;
		virtual_y_AG <= virtual_y;
	end
	assign {color[8+:8], character[8+:8], color[0+:8], character[0+:8]} = videoMemory_response[32:1];
	reg [3:0] bitmap_row;
	reg [2:0] bitmap_col;
	reg [6:0] ascii_code;
	reg [7:0] color_code;
	wire bitmap_pixel;
	always @(posedge clock) begin
		bitmap_col <= virtual_x_AG[2:0];
		bitmap_row <= virtual_y_AG[3:0];
		ascii_code <= character[(character_cell_index_AG[0] * 8)+:7];
		color_code <= color[(character_cell_index_AG[0] * 8)+:8];
	end
	charmap charmap(
		.clk(clock),
		.ascii_code(ascii_code),
		.row(bitmap_row),
		.col(bitmap_col),
		.pixel(bitmap_pixel)
	);
	reg [(SVO_BITS_PER_PIXEL - 1):0] foreground;
	reg [(SVO_BITS_PER_PIXEL - 1):0] background;
	function [(SVO_BITS_PER_PIXEL - 1):0] convertColor;
		input reg [3:0] color_code;
		case (color_code)
			8'h0: convertColor = svo_rgb(8'h0, 8'h0, 8'h0);
			8'h1: convertColor = svo_rgb(8'h0, 8'h0, 8'hFF);
			8'h2: convertColor = svo_rgb(8'h0, 8'hFF, 8'h0);
			8'h3: convertColor = svo_rgb(8'h00, 8'hFF, 8'hFF);
			8'h4: convertColor = svo_rgb(8'hFF, 8'h00, 8'h00);
			8'h5: convertColor = svo_rgb(8'hFF, 8'h0, 8'hFF);
			8'h6: convertColor = svo_rgb(8'h31, 8'h0C, 8'h0C);
			8'h7: convertColor = svo_rgb(8'h80, 8'h80, 8'h80);
			8'h8: convertColor = svo_rgb(8'h33, 8'h33, 8'h33);
			8'h9: convertColor = svo_rgb(8'h7E, 8'hE0, 8'hFF);
			8'hA: convertColor = svo_rgb(8'ha1, 8'hee, 8'h33);
			8'hB: convertColor = svo_rgb(8'hFF, 8'h80, 8'h00);
			8'hC: convertColor = svo_rgb(8'hFF, 8'hC0, 8'hCB);
			8'hD: convertColor = svo_rgb(8'h80, 8'h00, 8'hFF);
			8'hE: convertColor = svo_rgb(8'hFF, 8'hFF, 8'h0);
			8'hF: convertColor = svo_rgb(8'hFF, 8'hFF, 8'hFF);
		endcase
	endfunction
	always @(*) begin
		foreground = convertColor(color_code[3:0]);
		background = convertColor(color_code[7:4]);
	end
	assign out_axis_tdata = (bitmap_pixel ? foreground : background);
endmodule
// removed typedef: VirtualAddress_t
// removed typedef: Word_t
// removed typedef: PhysicalAddress_t
// removed typedef: ControlStatusRegisterName_t
// removed typedef: PerformanceCounter_t
// removed typedef: PrivilegeLevel_t
// removed typedef: InterruptStatus_t
// removed typedef: Status_t
// removed typedef: AddressTranslationProtection_t
// removed typedef: ExceptionCause_t
// removed typedef: CacheIndex_t
// removed typedef: CacheBlockOffset_t
// removed typedef: CacheVirtualTag_t
// removed typedef: CachePhysicalTag_t
// removed typedef: CacheSetNumber_t
// removed typedef: CacheRequestType_t
// removed typedef: CacheRequest_t
// removed typedef: CacheTagEntry_t
// removed typedef: CacheResponse_t
// removed typedef: TLBIndex_t
// removed typedef: TranslationEntry_t
// removed typedef: TranslationResponse_t
// removed typedef: TranslationFlushMode_t
// removed typedef: MemoryRequest_t
// removed typedef: ResponseType_t
// removed typedef: TranslationType_t
// removed typedef: AddressTranslationResponse_t
// removed typedef: MemoryResponse_t
// removed typedef: SimpleMemoryRequest_t
// removed typedef: SimpleMemoryResponse_t
module VRAM (
	memory_request,
	memory_response,
	memory_clock,
	memory_clear,
	display_request,
	display_response,
	display_clock,
	display_clear
);
	localparam [31:0] KEEP = 32'd0;
	localparam [31:0] PAGE_FAULT = 32'd5;
	input wire [70:0] memory_request;
	output reg [103:0] memory_response;
	input wire memory_clock;
	input wire memory_clear;
	input wire [70:0] display_request;
	output reg [103:0] display_response;
	input wire display_clock;
	input wire display_clear;
	function automatic [33:0] translateVirtualAddress;
		input reg [(32 - 1):0] virtualAddress;
		input reg [47:0] translation;
		translateVirtualAddress = {translation[27:6], virtualAddress[11:0]};
	endfunction
	function automatic [113:0] addressTranslationResponse;
		input reg [(32 - 1):0] virtualAddress;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		reg [33:0] physicalAddress;
		begin
			physicalAddress = translateVirtualAddress(virtualAddress, translation);
			addressTranslationResponse = {physicalAddress, translationType, translation};
		end
	endfunction
	function automatic [278:0] memoryResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		input reg [31:0] responseType;
		input reg [(32 - 1):0] readData;
		reg [113:0] addressTranslation;
		begin
			addressTranslation = addressTranslationResponse(request[95:64], translationType, translation);
			memoryResponse = {responseType, request, addressTranslation, readData};
		end
	endfunction
	function automatic [278:0] memoryFaultResponse;
		input reg [100:0] request;
		input reg [31:0] translationType;
		input reg [47:0] translation;
		memoryFaultResponse = memoryResponse(request, translationType, translation, PAGE_FAULT, 32'hBADF00D);
	endfunction
	function automatic [70:0] simpleMemoryRead;
		input reg [33:0] physicalAddress;
		simpleMemoryRead = {physicalAddress, 1'b1, 4'b0, 32'hBADF00D};
	endfunction
	function automatic isInVRAM;
		input reg [33:0] address;
		isInVRAM = ((32'h1000 <= address) && (address < (32'h1000 + 'd4096)));
	endfunction
	function automatic isInRemote;
		input reg [33:0] address;
		isInRemote = ((32'h2000 <= address) && (address < (32'h2000 + 'd4096)));
	endfunction
	function automatic [70:0] simpleMemoryWrite;
		input reg [33:0] physicalAddress;
		input reg [3:0] writeEnable;
		input reg [(32 - 1):0] writeData;
		simpleMemoryWrite = {physicalAddress, 1'b0, writeEnable, writeData};
	endfunction
	function automatic [100:0] memoryReadRequest;
		input reg [(32 - 1):0] address;
		input reg readEnable;
		memoryReadRequest = {readEnable, 4'b0, address, 32'hBADF00D, KEEP};
	endfunction
	// removed an assertion item
	// removed an assertion item
	generate
		if ((! ('d4096 > 0))) begin
			DoesNotExist foo();
		end
	endgenerate
	generate
		if ((! ((32'h1000 % 'd4096) == 0))) begin
			DoesNotExist foo();
		end
	endgenerate
	generate
		if ((! (((32'h1000 + 'd4096) % 'd4096) == 0))) begin
			DoesNotExist foo();
		end
	endgenerate
	localparam NB_COL = 4;
	localparam COL_WIDTH = 8;
	generate
		if ((! (32 == (NB_COL * COL_WIDTH)))) begin
			DoesNotExist foo();
		end
	endgenerate
	xilinx_bram_double_clock #(
		.NB_COL(NB_COL),
		.COL_WIDTH(COL_WIDTH),
		.RAM_DEPTH(1024)
	) vram(
		.addra(display_request[48:39]),
		.dina(32'hBADF00D),
		.clka(display_clock),
		.wea(4'b0),
		.douta(display_response[32:1]),
		.addrb(memory_request[48:39]),
		.dinb(memory_request[31:0]),
		.clkb(memory_clock),
		.web(memory_request[35:32]),
		.doutb(memory_response[32:1])
	);
	wire [70:0] memoryRequest_saved;
	wire [70:0] displayRequest_saved;
	Register #(.WIDTH(71)) memoryRequestRegister(
		.q(memoryRequest_saved),
		.d(memory_request),
		.enable(1'b1),
		.clock(memory_clock),
		.clear(memory_clear)
	);
	Register #(.WIDTH(71)) displayRequestRegister(
		.q(displayRequest_saved),
		.d(display_request),
		.enable(1'b1),
		.clock(display_clock),
		.clear(display_clear)
	);
	always @(*) begin
		memory_response[103:33] = memoryRequest_saved;
		display_response[103:33] = displayRequest_saved;
		memory_response[0:0] = isInVRAM(memoryRequest_saved[70:37]);
		display_response[0:0] = isInVRAM(displayRequest_saved[70:37]);
	end
endmodule
