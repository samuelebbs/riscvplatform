//# PRINT AFTER memory/translation_lookaside_buffer.sv
`default_nettype none

module TLBEvictionSelector(
    input TranslationResponse_t instructionResponse, dataResponse,

    input logic tlbSelectIndex,
    output TLBIndex_t evictionIndex,

    input logic clock, clear
);

    NotMostRecentlyUsedPolicy #(.WIDTH($bits(TLBIndex_t))) policy(
        .select(tlbSelectIndex),
        .index(evictionIndex),
        .recentValid(
            {instructionResponse.entry.isValid, dataResponse.entry.isValid}
        ),
        .recent({instructionResponse.index, dataResponse.index}),
        .clock,
        .clear
    );

endmodule