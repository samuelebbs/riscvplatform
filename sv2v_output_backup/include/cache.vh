//# PRINT AFTER memory/memory_arbiter.sv
`ifndef __CACHE_VH
`define __CACHE_VH

// This entire file assumes XLEN = 32
`include "riscv_types.vh"
`include "config.vh"


`define CACHE_BLOCK_SIZE 'd16

`define CACHE_VIRTUAL_TAG_POISON 20'h22BAD
`define CACHE_PHYSICAL_TAG_POISON 22'h277BAD

typedef logic [5:0] CacheIndex_t;
typedef logic [3:0] CacheBlockOffset_t;
typedef logic [19:0] CacheVirtualTag_t;
typedef logic [21:0] CachePhysicalTag_t;
`ifdef SINGLE_SET
typedef logic [0:0] CacheSetNumber_t;
`else
typedef logic [$clog2(`CACHE_SETS)-1:0] CacheSetNumber_t;
`endif

typedef enum {CACHE_READ, CACHE_WRITE, CACHE_DRAM_FILL} CacheRequestType_t;


typedef struct packed {
    CacheIndex_t             index      ;
    CacheBlockOffset_t       blockOffset;
    // This field is only valid if requestType is a write
    CachePhysicalTag_t            tag        ;
    Word_t                   writeData     ;
    CacheRequestType_t       requestType;
    logic                    isValid    ;
    logic              [3:0] writeEnable;
    // The user must ensure that this set is safe to write to using the memory
    // locking system.
    CacheSetNumber_t writeSet;
} CacheRequest_t;

typedef struct packed {
    logic      isValid;
    logic      isDirty;
    CachePhysicalTag_t tag    ;
} CacheTagEntry_t;

typedef struct packed {
    CacheRequest_t  request ;
    Word_t          readData ;
    CacheTagEntry_t tagEntry;
    CacheSetNumber_t readSet;
} CacheResponse_t;


// The goal of this interface is to abstract away cache structure from the rest
// of the memory controller. The memory controller request abstract cache
// operations and receives abstract cache responses back with accompanying
// metadata.
interface CacheInterface();
    CacheRequest_t request;

    // Visible on the next cycle

    // If the response's tagEntry is valid, then it implies that the physical
    // address provided matches the cache physical tag.
    CacheResponse_t response;
    PhysicalAddress_t physicalAddress;

    CacheResponse_t [`CACHE_SETS-1:0] rawResponses;

    modport Requester(
        output request,
        // Next clock
        output physicalAddress,
        input response
    );

    modport Filler(
        output request,
        // Next clock
        output physicalAddress,
        input response,

        input rawResponses
    );

    modport Cache(
        input request,
        // Next Clock
        input physicalAddress,
        output response,
        output rawResponses
    );

endinterface

interface CacheSetInterface(
    input CacheRequest_t request,
    output CacheResponse_t response
);

    modport CacheSet(
        input request,
        output response
    );

endinterface


`define CACHE_FUNCTIONS \
    function automatic CacheVirtualTag_t cacheVirtualTag(VirtualAddress_t address);\
        return address[31:12];\
    endfunction : cacheVirtualTag\
\
    function automatic CacheIndex_t cacheVirtualIndex(VirtualAddress_t address);\
        return address[11:6];\
    endfunction\
\
    function automatic CacheBlockOffset_t cacheVirtualBlockOffset(VirtualAddress_t address);\
        return address[5:2];\
    endfunction\
\
    function automatic CacheIndex_t cachePhysicalIndex(PhysicalAddress_t address);\
        return address[11:6];\
    endfunction\
\
    function automatic CacheBlockOffset_t cachePhysicalBlockOffset(PhysicalAddress_t address);\
        return address[5:2];\
    endfunction\
\
    function automatic CachePhysicalTag_t cachePhysicalTag(PhysicalAddress_t address);\
        return address[33:12];\
    endfunction\
\
    function automatic logic isCacheTagHit(CacheResponse_t cacheResponse, CachePhysicalTag_t expectedTag);\
        return cacheResponse.request.isValid && \
            cacheResponse.tagEntry.isValid &&   \
            cacheResponse.tagEntry.tag == expectedTag;\
    endfunction\
\
    function automatic logic isCacheHit(CacheResponse_t cacheResponse);\
        return cacheResponse.tagEntry.isValid;\
    endfunction\
\
    function automatic logic isCacheDirty(CacheResponse_t cacheResponse);\
        return cacheResponse.tagEntry.isValid && cacheResponse.tagEntry.isDirty;\
    endfunction\
\
    function automatic CacheRequest_t readRequestManual(CacheIndex_t index, CacheBlockOffset_t offset);\
        return '{\
            index: index,\
            blockOffset: offset,\
            tag: `CACHE_PHYSICAL_TAG_POISON,\
            writeData: `WORD_POISON,\
            requestType: CACHE_READ,\
            isValid: 1'b1,\
            writeEnable: 4'b0,\
            writeSet: '0\
        };\
    endfunction\
\
    function automatic CacheRequest_t readRequest(PhysicalAddress_t physicalAddress);\
        CacheIndex_t index;\
        CacheBlockOffset_t offset;\
        index = cachePhysicalIndex(physicalAddress);\
        offset = cachePhysicalBlockOffset(physicalAddress);\
        return readRequestManual(index, offset);\
    endfunction\
\
    function automatic PhysicalAddress_t cachePhysicalAddress(CacheResponse_t cacheResponse);\
        return {cacheResponse.tagEntry.tag, cacheResponse.request.index, cacheResponse.request.blockOffset, 2'b0};\
    endfunction\
\
    function automatic CacheRequest_t cleanWriteRequest(\
        PhysicalAddress_t address,\
        Word_t writeData,\
        CacheSetNumber_t setNumber\
    );\
        CacheIndex_t index;\
        CacheBlockOffset_t offset;\
        CachePhysicalTag_t tag;\
        index = cachePhysicalIndex(address);\
        offset = cachePhysicalBlockOffset(address);\
        tag = cachePhysicalTag(address);\
        return '{\
            index: index,\
            blockOffset: offset,\
            tag: tag,\
            writeData: writeData,\
            requestType: CACHE_DRAM_FILL,\
            isValid: 1'b1,\
            writeEnable: 4'hf,\
            writeSet: setNumber\
        };\
    endfunction\
\
    function automatic CacheRequest_t cacheReadRequest(VirtualAddress_t address);\
        CacheIndex_t index;\
        CacheBlockOffset_t offset;\
        index = cacheVirtualIndex(address);\
        offset = cacheVirtualBlockOffset(address);\
        return readRequestManual(index, offset);\
    endfunction\
\
    function automatic logic isCacheAddress(PhysicalAddress_t address);\
        return !(`RESERVED_LOW <= address && address < `RESERVED_HIGH);\
    endfunction\
\
    function automatic CacheRequest_t cacheDirtyWrite(\
        PhysicalAddress_t address,\
        logic [3:0] writeEnable, \
        Word_t writeData,\
        CacheSetNumber_t setNumber\
    );\
        CacheIndex_t index;\
        CacheBlockOffset_t offset;\
        CachePhysicalTag_t tag;\
        index = cachePhysicalIndex(address);\
        offset = cachePhysicalBlockOffset(address);\
        tag = cachePhysicalTag(address);\
        return '{\
            index: index,\
            blockOffset: offset,\
            tag: tag,\
            writeData: writeData,\
            requestType: CACHE_WRITE,\
            isValid: 1'b1,\
            writeEnable: writeEnable,\
            writeSet: setNumber\
        };\
    endfunction

`endif