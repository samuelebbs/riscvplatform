//# PRINT AFTER include/config.vh
`ifndef __BOARD_CONFIG_VH
`define __BOARD_CONFIG_VH

//////////////////////////////////////////////////
// Board Config
//////////////////////////////////////////////////

// TODO: It would be nice if the make file generated a header document based on
// the board config instead of forcing the user to change the source code here.

// Only define one board type
`define PYNQ_Z2
// `define ZEDBOARD

`ifdef PYNQ_Z2
`define HDMI_OUTPUT
`elsif ZEDBOARD
`define VGA_OUTPUT
`endif

////////////////////////////////////////////////////////////////
// Video Configuration
//
// The resolution and frame rate should be configured in the block diagram. The
// frequency should be manually encoded in the block diagram based on the
// following table
//
// HFP = Horizontal Front Porch
// HSP = Horizontal Sync Pulse
// HBP = Horizontal Back Porch
// VFP = Vertical Front Porch
// VSP = Vertical Sync Pulse
// VBP = Vertical Back Porch
// Frequency = (Width + HFP + HSP + HBP)*(Height + VFP + VSP + VBP) * FrameRate
//
// Examples (round to nearest 0.25 MHz)
// 1280x720@60 = (1280 + 64 + 128 + 192) * (720 + 3 + 5 + 20) * 60 = 74.75 MHz
// 1920x1080@60 = (1920 + 128 + 200 + 328) * (1080 + 3 + 5 + 32) * 60 = 173 MHz
//
// Credit: https://github.com/cliffordwolf/SimpleVOut
////////////////////////////////////////////////////////////////

`endif