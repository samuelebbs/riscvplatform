//# PRINT AFTER chip.sv
`ifndef __MEMORY_VH
`define __MEMORY_VH

`include "riscv_types.vh"
`include "riscv_csr.vh"
`include "translation.vh"

`define MAX_MEMORY_LATENCY 100

typedef struct packed {
    logic readEnable;
    logic [3:0] writeEnable; // byte write
    VirtualAddress_t virtualAddress;
    Word_t writeData;
    TranslationFlushMode_t flushMode;
} MemoryRequest_t;

typedef enum {
    INVALID_MEMORY_RESPONSE = 'd0,
    CACHE_HIT,
    CACHE_MISS,
    VRAM_IO,
    REMOTE_IO,
    PAGE_FAULT,
    TRANSLATION_INVALIDATE
} ResponseType_t;

typedef enum {
    INVALID_TRANSLATION,
    VIRTUAL_MEMORY_DISABLED,
    TLB_HIT,
    TLB_MISS,
    TABLE_WALK_FAULT,
    TLB_INVALIDATE
} TranslationType_t;

typedef struct packed {
    PhysicalAddress_t physicalAddress;
    TranslationType_t translationType;
    TranslationEntry_t translationEntry;
} AddressTranslationResponse_t;

typedef struct packed {
    ResponseType_t responseType;
    MemoryRequest_t request;
    AddressTranslationResponse_t translationResponse;
    Word_t readData;
} MemoryResponse_t;

typedef struct packed {
    PhysicalAddress_t physicalAddress;
    logic readEnable;
    logic [3:0] writeEnable;
    Word_t writeData;
} SimpleMemoryRequest_t;

typedef struct packed {
    SimpleMemoryRequest_t request;
    Word_t readData;
    logic responseValid;
} SimpleMemoryResponse_t;

interface MemoryControl();
    // If any of these bits are changed, there better be a full pipeline flush
    logic supervisorPagesEnabled;
    logic userPagesEnabled;
    logic virtualMemoryEnabled;
    PhysicalAddress_t rootPageTable;
    // TODO: add a control bit for MXR

    modport Core(
        output supervisorPagesEnabled,
        output userPagesEnabled,
        output virtualMemoryEnabled,
        output rootPageTable
    );

    modport Memory(
        input supervisorPagesEnabled,
        input userPagesEnabled,
        input virtualMemoryEnabled,
        input rootPageTable
    );

endinterface : MemoryControl

interface MemoryInterface();
    MemoryRequest_t request;
    MemoryResponse_t response;
    logic ready;

    modport Memory(
        output ready,
        input request,
        output response
    );

    modport Requester(
        input ready,
        output request,
        input response
    );

endinterface

interface MemoryMappedIOInterface();
    logic clock, clear;

    SimpleMemoryRequest_t request;
    // Visible on the next clock cycle
    SimpleMemoryResponse_t response;

    modport Requester(
        output request,
        input response,

        output clock, clear
    );

    modport Device(
        input request,
        output response,

        input clock, clear
    );

endinterface

`define MEMORY_FUNCTIONS \
    function automatic PhysicalAddress_t translateVirtualAddress(\
        VirtualAddress_t virtualAddress,\
        TranslationEntry_t translation\
    );\
        return {translation.physicalTag, virtualAddress[11:0]};\
    endfunction\
\
    function automatic AddressTranslationResponse_t addressTranslationResponse(\
        VirtualAddress_t virtualAddress,\
        TranslationType_t translationType,\
        TranslationEntry_t translation\
    );\
        PhysicalAddress_t physicalAddress;\
        physicalAddress = translateVirtualAddress(virtualAddress, translation);\
        return '{\
            physicalAddress: physicalAddress,\
            translationType: translationType,\
            translationEntry: translation\
        };\
    endfunction\
\
    function automatic MemoryResponse_t memoryResponse(\
        MemoryRequest_t request,\
        TranslationType_t translationType,\
        TranslationEntry_t translation,\
        ResponseType_t responseType,\
        Word_t readData\
    );\
        AddressTranslationResponse_t addressTranslation;\
        addressTranslation = addressTranslationResponse(\
            request.virtualAddress,\
            translationType,\
            translation\
        );\
        return '{\
            request: request,\
            responseType: responseType,\
            translationResponse: addressTranslation,\
            readData: readData\
        };\
    endfunction\
\
    function automatic MemoryResponse_t memoryFaultResponse(\
        MemoryRequest_t request,\
        TranslationType_t translationType,\
        TranslationEntry_t translation\
    );\
        return memoryResponse(\
            request, \
            translationType, \
            translation, \
            PAGE_FAULT,\
            `WORD_POISON\
        );\
    endfunction\
\
    function automatic SimpleMemoryRequest_t simpleMemoryRead(\
        PhysicalAddress_t physicalAddress\
    );\
        return '{\
            physicalAddress: physicalAddress,\
            readEnable: 1'b1,\
            writeEnable: 4'b0,\
            writeData: `WORD_POISON\
        };\
    endfunction\
\
    function automatic logic isInVRAM(PhysicalAddress_t address);\
        return `VRAM_START <= address && address < `VRAM_END;\
    endfunction\
\
    function automatic logic isInRemote(PhysicalAddress_t address);\
        return `REMOTE_START <= address && address < `REMOTE_END;\
    endfunction\
\
    function automatic SimpleMemoryRequest_t simpleMemoryWrite(\
        PhysicalAddress_t physicalAddress,\
        logic [3:0] writeEnable,\
        Word_t writeData\
    );\
        return '{\
            physicalAddress: physicalAddress,\
            readEnable: 1'b0,\
            writeEnable: writeEnable,\
            writeData: writeData\
        };\
    endfunction\
\
    function automatic MemoryRequest_t memoryReadRequest(\
        VirtualAddress_t address,\
        logic readEnable\
    );\
        return '{\
            readEnable: readEnable,\
            writeEnable: 4'b0,\
            virtualAddress: address, \
            writeData: `WORD_POISON,\
            flushMode: KEEP\
        };\
    endfunction



`endif