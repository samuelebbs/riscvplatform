//# PRINT AFTER memory/cache_set.sv
`ifndef __TABLE_WALK_VH
`define __TABLE_WALK_VH

`include "riscv_types.vh"

typedef logic [11:0] PageOffset_t;
typedef logic [1:0][9:0] VirtualPageNumber_t;
typedef logic [21:0] PhysicalPageNumber_t;


typedef enum {
    INVALID_TABLE_WALK,
    TRANSLATION_FILLED,
    TRANSLATION_FAULT
} TableWalkResult_t;

typedef struct packed {
    // MSB
    PhysicalPageNumber_t ppn;
    logic [1:0] rsw;
    logic dirty;
    logic accessed;
    logic isGlobal;
    logic userAccessable;
    logic executable;
    logic writable;
    logic readable;
    logic valid;
    // LSB
} PageTableEntry_t;

interface TableWalkInterface();

    VirtualAddress_t virtualAddress;
    logic requestTranslation;

    TableWalkResult_t result;

    modport TableWalk(
        input virtualAddress,
        input requestTranslation,
        output result
    );

    modport Requester(
        output virtualAddress,
        output requestTranslation,
        input result
    );

endinterface

`endif