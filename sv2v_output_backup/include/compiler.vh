//# PRINT AFTER include/board_config.vh
`ifndef __COMPILER_VH
`define __COMPILER_VH

`define STRINGIFY(x) `"x`"

`ifdef SIMULATION_18447
`define STATIC_ASSERT(CONDITION) \
generate\
    if(!(CONDITION)) begin\
        $error("%s.%d Condition Failed: %s", `__FILE__, `__LINE__, `STRINGIFY(CONDITION));\
        end\
        endgenerate
`else
`define STATIC_ASSERT(CONDITION) \
generate\
    if(!(CONDITION)) begin\
        DoesNotExist foo();\
        end\
        endgenerate
`endif

`define DO_NOTHING ()

`define ROUND_TO_TYPE(e, eType) ((e) & ((1 << $bits(eType)) - 1))

`endif