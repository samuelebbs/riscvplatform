//# PRINT AFTER memory/memory_controller.sv
`ifndef __DRAM_VH
`define __DRAM_VH

`include "riscv_types.vh"

`define DRAM_TRANSACTION_SIZE 'd16

// There is an underlying assumption that DRAM interface is always ready, so
// there is no need for an explicit ready bit.

interface DRAMInterface();
    PhysicalAddress_t physicalAddress;
    logic readEnable;
    logic writeEnable;
    Word_t writeData;

    // Visible on the next clock cycle
    logic responseValid;
    Word_t readData;
    logic transactionComplete;

    modport Memory(
        output physicalAddress,
        output readEnable,
        output writeEnable,
        output writeData,

        input responseValid,
        input readData,
        input transactionComplete
    );

    modport DRAM(
        input physicalAddress,
        input readEnable,
        input writeEnable,
        input writeData,

        output responseValid,
        output readData,
        output transactionComplete
    );

endinterface

interface DRAMAXIInterface();
    logic [`DRAM_TRANSACTION_SIZE-1:0][31:0] writeData;
    logic [`DRAM_TRANSACTION_SIZE-1:0][31:0] readData;
    PhysicalAddress_t addressBase;
    logic readEnable, writeEnable;
    logic requestAccepted, requestCompleted, requestError;
    modport Chip(
        output writeData,
        input readData,
        output addressBase,
        output readEnable, writeEnable,
        input requestAccepted, requestCompleted, requestError
    );

    modport AXI(
        input writeData,
        output readData,
        input addressBase,
        input readEnable, writeEnable,
        output requestAccepted, requestCompleted, requestError
    );

endinterface

`endif