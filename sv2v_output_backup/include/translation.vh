//# PRINT AFTER memory/table_walk.sv
`ifndef __TRANSLATION_VH
`define __TRANSLATION_VH

`include "cache.vh"

typedef logic [3:0] TLBIndex_t;

typedef struct packed {
    CacheVirtualTag_t virtualTag;
    CachePhysicalTag_t physicalTag;
    logic isValid;
    logic readable;
    logic writable;
    logic executable;
    logic userAccessable;
    logic isGlobal;
} TranslationEntry_t;

typedef struct packed {
    TranslationEntry_t entry;
    TLBIndex_t index;
} TranslationResponse_t;

typedef enum {
    KEEP = 'd0,
    INVALIDATE_PAGE,
    INVALIDATE_ALL
} TranslationFlushMode_t;

interface TranslationInterface();
    CacheVirtualTag_t virtualTag;
    logic virtualTagValid;

    TranslationFlushMode_t flushMode;
    TranslationResponse_t translation;

    modport Requester(
        output virtualTag,
        output virtualTagValid,
        output flushMode,
        input translation
    );

    modport TLB(
        input virtualTag,
        input virtualTagValid,
        input flushMode,
        output translation
    );

endinterface : TranslationInterface

interface TranslationFill();
    TranslationResponse_t newTranslation;

    // Passive listener to determine the best translation to evict
    TranslationResponse_t instructionResponse, dataResponse;

    modport TableWalk(
        output newTranslation,

        input instructionResponse, dataResponse
    );

    modport TLB(
        input newTranslation,

        output instructionResponse, dataResponse
    );

endinterface

`define TRANSLATION_FUNCTIONS \
    function automatic TranslationEntry_t directMappedTranslation(VirtualAddress_t address);\
        CacheVirtualTag_t virtualTag;\
        CachePhysicalTag_t physicalTag;\
        virtualTag = cacheVirtualTag(address);\
        physicalTag = {2'b0, virtualTag}; // Direct mapped\
        return '{\
            virtualTag: virtualTag,\
            physicalTag: physicalTag,\
            isValid: 1'b1,\
            readable: 1'b1,\
            writable: 1'b1,\
            executable: 1'b1,\
            userAccessable: 1'b0, /* Assume direct mapped is supervisor pages */\
            isGlobal: 1'b1\
        };\
    endfunction : directMappedTranslation


`endif