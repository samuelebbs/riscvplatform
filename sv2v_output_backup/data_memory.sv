//# PRINT AFTER memory/instruction_memory.sv
`default_nettype none

`include "memory.vh"
`include "config.vh"
`include "table_walk.vh"
`include "dram_fill.vh"
`include "translation.vh"
`include "cache.vh"
`include "memory_arbiter.vh"
`include "compiler.vh"
`include "memory_controller.vh"

module DataMemory(
    MemoryInterface.Memory request,

    MemoryArbiterInterface.Requester arbiter,
    MemoryControl.Memory control,
    TableWalkInterface.Requester tableWalk,
    DRAMFillInterface.Requester dram,

    TranslationInterface.Requester tlb,

    CacheInterface.Requester cache,
    MemoryMappedIOInterface.Requester vram,
    MemoryMappedIOInterface.Requester remote,

    input logic clock, clear
);

    `CACHE_FUNCTIONS
    `MEMORY_FUNCTIONS

    readEnableOnly: assert property (
        @(posedge clock) disable iff(clear)
        request.request.readEnable |->
        !(|request.request.writeEnable) &&
        (request.request.flushMode == KEEP)
    );

    writeEnableOnly: assert property (
        @(posedge clock) disable iff(clear)
        |request.request.writeEnable |->
        !request.request.readEnable &&
        (request.request.flushMode == KEEP)
    );

    invalidateOnly: assert property (
        @(posedge clock) disable iff(clear)
        (request.request.flushMode != KEEP) |->
        !request.request.readEnable &&
        !(|request.request.writeEnable)
    );

    // Since we have a TLB invalidate, we know the next operation shouldn't be
    // another memory operation since we need to fence to protect the instruction stream
    invalidateMustFence: assert property (
        @(posedge clock) disable iff(clear)
        (request.request.flushMode != KEEP) && request.ready |=>
        request.request.flushMode == KEEP &&
        !request.request.readEnable &&
        !(|request.request.writeEnable)
    );

    addressAligned: assert property ( @(posedge clock) disable iff(clear)
        request.request.readEnable ||
        (|request.request.writeEnable) ||
        (request.request.flushMode == INVALIDATE_PAGE) |->
        request.request.virtualAddress[1:0] == 2'b0
    );

    MemoryMode_t memoryMode;
    VirtualAddress_t virtualAddress;
    ResponseType_t responseType, responseType_selected;
    PhysicalAddress_t physicalAddress;
    Word_t readData_selected;
    MemoryRequest_t request_saved;
    logic memoryHit;

    MemoryController dataController(
        .request,
        .arbiter,
        .control,
        .tableWalk,
        .dram,
        .cacheResponse(cache.response),

        .translation_tlb(tlb.translation.entry),

        .memoryMode,
        .virtualAddress,
        .responseType,
        .physicalAddress,
        .responseType_selected,
        .readData_selected,
        .request_saved,

        .memoryHit,

        .clock,
        .clear
    );

    assign vram.clock = clock;
    assign vram.clear = clear;
    assign remote.clock = clock;
    assign remote.clear = clear;

    `STATIC_ASSERT(`VRAM_SIZE == `PAGE_SIZE);
    `STATIC_ASSERT(`REMOTE_SIZE == `PAGE_SIZE);
    `STATIC_ASSERT(`PAGE_SIZE == 'h1000);

    assign tlb.virtualTag = cacheVirtualTag(virtualAddress);

    logic inVRAM, inRemote, inCache;
    assign inVRAM = isInVRAM(physicalAddress);
    assign inRemote = isInRemote(physicalAddress);
    assign inCache = isCacheAddress(physicalAddress);
    assign cache.physicalAddress = physicalAddress;

    always_comb begin
        cache.request = '{default:0};
        tlb.virtualTagValid = 1'b0;
        vram.request = '{default:0};
        remote.request = '{default:0};
        tlb.flushMode = KEEP;

        unique case(memoryMode)
            READ_MODE: begin
                cache.request = cacheReadRequest(virtualAddress);
                tlb.virtualTagValid = 1'b1;
                // Speculate that the translation is in range, then we'll check
                // it later
                vram.request = simpleMemoryRead(
                    `VRAM_START + virtualAddress[11:0]
                );
                remote.request = simpleMemoryRead(
                    `REMOTE_START + virtualAddress[11:0]
                );
            end
            WRITE_MODE: begin
                tlb.virtualTagValid = 1'b1;
                unique if(inVRAM) begin
                    vram.request = simpleMemoryWrite(
                        physicalAddress,
                        request_saved.writeEnable,
                        request_saved.writeData
                    );
                end else if(inRemote) begin
                    remote.request = simpleMemoryWrite(
                        physicalAddress,
                        request_saved.writeEnable,
                        request_saved.writeData
                    );
                end else begin
                    cache.request = cacheDirtyWrite(
                        physicalAddress,
                        request_saved.writeEnable,
                        request_saved.writeData,
                        /* Use whatever set we read from as the write set */
                        cache.response.readSet
                    );
                end
            end
            INVALIDATE_MODE: begin
                tlb.virtualTagValid = 1'b1;
                tlb.flushMode = request.request.flushMode;
            end
            NO_MODE: begin
                // Do nothing
            end
        endcase
    end

    vramWriteAlwaysSucceed: assert property (
        @(posedge clock) disable iff(clear)
        |vram.request.writeEnable |=> vram.response.responseValid
    );

    remoteWriteAlwaysSucceed: assert property (
        @(posedge clock) disable iff(clear)
        |remote.request.writeEnable |=> remote.response.responseValid
    );

    inCacheIfNotIO: assert property (
        @(posedge clock) disable iff(clear)
        !inVRAM && !inRemote |-> inCache
    );

    cacheNotIO: assert property (
        @(posedge clock) disable iff(clear)
        inCache |-> !inVRAM && !inRemote
    );

    validImpliesHit: assert property (
        @(posedge clock) disable iff(clear)
        inCache && memoryHit |->
        isCacheTagHit(cache.response, cachePhysicalTag(physicalAddress))
    );

    // Response is a clock cycle delay from request

    always_comb begin
        readData_selected = `WORD_POISON;
        responseType_selected = responseType;
        memoryHit = 1'b0;
        unique if(inVRAM) begin
            readData_selected = vram.response.readData;
            responseType_selected = VRAM_IO;
            memoryHit = 1'b1;
        end else if(inRemote) begin
            readData_selected = remote.response.readData;
            responseType_selected = REMOTE_IO;
            memoryHit = 1'b1;
        end else begin
            readData_selected = cache.response.readData;
            responseType_selected = responseType;
            // Valid implies hit
            memoryHit = isCacheHit(cache.response);
        end
    end

endmodule