#!/usr/bin/python

# templateFile = "tests/testtemplate.S"
# templateFile = "tests/branchTestTemplate.S"
# templateFile = "tests/jumpTemplate.S"
# templateFile = "tests/jumpRTemplate.S"
# templateFile = "tests/memTemplate.S"

import sys

if len(sys.argv) < 2:
    print('Usage: {} template/file/path.S'.format(sys.argv[0]))
    exit(2)

templateFile = sys.argv[1]
print('Using template: {}'.format(templateFile))

templateList = []
with open(templateFile) as f:
    templateList = f.readlines()

import re
import random

def seedRegisters(outFile):

    outFile.write('# Seeding Register Values Randomly\n')
    for registerNumber in xrange(1, 32):
        initialValue = random.randint(0, 4294967295)
        outFile.write('\tli x' + str(registerNumber) + ', ' + hex(initialValue) + '\n')

memoryMax = 4096

def randomRegister():
    return 'x' + str(random.randint(0, 31))

def randomHex(max):
    return hex(random.randint(0, max))

def random12BitValue():
    return random.randint(-2048, 2047)

def random12BitImm():
    return str(random12BitValue())

def random20BitImm():
    base = 2 ** 19
    return str(random.randint(-base - 1, base))

def random20BitHexImm():
    base = 2 ** 20
    return hex(random.randint(0, base-1))

simpleInstructions = [
    lambda: ('\tsll\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tsrl\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tsra\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tadd\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tsub\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\txor\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tand\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tor\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tslt\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tsltu\t' + randomRegister() + ', ' + randomRegister() + ', ' + randomRegister()),
    lambda: ('\tslli\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + randomHex(31)),
    lambda: ('\tsrli\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + randomHex(31)),
    lambda: ('\tsrai\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + randomHex(31)),
    lambda: ('\taddi\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + random12BitImm()),
    lambda: ('\txori\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + random12BitImm()),
    lambda: ('\tori\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + random12BitImm()),
    lambda: ('\tandi\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + random12BitImm()),
    lambda: ('\tslti\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + random12BitImm()),
    lambda: ('\tsltiu\t' + randomRegister() + ',\t' + randomRegister() + ',\t' + random12BitImm()),
    lambda: ('\tlui\t' + randomRegister() + ',\t' + random20BitHexImm()),
    lambda: ('\tauipc\t' + randomRegister() + ',\t' + random20BitHexImm()),
    # lambda: ('\tmul\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\tmulh\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\tmulhu\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\tmulhsu\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\tdiv\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\tdivu\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\trem\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
    # lambda: ('\tremu\t{},\t{},{}'.format(randomRegister(), randomRegister(), randomRegister())),
]

def loadInstruction(register, value):
    return '\taddi\t' + register + ',\tzero,\t' + str(value) + '\n'

def branchInstructionString(name, label):
    return '\t' + name + '\tx2,\tx3,\t' + label

branchInstructions = [
    lambda label: loadInstruction('x2', 1) + loadInstruction('x3', 1) + branchInstructionString('beq', label),
    lambda label: loadInstruction('x2', 1) + loadInstruction('x3', -2) + branchInstructionString('bne', label),
    lambda label: loadInstruction('x2', -1) + loadInstruction('x3', 1) + branchInstructionString('blt', label),
    lambda label: loadInstruction('x2', 1) + loadInstruction('x3', -1) + branchInstructionString('bge', label),
    lambda label: loadInstruction('x2', 1) + loadInstruction('x3', -1) + branchInstructionString('bltu', label),
    lambda label: loadInstruction('x2', -1) + loadInstruction('x3', 1) + branchInstructionString('bgeu', label),

]

# for instructionLamba in simpleInstructions:
#     print instructionLamba()

# for branch in branchInstructions:
#     print branch('label1')

def seedSimpleInstructions(outFile):

    outFile.write('# Seeding Simple Instructions\n')
    instructionCount = random.randint(1, 100)
    for i in xrange(0, instructionCount):
        index = random.randint(0, len(simpleInstructions)-1)
        outFile.write(simpleInstructions[index]() + '\n')


def generateNumber(binaryFormat, generator):
    binaryString = ""
    generatorIndex = 0
    print "Format: " + binaryFormat + " Gen: " + generator
    for c in binaryFormat:
        if c == '0' or c == '1':
            binaryString += c
        elif c == '?':
            binaryString += str(random.randint(0,1))
        elif c == '*':
            binaryString += generator[generatorIndex]
            generatorIndex += 1
        else:
            print "Bad Binary Format"
            exit(11)

    value = int(binaryString, 2)
    print "String: " + binaryString + " Value: " + str(value)

    return value

def insertRandomBranchInstructionSequence(outFile, label):
    index = random.randint(0, len(branchInstructions)-1)
    outFile.write(branchInstructions[index](label) + '\n')

def insertJumpSled(outFile, index, jumpDistance):
    nopCount = (jumpDistance / 4) - 1
    errorValue = -1107 + index
    outFile.write('# Jump Count: ' + hex(jumpDistance) + '\n')
    registerNumber = 10 + index

    for _ in xrange(0, nopCount):
        outFile.write('\taddi\tx' + str(registerNumber) + ',\tzero,\t' + str(errorValue) + '\n')
    outFile.write("# Error Value For Jump: " + str(errorValue) + '\n')


def insertBranchSequence(outFile):
    outFile.write('# Generating Branch Sequences\n')
    for i in xrange(1, 16):

        label = 'label' + str(i)

        insertRandomBranchInstructionSequence(outFile, label)

        jumpDistance = generateNumber("**????**??00", "{0:04b}".format(i))

        insertJumpSled(outFile, i, jumpDistance)
        outFile.write(label + ':\n')


    outFile.write('\tnop\n')

def insertJumpSequence(outFile):
    outFile.write('# Generating Jump Sequences\n')
    for i in xrange(1, 16):
        label = 'label' + str(i)

        returnAddress = randomRegister() if random.randint(0, 1) == 1 else 'zero'

        outFile.write('\tjal\t' + returnAddress + ',\t' + label + '\n')

        jumpDistance = generateNumber("*??????***????????00", "{0:04b}".format(i))

        insertJumpSled(outFile, i, jumpDistance)
        outFile.write(label + ':\n')


    outFile.write('\tnop\n')


loadStoreHash = {
    1: ['sb', 'lb', 'lbu'],
    2: ['sh', 'lh', 'lhu'],
    4: ['sw', 'lw']
}

def selectStoreInstruction(datasize):
    row = loadStoreHash[datasize]
    return row[0] # Because we know the first item is all of the stores

def selectLoadStoreInstruction(datasize):
    row = loadStoreHash[datasize]
    return random.choice(row)


def insertLoadStoreInstruction(immediate, outFile, provider):
    address = random.randint(0, 4095)
    datasize = 2 ** random.randint(0,2)
    address = address & ~(datasize - 1)

    # print "Address: " + hex(address) + ' DataSize: ' + str(datasize)

    baseRegister = randomRegister()
    addressRegister = randomRegister()
    dataRegister = randomRegister()

    while baseRegister == addressRegister:
        addressRegister = randomRegister()

    if addressRegister == 'x0' or baseRegister == 'x0':
        baseRegister = 'x1'
        addressRegister = 'x2'

    # if register == 'x0' or register == 'x1':
    #     register = 'x2'

    # sourceRegister = randomRegister()
    # if sourceRegister == 'x1':
    #     sourceRegister = 'x2'

    registerValue = address - immediate

    instruction = provider(datasize)

    # outFile.write('\tli\t{},\t{}\n'.format(register, registerValue))
    outFile.write('\tla\t{},\tmydata\n'.format(baseRegister))
    outFile.write('\tandi\t{},\t{},\t2044\n'.format(addressRegister, addressRegister))
    outFile.write('\tadd\t{},\t{},\t{}\n'.format(addressRegister, addressRegister, baseRegister))
    outFile.write('\t{}\t{},\t({})\n'.format(instruction, dataRegister, addressRegister))


def insertStoreSequence(outFile):
    outFile.write('# Generating Store Sequence\n')
    for i in xrange(0, 8):
        immediate = generateNumber("?????**???*", "{0:03b}".format(i))
        insertLoadStoreInstruction(immediate, outFile, selectStoreInstruction)
        insertLoadStoreInstruction(-immediate, outFile, selectStoreInstruction)

def insertLoadStoreSequence(outFile):
    outFile.write('# Generating Load/Store Sequence\n')
    for i in xrange(0,random.randint(1, 100)):
        immediate = random12BitValue()

        insertLoadStoreInstruction(immediate, outFile, selectLoadStoreInstruction)


def insertCheckSum(outFile):
    outFile.write('# Generating Checksum of Memory\n')
    outFile.write('# Checksum Stored in x1\n')
    # for i in xrange(0, 4096, 4):
    #     address = i + memoryBaseAddress

    #     outFile.write('\tli\tx2,\t' + hex(address) + '\n')
    #     outFile.write('\tlw\tx2,\t0(x2)\n')
    #     outFile.write('\tadd\tx1,\tx1,\tx2\n')
    outFile.write('\tla\tx2,\tmydata\n')
    outFile.write('\tla\tx5,\tdata_end\n')
    outFile.write('\tli\tx1,\t' + hex(0) + '\n\n')

    outFile.write('loop:\n')
    outFile.write('\tlw\tx6,\t0(x2)\n') # x6 = M[x2]
    outFile.write('\tadd\tx1,\tx1,\tx6\n') # x1 += x6
    outFile.write('\taddi\tx2,\tx2,\t0x4\n') # x2 += 4
    outFile.write('\tbne\tx2,\tx5\t,loop\n') # if (x2 != x5) goto loop



#maxJumpDistance = 4294967295
maxJumpDistance = 2 ** 16


def insertJumpRSequence(outFile):
    outFile.write('# Generating Jump R Sequences\n')
    for i in xrange(1, random.randint(2, 16)):
        label = 'label' + str(i)

        returnAddress = randomRegister() if random.randint(0, 1) == 1 else 'zero'

        jumpAddress = random.randint(4, maxJumpDistance) & ~0x3
        upperPart = jumpAddress >> 12
        lowerPart = jumpAddress & 0xFFF

        if lowerPart > 0x7FF:
            upperPart += 1
            lowerPart = jumpAddress - (upperPart << 12)
            # Because the lower part will be considered a negative signed value

        register = randomRegister()
        if register == 'x0':
            register = 'x1'

        # Loading the value into the register
        outFile.write('\tauipc\t' + register + ',\t' + hex(upperPart) + '\n')
        outFile.write('\tjalr\t' + returnAddress + ',\t' + register + ',\t' + str(lowerPart) + '\n')
        outFile.write('# Jumping to label: ' + label + '\n')
        insertJumpSled(outFile, i, jumpAddress)
        outFile.write(label + ":\n")

    outFile.write('\tnop\n')

def insertAll(outFile):
    outFile.write('# Seeding All Instructions\n')
    instructionCount = random.randint(10000, 1000000)
    # instructionCount = 20
    outFile.write('# Total Instructions: {}\n\n'.format(instructionCount))
    print('Making a test with: {} instructions'.format(instructionCount))
    for i in xrange(0, instructionCount):
        if random.randint(0, 3) < 3:
            index = random.randint(0, len(simpleInstructions)-1)
            outFile.write(simpleInstructions[index]() + '\n')
        else:
            immediate = random12BitValue()
            insertLoadStoreInstruction(immediate, outFile, selectLoadStoreInstruction)


import subprocess

def createTestWithFileName(filename):
    test = open(filename, 'w')
    test.write('# Auto-Generated Test\n')

    for line in templateList:
        match = re.search('%(.*)%', line)
        if match == None:
            test.write(line)
        else:
            command = match.group(1)
            if command == "SEED_REGISTERS":
                seedRegisters(test)
            elif command == 'SIMPLE_INSTRUCTIONS':
                seedSimpleInstructions(test)
            elif command == 'BRANCH':
                insertBranchSequence(test)
            elif command == 'JUMP':
                insertJumpSequence(test)
            elif command == 'JUMP_R':
                insertJumpRSequence(test)
            elif command == 'LOAD_STORE':
                insertLoadStoreSequence(test)
            elif command == 'STORE':
                insertStoreSequence(test)
            elif command == 'CHECK_SUM':
                insertCheckSum(test)
            elif command == 'ALL':
                insertAll(test)

            print command

    test.close()
    bashCommand = './generateGolden.py ' + filename
    print "Executing: " + bashCommand
    returnValue = subprocess.call(bashCommand.split())
    if returnValue != 0:
        print "Generating Test Failed\n"
        exit(9)


for testNumber in xrange(67, 69):
    filename = 'tests/generated/test-{}.S'.format(testNumber)
    createTestWithFileName(filename)

# createTestWithFileName("tests/generated/test-42.S")

print "Tests Generated\n"



