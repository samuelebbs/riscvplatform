#!/usr/bin/python

import os
import subprocess
import sys

# Use the reference simulator for RV32I
# GOLDEN_EXECUTABLE = 'riscv-ref-sim '
# Use arch simulator for RV32I+MAS
GOLDEN_EXECUTABLE = 'make arch-run TEST='

def generateGolden(path):
        # This will remove the extension
        filepath = os.path.splitext(path)[0]
        regfile = '{}.reg'.format(filepath)
        vramfile = '{}.vram'.format(filepath)
        logfile = '{}.log'.format(filepath)
        inputFile = '{}.inp'.format(filepath)

        bashCommand = 'make assemble TEST={}'.format(path)
        subprocess.call(bashCommand.split())

        bashCommand = '{}{}'.format(GOLDEN_EXECUTABLE, path)
        p = subprocess.Popen(bashCommand.split(), stdin=subprocess.PIPE)
        p.communicate(input='log {}\ninput {}\ngo\nrdump {}\nmdump 0x1000 0x2000 {}\n'.format(logfile, inputFile, regfile, vramfile))
        # This will raise an error if the command failed

if len(sys.argv) < 2:
    print('USAGE: {} test/file/path.S'.format(sys.argv[0]))
    exit(2)

generateGolden(sys.argv[1])
