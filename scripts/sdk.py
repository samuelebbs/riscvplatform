#!/bin/python3

import sys
import os
import shutil

if len(sys.argv) < 4:
    print("Usage: {} <version> <impl> <board_name>".format(sys.argv[0]))
    exit(2)

# Delete SDK folder
# Copy hdf
# Write sdk.tcl to versionFolder/sdk.tcl

versionFolder = sys.argv[1]
implementation = sys.argv[2]
board_name = sys.argv[3]
# This is so sad, but Windows sucks
projectRoot = os.getcwd()
# This will convert /mnt/c to c:
windowsRoot = 'c:{}'.format(projectRoot[6:])

vivadoRoot = os.path.join(projectRoot, 'vivado')
versionPath = os.path.join(vivadoRoot, versionFolder)
tclScriptPath = os.path.join(versionPath, 'sdk.tcl')
runScriptPath = os.path.join(versionPath, 'sdk_run.tcl')
deployScriptPath = os.path.join(versionPath, 'sdk_deploy.tcl')
sdkPath = os.path.join(versionPath, board_name, '{}.sdk'.format(board_name))
vivadoHDF = os.path.join(versionPath, board_name, '{}.runs'.format(board_name), implementation, 'design_1_wrapper.sysdef')
sdkHDF = os.path.join(sdkPath, 'design_1_wrapper.hdf')

print('Setting up file system for {}'.format(sdkPath))

if os.path.isdir(sdkPath):
	shutil.rmtree(sdkPath)
os.mkdir(sdkPath)
shutil.copyfile(vivadoHDF, sdkHDF)

def writeTemplate(template, path):
    print('Generating TCL Script: {}'.format(path))
    script_body = template.format(projectRoot=windowsRoot, versionFolder=versionFolder, board_name=board_name)

    scriptFile = open(path, 'w')
    scriptFile.write(script_body)
    scriptFile.close()

make_template = """
# Set Workspace
setws {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk
createhw -name design_1_wrapper_hw_platform_0 -hwspec {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk/design_1_wrapper.hdf

createapp -name riscv_bootloader -app {{Hello World}} -proc ps7_cortexa9_0 -hwproject design_1_wrapper_hw_platform_0 -os standalone
"""

writeTemplate(make_template, tclScriptPath)


deploy_template = """
setws {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk

projects -clean
projects -build

puts "connecting"
connect
puts [targets]
targets -set -nocase -filter {{name =~ "ARM*#0" }}
# Reset everything
puts "reseting everything"
rst -cores
rst -system
rst
puts "deploying to FPGA"
fpga  {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk/design_1_wrapper_hw_platform_0/design_1_wrapper.bit
loadhw {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk/design_1_wrapper.hdf
puts [targets]
puts [state]
"""

writeTemplate(deploy_template, deployScriptPath)

run_template = """
setws {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk

source {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk/design_1_wrapper_hw_platform_0/ps7_init.tcl

connect
puts [targets]
targets -set -nocase -filter {{name =~ "ARM*#0" }}

rst -processor
ps7_init
ps7_post_config
puts "programming ARM core"
dow -clear {projectRoot}/vivado/{versionFolder}/{board_name}/{board_name}.sdk/riscv_bootloader/Debug/riscv_bootloader.elf
puts [targets]
puts [state]

puts "starting RISC-V core"
con -block

# bpadd -addr &main
# bpadd -addr &exit
# con -block -timeout 500
# puts [rrd]
# con
# puts [rrd]
# con
"""

writeTemplate(run_template, runScriptPath)

print('All done')
