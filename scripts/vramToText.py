#!/usr/bin/python

import sys
import os

if len(sys.argv) < 2:
    print('Usage: {} <test>.vram'.format(sys.argv[0]))
    exit(2)

vram = open(sys.argv[1])
vram.readline()
vram.readline()
vram.readline()

for row in xrange(0, 25):
    for col in xrange(0, 40):
        line = vram.readline()
        parts = line.split(' ')
        digits = [parts[1][2:], parts[3][2:]]
        for digit in digits:
            intValue = int(digit, 16)
            charValue = chr(intValue)
            sys.stdout.write(charValue)
    print('')
    # intValue = int(parts[1][2:], 16)
    # let = chr(intValue)
    # # sys.stdout.write(str(intValue))
    # if(intValue != 0):
    #     print("Parts[1]: {} intValue: {} chr: {}", parts[1], intValue, let)



