#!/bin/bash

set -e

NAME=$1
shift

SIDES=${SIDES:-duplex}
COLUMNS=${COLUMNS:-2}
TABSTOP=${TABSTOP:-8}

if ! [ -x "$(command -v ghead)" ]; then
figlet -k $NAME | a2ps -M Letter --sides=tumble -1 -r --title=$NAME --stdin=$NAME -o - - | head -n -3
else
figlet -k $NAME | a2ps -M Letter --sides=tumble -1 -r --title=$NAME --stdin=$NAME -o - - | ghead -n -3
fi

a2ps --delegate no --toc -M Letter --tabsize=$TABSTOP --sides=$SIDES -r --columns=$COLUMNS --chars-per-line 80 -o - --prologue=410fixed $*