#!/bin/python3

import sys
import os
import shutil
import subprocess

if len(sys.argv) < 3:
	print('Usage: {} <version> <board_name>'.format(sys.argv[0]))
	exit(2)

# Copy files over
# Update .cproject

versionFolder = sys.argv[1]
board_name = sys.argv[2]
projectRoot = os.getcwd()

sdkRoot = os.path.join(projectRoot, 'vivado', versionFolder, board_name, '{}.sdk'.format(board_name))
testPath = os.path.join(projectRoot, 'output', 'sdk', 'sdk.test.o')
bootloaderPath = os.path.join(sdkRoot, 'riscv_bootloader')
bootloaderSrc = os.path.join(bootloaderPath, 'src')

if not os.path.exists(testPath):
	print('Test missing: {}'.format(testPath))
	exit(3)

if not os.path.exists(bootloaderSrc):
	print('SDK Missing: {}'.format(sdkRoot))
	exit(4)

srcFind = "find -L {} -type f -name *.c -o -name *.h -o -name *.ld".format('sdk')

def executeCommand(command):
    print(command)
    output = subprocess.run(command.split(), stdout=subprocess.PIPE)
    return output.stdout

def toList(output):
    return [file.decode("utf-8") for file in output.splitlines()]

sources = toList(executeCommand(srcFind))

# sourceFiles = [
# 	'helloworld.c',
# 	'main.c',
# 	'lscript.ld',
# 	'remote.h',
# 	'riscv_register_names.h',
# 	'rdump.c',
# 	'rdump.h',
# 	'libc_extensions.h',
# ]

print('Copying files')
for fileName in sources:
	filePath = os.path.join(projectRoot, fileName)
	srcPath = os.path.join(bootloaderSrc, fileName.rsplit('/', 1)[1])
	shutil.copyfile(filePath, srcPath)

testSrcPath = os.path.join(bootloaderSrc, 'sdk.test.o')
shutil.copyfile(testPath, testSrcPath)


import xml.etree.ElementTree as ET
import random

projectPath = os.path.join(bootloaderPath, '.cproject')

print('Reading cproject: {}'.format(projectPath))
tree = ET.parse(projectPath)
root = tree.getroot()

linkers = [element for element in root.iter('tool') if element.attrib['superClass'] == 'xilinx.gnu.armv7.c.toolchain.linker.debug']

if len(linkers) != 1:
	print('No linker in cproject {}'.format(projectPath))
	exit(5)

linker = linkers[0]

userObjectsOption = ET.SubElement(linker, 'option')
userObjectsOption.set('valueType', 'userObjs')
userObjectsOption.set('id', 'xilinx.gnu.c.link.option.userobjs.{}'.format(random.randint(0, 2**30)))
userObjectsOption.set('superClass', 'xilinx.gnu.c.link.option.userobjs')

listOptionValue = ET.SubElement(userObjectsOption, 'listOptionValue')
listOptionValue.set('builtIn', 'false')
listOptionValue.set('value', '"${workspace_loc:/${ProjName}/src/sdk.test.o}"')

print('Writing cproject: {}'.format(projectPath))

cprojectFile = open(projectPath, 'w')
cprojectFile.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?><?fileVersion 4.0.0?>')

tempPath = '{}.tmp'.format(projectPath)
tree.write(tempPath)
tempFile = open(tempPath, 'r')

for line in tempFile:
	cprojectFile.write(line)

tempFile.close()
cprojectFile.close()
os.remove(tempPath)

print('Patching completed')