#!/bin/python3

import sys
import os
import subprocess
import re

if len(sys.argv) < 2:
    print("Usage: {} <root>".format(sys.argv[0]))
    exit(2)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

ROOT = sys.argv[1]

SOURCE_EXTENSIONS = ['.c', '.S', '.h', '.sv', '.vh', '.v', '.svh']
SOURCE_ARGS = ['-o -name *{}'.format(extension) for extension in SOURCE_EXTENSIONS]

FIND_SOURCE_COMMAND = 'find -L {} -type f -name README {}'.format(ROOT, ' '.join(SOURCE_ARGS))

def executeCommand(command):
    eprint(command)
    output = subprocess.run(command.split(), stdout=subprocess.PIPE)
    return output.stdout

def toList(output):
    return [file.decode("utf-8") for file in output.splitlines()]

sources = toList(executeCommand(FIND_SOURCE_COMMAND))

# Uncomment these lines to just dump a simple list without using the in-code order
# print('\n'.join(sources))
# exit(0)

ordering = {}

for file in sources:
    with open(file) as fp:
        for line in fp:
            match = re.search('//# ([A-Z]+) ([A-Z]+) ?(.*)$', line)
            if match == None:
                eprint('WARN: Unable to find print command: {} COMMAND: {}'.format(file, line))
                break
            else:
                magic = match.group(1)
                if magic != 'PRINT':
                    eprint('Error: Bad Magic {} in line: {}'.format(magic, line))
                    exit(4)
                keyword = match.group(2)
                if keyword == 'FIRST':
                    ordering[file] = '<first>'
                elif keyword == 'AFTER':
                    ordering[file] = match.group(3)
                elif keyword == 'NEVER':
                    eprint("WARN: Not printing {}".format(file))
                else:
                    eprint('Error: Unknown keyword {} in line: {}'.format(keyword, line))
                    exit(5)
                break


first = {k:v for (k,v) in ordering.items() if v == '<first>'}

if len(first) != 1:
    eprint("Error: Incorrect first item {}".format(first))
    exit(6)

last = next(iter(first.keys()))
print(last)
del ordering[last]

while len(ordering) > 0:
    found = False
    for (file, previous) in ordering.items():
        if last.endswith('/{}'.format(previous)):
            print(file)
            del ordering[file]
            last = file
            found = True
            break
    if not found:
        eprint("Unable to find the next file to print after: {} left: {}".format(last, ordering))
        # This is an error, but it is much more helpful for this to return 0 as a debug tool
        exit(0)
