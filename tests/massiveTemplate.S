# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
    %SEED_REGISTERS%

        la x1, mydata

    %STORE%

    %LOAD_STORE%

    %ALL%

    %CHECK_SUM%

        li a0, 0xa
        ecall
