
// #include <stdio.h>

// void add1(unsigned long long x, unsigned long long* result) {
	// *result = x + 1;
// }

typedef unsigned long long u_quad_t;    /* 64 bits, unsigned */

// return a % b;
u_quad_t __umoddi3(u_quad_t a, u_quad_t b) {

	while(a >= b) {
		a -= b;
	}
	
	return a;
}


int main(void) {
	
	int base = 0x10;
	for(unsigned long long x = 0; x < 0xFFFFFFFFFFFFllu; x++) {
		if(x % base >= (long unsigned int)base) {
			return -1;
		}
	}
	// unsigned long long x = 0x1234567890abcllu;
	// unsigned long long y = 0x0;
	// add1(x, &y);
	// int y = 16;
	// char buf[32];
	// sprintf(buf, "Hello: %x", y);
    // return x % y;
	// return (unsigned int)y;
	return 0;
}