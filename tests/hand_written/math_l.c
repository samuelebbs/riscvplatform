
int main(void) {
    volatile long u    = 0x100d;
    int           base = 16;
    if (u % base != 0xd) {
        return 1;
    }

    volatile long n = 3;
    if (n / n != 1) {
        return 2;
    }

    if (n % n != 0) {
        return 3;
    }

    volatile long big = 0x80000000;
    // volatile long zero = 0x0;

    // if (big / zero != -1) {
    //     return 4;
    // }

    if (big % base != 0x0) {
        return 5;
    }

    if ((big + 1ll) % base != -0xf) {
        return 6;
    }

    // if (big % zero != big) {
    //     return 7;
    // }

    if (big / -1 != (long)0x80000000) {
        return 11;
    }

    big = -1;
    if (big % base != -1) {
        return 8;
    }

    if (big / big != 1) {
        return 9;
    }

    if (sizeof(long) != 4) {
        return 100;
    }


    return 0xD00D;
}