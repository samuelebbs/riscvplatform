# Use sfence to update an alias

#define SET_PAGE_TABLE(symbol, perms)\
    /* Find the physical page number for main (t1) */\
    la t0, symbol;\
    srli t1, t0, 12;\
\
    /* Setup the page table entry (t2) */\
    slli t2, t1, 10;\
    ori t2, t2, perms;\
\
    /* Compute the address of the page table entry (t3) */\
    slli t3, t1, 2; /* t3 = physical page number * 4 */\
    la t4, page_table;\
    add t3, t3, t4; /* t3 = page_table + ppn * 4 */\
\
    /* Store the page table entry */\
    sw t2, 0(t3)


# Hopefully this is page aligned
.data
page_directory:
.space 4096
page_table:
.space 4096
buffer1:
.space 4096
buffer2:
.space 4096

.text
.global main
main:
    ######## Setup the page table in the page directory at index 0 ########

    # Find the physical page number of the page table (t1)
    la t0, page_table
    srli t1, t0, 12

    # Setup the page table entry (t2)
    slli t2, t1, 10
    # Valid non-leaf
    ori t2, t2, 0x1

    # Assume PPN[21:10] is zero, so set the page table at entry 0
    la t3, page_directory
    sw t2, 0(t3)

    ####### Setup the kernel code entry in the page table #########
    SET_PAGE_TABLE(main, 0xf);

    ###### Setup the page table as writable by the kernel #######
    SET_PAGE_TABLE(page_table, 0xf);

    ###### Setup buffer1 as writable by the kernel #######
    SET_PAGE_TABLE(buffer1, 0xf);

    ###### Setup buffer2 as writable by the kernel #######
    SET_PAGE_TABLE(buffer2, 0xf);

    ##### Sanity checks #####
    la s0, buffer1
    la s1, buffer2
    li a0, 0x11223344
    li a1, 0x55667788
    li a2, 0xaabbccdd
    li a3, 0x12345678
    sw a0, 0xc(s0)
    sw a1, 0xc(s1)
    sw a2, 0x3c(s0)
    sw a3, 0x3c(s1)

    lw s11, 0xc(s0) # Expects 0x11223344
    lw s10, 0xc(s1) # Expects 0x55667788
    lw s9, 0x3c(s0) # Expects 0xaabbccdd
    lw s8, 0x3c(s1) # Expects 0x12345678

    ###### Enable Virtual Memory #######

    # Compute the physical page number of the page directory (t1)
    la t0, page_directory
    srli t1, t0, 12

    # Setup the satp entry (t2)
    # Set the highest order bit (virtual memory enable)
    li t2, 0x80000000
    # Or in the physical page number of the page directory
    or t2, t2, t1

    # Enable virtual memory
    CSRW satp, t2

    #### Sanity checks #####
    lw s7, 0xc(s0) # Expects 0x11223344
    lw s6, 0xc(s1) # Expects 0x55667788
    lw s5, 0x3c(s0) # Expects 0xaabbccdd
    lw s4, 0x3c(s1) # Expects 0x12345678

    #### Update the page table for buffer2 to point to buffer1

    # Find the physical page number for buffer1 (t1)
    la t0, buffer1
    srli t1, t0, 12

    # Find the physical page number for buffer2 (t2)
    la t0, buffer2
    srli t2, t0, 12

    # Setup the page table entry using the frame of buffer1 (t3)
    slli t3, t1, 10
    ori t3, t3, 0xf # kernel|READ|WRITE|EXECUTE|VALID

    # Compute the address of the page table entry for buffer2 (t3)
    slli t4, t2, 2 # t3 = ppn * 4
    la t5, page_table
    add t4, t4, t5

    # Store the page table entry
    sw t3, 0(t4)
    sfence.vma x0, x0 # Flush everything

    ##### Check #######
    lw a7, 0xc(s1) # Expects 0x11223344
    lw a6, 0x3c(s1) # Expects 0xaabbccdd
    lw a5, 0xc(s0) # Expects 0x11223344
    lw a4, 0x3c(s0) # Expects 0xaabbccdd

    li a0, 0x55556666
    li a1, 0x99998888
    sw a0, 0xc(s1)
    sw a1, 0x3c(s1)

    ##### Check #######
    lw t0, 0xc(s0) # Expects 0x55556666
    lw t1, 0x3c(s0) # Expects 0x99998888
    lw t2, 0xc(s1) # Expects 0x55556666
    lw t3, 0x3c(s1) # Expects 0x99998888

    li a0, 0xa
    ecall