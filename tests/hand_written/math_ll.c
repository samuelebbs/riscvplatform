
int main(void) {
    volatile long long u    = 0x100dll;
    int                base = 16;
    if (u % base != 0xdll) {
        return 1;
    }

    volatile long long n = 3;
    if (n / n != 1) {
        return 2;
    }

    if (n % n != 0) {
        return 3;
    }

    volatile long long big  = 0x80000000ll;
    volatile long long neg1 = -1ll;
    // volatile long long zero = 0x0;

    // if (big / zero != 0xFFFFFFFFFFFFFFFFll) {
    //     return 4;
    // }

    if (big % base != 0x0ll) {
        return 5;
    }

    if ((big + 1ll) % base != 0x1ll) {
        return 6;
    }

    // if (big % zero != big) {
    //     return 7;
    // }

    if (big / neg1 != (long long)0xFFFFFFFF80000000ll) {
        return 11;
    }

    big = -1ll;
    if (big % base != -1ll) {
        return 8;
    }

    if (big / big != 1) {
        return 9;
    }

    big = 0x8000000000000000ll;
    if (big / -1 != big) {
        return 10;
    }

    if (sizeof(long long) != 8) {
        return 100;
    }


    return 0xF00D;
}