    .text
    .global main
main:
    li x1, 0x1
    li x2, -0x1
    li x3, 0x7FFFFFFF
    li x4, 0x80000000
    DIVU x5, x1, x1 # 1 / 1 = 1
    DIVU x6, x1, x0 # 1 / 0 = 0xFFFF_FFFF
    DIVU x7, x1, x4 # 1 / 0x8000_0000 = 0
    DIVU x8, x1, x3 # 1 / 0x7FFF_FFFF = 0
    DIVU x9, x1, x2 # 1 / -1 = 0
    DIVU x31, x2, x1 # -1 / 1 = -1
    DIVU x11, x2, x0 # -1 / 0 = 0xFFFF_FFFF
    DIVU x12, x2, x4 # -1 / 0x8000_0000 = 1
    DIVU x13, x2, x3 # -1 / 0x7FFF_FFFF = 2
    DIVU x14, x2, x2  # -1 / -1 = 1
    DIVU x15, x0, x1 # 0 / 1 = 0
    DIVU x16, x0, x4 # 0 / 0x8000_0000 = 0
    DIVU x17, x0, x2 # 0 / -1 = 0
    DIVU x18, x4, x2 # 0x8000_0000 / -1 = 0
    DIVU x19, x4, x0 # 0x8000_0000 / 0 = 0xFFFF_FFFF
    DIVU x20, x4, x1 # 0x8000_0000 / 1 = 0x8000_0000
    DIVU x21, x4, x3 # 0x8000_0000 / 0x7FFF_FFFF = 1
    DIVU x22, x4, x4 # 0x8000_0000 / 0x8000_0000 = 1
    DIVU x23, x3, x1 # 0x7FFF_FFFF / 1 = 0x7FFF_FFFF
    DIVU x24, x3, x2 # 0x7FFF_FFFF / -1 = 0
    DIVU x25, x3, x0 # 0x7FFF_FFFF / 0 = 0xFFFF_FFFF
    DIVU x26, x3, x3 # 0x7FFF_FFFF / 0x7FFF_FFFF = 1
    DIVU x27, x3, x4 # 0x7FFF_FFFF / 0x8000_0000 = 0
    li x28, 35
    li x29, 7
    DIVU x28, x28, x29 # 35 / 7 = 5 (Sanity Check)
    li x30, 0x7FFF0000
    li x29, 0x7000FFFF
    DIVU x29, x30, x29 # 0x7FFF_0000 / 0x7000FFFF = 1
    li x1, 0xFFFF
    DIVU x30, x30, x1 # 0x7FFF_0000 / 0xFFFF = 0x7FFF

    li a0, 0xa
    ecall