
int test(int n) {
    if (n <= 0) {
        return 0;
    } else {
        return test(n - 1) + 1;
    }
}

int main() {
    return test(2);
}