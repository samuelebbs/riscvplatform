# additest.s
#
# Addi Instruction test
#
# This test simply tests that the addi instruction is working as expected.

    .text                       # Declare the code to be in the .text segment
    .global main                # Make main visible to the linker
main:
    addi    t0, zero, 10        # t0 (x5) = 10
    addi    t1, zero, 5         # t1 (x6) = 5
    addi    t2, zero, 7
    addi    t3, zero, 9
    addi    t4, zero, 30
    addi    t0, t0,   50        # t0 free
    addi    t5, zero, 90        # t1 free
    addi    t2, t1,   300       # t2 free    # t2 (x7) = t1 + 300
    addi    t3, t1,   500       # t3 free   # t3 (x28) = 500
    # This is not a data hazard because we are smashing the old value of t5
    addi    t5, t1,   34        # t4 free t4 (x29) = t3 + 34
    addi    t4, t4,   45        # t0 free t4 (x29) = t4 + 45

    addi    a0, zero, 0xa       # a0 (x10) = 0xa
    nop
    nop
    nop
    nop
    ecall                       # Terminate the simulation by passing 0xa to
                                # ecall in register a0 (x10).
