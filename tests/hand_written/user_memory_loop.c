
#include <stdbool.h>
#include <stddef.h>

bool userMemory_safeCopy(char*  sourcePointer,
                         char*  destinationPointer,
                         size_t length) {
    for (size_t i = 0; i < length; i++) {
        char t0               = sourcePointer[i];
        destinationPointer[i] = t0;
    }
    return true;
}

int main(void) {
    userMemory_safeCopy((char*)0xDEAD, (char*)0xD00D, 5);
    return 5;
}