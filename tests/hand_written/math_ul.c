
int main(void) {
    volatile unsigned long u    = 0x100d;
    int                    base = 16;
    if (u % base != 0xd) {
        return 1;
    }

    volatile unsigned long n = 3;
    if (n / n != 1) {
        return 2;
    }

    if (n % n != 0) {
        return 3;
    }

    volatile unsigned long big = 0x80000000;
    // volatile unsigned long zero = 0x0;

    // if (big / zero != 0xFFFFFFFF) {
    //     return 4;
    // }

    if (big % base != 0x0) {
        return 5;
    }

    if ((big + 1) % base != 0x1) {
        return 6;
    }

    // if (big % zero != big) {
    //     return 7;
    // }

    big = 0xFFFFFFFF;
    if (big % base != 0xF) {
        return 8;
    }

    if (big / big != 1) {
        return 9;
    }

    if (sizeof(unsigned long) != 4) {
        return 100;
    }

    return 0xB33F;
}