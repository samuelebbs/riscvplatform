
.data
mydata:
    .space 8192
data_end:

.text
.global main
main:
    # Set up memory with a few values
    la x2, mydata
    li x3, 0x55
    sw x3, 0(x2)
    li x3, 0x55667788
    sw x3, 4(x2)
    li x3, 0x11223344
    li x31, 4096
    add x31, x2, x31
    sw x3, 0(x31)


    # Try some operations
    # simple read
    lw x4, 0(x2) # x4 = 0x55

    # Abort due to load on same address
    lr.W x5, (x2) # x5 = 0x55
    lw x6, 0(x2)  # x6 = 0x55 - Reservation aborted
    li x7, 0x12344321
    sc.W x8, x7, (x2) # Should fail, x8 = 1
    lw x9, 0(x2) # Should be 0x55

    # Abort due to read on same cache line

    lr.W x11, 0(x2) # x11 = 0x55
    lw x12, 4(x2) # Cache line conflict - Our specification says abort x12 = 0x55667788
    li x13, 0x99884455
    sc.W x14, x13, (x2) # Should fail, x14 = 1
    lw x15, 0(x2) # should be 0x55

    # Abort due to read different page
    lr.W x16, 0(x2) # x16 = 0x55
    lw x17, 0(x31) # x17 = 0x11223344
    li x13, 0x1234567
    sc.W x18, x13, (x2)
    lw x19, 0(x2) # should be 0x55

    # Success
    lr.W x20, 0(x2) # should be 0x55
    addi x21, x20, 1
    sc.W x22, x21, (x2) # should be 0
    lw x23, 0(x2) # should be 0x56

    li a0, 0xa
    ecall

