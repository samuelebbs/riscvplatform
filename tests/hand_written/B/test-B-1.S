# addi test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 0x7FFFFFFF
    li x2, -2147483648
    li x3, 1
    li x4, -1
    addi x4, x1, -1
    addi x5, x2, -1
    addi x6, x1, 1
    addi x7, x2, 1
#    addi x8, x3, 0x7FFFFFFF
#    addi x9, x4, 0x7FFFFFFF
#    addi x10, x3, -2147483648
#    addi x11, x4, -2147483648
#    addi x12, x1, 0x7FFFFFFF
#    addi x13, x2, 0x7FFFFFFF
#    addi x14, x1, -2147483648
#    addi x15, x2, -2147483648
    addi x16, x1, 0
    addi x17, x2, 0
    addi x18, x3, 0
    addi x19, x4, 0
    addi x20, x3, 1
    addi x21, x4, 1
    addi x22, x3, -1
    addi x23, x4, -1

    li a0, 0xa
    ecall
