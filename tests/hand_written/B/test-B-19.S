#sltu test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    sltu x5, x1, x2
    sltu x6, x2, x1
    sltu x7, x1, x1
    sltu x8, x1, x2
    sltu x9, x1, x3
    sltu x10, x1, x4
    sltu x11, x2, x1
    sltu x12, x2, x2
    sltu x13, x2, x3
    sltu x14, x2, x4
    sltu x15, x3, x1
    sltu x16, x3, x2
    sltu x17, x3, x3
    sltu x18, x3, x4
    sltu x19, x4, x1
    sltu x20, x4, x2
    sltu x21, x4, x3
    sltu x22, x4, x4
    sltu x23, x0, x1
    sltu x24, x0, x2
    sltu x25, x0, x3
    sltu x26, x0, x4
    sltu x27, x1, x0
    sltu x28, x2, x0
    sltu x29, x3, x0
    sltu x30, x4, x0


    li a0, 0xa
    ecall