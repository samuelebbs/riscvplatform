# sltiu test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    sltiu x4, x1, -1
    sltiu x5, x2, -1
    sltiu x6, x1, 1
    sltiu x7, x2, 1
    // sltiu x8, x3, 2147483647
    // sltiu x9, x4, 2147483647
    // sltiu x10, x3, -2147483648
    // sltiu x11, x4, -2147483648
    // sltiu x12, x1, 2147483647
    // sltiu x13, x2, 2147483647
    // sltiu x14, x1, -2147483648
    // sltiu x15, x2, -2147483648
    sltiu x16, x1, 0
    sltiu x17, x2, 0
    sltiu x18, x3, 0
    sltiu x19, x4, 0
    sltiu x20, x3, 1
    sltiu x21, x4, 1
    sltiu x22, x3, -1
    sltiu x23, x4, -1

    li a0, 0xa
    ecall
