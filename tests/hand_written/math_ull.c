
int main(void) {
    volatile unsigned long long u    = 0x100dllu;
    int                         base = 16;
    if (u % base != 0xdllu) {
        return 1;
    }

    volatile unsigned long long n = 3;
    if (n / n != 1) {
        return 2;
    }

    if (n % n != 0) {
        return 3;
    }

    volatile unsigned long long big = 0x80000000llu;
    // volatile unsigned long long zero = 0x0;

    // if (big / zero != 0xFFFFFFFFFFFFFFFFllu) {
    //     return 4;
    // }

    if (big % base != 0x0llu) {
        return 5;
    }

    if ((big + 1llu) % base != 0x1llu) {
        return 6;
    }

    // if (big % zero != big) {
    //     return 7;
    // }

    big = 0xFFFFFFFFFFFFFFFFllu;
    if (big % base != 0xFllu) {
        return 8;
    }

    if (big / big != 1) {
        return 9;
    }

    if (sizeof(unsigned long long) != 8) {
        return 100;
    }


    return 0xFACE;
}