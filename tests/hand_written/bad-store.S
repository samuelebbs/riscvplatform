# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:


# Generating Checksum of Memory
# Checksum Stored in x1
    la  x2, data
    addi x5, x2, 0x8
	li	x1,	0x0

    li x12, 0xf00d
    la x13, data

    nop
    nop
    nop
    nop
    addi x12, x12, -0xd

    sw x12, 0(x13)
    lw x14, 0(x2)
    lw x15, 4(x2)

    li a0, 0xa
    ecall
