# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
    %BRANCH%

    j exitPart1

exitPart2:
    li a0, 0xa
    ecall

exitPart1:
    addi x1, zero, 1
    addi x2, zero, 1
    beq x1, x2, exitPart2

