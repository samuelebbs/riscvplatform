# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x7b102591
	li x2, 0xc176f04c
	li x3, 0xfb239a4b
	li x4, 0x42959c1e
	li x5, 0xf66b9507
	li x6, 0xc56fbedd
	li x7, 0x521ab29d
	li x8, 0x39630897
	li x9, 0xdfdedcb1
	li x10, 0x587f5eb2
	li x11, 0x9205adaf
	li x12, 0x6f9b10ba
	li x13, 0x2d55c492
	li x14, 0x21d5b5da
	li x15, 0xe4024490
	li x16, 0x7a48b86a
	li x17, 0x7a29b5ff
	li x18, 0x57049bf8
	li x19, 0xb12f4ed4
	li x20, 0x8570915a
	li x21, 0x9d2f175a
	li x22, 0x85ecbc5b
	li x23, 0xb7ff2aa6
	li x24, 0x41636d53
	li x25, 0x676439
	li x26, 0x5c1aec80
	li x27, 0xedc75f18
	li x28, 0x3d2d86c
	li x29, 0xbf2e4d36
	li x30, 0xe6b85a7d
	li x31, 0xecea794a

# Seeding Simple Instructions
	sll	x19, x22, x3
	srl	x18, x30, x4
	xor	x13, x3, x28
	or	x2, x25, x29
	slt	x14, x9, x24
	sll	x19, x12, x26
	sltu	x10, x13, x29
	or	x17, x25, x20
	xor	x6, x21, x29
	xor	x26, x27, x20
	add	x20, x21, x30
	add	x26, x30, x11
	sra	x27, x13, x28
	or	x23, x16, x11
	sub	x21, x7, x30
	or	x17, x16, x29
	xor	x0, x8, x18
	srl	x25, x3, x15
	slt	x30, x31, x22
	sub	x5, x13, x31
	and	x5, x12, x29
	or	x20, x27, x29
	xor	x10, x4, x9
	srl	x3, x5, x9
	slt	x3, x15, x7
	sltu	x5, x4, x11
	xor	x18, x7, x28
	xor	x30, x27, x13
	srl	x27, x6, x2
	sll	x16, x16, x15
	sub	x27, x26, x11
	sltu	x23, x15, x10
	or	x2, x22, x17
	and	x15, x11, x0
	and	x3, x1, x25

    li a0, 0xa
    ecall
