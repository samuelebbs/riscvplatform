# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0xbd42610e
	li x2, 0xf9d785d
	li x3, 0xe695777c
	li x4, 0x6e043d34
	li x5, 0xda520dfc
	li x6, 0xa2b69ab5
	li x7, 0xefcb5ed
	li x8, 0x75cc93d5
	li x9, 0x5c602c89
	li x10, 0x7915173d
	li x11, 0xfd324c5c
	li x12, 0xe0e74361
	li x13, 0xf57c6622
	li x14, 0x2c6a3127
	li x15, 0x199c9617
	li x16, 0xfeebcfc6
	li x17, 0xfb160fac
	li x18, 0xacab9409
	li x19, 0x2ea53769
	li x20, 0xac8e7399
	li x21, 0x2ee9bf9f
	li x22, 0xfc22d40
	li x23, 0x41644d69
	li x24, 0x842f7d32
	li x25, 0xff9cddfa
	li x26, 0xde470239
	li x27, 0xb91f62a9
	li x28, 0xdb19b02a
	li x29, 0x4802dc4e
	li x30, 0x21ba5e18
	li x31, 0x95ca9ef7

        la x1, mydata

# Generating Store Sequence
	li	x2,	-1814
	add	x2,	x1,	x2
	sh	x18,	1934(x2)
	li	x2,	6014
	add	x2,	x1,	x2
	sb	x29,	-1934(x2)
	li	x24,	-1406
	add	x24,	x1,	x24
	sb	x17,	1679(x24)
	li	x18,	3435
	add	x18,	x1,	x18
	sb	x4,	-1679(x18)
	li	x2,	2742
	add	x2,	x1,	x2
	sw	x26,	790(x2)
	li	x4,	4079
	add	x4,	x1,	x4
	sb	x24,	-790(x4)
	li	x6,	-1265
	add	x6,	x1,	x6
	sb	x28,	1759(x6)
	li	x2,	4337
	add	x2,	x1,	x2
	sb	x23,	-1759(x2)
	li	x19,	1115
	add	x19,	x1,	x19
	sb	x11,	866(x19)
	li	x30,	2796
	add	x30,	x1,	x30
	sh	x22,	-866(x30)
	li	x10,	1329
	add	x10,	x1,	x10
	sh	x31,	1129(x10)
	li	x3,	3885
	add	x3,	x1,	x3
	sw	x21,	-1129(x3)
	li	x3,	596
	add	x3,	x1,	x3
	sw	x10,	816(x3)
	li	x19,	2848
	add	x19,	x1,	x19
	sh	x30,	-816(x19)
	li	x7,	2859
	add	x7,	x1,	x7
	sb	x14,	1081(x7)
	li	x5,	4223
	add	x5,	x1,	x5
	sh	x12,	-1081(x5)

# Generating Load/Store Sequence
	li	x28,	3459
	add	x28,	x1,	x28
	sb	x9,	-1873(x28)
	li	x25,	2067
	add	x25,	x1,	x25
	sw	x7,	-1679(x25)
	li	x9,	2995
	add	x9,	x1,	x9
	lhu	x8,	-1447(x9)
	li	x25,	781
	add	x25,	x1,	x25
	lhu	x26,	-93(x25)
	li	x28,	2050
	add	x28,	x1,	x28
	lw	x29,	-50(x28)
	li	x4,	913
	add	x4,	x1,	x4
	lb	x20,	-778(x4)
	li	x29,	342
	add	x29,	x1,	x29
	lw	x6,	666(x29)
	li	x12,	1594
	add	x12,	x1,	x12
	lb	x31,	-1130(x12)
	li	x28,	3437
	add	x28,	x1,	x28
	lb	x10,	144(x28)
	li	x25,	3658
	add	x25,	x1,	x25
	lb	x23,	420(x25)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
