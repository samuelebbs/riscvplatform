# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x37be7104
	li x2, 0x9d011b4c
	li x3, 0x47120623
	li x4, 0xc1ca5dbc
	li x5, 0x4074ba6
	li x6, 0x698065c0
	li x7, 0x56bc54b
	li x8, 0xc1a6aca7
	li x9, 0x96d1f828
	li x10, 0x57b317a3
	li x11, 0x6a345012
	li x12, 0x5a711069
	li x13, 0xef85f778
	li x14, 0xefa823fc
	li x15, 0xa5a4a97a
	li x16, 0x71b1c247
	li x17, 0xdaa44e0
	li x18, 0x22914e17
	li x19, 0x199c586e
	li x20, 0x7d64c0b
	li x21, 0x4a1cc35e
	li x22, 0xf0ac1ccc
	li x23, 0x2a0865c6
	li x24, 0x2ede5499
	li x25, 0x1d98de76
	li x26, 0x42ada0c9
	li x27, 0xd9f4833a
	li x28, 0xe882694d
	li x29, 0x9469f8eb
	li x30, 0xe8b8d415
	li x31, 0xe471ce3a

        la x1, mydata

# Generating Store Sequence
	li	x27,	-206
	add	x27,	x1,	x27
	sw	x26,	1602(x27)
	li	x14,	1920
	add	x14,	x1,	x14
	sb	x25,	-1602(x14)
	li	x2,	1352
	add	x2,	x1,	x2
	sb	x10,	1101(x2)
	li	x31,	3685
	add	x31,	x1,	x31
	sh	x16,	-1101(x31)
	li	x21,	1637
	add	x21,	x1,	x21
	sb	x18,	1942(x21)
	li	x15,	3106
	add	x15,	x1,	x15
	sb	x3,	-1942(x15)
	li	x20,	525
	add	x20,	x1,	x20
	sh	x9,	849(x20)
	li	x11,	3645
	add	x11,	x1,	x11
	sh	x28,	-849(x11)
	li	x30,	1591
	add	x30,	x1,	x30
	sb	x11,	98(x30)
	li	x23,	3792
	add	x23,	x1,	x23
	sh	x5,	-98(x23)
	li	x12,	2168
	add	x12,	x1,	x12
	sb	x26,	367(x12)
	li	x14,	1784
	add	x14,	x1,	x14
	sb	x9,	-367(x14)
	li	x24,	-1271
	add	x24,	x1,	x24
	sb	x20,	1840(x24)
	li	x7,	5292
	add	x7,	x1,	x7
	sb	x10,	-1840(x7)
	li	x11,	1399
	add	x11,	x1,	x11
	sh	x30,	255(x11)
	li	x31,	2759
	add	x31,	x1,	x31
	sw	x25,	-255(x31)

# Generating Load/Store Sequence
	li	x21,	3305
	add	x21,	x1,	x21
	lbu	x5,	184(x21)
	li	x2,	425
	add	x2,	x1,	x2
	lw	x13,	843(x2)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
