# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0xdcf3c723
	li x2, 0xa2b986f8
	li x3, 0x47ae33cb
	li x4, 0xc0a7c4f5
	li x5, 0x4e7ebe63
	li x6, 0x10b79b2f
	li x7, 0x85aa2ac3
	li x8, 0xe495ce6f
	li x9, 0x77ddecd3
	li x10, 0x7cd87823
	li x11, 0x2e54a3b8
	li x12, 0x47029b09
	li x13, 0xf76af1a6
	li x14, 0x4b14c501
	li x15, 0x174a911b
	li x16, 0x4475731a
	li x17, 0x6e4ee773
	li x18, 0xc1223234
	li x19, 0xa97872c7
	li x20, 0xc5e52d3b
	li x21, 0x64b49114
	li x22, 0xe4999649
	li x23, 0x9b8ded67
	li x24, 0x38af6abb
	li x25, 0xd83ab2b2
	li x26, 0x61a43399
	li x27, 0x1cddb0d2
	li x28, 0xffdf14e5
	li x29, 0xab912aa6
	li x30, 0x1e31a08f
	li x31, 0x103c8115

# Seeding Simple Instructions
	sltu	x12, x19, x31
	xor	x30, x12, x17
	sltu	x22, x9, x30
	sltu	x27, x11, x30
	sra	x28, x15, x28
	sub	x4, x14, x15
	xor	x16, x18, x13
	sub	x13, x6, x14
	or	x24, x10, x5
	or	x30, x26, x18
	and	x9, x5, x14
	or	x19, x3, x12
	sra	x16, x23, x6
	add	x31, x3, x16
	srl	x26, x8, x20
	xor	x4, x11, x2
	add	x23, x27, x22
	add	x30, x26, x21
	sltu	x7, x13, x14
	slt	x9, x23, x0
	xor	x1, x26, x7
	sra	x18, x16, x29
	or	x17, x17, x20
	slt	x3, x13, x12
	srl	x11, x7, x28
	sra	x6, x26, x22
	sltu	x11, x30, x26
	sra	x0, x20, x12
	sra	x15, x22, x10
	sub	x10, x29, x6
	and	x25, x29, x21
	or	x8, x27, x0
	sra	x2, x21, x12
	srl	x28, x16, x31
	sltu	x25, x26, x5
	add	x15, x2, x20
	sub	x4, x24, x22
	sub	x30, x15, x11
	sub	x20, x19, x4
	sltu	x13, x20, x22

    li a0, 0xa
    ecall
