# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x11f436f
	li x2, 0xe7eec84e
	li x3, 0x48a4d4c3
	li x4, 0x8b0a69c6
	li x5, 0xe0d19590
	li x6, 0xe0bd56db
	li x7, 0xe3a68b3d
	li x8, 0x7463a0b8
	li x9, 0xc71d6798
	li x10, 0x2a076cc4
	li x11, 0x69405ee2
	li x12, 0xe58eba4
	li x13, 0xd4232dbc
	li x14, 0x7c312b1e
	li x15, 0xc5e18642
	li x16, 0x1a00aef5
	li x17, 0x8a0b05c2
	li x18, 0x616f41bd
	li x19, 0xdea25b8e
	li x20, 0x7929f783
	li x21, 0x76abc8f5
	li x22, 0xd26b5707
	li x23, 0xa92e308
	li x24, 0x38aa63a1
	li x25, 0xc5b1ab77
	li x26, 0xf4a1db76
	li x27, 0xca11db6
	li x28, 0xe73668c6
	li x29, 0x135c8b63
	li x30, 0x33815924
	li x31, 0xb1cbdde

        la x1, mydata

# Generating Store Sequence
	li	x29,	104
	add	x29,	x1,	x29
	sh	x7,	974(x29)
	li	x26,	3544
	add	x26,	x1,	x26
	sh	x8,	-974(x26)
	li	x2,	747
	add	x2,	x1,	x2
	sw	x7,	77(x2)
	li	x26,	566
	add	x26,	x1,	x26
	sb	x16,	-77(x26)
	li	x13,	2788
	add	x13,	x1,	x13
	sh	x21,	146(x13)
	li	x24,	2540
	add	x24,	x1,	x24
	sh	x24,	-146(x24)
	li	x21,	-533
	add	x21,	x1,	x21
	sh	x11,	1881(x21)
	li	x13,	2970
	add	x13,	x1,	x13
	sb	x25,	-1881(x13)
	li	x22,	2643
	add	x22,	x1,	x22
	sb	x19,	230(x22)
	li	x2,	3052
	add	x2,	x1,	x2
	sh	x22,	-230(x2)
	li	x7,	3363
	add	x7,	x1,	x7
	sw	x27,	225(x7)
	li	x26,	4088
	add	x26,	x1,	x26
	sb	x13,	-225(x26)
	li	x24,	730
	add	x24,	x1,	x24
	sw	x19,	506(x24)
	li	x2,	3462
	add	x2,	x1,	x2
	sw	x4,	-506(x2)
	li	x10,	727
	add	x10,	x1,	x10
	sb	x15,	691(x10)
	li	x5,	2535
	add	x5,	x1,	x5
	sh	x10,	-691(x5)

# Generating Load/Store Sequence
	li	x11,	946
	add	x11,	x1,	x11
	lbu	x15,	246(x11)
	li	x12,	2970
	add	x12,	x1,	x12
	lhu	x3,	-364(x12)
	li	x22,	4721
	add	x22,	x1,	x22
	sw	x7,	-1017(x22)
	li	x20,	965
	add	x20,	x1,	x20
	sw	x22,	1563(x20)
	li	x20,	987
	add	x20,	x1,	x20
	lb	x4,	-698(x20)
	li	x13,	1548
	add	x13,	x1,	x13
	lh	x20,	-16(x13)
	li	x5,	2263
	add	x5,	x1,	x5
	lbu	x20,	-376(x5)
	li	x2,	10
	add	x2,	x1,	x2
	lh	x23,	1210(x2)
	li	x17,	3629
	add	x17,	x1,	x17
	lbu	x16,	-1068(x17)
	li	x31,	2961
	add	x31,	x1,	x31
	lb	x25,	-1846(x31)
	li	x16,	3485
	add	x16,	x1,	x16
	sh	x11,	-1347(x16)
	li	x20,	-1203
	add	x20,	x1,	x20
	sw	x25,	1839(x20)
	li	x2,	2961
	add	x2,	x1,	x2
	lhu	x18,	-1539(x2)
	li	x11,	922
	add	x11,	x1,	x11
	lw	x8,	614(x11)
	li	x2,	1448
	add	x2,	x1,	x2
	lbu	x3,	46(x2)
	li	x29,	1568
	add	x29,	x1,	x29
	lb	x28,	416(x29)
	li	x14,	1621
	add	x14,	x1,	x14
	lh	x14,	175(x14)
	li	x2,	-1438
	add	x2,	x1,	x2
	lb	x13,	1527(x2)
	li	x17,	3178
	add	x17,	x1,	x17
	sh	x7,	832(x17)
	li	x16,	3094
	add	x16,	x1,	x16
	sh	x28,	-1866(x16)
	li	x30,	1858
	add	x30,	x1,	x30
	lw	x11,	-354(x30)
	li	x3,	749
	add	x3,	x1,	x3
	lh	x7,	825(x3)
	li	x4,	2607
	add	x4,	x1,	x4
	lbu	x16,	-487(x4)
	li	x2,	328
	add	x2,	x1,	x2
	lw	x20,	844(x2)
	li	x30,	2599
	add	x30,	x1,	x30
	lh	x3,	553(x30)
	li	x5,	1230
	add	x5,	x1,	x5
	lbu	x22,	175(x5)
	li	x25,	-63
	add	x25,	x1,	x25
	sh	x8,	1131(x25)
	li	x31,	37
	add	x31,	x1,	x31
	sw	x9,	951(x31)
	li	x10,	1892
	add	x10,	x1,	x10
	lbu	x3,	1956(x10)
	li	x11,	3380
	add	x11,	x1,	x11
	lb	x10,	-935(x11)
	li	x6,	3084
	add	x6,	x1,	x6
	sb	x23,	844(x6)
	li	x10,	-636
	add	x10,	x1,	x10
	sw	x12,	1284(x10)
	li	x25,	-486
	add	x25,	x1,	x25
	lbu	x22,	1702(x25)
	li	x4,	905
	add	x4,	x1,	x4
	sb	x8,	-87(x4)
	li	x22,	2282
	add	x22,	x1,	x22
	sb	x2,	-1910(x22)
	li	x28,	-1198
	add	x28,	x1,	x28
	lhu	x18,	1950(x28)
	li	x11,	2720
	add	x11,	x1,	x11
	lw	x10,	948(x11)
	li	x18,	3511
	add	x18,	x1,	x18
	lh	x17,	-1339(x18)
	li	x10,	2744
	add	x10,	x1,	x10
	lbu	x2,	-1160(x10)
	li	x5,	1795
	add	x5,	x1,	x5
	sb	x26,	-1575(x5)
	li	x29,	1968
	add	x29,	x1,	x29
	lhu	x4,	562(x29)
	li	x25,	643
	add	x25,	x1,	x25
	lw	x21,	-159(x25)
	li	x15,	3488
	add	x15,	x1,	x15
	sb	x27,	-2006(x15)
	li	x13,	3854
	add	x13,	x1,	x13
	lbu	x0,	-1251(x13)
	li	x31,	4207
	add	x31,	x1,	x31
	lw	x31,	-935(x31)
	li	x13,	801
	add	x13,	x1,	x13
	sh	x14,	-615(x13)
	li	x5,	2435
	add	x5,	x1,	x5
	lbu	x18,	-64(x5)
	li	x13,	1652
	add	x13,	x1,	x13
	lhu	x30,	-1604(x13)
	li	x24,	4971
	add	x24,	x1,	x24
	lb	x24,	-1143(x24)
	li	x9,	4954
	add	x9,	x1,	x9
	sw	x24,	-1734(x9)
	li	x15,	3736
	add	x15,	x1,	x15
	lw	x20,	284(x15)
	li	x13,	306
	add	x13,	x1,	x13
	lb	x0,	222(x13)
	li	x9,	-27
	add	x9,	x1,	x9
	sw	x2,	1971(x9)
	li	x22,	-604
	add	x22,	x1,	x22
	sb	x23,	1420(x22)
	li	x19,	1415
	add	x19,	x1,	x19
	sb	x26,	1214(x19)
	li	x2,	2472
	add	x2,	x1,	x2
	lhu	x11,	294(x2)
	li	x3,	2578
	add	x3,	x1,	x3
	lhu	x5,	640(x3)
	li	x10,	2249
	add	x10,	x1,	x10
	lb	x17,	-1389(x10)
	li	x21,	582
	add	x21,	x1,	x21
	lh	x13,	488(x21)
	li	x8,	1967
	add	x8,	x1,	x8
	sh	x12,	1623(x8)
	li	x3,	3706
	add	x3,	x1,	x3
	sw	x31,	122(x3)
	li	x24,	3106
	add	x24,	x1,	x24
	lw	x10,	342(x24)
	li	x5,	3402
	add	x5,	x1,	x5
	lbu	x20,	343(x5)
	li	x14,	1738
	add	x14,	x1,	x14
	lb	x14,	-424(x14)
	li	x2,	5416
	add	x2,	x1,	x2
	sb	x13,	-1579(x2)
	li	x26,	1668
	add	x26,	x1,	x26
	lb	x11,	1102(x26)
	li	x7,	4163
	add	x7,	x1,	x7
	sb	x19,	-1997(x7)
	li	x19,	-415
	add	x19,	x1,	x19
	sh	x11,	865(x19)
	li	x4,	4698
	add	x4,	x1,	x4
	sb	x6,	-851(x4)
	li	x10,	3328
	add	x10,	x1,	x10
	lb	x21,	201(x10)
	li	x22,	2027
	add	x22,	x1,	x22
	sw	x3,	1525(x22)
	li	x30,	3492
	add	x30,	x1,	x30
	sh	x13,	-876(x30)
	li	x5,	148
	add	x5,	x1,	x5
	lhu	x9,	1030(x5)
	li	x22,	1349
	add	x22,	x1,	x22
	lhu	x20,	465(x22)
	li	x6,	1149
	add	x6,	x1,	x6
	sw	x28,	119(x6)
	li	x4,	2405
	add	x4,	x1,	x4
	lbu	x4,	775(x4)
	li	x14,	1640
	add	x14,	x1,	x14
	lh	x31,	-184(x14)
	li	x9,	2139
	add	x9,	x1,	x9
	lhu	x21,	1565(x9)
	li	x30,	2998
	add	x30,	x1,	x30
	lh	x4,	-64(x30)
	li	x27,	5711
	add	x27,	x1,	x27
	lb	x19,	-1950(x27)
	li	x5,	3809
	add	x5,	x1,	x5
	lw	x25,	167(x5)
	li	x3,	3209
	add	x3,	x1,	x3
	lh	x26,	-7(x3)
	li	x28,	1867
	add	x28,	x1,	x28
	sb	x8,	-1179(x28)
	li	x14,	-872
	add	x14,	x1,	x14
	sh	x31,	966(x14)
	li	x8,	285
	add	x8,	x1,	x8
	sb	x12,	-29(x8)
	li	x17,	-638
	add	x17,	x1,	x17
	lhu	x3,	1370(x17)
	li	x23,	1970
	add	x23,	x1,	x23
	sb	x5,	1166(x23)
	li	x18,	3445
	add	x18,	x1,	x18
	lh	x14,	611(x18)
	li	x14,	5853
	add	x14,	x1,	x14
	lbu	x19,	-1862(x14)
	li	x29,	1259
	add	x29,	x1,	x29
	sw	x15,	877(x29)
	li	x16,	1284
	add	x16,	x1,	x16
	lbu	x23,	1658(x16)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
