# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x1df97360
	li x2, 0x10548ba0
	li x3, 0x3c68feb2
	li x4, 0x4e065bca
	li x5, 0xcb9221ac
	li x6, 0x993c551e
	li x7, 0x40bd8dbb
	li x8, 0xb185c690
	li x9, 0x32ac6ba4
	li x10, 0xa1bb6ba3
	li x11, 0x47fa8772
	li x12, 0x598ffa2e
	li x13, 0x8e052fa6
	li x14, 0x6b857f18
	li x15, 0x82378452
	li x16, 0x898bc4f1
	li x17, 0x156e51d4
	li x18, 0x971182b0
	li x19, 0xc1200b8
	li x20, 0xc8b3f5a7
	li x21, 0xfd86f96c
	li x22, 0x7735a95b
	li x23, 0x721cbc87
	li x24, 0x821d77d4
	li x25, 0x64b43349
	li x26, 0x942cda51
	li x27, 0x9afaa10b
	li x28, 0x7d313599
	li x29, 0xce733cbb
	li x30, 0xf7fc4237
	li x31, 0xd45932c2

        la x1, mydata

# Generating Store Sequence
	la	x19,	mydata
	andi	x31,	x31,	2044
	add	x31,	x31,	x19
	sb	x18,	(x31)
	la	x1,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x1
	sw	x9,	(x4)
	la	x20,	mydata
	andi	x19,	x19,	2044
	add	x19,	x19,	x20
	sh	x20,	(x19)
	la	x19,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x19
	sh	x20,	(x4)
	la	x1,	mydata
	andi	x17,	x17,	2044
	add	x17,	x17,	x1
	sb	x24,	(x17)
	la	x29,	mydata
	andi	x11,	x11,	2044
	add	x11,	x11,	x29
	sb	x19,	(x11)
	la	x1,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x1
	sh	x19,	(x21)
	la	x17,	mydata
	andi	x2,	x2,	2044
	add	x2,	x2,	x17
	sh	x29,	(x2)
	la	x10,	mydata
	andi	x31,	x31,	2044
	add	x31,	x31,	x10
	sw	x22,	(x31)
	la	x22,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x22
	sh	x29,	(x21)
	la	x31,	mydata
	andi	x24,	x24,	2044
	add	x24,	x24,	x31
	sb	x20,	(x24)
	la	x3,	mydata
	andi	x25,	x25,	2044
	add	x25,	x25,	x3
	sh	x28,	(x25)
	la	x15,	mydata
	andi	x18,	x18,	2044
	add	x18,	x18,	x15
	sb	x4,	(x18)
	la	x15,	mydata
	andi	x26,	x26,	2044
	add	x26,	x26,	x15
	sh	x24,	(x26)
	la	x18,	mydata
	andi	x13,	x13,	2044
	add	x13,	x13,	x18
	sh	x9,	(x13)
	la	x7,	mydata
	andi	x18,	x18,	2044
	add	x18,	x18,	x7
	sw	x10,	(x18)

# Generating Load/Store Sequence
	la	x28,	mydata
	andi	x5,	x5,	2044
	add	x5,	x5,	x28
	sw	x1,	(x5)
	la	x14,	mydata
	andi	x18,	x18,	2044
	add	x18,	x18,	x14
	sb	x10,	(x18)
	la	x8,	mydata
	andi	x10,	x10,	2044
	add	x10,	x10,	x8
	sh	x28,	(x10)
	la	x9,	mydata
	andi	x13,	x13,	2044
	add	x13,	x13,	x9
	sb	x15,	(x13)
	la	x29,	mydata
	andi	x16,	x16,	2044
	add	x16,	x16,	x29
	lbu	x0,	(x16)
	la	x19,	mydata
	andi	x27,	x27,	2044
	add	x27,	x27,	x19
	sb	x22,	(x27)
	la	x14,	mydata
	andi	x19,	x19,	2044
	add	x19,	x19,	x14
	lbu	x15,	(x19)
	la	x4,	mydata
	andi	x30,	x30,	2044
	add	x30,	x30,	x4
	sw	x20,	(x30)
	la	x26,	mydata
	andi	x28,	x28,	2044
	add	x28,	x28,	x26
	lw	x12,	(x28)
	la	x28,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x28
	lb	x15,	(x21)
	la	x28,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x28
	sw	x1,	(x4)
	la	x6,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x6
	sw	x5,	(x4)
	la	x3,	mydata
	andi	x10,	x10,	2044
	add	x10,	x10,	x3
	lbu	x17,	(x10)
	la	x6,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x6
	lbu	x13,	(x21)
	la	x13,	mydata
	andi	x3,	x3,	2044
	add	x3,	x3,	x13
	lhu	x24,	(x3)
	la	x17,	mydata
	andi	x2,	x2,	2044
	add	x2,	x2,	x17
	lw	x5,	(x2)
	la	x31,	mydata
	andi	x7,	x7,	2044
	add	x7,	x7,	x31
	sb	x21,	(x7)
	la	x10,	mydata
	andi	x14,	x14,	2044
	add	x14,	x14,	x10
	lw	x20,	(x14)
	la	x31,	mydata
	andi	x16,	x16,	2044
	add	x16,	x16,	x31
	sb	x16,	(x16)
	la	x16,	mydata
	andi	x25,	x25,	2044
	add	x25,	x25,	x16
	sh	x31,	(x25)
	la	x17,	mydata
	andi	x9,	x9,	2044
	add	x9,	x9,	x17
	lw	x23,	(x9)
	la	x3,	mydata
	andi	x1,	x1,	2044
	add	x1,	x1,	x3
	sb	x11,	(x1)
	la	x12,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x12
	lh	x14,	(x4)
	la	x19,	mydata
	andi	x1,	x1,	2044
	add	x1,	x1,	x19
	lhu	x4,	(x1)
	la	x14,	mydata
	andi	x16,	x16,	2044
	add	x16,	x16,	x14
	sw	x1,	(x16)
	la	x20,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x20
	lhu	x28,	(x21)
	la	x4,	mydata
	andi	x16,	x16,	2044
	add	x16,	x16,	x4
	lbu	x13,	(x16)
	la	x19,	mydata
	andi	x20,	x20,	2044
	add	x20,	x20,	x19
	sh	x5,	(x20)
	la	x7,	mydata
	andi	x3,	x3,	2044
	add	x3,	x3,	x7
	sw	x8,	(x3)
	la	x8,	mydata
	andi	x25,	x25,	2044
	add	x25,	x25,	x8
	lw	x1,	(x25)
	la	x11,	mydata
	andi	x31,	x31,	2044
	add	x31,	x31,	x11
	lbu	x0,	(x31)
	la	x14,	mydata
	andi	x12,	x12,	2044
	add	x12,	x12,	x14
	lbu	x20,	(x12)
	la	x22,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x22
	lh	x3,	(x21)
	la	x27,	mydata
	andi	x21,	x21,	2044
	add	x21,	x21,	x27
	lhu	x14,	(x21)
	la	x15,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x15
	lhu	x30,	(x4)
	la	x1,	mydata
	andi	x15,	x15,	2044
	add	x15,	x15,	x1
	lh	x2,	(x15)
	la	x22,	mydata
	andi	x18,	x18,	2044
	add	x18,	x18,	x22
	lh	x8,	(x18)
	la	x11,	mydata
	andi	x15,	x15,	2044
	add	x15,	x15,	x11
	sb	x17,	(x15)
	la	x6,	mydata
	andi	x13,	x13,	2044
	add	x13,	x13,	x6
	lb	x26,	(x13)
	la	x12,	mydata
	andi	x2,	x2,	2044
	add	x2,	x2,	x12
	lbu	x22,	(x2)
	la	x13,	mydata
	andi	x6,	x6,	2044
	add	x6,	x6,	x13
	lbu	x30,	(x6)
	la	x31,	mydata
	andi	x25,	x25,	2044
	add	x25,	x25,	x31
	sh	x19,	(x25)
	la	x25,	mydata
	andi	x1,	x1,	2044
	add	x1,	x1,	x25
	sb	x10,	(x1)
	la	x27,	mydata
	andi	x3,	x3,	2044
	add	x3,	x3,	x27
	lw	x22,	(x3)
	la	x29,	mydata
	andi	x11,	x11,	2044
	add	x11,	x11,	x29
	lhu	x22,	(x11)
	la	x15,	mydata
	andi	x8,	x8,	2044
	add	x8,	x8,	x15
	lw	x0,	(x8)
	la	x20,	mydata
	andi	x18,	x18,	2044
	add	x18,	x18,	x20
	lhu	x20,	(x18)
	la	x31,	mydata
	andi	x2,	x2,	2044
	add	x2,	x2,	x31
	lw	x25,	(x2)
	la	x18,	mydata
	andi	x13,	x13,	2044
	add	x13,	x13,	x18
	lhu	x0,	(x13)
	la	x19,	mydata
	andi	x26,	x26,	2044
	add	x26,	x26,	x19
	sw	x0,	(x26)
	la	x7,	mydata
	andi	x15,	x15,	2044
	add	x15,	x15,	x7
	lhu	x20,	(x15)
	la	x25,	mydata
	andi	x16,	x16,	2044
	add	x16,	x16,	x25
	lw	x22,	(x16)
	la	x18,	mydata
	andi	x6,	x6,	2044
	add	x6,	x6,	x18
	lw	x13,	(x6)
	la	x4,	mydata
	andi	x7,	x7,	2044
	add	x7,	x7,	x4
	lb	x1,	(x7)
	la	x12,	mydata
	andi	x10,	x10,	2044
	add	x10,	x10,	x12
	sb	x5,	(x10)
	la	x20,	mydata
	andi	x16,	x16,	2044
	add	x16,	x16,	x20
	sw	x9,	(x16)
	la	x30,	mydata
	andi	x8,	x8,	2044
	add	x8,	x8,	x30
	sw	x6,	(x8)
	la	x3,	mydata
	andi	x14,	x14,	2044
	add	x14,	x14,	x3
	lb	x16,	(x14)
	la	x25,	mydata
	andi	x22,	x22,	2044
	add	x22,	x22,	x25
	sh	x24,	(x22)
	la	x20,	mydata
	andi	x7,	x7,	2044
	add	x7,	x7,	x20
	lhu	x13,	(x7)
	la	x13,	mydata
	andi	x1,	x1,	2044
	add	x1,	x1,	x13
	lbu	x22,	(x1)
	la	x16,	mydata
	andi	x13,	x13,	2044
	add	x13,	x13,	x16
	sw	x4,	(x13)
	la	x16,	mydata
	andi	x3,	x3,	2044
	add	x3,	x3,	x16
	lhu	x10,	(x3)
	la	x14,	mydata
	andi	x2,	x2,	2044
	add	x2,	x2,	x14
	lbu	x6,	(x2)
	la	x12,	mydata
	andi	x30,	x30,	2044
	add	x30,	x30,	x12
	sw	x10,	(x30)
	la	x28,	mydata
	andi	x23,	x23,	2044
	add	x23,	x23,	x28
	lbu	x16,	(x23)
	la	x6,	mydata
	andi	x19,	x19,	2044
	add	x19,	x19,	x6
	sw	x16,	(x19)
	la	x14,	mydata
	andi	x30,	x30,	2044
	add	x30,	x30,	x14
	lw	x21,	(x30)
	la	x17,	mydata
	andi	x19,	x19,	2044
	add	x19,	x19,	x17
	lw	x11,	(x19)
	la	x19,	mydata
	andi	x14,	x14,	2044
	add	x14,	x14,	x19
	lh	x13,	(x14)
	la	x30,	mydata
	andi	x15,	x15,	2044
	add	x15,	x15,	x30
	lb	x23,	(x15)
	la	x14,	mydata
	andi	x28,	x28,	2044
	add	x28,	x28,	x14
	sw	x21,	(x28)
	la	x1,	mydata
	andi	x14,	x14,	2044
	add	x14,	x14,	x1
	lw	x23,	(x14)
	la	x1,	mydata
	andi	x8,	x8,	2044
	add	x8,	x8,	x1
	sh	x8,	(x8)
	la	x22,	mydata
	andi	x4,	x4,	2044
	add	x4,	x4,	x22
	lh	x21,	(x4)
	la	x29,	mydata
	andi	x17,	x17,	2044
	add	x17,	x17,	x29
	lh	x22,	(x17)

# Seeding All Instructions
# Total Instructions: 20

	sltu	x22, x27, x28
	auipc	x28,	0x33b6e
	sltiu	x8,	x20,	1862
	slt	x7, x5, x22
	la	x26,	mydata
	andi	x7,	x7,	2044
	add	x7,	x7,	x26
	lhu	x7,	(x7)
	slli	x31,	x10,	0x3
	srl	x22, x4, x9
	la	x30,	mydata
	andi	x27,	x27,	2044
	add	x27,	x27,	x30
	lh	x23,	(x27)
	srl	x7, x9, x4
	sra	x14, x22, x30
	la	x22,	mydata
	andi	x7,	x7,	2044
	add	x7,	x7,	x22
	sh	x21,	(x7)
	xori	x4,	x21,	43
	la	x31,	mydata
	andi	x27,	x27,	2044
	add	x27,	x27,	x31
	lbu	x19,	(x27)
	sltiu	x5,	x17,	-309
	and	x19, x26, x2
	slti	x4,	x4,	1091
	la	x8,	mydata
	andi	x12,	x12,	2044
	add	x12,	x12,	x8
	lhu	x5,	(x12)
	xori	x29,	x5,	1677
	ori	x11,	x4,	-1407
	slt	x23, x4, x27

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
