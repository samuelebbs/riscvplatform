# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:

    %SEED_REGISTERS%

    %STORE%

    %LOAD_STORE%

    %CHECK_SUM%

    li a0, 0xa
    ecall
