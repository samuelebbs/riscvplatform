# ----------------------------------------------------------------------------
# User DIP Switches - Bank 35
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN F22 [get_ports {switch[0]}];  # "SW0"
set_property PACKAGE_PIN G22 [get_ports {switch[1]}];  # "SW1"
set_property PACKAGE_PIN H22 [get_ports {switch[2]}];  # "SW2"
set_property PACKAGE_PIN F21 [get_ports {switch[3]}];  # "SW3"
set_property PACKAGE_PIN H19 [get_ports {switch[4]}];  # "SW4"
set_property PACKAGE_PIN H18 [get_ports {switch[5]}];  # "SW5"
set_property PACKAGE_PIN H17 [get_ports {switch[6]}];  # "SW6"
set_property PACKAGE_PIN M15 [get_ports {switch[7]}];  # "SW7"

# ----------------------------------------------------------------------------
# User LEDs - Bank 33
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN T22    IOSTANDARD LVCMOS33} [get_ports {led[0]}];  # "LD0"
set_property -dict { PACKAGE_PIN T21    IOSTANDARD LVCMOS33} [get_ports {led[1]}];  # "LD1"
set_property -dict { PACKAGE_PIN U22    IOSTANDARD LVCMOS33} [get_ports {led[2]}];  # "LD2"
set_property -dict { PACKAGE_PIN U21    IOSTANDARD LVCMOS33} [get_ports {led[3]}];  # "LD3"
set_property -dict { PACKAGE_PIN V22    IOSTANDARD LVCMOS33} [get_ports {led[4]}];  # "LD4"
set_property -dict { PACKAGE_PIN W22    IOSTANDARD LVCMOS33} [get_ports {led[5]}];  # "LD5"
set_property -dict { PACKAGE_PIN U19    IOSTANDARD LVCMOS33} [get_ports {led[6]}];  # "LD6"
set_property -dict { PACKAGE_PIN U14    IOSTANDARD LVCMOS33} [get_ports {led[7]}];  # "LD7"

# ----------------------------------------------------------------------------
# User Push Buttons - Bank 34
# ----------------------------------------------------------------------------
set_property PACKAGE_PIN P16 [get_ports {button_C}];  # "BTNC"
set_property PACKAGE_PIN R16 [get_ports {button_D}];  # "BTND"
set_property PACKAGE_PIN N15 [get_ports {button_L}];  # "BTNL"
set_property PACKAGE_PIN R18 [get_ports {button_R}];  # "BTNR"
set_property PACKAGE_PIN T18 [get_ports {button_U}];  # "BTNU"

# ----------------------------------------------------------------------------
# VGA Output - Bank 33
# ----------------------------------------------------------------------------
set_property -dict { PACKAGE_PIN Y21   IOSTANDARD LVCMOS33 } [get_ports {vga_b[0]}];  # "VGA-B1"
set_property -dict { PACKAGE_PIN Y20   IOSTANDARD LVCMOS33 } [get_ports {vga_b[1]}];  # "VGA-B2"
set_property -dict { PACKAGE_PIN AB20   IOSTANDARD LVCMOS33 } [get_ports {vga_b[2]}];  # "VGA-B3"
set_property -dict { PACKAGE_PIN AB19   IOSTANDARD LVCMOS33 } [get_ports {vga_b[3]}];  # "VGA-B4"
set_property -dict { PACKAGE_PIN AB22   IOSTANDARD LVCMOS33 } [get_ports {vga_g[0]}];  # "VGA-G1"
set_property -dict { PACKAGE_PIN AA22   IOSTANDARD LVCMOS33 } [get_ports {vga_g[1]}];  # "VGA-G2"
set_property -dict { PACKAGE_PIN AB21   IOSTANDARD LVCMOS33 } [get_ports {vga_g[2]}];  # "VGA-G3"
set_property -dict { PACKAGE_PIN AA21   IOSTANDARD LVCMOS33 } [get_ports {vga_g[3]}];  # "VGA-G4"
set_property -dict { PACKAGE_PIN AA19   IOSTANDARD LVCMOS33 } [get_ports {vga_hs}];  # "VGA-HS"
set_property -dict { PACKAGE_PIN V20   IOSTANDARD LVCMOS33 } [get_ports {vga_r[0]}];  # "VGA-R1"
set_property -dict { PACKAGE_PIN U20   IOSTANDARD LVCMOS33 } [get_ports {vga_r[1]}];  # "VGA-R2"
set_property -dict { PACKAGE_PIN V19   IOSTANDARD LVCMOS33 } [get_ports {vga_r[2]}];  # "VGA-R3"
set_property -dict { PACKAGE_PIN V18   IOSTANDARD LVCMOS33 } [get_ports {vga_r[3]}];  # "VGA-R4"
set_property -dict { PACKAGE_PIN Y19   IOSTANDARD LVCMOS33 } [get_ports {vga_vs}];  # "VGA-VS"

# ----------------------------------------------------------------------------
# JC Pmod - Bank 13 - Left Corner Bottom Row
# ----------------------------------------------------------------------------
#set_property PACKAGE_PIN AB6 [get_ports {JC1_N}];  # "JC1_N"
#set_property PACKAGE_PIN AB7 [get_ports {JC1_P}];  # "JC1_P"
#set_property PACKAGE_PIN AA4 [get_ports {JC2_N}];  # "JC2_N"
#set_property PACKAGE_PIN Y4  [get_ports {JC2_P}];  # "JC2_P"
#set_property PACKAGE_PIN T6  [get_ports {JC3_N}];  # "JC3_N"
set_property PACKAGE_PIN R6  [get_ports {ps2_data}];  # "JC3_P"
#set_property PACKAGE_PIN U4  [get_ports {JC4_N}];  # "JC4_N"
set_property PACKAGE_PIN T4  [get_ports {ps2_clk}];  # "JC4_P"

# ----------------------------------------------------------------------------
# IO Level Configuration
# ----------------------------------------------------------------------------

# Note that the bank voltage for IO Bank 33 is fixed to 3.3V on ZedBoard.
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 33]];

# Set the bank voltage for IO Bank 34 to 1.8V by default.
# set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 34]];
# set_property IOSTANDARD LVCMOS25 [get_ports -of_objects [get_iobanks 34]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 34]];

# Set the bank voltage for IO Bank 35 to 1.8V by default.
# set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 35]];
# set_property IOSTANDARD LVCMOS25 [get_ports -of_objects [get_iobanks 35]];
set_property IOSTANDARD LVCMOS18 [get_ports -of_objects [get_iobanks 35]];

# Note that the bank voltage for IO Bank 13 is fixed to 3.3V on ZedBoard.
set_property IOSTANDARD LVCMOS33 [get_ports -of_objects [get_iobanks 13]];

# ----------------------------------------------------------------------------
# Timing Constraints
# ----------------------------------------------------------------------------
set_property ASYNC_REG 1 [get_cells -hierarchical -filter {
    NAME =~ */resetn_clk_pixel_q[*]* ||
    NAME =~ */resetn_clk_pixel_q_reg[*]* ||
    NAME =~ */locked_clk_q_reg[*]*
}]

set_false_path -to [get_pins -hierarchical -filter {
    NAME =~ */resetn_clk_pixel_q[*]*/D ||
    NAME =~ */resetn_clk_pixel_q_reg[*]*/D ||
    NAME =~ */locked_clk_q_reg[*]/D
}]