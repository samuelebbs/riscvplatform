
# TODO: Add support for selectively enabling DEBUG_EXTRA in the kernel make files
COMMON_CFLAGS = -DDEBUG -DEXTRA_DEBUG -Wall -Wextra -Werror -std=gnu99 -pedantic -g -Werror=implicit-function-declaration

# The compiler for test programs, and its flags. Even though integer
# multiplication and floating point instructions aren't supported by the
# processor, we can use libgcc's software implementation of these instructions.
RISCV_ARCH = -march=rv32i -mabi=ilp32
RISCV_CFLAGS = $(COMMON_CFLAGS) -static -nostdlib -nostartfiles $(RISCV_ARCH) -Wa,-march=rv32ia -mstrict-align -mpreferred-stack-boundary=3 -nostdinc -fno-strict-aliasing -fno-builtin -fno-stack-protector -fno-delete-null-pointer-checks -fwrapv -fno-aggressive-loop-optimizations
# -save-temps
# RISCV_CFLAGS = $(COMMON_CFLAGS) -static -nostartfiles -march=rv32i -mabi=ilp32

