/**
 * crt0.S
 *
 * RISC-V 32-bit Processor
 *
 * ECE 18-447
 * Carnegie Mellon University
 *
 * The startup file for C test programs. This handles interacting with the
 * simulator environment. This calls main, then invokes `ecall` to tell the
 * simulator that the program is done.
 *
 * The return value from main is placed into registers x2 (sp) and x3 (gp), as
 * RISC-V permits functions to return doubleword values. The lower 32-bits are
 * placed in x2, and the upper 32-bits are placed in x3. Regardless of the
 * return value, the starter code ensures that the simulator terminates
 * simulation by passing the appropriate value to ecall.
 *
 * Note that the simulator sets up the sp (x2) and gp (x3) registers, so the
 * startup code does not need to do this.
 *
 * Authors:
 *  - 2016 - 2017: Brandon Perez
 **/


# The stack must be aligned to at least 8 bytes since we have 64-bit words in the system for uint32_t.
# If this breaks life is miserable and it is extremely difficult to debug.
    .bss
stack_low:
    .align 8
    .space 1 * 1024 * 1024
stack_high:

/**
 * _start
 *
 * The entry point for all assembly programs. Handles invoking main, then
 * ending the program.
 **/
    .text                   // Declare the code to be in the .text segment
    .global _start          // Make _start visible to the linker
_start:
    la sp, stack_high
    la gp, .data
    call    main            // Call the user's program (jal ra, offset)
    mv x31, a0

    li a0, 0xa
    ecall                   // Terminate the program by doing a syscall. The
                            // return value a0 (x10) must be 0xa
